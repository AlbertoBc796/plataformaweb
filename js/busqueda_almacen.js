$(document).ready(pagination(1));

$(function(){
	
	$('#bs-prod').on('keyup',function(){
			var dato = $('#bs-prod').val();
			var url = '../php/busquedas/busca_almacen.php';
			$.ajax({
			type:'POST',
			url:url,
			data:'dato='+dato,
			success: function(datos){
				$('#agrega-registros4').html(datos);
			}
		});
		return false;
		});	
	
});

function pagination(partida){
	var url = '../php/paginaciones/paginacion_almacen.php';
	$.ajax({
		type:'POST',
		url:url,
		data:'partida='+partida,
		success:function(data){
			var array = eval(data);
			$('#agrega-registros4').html(array[0]);
			$('#pagination2').html(array[1]);
		}
	});
	return false;
}

function almacen() {
	var select = document.getElementById('operacion').value;
	n1 = document.getElementById("existencia2").value; 
	n2 = document.getElementById("cantidad").value;
	if (select == 1) {
		n3 = parseFloat(n1) + parseFloat(n2);
	}else{
		n3 = parseFloat(n1) - parseFloat(n2);
	}
    document.getElementById("saldoNuevo").value = n3;
}

// Función autocompletar
function autocompletar() {
	var minimo_letras = 1; // minimo letras visibles en el autocompletar
	var palabra = $('#buscador').val();
	//Contamos el valor del input mediante una condicional
	if (palabra.length >= minimo_letras) {
		$.ajax({
			url: '../php/autocompletadores/autocomplete_almacen_destino.php',
			type: 'POST',
			data: {palabra:palabra},
			success:function(data){
				$('#lista_id').show();
				$('#lista_id').html(data);
			}
		});
	} else {
		//ocultamos la lista
		$('#lista_id').hide();
	}
	$('#lista_id').hide();
}

// Funcion Mostrar valores
function set_item(opciones) {
	// Cambiar el valor del formulario input
	$('#buscador').val(opciones);
	// ocultar lista de proposiciones
	$('#lista_id').hide();
}