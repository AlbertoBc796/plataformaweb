$(document).ready(pagination(1));

$(document).ready(function(){
	$('#city').lwMultiSelect();
	$('.action').change(function(){
		if($(this).val() != ''){
			var action = $(this).attr("id");
			var query = $(this).val();
			var result = '';
            if(action == 'country'){
				result = 'city';
			}
			$.ajax({
				url:'fetch.php',
				method:"POST",
				data:{action:action, query:query},
				success:function(data){
					$('#'+result).html(data);
					if(result == 'city'){
						$('#city').data('plugin_lwMultiSelect').updateList();
					}
				}
			})
		}
	});
	$('#insert_data').on('submit', function(event){
        event.preventDefault();
        if($('#country').val() == ''){
            alert("Please Select Country");
            return false;
        }
        else if($('#city').val() == ''){
            alert("Please Select City");
            return false;
        }
    });
});

//Crear campos
$(function() {
    a=0;
    b=0;
    $('#agregar').on("click",function(){
        var dato = document.getElementById('items').innerHTML;
        var num = dato.split('/')

        var selectedName = document.getElementsByClassName('lwms-selected');
        var selectedId = document.getElementsByClassName('lwms-selected');
        for (var index = 0; index < num[0]; index++) {
            a++;
            names = selectedName[index].innerHTML;
            ids = selectedId[index].dataset.value;
            var unidad = document.createElement('INPUT');
            unidad.setAttribute('type','text');
            unidad.setAttribute('name','articulo[]');
            unidad.setAttribute('id','articulo'+a);
            unidad.setAttribute('class','form-control text-center');
            unidad.setAttribute("value", names);
            document.getElementById('articulos').appendChild(unidad);

            var id = document.createElement('INPUT');
            id.setAttribute('type','hidden');
            id.setAttribute('id','stock'+a);
            id.setAttribute('name','idstock[]');
            id.setAttribute('class','form-control id');
            id.setAttribute("value", ids);
            document.getElementById('articulos').appendChild(id);

            var cantidad = document.createElement('INPUT');
            cantidad.setAttribute('type','text');
            cantidad.setAttribute('id','cantidad'+a);
            cantidad.setAttribute('name','cantidad[]');
            cantidad.setAttribute('class','form-control text-center');
            document.getElementById('cantidades').appendChild(cantidad);
        
            b++;
            var dato = $('#stock'+b).val();
            var url = '../php/busquedas/busca_almacen_stock.php';
            $.ajax({
                type:'POST',
                url:url,
                data:'dato='+dato+'&number='+b,
                success: function(datos){
                    var array = eval(datos);
                    $('#actuales').append(''+(array[0]));
                    $('#precios').append(''+(array[1]));
                    $('#eliminaciones').append(''+(array[2]));
                }
            });//AJAX para cantidad y precio
        }//Ciclo FOR de articulos agregados
    });

});

function eliminaArticulo(numero) {
    document.getElementById('articulo'+numero).remove();
    document.getElementById('stock'+numero).remove();
    document.getElementById('cantidad'+numero).remove();
    document.getElementById('existencia'+numero).remove();
    document.getElementById('precio'+numero).remove();
    document.getElementById('eliminar'+numero).remove();
}

//Busquedas
$(function () {
    $('#bs-folio').on('keyup',function(){
		var dato = $('#bs-folio').val();
		var url = '../php/busquedas/busca_mantenimiento_folio.php';
		$.ajax({
			type:'POST',
			url:url,
			data:'dato='+dato,
			success: function(datos){
				$('#agrega-mantenimientos').html(datos);
			}
		});
		return false;
    });	
    
    $('#bs-unidad').on('keyup',function(){
		var dato = $('#bs-unidad').val();
		var url = '../php/busquedas/busca_mantenimiento_unidad.php';
		$.ajax({
			type:'POST',
			url:url,
			data:'dato='+dato,
			success: function(datos){
				$('#agrega-mantenimientos').html(datos);
			}
		});
		return false;
	});	
})

//Funtions called
function show1(){
    document.getElementById('proveedor').style.display ='none';
    document.getElementById('gasto').style.display ='none';
}
function show2(){
    document.getElementById('proveedor').style.display = 'block';
    document.getElementById('gasto').style.display = 'block';
}

//Pagination
function pagination(partida) {
    var url = '../php/paginaciones/paginacion_mantenimientos.php';
    $.ajax({
        type:'POST',
        url:url,
        data:'partida='+partida,
        success:function (data) {
            var array = eval(data);
            $('#agrega-mantenimientos').html(array[0]);
            $('#paginationM').html(array[1]);
        }
    });
    return false;
}