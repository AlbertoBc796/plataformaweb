$(document).ready(pagination(1));

$(function(){
    a = 0;
    $('#agrego').on("click",function(){
        a++;
        var div = document.createElement('div');
		div.setAttribute('id', 'concept'+a);
		div.setAttribute('class','form-row text-center');
        div.innerHTML = '<div class="form-group col-md-3">\
							<input type="text" class="form-control text-center" onKeyUp="this.value = this.value.toUpperCase();" name="concepto[]" required>\
                        </div>\
                        <div class="form-group col-md-1">\
							<input type="number" min=0 step="0.01" class="form-control text-center" name="cantidad[]" required>\
                        </div>\
                        <div class="form-group col-md-1">\
							<input type="number" min="0" step="0.01" class="form-control text-center" onKeyUp="reesultado6(); resultado7()" name="precio[]" required>\
                        </div>\
                        <div class="form-group col-md-1">\
							<input type="number" min=0 class="form-control text-center" name="precio_total[]" required readonly>\
						</div>';
		document.getElementById('canciones').appendChild(div);
		var url = '../php/busquedas/busca_gastos.php';
            $.ajax({
                type:'POST',
				url:url,
				data:'dato='+a,
                success: function(datos){
                    var array = eval(datos);
					$('#concept'+a).append(''+(array[0]));
					$('#concept'+a).append(''+(array[1]));
					$('#concept'+a).append(''+(array[2]));
                }
			});//AJAX para cantidad y precio
    });

    $('#elimino').on("click",function(e) {
        e.preventDefault();
        document.getElementById('concept'+a).remove();
        a--;
	});

	d=0;
	$('#agrego_factura').on("click",function(){
        d++;
        var div = document.createElement('div');
		div.setAttribute('id', 'folio'+d);
		div.setAttribute('class','form-row text-center');
        div.innerHTML = '<div class="form-group col-md-2" >\
				<label for="">FACTURA</label>\
				<input type="text" class="form-control text-center" onKeyUp="this.value = this.value.toUpperCase();" name="factura[]" required>\
			</div>\
			<div class="form-group col-md-4">\
				<label for="">PROVEEDOR</label>\
				<input type="text" class="form-control text-center" onKeyUp="this.value = this.value.toUpperCase();" name="proveedor[]" required>\
			</div>\
			<div class="form-group col-md-2">\
				<label for="">TOTAL</label>\
				<input type="number" min=0 class="form-control text-center total" id="total'+d+'" name="total" required readonly>\
			</div>\
			<div class="form-group col-md-2" style="display:none;">\
				<label for="">LIMITE</label>\
				<input type="number" min=0 class="form-control text-center" id="limite'+d+'" name="limite[]" required readonly>\
			</div>';
		document.getElementById('addFactura').appendChild(div);
	});
	$('#elimino_factura').on("click",function(e) {
		if (d > 0) {
			e.preventDefault();
			document.getElementById('folio'+d).remove();
			d--;
			c = document.getElementById('limite'+d).value;
		}
	});
	
	c = 0;
	$('#agrego_cheque').on("click",function(){
		c++;
		document.getElementById('limite'+d).value = c;
        var div = document.createElement('div');
		div.setAttribute('id', 'concept'+c);
		div.setAttribute('class','form-row text-center');
        div.innerHTML = '<div class="form-group col-md-3">\
							<input type="text" class="form-control text-center" onKeyUp="this.value = this.value.toUpperCase();" name="concepto[]" required>\
                        </div>\
                        <div class="form-group col-md-1">\
							<input type="number" min=0 step="0.01" class="form-control text-center" name="cantidad[]" required>\
                        </div>\
                        <div class="form-group col-md-1">\
							<input type="number" min="0" step="0.01" class="form-control text-center" onKeyUp="reesultado6(); resultado8('+d+');" name="precio[]" required>\
                        </div>\
                        <div class="form-group col-md-1">\
							<input type="number" min=0 class="form-control text-center total'+d+'" name="precio_total[]" required readonly>\
						</div>';
		document.getElementById('folio'+d).append(div);
		var url = '../php/busquedas/busca_gastos.php';
            $.ajax({
                type:'POST',
				url:url,
				data:'dato='+c,
                success: function(datos){
                    var array = eval(datos);
					$('#concept'+c).append(''+(array[0]));
					$('#concept'+c).append(''+(array[1]));
					$('#concept'+c).append(''+(array[2]));
                }
			});//AJAX para cantidad y precio
	});
	
	$('#elimino_cheque').on("click",function(e) {
        e.preventDefault();
        document.getElementById('concept'+c).remove();
		c--;
		document.getElementById('limite'+d).value = c;
	});

    $(document).on('keydown', '.form-control.text-center.huerta', function() {
        var id = this.id;
        var splitid = id.split('_');
        var index = splitid[1];
        $("#"+id).autocomplete({
            source: "../php/autocompletadores/autocomplete_almacen_destino.php",
            minLength: 1,
            select: function(event, ui) {
            event.preventDefault();
                $(this).val(ui.item.destino);       
                $("#idhuerta_"+index).val(ui.item.iddestino);
            }   
        });
    });

    $(document).on('keydown', '.form-control.text-center.camion', function() {
        var id = this.id;
        var splitid = id.split('_');
        var index = splitid[1];
        $("#"+id).autocomplete({
            source: "../php/autocompletadores/transporte.php",
            minLength: 1,
            select: function(event, ui) {
            event.preventDefault();
                $('#camion').val(ui.item.camion);
                $(this).val(ui.item.clave);
                $('#marca').val(ui.item.marca);
                $('#colors').val(ui.item.colors);
                $('#placas').val(ui.item.placas);
                $('#idtransporte_'+index).val(ui.item.idtransporte);
            }
        });
    });

    $('#bd-desde').on('change', function(){
		var desde = $('#bd-desde').val();
		var hasta = $('#bd-hasta').val();
		var dato = $('#bs-mov').val();
		var dato2 = $('#bs-prod').val();
		var dato3 = $('#bs-prov').val();
		var url = '../php/busquedas/busca_gasto_por_fecha.php';
		$.ajax({
		type:'POST',
		url:url,
		data:'dato='+dato+'&desde='+desde+'&hasta='+hasta+'&dato2='+dato2+'&dato3='+dato3,
		success: function(datos){
			$('#agrega-registros-gastos').html(datos);
		}
	});
	return false;
	});
	
	$('#bd-hasta').on('change', function(){
		var desde = $('#bd-desde').val();
		var hasta = $('#bd-hasta').val();
		var dato = $('#bs-mov').val();
		var dato2 = $('#bs-prod').val();
		var dato3 = $('#bs-prov').val();
		var url = '../php/busquedas/busca_gasto_por_fecha.php';
		$.ajax({
		type:'POST',
		url:url,
		data:'dato='+dato+'&desde='+desde+'&hasta='+hasta+'&dato2='+dato2+'&dato3='+dato3,
		success: function(datos){
			$('#agrega-registros-gastos').html(datos);
		}
	});
	return false;
    });

	$('#bs-prod').on('keyup',function(){
			var dato = $('#bs-prod').val();
			var url = '../php/busquedas/busca_gasto_por_factura.php';
			$.ajax({
			type:'POST',
			url:url,
			data:'dato='+dato,
			success: function(datos){
				$('#agrega-registros-gastos').html(datos);
			}
		});
		return false;
	});

	$('#bs-mov').on('keyup',function(){
		var dato = $('#bs-mov').val();
		var url = '../php/busquedas/busca_gasto_por_movimiento.php';
		$.ajax({
		type:'POST',
		url:url,
		data:'dato='+dato,
		success: function(datos){
			$('#agrega-registros-gastos').html(datos);
		}
	});
	return false;
});
});

function reesultado6(){
	var iva = document.getElementById('serv1').checked;
	var ivac = document.getElementById('serv3').checked;
	var n16=document.getElementsByName("cantidad[]");
	var n17= document.getElementsByName("precio[]");
	if (iva||ivac) {
		for(var i=0; i<n16.length; i++) {
			// alert((parseFloat(n17[i].value*100) * parseFloat(116))/10000)
			document.getElementsByName("precio_total[]")[i].value = (parseFloat(n16[i].value)) * (parseFloat(n17[i].value*100) * parseFloat(116))/10000;
		}
	}else{
		for(var i=0; i<n16.length; i++) {
			document.getElementsByName("precio_total[]")[i].value = parseFloat(n16[i].value*100) * parseFloat(n17[i].value*100) / 10000;
		}
	}
}

function resultado7(){
	var n20=document.getElementsByName("precio_total[]");
	var tot = parseFloat(0);
	for(var i=0; i<n20.length; i++) {
		a = parseFloat(tot) * 1000 + parseFloat(n20[i].value *1000);
		tot = a/1000;
	}
	document.getElementById('total').value = parseFloat(tot);
}

function resultado8(num){
	var n20 = document.getElementsByClassName("form-control text-center total"+num);
	var tot = parseFloat(0);
	for(var i=0; i<n20.length; i++) {
		a = parseFloat(tot) * 1000 + parseFloat(n20[i].value *1000);
		tot = a/1000;
	}
	document.getElementById('total'+num).value = parseFloat(tot);
}

function pagination(partida){
	var url = '../php/paginaciones/paginacion_gastos.php';
	$.ajax({
		type:'POST',
		url:url,
		data:'partida='+partida,
		success:function(data){
			var array = eval(data);
			$('#agrega-registros-gastos').html(array[0]);
			$('#pagination-gastos').html(array[1]);
		}
	});
	return false;
}

function showMsg() {
	var select = document.getElementById('pago').value;
    if (select == 9) {
        document.getElementById("num_cheque").style.display = "block";
        
    }else{
        document.getElementById("num_cheque").style.display = "none";
    }
}

function buscadorcito2(){
	var dato = $('#bs-mov').val();
	var dato2 = $('#bs-prod').val();
	var dato3 = $('#bs-prov').val();
	var desde = $('#bd-desde').val();
	var hasta = $('#bd-hasta').val();
	var url = '../php/busquedas/busca_gasto_por_fecha.php';
		$.ajax({
		type:'POST',
		url:url,
		data:'dato='+dato+'&desde='+desde+'&hasta='+hasta+'&dato2='+dato2+'&dato3='+dato3,
		success: function(datos){
			$('#agrega-registros-gastos').html(datos);
		}
	});
}

function reporteGastos(){
	var desde = $('#bd-desde').val();
	var hasta = $('#bd-hasta').val();
	var dato = $('#bs-mov').val();
	var dato2 = $('#bs-prod').val();
	var dato3 = $('#bs-prov').val();
	window.open('../php/impresiones/gastos_generales.php?desde='+desde+'&hasta='+hasta+'&dato='+dato+'&dato2='+dato2+'&dato3='+dato3);
}

function reporteGC(){
	var desde = $('#bd-desde').val();
	var hasta = $('#bd-hasta').val();
	var dato = $('#bs-mov').val();
	var dato2 = $('#bs-prod').val();
	var dato3 = $('#bs-prov').val();
	window.open('../php/impresiones/gastos_generales.php?desde='+desde+'&hasta='+hasta+'&dato='+dato+'&dato2='+dato2+'&dato3='+dato3);
}