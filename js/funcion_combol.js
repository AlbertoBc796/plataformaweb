$(function(){

	// Lista de Categorias
	$.post( '../php/autocompletadores/categoria.php' ).done( function(respuesta)
	{
		$( '#categoria' ).html( respuesta );
	});

	// lista de categorias	
	$('#categoria').change(function()
	{
		var subcategoria = $(this).val();
		
		// Lista de subcategorias
		$.post( '../php/autocompletadores/subcategoria.php', { categoria: subcategoria} ).done( function( respuesta )
		{
			$( '#subcategoria' ).html( respuesta );
		});
	});
	
})


$(function(){

	// Lista de MOVIMIENTOS
	$.post( '../php/autocompletadores/movimiento.php' ).done( function(respuesta)
	{
		$( '#movimiento' ).html( respuesta );
	});

	// lista de MOVIMIENTOS	
	$('#movimiento').change(function()
	{
		var pago = $(this).val();
		
		// Lista de PAGOS
		$.post( '../php/autocompletadores/pago.php', { movimiento: pago} ).done( function( respuesta )
		{
			$( '#pago' ).html( respuesta );
		});
	});
	
})


$(function(){

	// Lista de PRODUCTOS
	$.post( '../php/autocompletadores/producto_l.php' ).done( function(respuesta)
	{
		$( '#productol' ).html( respuesta );
	});

	// lista de PRODUCTOS	
	$('#productol').change(function()
	{
		var variedadl = $(this).val();
		
		// Lista de VARIEDAD
		$.post( '../php/autocompletadores/variedad_l.php', { productol: variedadl} ).done( function( respuesta )
		{
			$( '#variedadl' ).html( respuesta );
		});
	});
	
})
