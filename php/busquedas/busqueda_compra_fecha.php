<?php
include('../conexion.php');

$desde = $_POST['desde'];
$hasta = $_POST['hasta'];
if(isset($desde)==false){
	$desde = $hasta;
}
if(isset($hasta)==false){
	$hasta = $desde;
}
$sql = "SELECT c.idcompra_venta,date_format(c.fecha,'%d/%m/%Y') as fecha,h.nombre,c.kilos_corte,c.precio_corte,c.importe_corte, c.precio_flete,c.importe_flete,c.egresos,c.kilos_venta,c.precio_venta,c.total_venta,c.ingreso_total from compra_venta c inner join huerta h on c.idhuerta=h.idhuerta WHERE c.fecha BETWEEN '$desde' AND '$hasta' ORDER BY idcompra_venta ASC";

$registro = mysqli_query($mysqli, $sql);
echo '<table class="table table-striped table-bordered text-center">

		<thead>
        	<tr>
            	
                <th>FECHA</th>
                <th>HUERTA</th>
                <th>KILOS CORTE</th>
                <th>PRECIO CORTE</th>
                <th>IMPORTE CORTE</th>
                <th>PRECIO FLETE</th>
                <th>IMPORTE FLETE</th>
                <th>EGRESOS</th>
                <th>KILOS VENTA</th>
                <th>PRECIO VENTA</th>
                <th>TOTAL VENTA</th>
                <th>INGRESO TOTAL</th>
                <th>EDITAR</th>
                <th>ELIMINAR</th>
                <th>IMPRIMIR</th>
            </tr>
        </thead>    ';
if(mysqli_num_rows($registro)>0){

	while($registro2 = mysqli_fetch_array($registro)){
		echo '<tr>
		
				<td>'.$registro2['fecha'].'</td>
				<td>'.$registro2['nombre'].'</td>
				<td>'.$registro2['kilos_corte'].'</td>
				<td>'.$registro2['precio_corte'].'</td>
				<td>'.$registro2['importe_corte'].'</td>
				<td>'.$registro2['precio_flete'].'</td>
				<td>'.$registro2['importe_flete'].'</td>
				<td>'.$registro2['egresos'].'</td>
				<td>'.$registro2['kilos_venta'].'</td>
				<td>'.$registro2['precio_venta'].'</td>
				<td>'.$registro2['total_venta'].'</td>
				<td>'.$registro2['ingreso_total'].'</td>
				<td><a href=act_compra_venta.php?id='.$registro2['idcompra_venta'].'&idborrar=2"><img src="../img/act.png" alt="ACTUALIZAR" class="img-rounded"></a></td>

				<td><a href=../php/eliminaciones/eliminar_cv.php?id='.$registro2['idcompra_venta'].'&idborrar=2"><img src="../img/del.png" alt="ACTUALIZAR" class="img-rounded"></a></td>

				<td><a href=../php/impresiones/pdf.php?id='.$registro2['idcompra_venta'].'&idborrar=2"><img src="../img/imp.png" alt="ACTUALIZAR" class="img-rounded"></a></td>
				</tr>';
	}
}else{
	echo '<tr>
				<td colspan="16">No se encontraron resultados</td>
			</tr>';
}
echo '</table>';
$desde = $_POST['desde'];
$hasta = $_POST['hasta'];
if(isset($desde)==false){
	$desde = $hasta;
}
if(isset($hasta)==false){
	$hasta = $desde;
}
$sql = "SELECT sum(kilos_corte),sum(precio_corte),sum(importe_corte),sum(precio_flete),sum(importe_flete), 
sum(egresos),sum(kilos_venta),sum(precio_venta),sum(total_venta), sum(ingreso_total) from compra_venta WHERE fecha BETWEEN '$desde' AND '$hasta' ORDER BY idcompra_venta ASC";

$registro34 = mysqli_query($mysqli, $sql);
echo '<table class="table table-striped table-bordered text-center">';
if(mysqli_num_rows($registro34)>0){
	while($registro3 = mysqli_fetch_array($registro34)){
		echo
		 '<tr> 
				<td>TOTAL DE </td>
				<td>'.$registro3['sum(kilos_corte)'].'</td>
				<td>'.$registro3['sum(precio_corte)'].'</td>
				<td>'.$registro3['sum(importe_corte)'].'</td>
				<td>'.$registro3['sum(precio_flete)'].'</td>
				<td>'.$registro3['sum(importe_flete)'].'</td>
				<td>'.$registro3['sum(egresos)'].'</td>
				<td>'.$registro3['sum(kilos_venta)'].'</td>
				<td>'.$registro3['sum(precio_venta)'].'</td>
				<td>'.$registro3['sum(total_venta)'].'</td>
				<td>'.$registro3['sum(ingreso_total)'].'</td>
			</tr>
			</table>' ;
	}
}
?>