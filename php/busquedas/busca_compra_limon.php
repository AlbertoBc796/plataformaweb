<?php
include('../conexion.php');

$dato = $_POST['dato'];

//EJECUTAMOS LA CONSULTA DE BUSQUEDA

$sql = " SELECT c.idcompra_venta_limon,date_format(c.fecha,'%d/%m/%Y') as fecha,h.nombre,c.cajas_corte,c.precio_corte,c.importe_corte, c.precio_flete,c.importe_flete,c.egresos,c.cajas_venta,c.precio_venta,c.total_venta,c.ingreso_total from compra_venta_limon c inner join huerta h on c.idhuerta=h.idhuerta WHERE h.nombre LIKE '%$dato%'  ORDER BY idcompra_venta_limon ASC";

$registro = mysqli_query($mysqli, $sql);

//CREAMOS NUESTRA VISTA Y LA DEVOLVEMOS AL AJAX

echo '<table class="table table-striped table-bordered text-center">
        	<tr>
            	
                <th class="text-center">FECHA</th>
                <th class="text-center">HUERTA</th>
                <th class="text-center">CAJAS CORTE</th>
                <th class="text-center">PRECIO CORTE</th>
                <th class="text-center">IMPORTE CORTE</th>
                <th class="text-center">PRECIO FLETE</th>
                <th class="text-center">IMPORTE FLETE</th>
                <th class="text-center">EGRESOS</th>
                <th class="text-center">KILOS VENTA</th>
                <th class="text-center">PRECIO VENTA</th>
                <th class="text-center">TOTAL VENTA</th>
                <th class="text-center">INGRESO TOTAL</th>
                <th class="text-center">EDITAR</th>
                <th class="text-center">ELIMINAR</th>
                <th>IMPRIMIR</th>

            </tr>';

if(mysqli_num_rows($registro)>0){

	while($registro2 = mysqli_fetch_array($registro)){
		echo '<tr>
				
				<td>'.$registro2['fecha'].'</td>
				<td>'.$registro2['nombre'].'</td>
				<td>'.$registro2['cajas_corte'].'</td>
				<td>'.$registro2['precio_corte'].'</td>
				<td>'.$registro2['importe_corte'].'</td>
				<td>'.$registro2['precio_flete'].'</td>
				<td>'.$registro2['importe_flete'].'</td>
				<td>'.$registro2['egresos'].'</td>
				<td>'.$registro2['cajas_venta'].'</td>
				<td>'.$registro2['precio_venta'].'</td>
				<td>'.$registro2['total_venta'].'</td>
				<td>'.$registro2['ingreso_total'].'</td>
				<td><a href=act_compra_venta_limon.php?id='.$registro2['idcompra_venta_limon'].'&idborrar=2"><img src="../img/act.png" alt="ACTUALIZAR" class="img-rounded"></a></td>
				
				<td><a href=../php/eliminaciones/eliminar_cv_limon.php?id='.$registro2['idcompra_venta_limon'].'&idborrar=2"><img src="../img/del.png" alt="ACTUALIZAR" class="img-rounded"></a></td>

				<td><a href=../php/impresiones/pdf_limonb.php?id='.$registro2['idcompra_venta_limon'].'&idborrar=2"><img src="../img/imp.png" alt="ACTUALIZAR" class="img-rounded"></a></td>
				</tr>';
	}
}else{
	echo '<tr>
				<td colspan="16">No se encontraron resultados</td>
			</tr>';
}
echo '</table>';
?>