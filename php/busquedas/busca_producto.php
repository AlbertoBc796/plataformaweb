<?php
include('../conexion.php');
$dato = $_POST['dato'];
$sql = " SELECT c.idcompra_venta,date_format(c.fecha,'%d/%m/%Y') as 
	fecha,h.nombre,p.nombre as fruta,v.nombre as variedad, c.kilos_corte,c.precio_corte,c.importe_corte, c.precio_flete, c.idproducto,
	c.idvariedad, c.importe_flete,c.egresos,c.kilos_venta,c.precio_venta,c.total_venta,c.ingreso_total 
from compra_venta c 
inner join huerta h on c.idhuerta=h.idhuerta 
inner join producto p on c.idproducto = p.idproducto
inner join variedad v on c.idvariedad = v.idvariedad
WHERE p.nombre LIKE '%$dato%'  
ORDER BY idcompra_venta ASC";
$registro = mysqli_query($mysqli, $sql);

echo '<table class="table table-striped table-bordered text-center">
		<tr>
            <th class="text-center">FECHA</th>
            <th class="text-center">PRODUCTO</th>
            <th class="text-center">KILOS CORTE</th>
            <th class="text-center">PRECIO CORTE</th>
            <th class="text-center">IMPORTE CORTE</th>
            <th class="text-center">PRECIO FLETE</th>
            <th class="text-center">IMPORTE FLETE</th>
            <th class="text-center">EGRESOS</th>
            <th class="text-center">KILOS VENTA</th>
            <th class="text-center">PRECIO VENTA</th>
            <th class="text-center">TOTAL VENTA</th>
            <th class="text-center">INGRESO TOTAL</th>
            <th class="text-center">EDITAR</th>
            <th class="text-center">ELIMINAR</th>
            <th>IMPRIMIR</th>
        </tr>';

if(mysqli_num_rows($registro)>0){

	while($registro2 = mysqli_fetch_array($registro)){
		echo '<tr>
			
				<td>'.$registro2['fecha'].'</td>
				<td>'.$registro2['fruta'].' '.$registro2['variedad'].'</td>
				<td>'.$registro2['kilos_corte'].'</td>
				<td>'.$registro2['precio_corte'].'</td>
				<td>'.$registro2['importe_corte'].'</td>
				<td>'.$registro2['precio_flete'].'</td>
				<td>'.$registro2['importe_flete'].'</td>
				<td>'.$registro2['egresos'].'</td>
				<td>'.$registro2['kilos_venta'].'</td>
				<td>'.$registro2['precio_venta'].'</td>
				<td>'.$registro2['total_venta'].'</td>
				<td>'.$registro2['ingreso_total'].'</td>
				<td><a href=act_compra_venta.php?id='.$registro2['idcompra_venta'].'&idborrar=2"><img src="../img/act.png" alt="ACTUALIZAR" class="img-rounded"></a></td>
				
				<td><a href=../php/eliminaciones/eliminar_cv.php?id='.$registro2['idcompra_venta'].'&idborrar=2"><img src="../img/del.png" alt="ACTUALIZAR" class="img-rounded"></a></td>

				<td><a href=../php/impresiones/pdf.php?id='.$registro2['idcompra_venta'].'&idborrar=2"><img src="../img/imp.png" alt="ACTUALIZAR" class="img-rounded"></a></td>
				</tr>';
	}
}else{
	echo '<tr>
				<td colspan="16">No se encontraron resultados</td>
			</tr>';
}
echo '</table>';
?>