<?php
    include('../conexion.php');
    $dato = $_POST['dato'];
    $sql = "SELECT m.folio, date_format(m.fecha , '%d/%m/%Y') as fecha, m.unidad, m.medicion,
    m.idtipo, m.chofer, t.clave, m.idservicio, m.proveedor,m.gasto
    FROM mantenimientos m
    INNER JOIN transporte t ON t.idtransporte = m.unidad
    WHERE m.folio LIKE '%$dato%'
    GROUP BY m.folio ORDER BY m.folio DESC";
    $registro = mysqli_query($mysqli, $sql);

    echo '<table  class="table table-striped table-bordered  text-center">
    <thead>
        <tr>
            <th>FECHA</th>
            <th>FOLIO</th>
            <th>UNIDAD</th>
            <th>KM / HRS</th>
            <th>CHOFER</th>
            <th>PROVEEDOR</th>
            <th>GASTO</th>
            <th>ARTICULOS</th>
            <th>EDITAR</th>
            <th>ELIMINAR</th>
            <th>IMPRIMIR</th>
        </tr>
    </thead>';
    if (mysqli_num_rows($registro)>0) {
        while ($registro2 = mysqli_fetch_array($registro)) {
            $factura = $registro2['folio'];
            $query3 ="SELECT m.folio, m.refacciones, s.articulo,
            m.cantidad, s.preciounit from mantenimientos m
            inner join stock s on s.idstock = m.refacciones
            where m.folio = $factura";
            $registro3 = mysqli_query($mysqli,$query3);
            $articulos='';
            $gasto=0;
            while ($b = mysqli_fetch_array($registro3)) {
                $articulos .= $b['articulo'].', ';
                $gasto += $b['cantidad']*$b['preciounit'];
            }
            $articulos = substr($articulos,0,-2);
            if ($registro2['proveedor'] == '') {
                $proveedor = 'INTERNO';
            }else {
                $proveedor = $registro2['proveedor'];
            }
            if ($articulos == '') {
                $articulos = 'SERVICIO EXTERNO';
            }
            $factura = str_pad($factura, 4, "0", STR_PAD_LEFT);

            echo '<tr>
                <td>'.$registro2['fecha'].'</td>
                <td>'.$factura.'</td>
                <td>'.$registro2['clave'].'</td>
                <td>'.$registro2['medicion'].'</td>
                <td>'.$registro2['chofer'].'</td>
                <td>'.$proveedor.'</td>
                <td>$'.($registro2['gasto']+$gasto).'</td>
                <td>'.$articulos.'</td>
                <td><a href=act_mantenimiento.php?dato='.$registro2['folio'].'&idborrar=2"><img src="../img/act.png" alt="ACTUALIZAR" class="img-rounded"></a></td>
                <td><a href=../php/eliminaciones/eliminar_mantenimiento.php?dato='.$registro2['folio'].'&idborrar=2"><img src="../img/del.png" alt="ACTUALIZAR" class="img-rounded"></a></td>
                <td><a href=../php/impresiones/unidad.php?id='.$registro2['folio'].'&idborrar=2"><img src="../img/imp.png" alt="ACTUALIZAR" class="img-rounded"></a></td>
            </tr>';
        }
    }else {
        echo '<tr>
			<td colspan="16">No se encontraron resultados</td>
		</tr>';
    }
    echo '</table>';
?>