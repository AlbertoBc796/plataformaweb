<?php
include('../conexion.php');
$dato = $_POST['dato'];
$sql = " SELECT c.idcompra_venta,date_format(c.fecha , '%d/%m/%Y') as fecha,c.folio2,c.kilos_venta,c.precio_venta,
c.total_venta,c.cliente,p.nombre as prod,v.nombre,c.folion,c.facturacion,m.movimiento,pa.pago
	from compra_venta c inner join huerta h on c.idhuerta=h.idhuerta
	inner join producto p on p.idproducto = c.idproducto
	inner join variedad v on v.idvariedad = c.idvariedad
	inner join movimiento m on m.idmovimiento = c.idmovimiento
	inner join pago pa on pa.idpago = c.idpago 
	WHERE c.folio2 LIKE '%$dato%' OR c.facturacion LIKE '%$dato%' ORDER BY idcompra_venta ASC";

$registro = mysqli_query($mysqli, $sql);
echo '<table class="table table-striped table-bordered text-center">
	<tr>
        <th class="text-center" >FECHA</th>
        <th class="text-center">REPORTE VENTA</th>
        <th class="text-center">FACTURACION</th>
        <th class="text-center">MOVIMIENTO</th>
        <th class="text-center">PAGO</th>
        <th class="text-center">PRODUCTO</th>
        <th class="text-center">KILOS VENTA</th>
        <th class="text-center">PRECIO VENTA</th>
        <th class="text-center">TOTAL VENTA</th>
        <th class="text-center">CLIENTE</th>
        <th class="text-center">FOLIO DE FACTURACION</th>
        <th class="text-center">EDITAR</th>
        <th class="text-center">ELIMINAR</th>
        <th>IMPRIMIR</th>
    </tr>';

if(mysqli_num_rows($registro)>0){
	while($registro2 = mysqli_fetch_array($registro)){
		echo '<tr>
				<td>'.$registro2['fecha'].'</td>
				<td>'.$registro2['folio2'].'</td>
				<td>'.$registro2['facturacion'].'</td>
				<td>'.$registro2['movimiento'].'</td>
				<td>'.$registro2['pago'].'</td>
				<td>'.$registro2['prod'].' ' .$registro2['nombre'].'</td>
				<td>'.$registro2['kilos_venta'].'</td>
				<td>'.$registro2['precio_venta'].'</td>
				<td>'.$registro2['total_venta'].'</td>
				<td>'.$registro2['cliente'].'</td>
				<td>'.$registro2['folion'].'</td>
	
				<td><a href=act_compra_venta.php?id='.$registro2['idcompra_venta'].'&idborrar=2"><img src="../img/act.png" alt="ACTUALIZAR" class="img-rounded"></a></td>
				
				<td><a href=../php/eliminaciones/eliminar_cv.php?id='.$registro2['idcompra_venta'].'&idborrar=2"><img src="../img/del.png" alt="ACTUALIZAR" class="img-rounded"></a></td>

				<td><a href=../php/impresiones/factura_individual.php?id='.$registro2['idcompra_venta'].'&idborrar=2"><img src="../img/imp.png" alt="ACTUALIZAR" class="img-rounded"></a></td>
				</tr>';
	}
}else{
	echo '<tr>
				<td colspan="16">No se encontraron resultados</td>
			</tr>';
}
echo '</table>';
?>