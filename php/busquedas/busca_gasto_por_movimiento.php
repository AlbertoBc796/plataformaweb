<?php
include('../conexion.php');
$dato = $_POST['dato'];

$sql = "SELECT g.idgasto, date_format(g.fecha , '%d/%m/%Y') as fecha, m.movimiento, 
    pa.pago, g.idmovimiento, g.idfactura,g.proveedor,g.cantidad,g.precio,g.semana,
    g.idtransporte,t.clave,h.nombre as huerta,g.idhuerta, g.concepto,r.rubro,g.cheque
    FROM gastos g
    inner join rubro r on r.idrubro = g.idconcepto
    inner join movimiento m on m.idmovimiento = g.idmovimiento
    left join huerta h on h.idhuerta = g.idhuerta
    left join transporte t on t.idtransporte = g.idtransporte
    inner join pago pa on pa.idpago = g.idpago
    WHERE (m.movimiento LIKE '%$dato%' or pa.pago LIKE '%$dato%' or g.cheque LIKE '%$dato%') 
    order by g.fecha DESC";

$registro = mysqli_query($mysqli, $sql);
echo '<table class="table table-striped table-bordered text-center">
    <thead>
        <tr>
            <th>FECHA</th>
            <th>FACTURA</th>
            <th>MOVIMIENTO</th>
            <th>PROVEEDOR</th>
            <th>CONCEPTO</th>
            <th>RUBRO</th>
            <th>CANTIDAD</th>
            <th>TOTAL</th>
            <th>DESTINO</th>
            <th>EDITAR</th>
            <th>ELIMINAR</th>
        </tr>
    </thead>';

    if(mysqli_num_rows($registro)>0){
        while ($a=mysqli_fetch_array($registro)) {
            if ($a['clave']=='') {
                $brinco = '';
            }else{
                $brinco = '<br>';
            }
            echo '<tr>
            <td>'.$a['fecha'].'</td>
            <td>'.$a['idfactura'].'</td>
            <td>'.$a['movimiento'].' '.$a['pago'].'</td>
            <td>'.$a['proveedor'].'</td>
            <td>'.$a['concepto'].'</td>
            <td>'.$a['rubro'].'</td>
            <td>'.$a['cantidad'].'</td>
            <td>'.'$'.number_format($a['precio']*$a['cantidad'],2).'</td>
            <td>'.$a['clave'].$brinco.$a['huerta'].'</td>
            <td><a href=act_gastos.php?id='.$a['idgasto'].'&idborrar=2"><img src="../img/act.png" alt="ACTUALIZAR" class="img-rounded"></a></td>   
            <td><a href=../php/eliminaciones/eliminar_gasto.php?id='.$a['idgasto'].'&idborrar=2"><img src="../img/del.png" alt="ACTUALIZAR" class="img-rounded"></a></td>';
        }
    }else{
        echo '<tr>
            <td colspan="16">No se encontraron resultados</td>
        </tr>';
    }
    echo '</table>';
?>