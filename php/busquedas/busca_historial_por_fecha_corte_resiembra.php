<?php
include('../conexion.php');
$desde = $_POST['desde'];
$hasta = $_POST['hasta'];
if(isset($desde)==false){
	$desde = $hasta;
}
if(isset($hasta)==false){
	$hasta = $desde;
}
$sql = "  SELECT a.idhistorial_arboles,a.fecha,h.nombre,a.tabla,o.tipo,a.cantidad
from historial_arboles a
inner join operacion o on o.idoperacion = a.idoperacion
inner join huerta h on h.idhuerta  = a.idarboles order by fecha desc  ";
$registro = mysqli_query($mysqli, $sql);
echo '<table class="table table-striped table-bordered text-center">
		<thead>
        	<tr>                          
                <th>FECHA</th>
                  <th>ARTCULO</th>
                 <th>OPERACION</th>
              
                <th>CANTIDAD</th>
                <th>SALDO ANTERIOR</th>
                <th>SALDO ACTUAL</th>
                <th>ELIMINAR</th>                               
            </tr>
        </thead>';
if(  mysqli_num_rows($registro)>0){
	while($registro2 = mysqli_fetch_array ($registro)){
		echo '<tr>
                <td>'.$registro2['fecha'].'</td>
                <td>'.$registro2['nombre'].'</td>
                
                <td>'.$registro2['tabla'].'</td>

                <td>'.$registro2['tipo'].'</td>
                <td>'.$registro2['cantidad'].'</td>
             
        <td><a href=../php/eliminaciones/eliminar_historial_corte_resiembra.php?id='.$registro2['idhistorial_arboles'].'&idborrar=2"><img src="../img/del.png" alt="ACTUALIZAR" class="img-rounded"></a></td>   
		</tr>';
	}
}else{
	echo '<tr>
				<td colspan="16">No se encontraron resultados</td>
			</tr>';
}
echo '</table>';
?>