<?php
    include('../conexion.php');
    $sql="SELECT idcategoria,nombre FROM categoria";
    $sql2="SELECT idsubcategoria,nombre FROM subcategoria";
    $sql3="SELECT idhuerta, nombre FROM huerta order by nombre";
    $categoria = '<div class="form-group col-md-2">
                <select for="categoria" name="categoria[]" id="categoria" class="form-control browser-default text-center"  required>';
    $sub = '<div class="form-group col-md-2">
                <select name="subcategoria[]" id="subcategoria" class="form-control browser-default text-center"  required>';
    $registro = mysqli_query($mysqli, $sql);
    $registro2 = mysqli_query($mysqli, $sql2);
    $registro3 = mysqli_query($mysqli,$sql3);

    if (mysqli_num_rows($registro)>0) {
        while ($a=mysqli_fetch_array($registro)) {
            $categoria .= '<option value="'.$a['idcategoria'].'">'.$a['nombre'].'</option>';
        }
        while ($b=mysqli_fetch_array($registro2)) {
            $sub .= '<option value="'.$b['idsubcategoria'].'">'.$b['nombre'].'</option>';
        }
        $categoria .= '</select></div>';
        $sub .= '</select></div>';

        $huertas = '<div class="form-group col-md-1 text-center">
                            <input  type="text" placeholder="DETALLE" class="form-control text-center" onKeyUp="this.value = this.value.toUpperCase();" name="detalle[]" id="detalle">
                        </div>
                        <div class="form-group col-md-1 text-center">
                            <input  type="text" name="idsegmento[]" placeholder="HERRAMIENTA" class="form-control browser-default"  required></input>
                        </div>
                        <div class="form-group col-md-2 text-center">
                            <select  name="idhuerta[]" class="form-control browser-default"  required>';
        while ($c=mysqli_fetch_array($registro3)) {
            $huertas .= '<option value="'.$c['idhuerta'].'">'.$c['nombre'].'</option>';
        }
            $huertas .='</select></div>';
        $array = array(0 => $categoria, 1 => $sub, 2 => $huertas);
        echo json_encode($array);
    }
?>