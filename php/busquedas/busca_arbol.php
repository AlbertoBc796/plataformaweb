<?php
include('../conexion.php');
$dato = $_POST['dato'];
$sql = " SELECT a.idarboles,a.fecha,h.nombre,a.tabla,v.nombre as var,
a.produccion,a.mediano,a.resiembra, (produccion+mediano+resiembra) as total from arboles a
inner join huerta h on h.idhuerta = a.idhuerta
inner join variedad v on v.idvariedad = a.idvariedad WHERE h.nombre LIKE '%$dato%' ORDER BY h.nombre ASC";

$registro = mysqli_query($mysqli, $sql);
echo '<table class="table table-striped table-bordered text-center">
        	<tr>
                    <th>FECHA</th>
			        <th>HUERTA</th>
                    <th>TABLA</th>
                    <th>VARIEDAD</th>
                    <th>RESIEMBRA</th>
                    <th>MEDIANO</th>
                    <th>PRODUCCION</th>
                    <th>TOTAL</th>
                    <th>EDITAR</th>
                    <th>ELIMINAR</th>
            </tr>';
if(mysqli_num_rows($registro)>0){
	while($registro2 = mysqli_fetch_array($registro)){
		echo '<tr>
				
                <td>'.$registro2['fecha'].'</td>
                <td>'.$registro2['nombre'].'</td>
                <td>'.$registro2['tabla'].'</td>
                <td>'.$registro2['var'].'</td>
                 <td>'.$registro2['resiembra'].'</td>
                <td>'.$registro2['mediano'].'</td>
                <td>'.$registro2['produccion'].'</td>
                  <td>'.$registro2['total'].'</td>
        <td><a href=act_arboles.php?id='.$registro2['idarboles'].'&idborrar=2"><img src="../img/act.png" alt="ACTUALIZAR" class="img-rounded"></a></td>

        <td><a href=../php/eliminaciones/eliminar_arboles.php?id='.$registro2['idarboles'].'&idborrar=2"><img src="../img/del.png" alt="ELIMINAR" class="img-rounded"></a></td>

				</tr>';
	}
}else{
	echo '<tr>
				<td colspan="16">No se encontraron resultados</td>
			</tr>';
}
echo '</table>';
?>