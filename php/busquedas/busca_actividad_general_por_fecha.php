<?php
include('../conexion.php');
$desde = $_POST['desde'];
$hasta = $_POST['hasta'];
if(isset($desde)==false){
	$desde = $hasta;
}
if(isset($hasta)==false){
	$hasta = $desde;
}
$sql = " SELECT a.idactividades_general,date_format(a.fecha,'%d/%m/%Y') as fecha,a.semana,a.mes,a.rubro,
        p.nombre,p.apellidop,p.apellidom,a.cargo,a.salario,a.diat,a.pago_dia,a.idherramienta_segmento,
        c.nombre as nc,s.nombre as ns,a.detalle,a.producto,a.cantidad,a.precio,a.importe,
        h.nombre as h_nombre from actividades_general a
        inner join persona p on p.idpersona = a.idpersona
        inner join categoria c on c.idcategoria = a.idcategoria
        inner join subcategoria s on s.idsubcategoria = a.idsubcategoria
        inner join huerta h on h.idhuerta = a.idhuerta WHERE a.fecha BETWEEN '$desde' AND '$hasta' order by a.fecha ASC ";
$registro = mysqli_query($mysqli, $sql);
echo '<table class="table table-striped table-bordered text-center">
        <thead>
        <tr>
                <th>FOLIO</th>
                <th>FECHA</th>
                <th>SEMANA</th>
                <th>MES</th>
                <th class="oculto"  style="display:none;">RUBRO</th>
                <th>EMPLEADO</th>
                <th>CARGO</th>
                <th>SALARIO</th>
                <th>DIA T.</th>
                <th>PAGO</th>
                <th>ACTIVIDAD</th>
                <th>SUB. ACTIVIDAD</th>
                <th class="oculto"  style="display:none;">DETALLE</th>
                <th class="oculto"  style="display:none;">HERRAMIENTA/SEGMENTO</th>
                <th class="oculto"  style="display:none;">PRODUCTO</th>
                <th class="oculto"  style="display:none;">CANTIDAD</th>
                <th class="oculto"  style="display:none;">PRECIO</th>
                <th class="oculto"  style="display:none;">IMPORTE</th>
                <th>HUERTA</th>
                <th>EDITAR</th>
                <th>ELIMINAR</th>
        </tr>
        </thead>';
if(  mysqli_num_rows($registro)>0){
	while($registro2 = mysqli_fetch_array ($registro)){
		echo '<tr>
		<td>'.$registro2['idactividades_general'].'</td>
        <td>'.$registro2['fecha'].'</td>
        <td>'.$registro2['semana'].'</td>
        <td>'.$registro2['mes'].'</td>
        <td class="oculto"  style="display:none;">'.$registro2['rubro'].'</td>
        <td>'.$registro2['nombre'].' '.$registro2['apellidop'].' '.$registro2['apellidom'].'</td>
        <td>'.$registro2['cargo'].'</td>
        <td>'.$registro2['salario'].'</td>
        <td>'.$registro2['diat'].'</td>
        <td>'.$registro2['pago_dia'].'</td>
        <td>'.$registro2['nc'].'</td>
        <td>'.$registro2['ns'].'</td>
        <td class="oculto"  style="display:none;">'.$registro2['detalle'].'</td>
        <td class="oculto"  style="display:none;">'.$registro2['idherramienta_segmento'].'</td>
        <td class="oculto"  style="display:none;">'.$registro2['producto'].'</td>
        <td class="oculto"  style="display:none;">'.$registro2['cantidad'].'</td>
        <td class="oculto"  style="display:none;">'.$registro2['precio'].'</td>
        <td class="oculto"  style="display:none;">'.$registro2['importe'].'</td>
        <td>'.$registro2['h_nombre'].'</td>

        <td><a href=actualizar_actividades_general.php?id='.$registro2['idactividades_general'].'&idborrar=2"><img src="../img/act.png" alt="ACTUALIZAR" class="img-rounded"></a></td>   
        <td><a href=../php/eliminaciones/eliminar_actividades_general.php?id='.$registro2['idactividades_general'].'&idborrar=2"><img src="../img/del.png" alt="ACTUALIZAR" class="img-rounded"></a></td>
        ';
	}
}else{
	echo '<tr>
			<td colspan="16">No se encontraron resultados</td>
			</tr>';
}
echo '</table>';
?>