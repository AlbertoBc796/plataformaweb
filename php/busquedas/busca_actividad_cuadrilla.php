<?php
    include('../conexion.php');
    $dato = $_POST['dato'];
    if ($dato != '') {
        # code...
        $sql = "SELECT e.idpersona, e.idcargo, e.salario, e.idcuadrilla,
    concat(p.nombre,' ',p.apellidop,' ',p.apellidom) as ncp,a.cargo,
    a.idcargo from empleado e
    inner join persona p on p.idpersona = e.idpersona
    inner join cargo a on a.idcargo = e.idcargo
    where e.idcuadrilla = $dato";
    
    $consulta="SELECT idhuerta,nombre FROM  huerta order by nombre";
    $ress=mysqli_query($mysqli,$consulta);
    $huertas="";
    while($arreglo=mysqli_fetch_array($ress))
    {
        $huertas=$huertas."<option value=\"".$arreglo['idhuerta']."\">".$arreglo['nombre']."  </option>";
    }

	$consulta2="SELECT idcategoria,nombre FROM  categoria order by nombre";
    $ress2=mysqli_query($mysqli,$consulta2);
    $categorias="";
    while($arreglo=mysqli_fetch_array($ress2))
    {
        $categorias=$categorias."<option value=\"".$arreglo['idcategoria']."\">".$arreglo['nombre']."  </option>";
    }
	
	$consulta3="SELECT idsubcategoria,nombre FROM  subcategoria order by nombre";
    $ress3=mysqli_query($mysqli,$consulta3);
    $subcategorias="";
    while($arreglo=mysqli_fetch_array($ress3))
    {
        $subcategorias=$subcategorias."<option value=\"".$arreglo['idsubcategoria']."\">".$arreglo['nombre']."  </option>";
    }
    
    $consulta4="SELECT s.idstock as id, s.articulo as art FROM stock s where s.clave = 'HERRAMIENTA'
    union all 
    SELECT t.idtransporte as id, t.clave as art from transporte t WHERE (t.clave LIKE 'MA%' OR t.clave LIKE 'IM%') order by id";
    $ress4=mysqli_query($mysqli,$consulta4);
    $herramientas='<option value="">SIN HERRAMIENTA</option>';
    while($arreglo=mysqli_fetch_array($ress4))
    {
        $herramientas=$herramientas.'<option value="'.$arreglo['art'].'">'.$arreglo['art'].'  </option>';
    }
    
    $registro = mysqli_query($mysqli, $sql);
    if(mysqli_num_rows($registro)>0){
        echo '<table class="table table-striped table-bordered text-center">
        <thead>
            <tr>
                <th>NOMBRE</th>
                <th class="oculto" style="display:none;">CARGO</th>
                <th class="oculto" style="display:none;">SALARIO</th>
                <th>DIAS</th>
                <th>PAGO</th>
                <th>ACTIVIDAD</th>
                <th>SUB. ACT</th>
                <th>DETALLE 2</th>
                <th>HERRAMIENTA</th>
                <th>HUERTA</th>
            </tr>
        </thead>
        <tr>';
        while ($a = mysqli_fetch_array($registro)) {
            echo'<td><input  type="text" class="form-control text-center"   name="nombre" id="nombre" value="'.$a['ncp'].'">
            <input onKeyUp="this.value = this.value.toUpperCase();" id="idpersona" name="idpersona[]" class="form-control" type="hidden" value="'.$a['idpersona'].'"></td>
            <td class="oculto" style="display:none;"><input  type="text" class="form-control text-center" name="cargo[]" id="cargo" value="'.$a['cargo'].'"></td>
            <td class="oculto" style="display:none;"><input  type="text" class="form-control text-center" name="salario[]" id="salario" value="'.$a['salario'].'"></td>
            <td><input  type="number" min="0" step="0.1" class="form-control text-center"   name="diat[]" id="diat"></td>
            <td><input  type="text" class="form-control text-center" onkeyup="reesultado6()" readonly name="importe[]" id="importe" ></td>
            <td><select for="categoria" name="categoria[]" id="categoria" class="form-control browser-default text-center"  required>'.$categorias.'</td>
            <td><select name="subcategoria[]" id="subcategoria" class="form-control browser-default text-center"  required>'.$subcategorias.'</td>
            <td><input  type="text" class="form-control text-center" onKeyUp="this.value = this.value.toUpperCase();" name="detalle[]" id="detalle"></td>
            <td><select  name="idsegmento[]" class="form-control browser-default"  required>'.$herramientas.'</select></td>
            <td><select  name="idhuerta[]" class="form-control browser-default"  required>'.$huertas.'</select></td>
        </tr>';
        }
        echo '</table>';
    }
    }
    
?>