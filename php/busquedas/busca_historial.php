<?php
include('../conexion.php');
session_start();
$dato = $_POST['dato'];
$dato2 = $_POST['dato2'];

$sql = " SELECT h.idhistorial, date_format( h.fecha,'%d-%m-%Y') as fecha,s.articulo,o.tipo,h.saldoant,h.viaje,h.lugar,h.cantidad, 
    CASE
      WHEN h.idoperacion  = 1 THEN (cantidad+saldoant)
      WHEN h.idoperacion  = 2 THEN (saldoant-cantidad)
      WHEN h.idoperacion  = 3 THEN (cantidad+saldoant)
      WHEN h.idoperacion  = 4 THEN (cantidad)
    END as total
    FROM historial h 
    inner join stock s on s.idstock = h.idstock
    inner join operacion o on o.idoperacion = h.idoperacion
    WHERE s.articulo  LIKE '%$dato%' AND o.tipo LIKE '%$dato2%' ORDER BY h.fecha DESC";

  $registro = mysqli_query($mysqli, $sql);
  if($_SESSION['tipo'] == 1){
    $var1='<th>ELIMINAR</th>';
  }else {
    $var1 = '';
  }
  echo '<table class="table table-striped table-bordered text-center">
        <thead>
            <tr>                          
                <th>FECHA</th>
                <th>ARTCULO</th>
                <th>OPERACION</th>
                <th>CANTIDAD</th>
                <th>EXISTENCIA</th>
                <th>ACTUAL</th>
                <th>DESTINO</th>'.$var1.
            '</tr>
        </thead>';
if(mysqli_num_rows($registro)>0){
	while($registro2 = mysqli_fetch_array($registro)){
		if($_SESSION['tipo'] == 1){
      $var2 = '<td><a href=../php/eliminaciones/eliminar_historial.php?id='.$registro2['idhistorial'].'&idborrar=2"><img src="../img/del.png" alt="ELIMINAR" class="img-rounded"></a></td>';
  }else {
      $var2 = '';
  }
  if ($registro2['viaje'] == '') {
      $var3 = '';
  }else {
      $var3 = '<br>';
  }
echo '<tr>
          <td>'.$registro2['fecha'].'</td>
          <td>'.$registro2['articulo'].'</td>
          <td>'.$registro2['tipo'].'</td>
          <td>'.$registro2['cantidad'].'</td>
          <td>'.$registro2['saldoant'].'</td>
          <td>'.$registro2['total'].'</td>
          <td>'.$registro2['viaje'].$var3.$registro2['lugar'].'</td>'.
          $var2.
'</tr>';
	}
}else{
	echo '<tr>
				<td colspan="16">No se encontraron resultados</td>
			</tr>';
}
echo '</table>';
?>