<?php
    include('../conexion.php');
    $dato = $_POST['dato'];
    $sql = "SELECT date_format( h.fecha,'%d/%m/%Y <br> %H:%i:%s') as fecha,h.idusuario,
    h.actividad,h.equipo, u.usuario as username from historial_users h
    inner join usuarios u on u.idusuarios = h.idusuario
    WHERE usuario LIKE '%$dato%'
    ORDER BY fecha DESC ";
    $registro = mysqli_query($mysqli, $sql);
    echo '<table class="table table-striped table-bordered text-center">
    <thead>
        <tr>
            <th>FECHA</th>
            <th>USUARIO</th>
            <th>ACTIVIDAD</th>
            <th>EQUIPO</th>
        </tr>
    </thead>';
    if (mysqli_num_rows($registro)>0) {
        while ($registro2 = mysqli_fetch_array($registro)) {
            echo '<tr>
                <td>'.$registro2['fecha'].'</td>
                <td>'.$registro2['username'].'</td>
                <td>'.$registro2['actividad'].'</td>
                <td>'.$registro2['equipo'].'</td>
            </tr>';
        }
    }else {
        echo '<tr>
			<td colspan="16">No se encontraron resultados</td>
		</tr>';
    }
    echo '</table>';
?>