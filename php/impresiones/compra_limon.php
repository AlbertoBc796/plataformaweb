<?php
require('../conexion.php');
require('../../fpdf/fpdf.php');
class PDF extends FPDF
{
// Cabecera de página
function Header()
{

if(strlen($_GET['desde'])>0 and strlen($_GET['hasta'])>0 and strlen($_GET['dato'])>0 and strlen($_GET['dato2'])>0  and strlen($_GET['dato56'])>0){
    $dato = $_GET['dato'];
    $dato2 = $_GET['dato2'];
    $dato56 = $_GET['dato56'];
    $desde = $_GET['desde'];
    $hasta = $_GET['hasta'];
    $verDesde = $desde;
    $verHasta = $hasta;
}else{

    $desde = $_GET['desde'];
    $hasta = $_GET['hasta'];
    $dato = 'XXX-XXX-XXX';
    $dato2 = 'LIMON';
    $dato56 = 'PERSA';
    $verDesde = $desde;
    $verHasta = $hasta;
}
    // Logo
    $this->Image('../../img/logo.jpg',10,5,35);

    // Arial bold 15
    $this->SetFont('Arial','B',15);
    // Movernos a la derecha
    $this->Cell(130,10,'',0,0,'c');

    $this->Cell(140, 10, 'AGRICOLA MEN (Huertas)',0,0,'C',0);
    $this->Cell(100, 10, 'Martinez de la Torre, Ver '.date('d-m-Y').'', 0,0,'C',0);
    $this->Ln(20);
    $this->Cell(130, 8, '', 0);
    $this->Cell(140, 8, 'REPORTE DE COSECHA - VENTA', 0,0,'C',0);
    $this->Cell(100, 8, '', 0);
    $this->Ln(20);
    $this->SetFont('Arial', 'B', 10);
    $this->Cell(45, 8, '', 0);
    $this->Cell(100, 8, 'DE LA FECHA :  '.$verDesde.'  HASTA LA FECHA   '.$verHasta, 0); #colocar las variables desde y hasta
     $this->Cell(120, 8, '', 0);
     $this->Cell(40, 8, 'PRODUCTO: '.$dato2. ' ' .$dato56, 0,0,'C');
    $this->Cell(45, 8, '', 0);
    $this->Ln(20);
}

// Pie de página
function Footer()
{
    // Posición: a 1,5 cm del final
    $this->SetY(-15);
    // Arial italic 8
    $this->SetFont('Arial','I',8);
    // Número de página
    $this->Cell(0,10,'Pagina '.$this->PageNo().'/{nb}',0,0,'C');
}
}

// Creación del objeto de la clase heredada
$pdf = new PDF('l','mm','A3');
$pdf->SetMargins(1, 5 , 0); 
$pdf->AliasNbPages();
$pdf->AddPage();
    $pdf->SetFont('Arial', 'B', 8);
    $pdf->SetFillColor(232,232,232);
    $pdf->Cell(6, 8, '#', 1,0,'C', 1);
    $pdf->Cell(14, 8, 'Fecha', 1,0,'C', 1);
    $pdf->Cell(20, 8, 'Huerta', 1,0,'C', 1);
    $pdf->Cell(16, 8, 'Camion', 1,0,'C', 1);
    $pdf->Cell(18, 8, 'Marca', 1,0,'C', 1);
    $pdf->Cell(18, 8, 'Color', 1,0,'C', 1);
    $pdf->Cell(16, 8, 'Placas', 1,0,'C', 1);
    $pdf->Cell(43, 8, 'Chofer', 1,0,'C', 1);
    $pdf->Cell(13, 8, 'R. Corte', 1,0,'C', 1);

    $pdf->Cell(15, 8, 'C. corte', 1,0,'C', 1);

    $pdf->Cell(15, 8, 'P. Corte', 1,0,'C', 1);
    $pdf->Cell(15, 8, 'Imp. Corte', 1,0,'C', 1);
    $pdf->Cell(15, 8, 'Pesaje', 1,0,'C', 1);
    $pdf->Cell(15, 8, 'P. Flete', 1,0,'C', 1);
    $pdf->Cell(15, 8, 'Imp. Flete', 1,0,'C', 1);

    $pdf->Cell(17, 8, 'Egresos', 1,0,'C', 1);
    $pdf->Cell(13, 8, 'R. Venta', 1,0,'C', 1);
    $pdf->Cell(13, 8, 'C. Venta', 1,0,'C', 1);
    $pdf->Cell(13, 8, 'Merma', 1,0,'C', 1);

    $pdf->Cell(17, 8, 'Kg. Venta', 1,0,'C', 1);
    $pdf->Cell(15, 8, 'P. Venta', 1,0,'C', 1);
    $pdf->Cell(15, 8, 'T. Venta', 1,0,'C', 1);
    $pdf->Cell(17, 8, 'Ingresos', 1,0,'C', 1);
    $pdf->Cell(40, 8, 'Cliente', 1,0,'C', 1);
$pdf->Ln(8);
$pdf->SetFont('Arial', '', 7);
extract($_GET);


$h2="SELECT count(*) as cuantos from compra_venta_limon c  
inner join huerta h on c.idhuerta=h.idhuerta
inner join producto p on p.idproducto = c.idproducto
inner join variedad v on v.idvariedad = c.idvariedad
WHERE h.nombre LIKE '%$dato%'  AND p.nombre LIKE '%$dato2%' AND v.nombre LIKE '%$dato56%' AND c.fecha BETWEEN '$desde' AND '$hasta'";
$r2 = mysqli_query($mysqli,$h2);
while ($row2=mysqli_fetch_assoc($r2)) {
    $a2 = $row2['cuantos']; 
}


$query = "  
SELECT c.fecha,h.nombre AS HUERTITA,p.nombre,p.idproducto ,c.folio1,c.camion,c.marca,c.color,
c.placas,c.chofer,c.cajas_corte,c.precio_corte,c.importe_corte, c.precio_flete,c.importe_flete,
c.pesaje,c.egresos,c.c_venta,c.merma,
c.cajas_venta,c.precio_venta,c.total_venta,c.ingreso_total,c.folio2,c.cliente,v.nombre from  compra_venta_limon c inner join huerta h on c.idhuerta=h.idhuerta
    inner join producto p on p.idproducto = c.idproducto
    inner join variedad v on v.idvariedad = c.idvariedad
  WHERE h.nombre LIKE '%$dato%'  AND p.nombre LIKE '%$dato2%' AND v.nombre LIKE '%$dato56%' AND c.fecha BETWEEN '$desde' AND '$hasta' order by c.fecha asc";


$x=0;
$productos = mysqli_query($mysqli, $query);
while($compra = mysqli_fetch_array($productos)){

    if ( $x <= $a2 ) {

    $pdf->Cell(6, 8, $x = $x+1, 1,0,'C');
    $pdf->Cell(14, 8,$compra['fecha'], 1,0,'C');
    $pdf->Cell(20, 8, $compra['HUERTITA'], 1,0,'C');
    $pdf->Cell(16, 8,$compra['camion'], 1,0,'C');
    $pdf->Cell(18, 8,$compra['marca'], 1,0,'C');
    $pdf->Cell(18, 8,$compra['color'], 1,0,'C');
    $pdf->Cell(16, 8,$compra['placas'], 1,0,'C');
    $pdf->Cell(43, 8,$compra['chofer'], 1,0,'C');
    $pdf->Cell(13, 8, $compra['folio1'], 1,0,'C');
    $pdf->Cell(15, 8,$compra['cajas_corte'], 1,0,'C');
    $pdf->Cell(15, 8,'$'.$compra['precio_corte'], 1,0,'C');
    $pdf->Cell(15, 8,'$'.$compra['importe_corte'], 1,0,'C');
    $pdf->Cell(15, 8,'$'.$compra['pesaje'], 1,0,'C');
    $pdf->Cell(15, 8,'$'.$compra['precio_flete'], 1,0,'C');
    $pdf->Cell(15, 8,'$'.$compra['importe_flete'],1,0,'C');
    $pdf->Cell(17, 8,'$'.$compra['egresos'],1,0,'C');

    $pdf->Cell(13, 8, $compra['folio2'], 1,0,'C');

    $pdf->Cell(13, 8, $compra['c_venta'], 1,0,'C');
    $pdf->Cell(13, 8, $compra['merma'], 1,0,'C');
    $pdf->Cell(17, 8,$compra['cajas_venta'], 1,0,'C');
    $pdf->Cell(15, 8,'$'.$compra['precio_venta'], 1,0,'C');
    $pdf->Cell(15, 8,'$'.$compra['total_venta'],1,0,'C');
    $pdf->Cell(17, 8,'$'.$compra['ingreso_total'],1,0,'C');
    $pdf->Cell(40, 8,$compra['cliente'],1,0,'C');
    $pdf->Ln(8);
        
    }
   
}

    $pdf->SetFillColor(232,232,232);
    $pdf->Cell(350, 8, '', 0,0,'C', 0);
    $pdf->Ln(8);
    $pdf->SetFont('Arial', '', 7);

//CONSULTA DE TOTAL 
$query = "SELECT h.nombre ,p.nombre,p.idproducto,v.nombre, sum(cajas_corte),sum(precio_corte),sum(importe_corte),
sum(pesaje),sum(precio_flete),sum(importe_flete), sum(egresos),sum(c_venta),sum(merma),
sum(cajas_venta),sum(precio_venta),sum(total_venta), sum(ingreso_total) from compra_venta_limon c inner join 
huerta h on c.idhuerta=h.idhuerta 
inner join producto p on p.idproducto = c.idproducto
inner join variedad v on v.idvariedad = c.idvariedad 
WHERE h.nombre LIKE '%$dato' AND  p.nombre LIKE '%$dato2' AND v.nombre LIKE '%$dato56%' AND c.fecha BETWEEN '$desde' AND '$hasta'";
$productos = mysqli_query($mysqli, $query);
while($compra = mysqli_fetch_array($productos)){
    $pdf->Cell(108, 8, '', 0,0,'C');
    $pdf->SetFont('Arial', 'B', 7);
    $pdf->Cell(56, 8,'TOTALES:', 0,0,'C');
    $pdf->SetFillColor(232,232,232);
    $pdf->Cell(15, 8,$compra['sum(cajas_corte)'], 1,0,'C',1);
    $pdf->Cell(15, 8,'$'.$compra['sum(precio_corte)'], 1,0,'C',1);
    $pdf->Cell(15, 8,'$'.$compra['sum(importe_corte)'], 1,0,'C',1);
    $pdf->Cell(15, 8,'$'.$compra['sum(pesaje)'], 1,0,'C',1);
    $pdf->Cell(15, 8,'$'.$compra['sum(precio_flete)'], 1,0,'C',1);
    $pdf->Cell(15, 8,'$'.$compra['sum(importe_flete)'],1,0,'C',1);
    $pdf->Cell(17, 8,'$'.$compra['sum(egresos)'],1,0,'C',1);
    $pdf->Cell(13, 8,'',0,0,'C',0);
    $pdf->Cell(13, 8,$compra['sum(c_venta)'], 1,0,'C',1);
    $pdf->Cell(13, 8,$compra['sum(merma)'], 1,0,'C',1);
    $pdf->Cell(17, 8,$compra['sum(cajas_venta)'], 1,0,'C',1);
    $pdf->Cell(15, 8,'$'.$compra['sum(precio_venta)'], 1,0,'C',1);
    $pdf->Cell(15, 8,'$'.$compra['sum(total_venta)'],1,0,'C',1);
    $pdf->Cell(17, 8,'$'.$compra['sum(ingreso_total)'],1,0,'C',1);
    $pdf->Cell(24, 8,'', 0,0,'C',0);
    $pdf->Ln(8);
}
$pdf->Output('reporte.pdf','D');
