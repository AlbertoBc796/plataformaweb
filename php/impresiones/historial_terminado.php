<?php
require('../conexion.php');
require('../../fpdf/fpdf.php');
class PDF extends FPDF
{
// Cabecera de página
function Header()
{
if(strlen($_GET['desde'])>0 and strlen($_GET['hasta'])>0 and strlen($_GET['dato'])>0){
    $dato = $_GET['dato'];
    $desde = $_GET['desde'];
    $hasta = $_GET['hasta'];
    $verDesde = $desde;
    $verHasta = $hasta;
}else{
    $desde = $_GET['desde'];
    $hasta = $_GET['hasta'];
    $dato = 'XXX-XXX-XXX';
    $verDesde = $desde;
    $verHasta = $hasta;
}
    
    // Logo
    $this->Image('../../img/logo.jpg',20,5,40);
    // Arial bold 15
    $this->SetFont('Arial','B',12);
    // Movernos a la derecha
    $this->Cell(110,10,'',0,0,'c');
    $this->Cell(100, 10, 'AGRICOLA MEN (Historial)',0,0,'C',0);
    $this->Cell(120, 10, 'Martinez de la Torre, Ver '.date('d-m-Y').'', 0,0,'C',0);
    $this->Ln(20);
    $this->Cell(110, 8, '', 0);
    $this->Cell(100, 8, 'REPORTE DE ALMACEN ', 0,0,'C',0);
    $this->Cell(100, 8, '', 0);
    $this->Ln(20);
    $this->SetFont('Arial', 'B', 12);
    $this->Cell(6, 8, '', 0);
    $this->Cell(115, 8, 'DE LA FECHA :'.date('d-m-Y',strtotime($verDesde)).'  HASTA LA FECHA  '.date('d-m-Y',strtotime($verHasta)), 0,0,'C',0); #colocar las variables desde y hasta
    $this->Ln(15);
}

function Footer()
{
    // Posición: a 1,5 cm del final
    $this->SetY(-15);
    // Arial italic 8
    $this->SetFont('Arial','I',8);
    // Número de página
    $this->Cell(0,10,'Pagina '.$this->PageNo().'/{nb}',0,0,'C');
}
}
// Creación del objeto de la clase heredada
$pdf = new PDF('l','mm','A3'); 
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFillColor(232,232,232);
$pdf->SetFont('Arial', 'B', 9);
$pdf->Cell(10, 8,'#', 1,0,'C',1);
$pdf->Cell(30, 8,'FECHA',1,0,'C',1);
$pdf->Cell(35, 8,'CLAVE',1,0,'C',1);
$pdf->Cell(90, 8,'ARTICULO', 1,0,'C',1);
$pdf->Cell(45, 8,'OPERACION ', 1,0,'C',1);
$pdf->Cell(20, 8,'CANTIDAD', 1,0,'C',1);
$pdf->Cell(20, 8,'EXIST.', 1,0,'C',1);
$pdf->Cell(65, 8,'DESTINO', 1,0,'C',1);
$pdf->Cell(20, 8,'ACTUAL', 1,0,'C',1);
$pdf->Cell(20, 8,'COSTO', 1,0,'C',1);
$pdf->Ln(8);
$pdf->SetFont('Arial', '', 9);
extract($_GET);
$h2="SELECT count(*) as cuantos from historial h  
inner join stock s on s.idstock = h.idstock
inner join operacion o on o.idoperacion = h.idoperacion 
where s.articulo LIKE '%$dato%' AND o.tipo LIKE '%$dato2%' AND h.fecha BETWEEN '$desde' AND '$hasta'";
$r2 = mysqli_query($mysqli,$h2);
while ($row2=mysqli_fetch_assoc($r2)) {
    $a2 = $row2['cuantos']; 
}

$sql=("SELECT h.idhistorial, h.fecha,s.articulo,s.clave,s.preciounit,o.tipo,h.saldoant,h.viaje,h.lugar,h.cantidad, 
    CASE
        WHEN h.idoperacion  = 1 THEN (cantidad+saldoant)
        WHEN h.idoperacion  = 2 THEN (saldoant-cantidad)
        WHEN h.idoperacion  = 3 THEN (cantidad+saldoant)
        WHEN h.idoperacion  = 4 THEN (cantidad)
        WHEN h.idoperacion  = 5 THEN (saldoant-cantidad)
    END as total
    FROM historial h 
    inner join stock s on s.idstock = h.idstock
    inner join operacion o on o.idoperacion = h.idoperacion
    WHERE s.articulo LIKE '%$dato%' AND o.tipo LIKE '%$dato2%' AND h.fecha BETWEEN '$desde' AND '$hasta' order by fecha desc");
$query=mysqli_query($mysqli,$sql);
$x=0;
$gastoTotal=0;

while($stock = mysqli_fetch_assoc($query)){
if ( $x <= $a2) {
    if ($x%25 == 0 && $x > 0) {
        $pdf->SetFillColor(232,232,232);
        $pdf->SetFont('Arial', 'B', 9);
        $pdf->Cell(10, 8,'#', 1,0,'C',1);
        $pdf->Cell(30, 8,'FECHA',1,0,'C',1);
        $pdf->Cell(35, 8,'CLAVE',1,0,'C',1);
        $pdf->Cell(90, 8,'ARTICULO', 1,0,'C',1);
        $pdf->Cell(45, 8,'OPERACION ', 1,0,'C',1);
        $pdf->Cell(20, 8,'CANTIDAD', 1,0,'C',1);
        $pdf->Cell(20, 8,'EXIST.', 1,0,'C',1);
        $pdf->Cell(65, 8,'DESTINO', 1,0,'C',1);
        $pdf->Cell(20, 8,'ACTUAL', 1,0,'C',1);
        $pdf->Cell(20, 8,'COSTO', 1,0,'C',1);
        $pdf->Ln(8);
        $pdf->SetFont('Arial', '', 9);
    }
    $pdf->Cell(10, 8,$x = $x+1,1, 0,'C');
    $pdf->Cell(30, 8,date('d-m-Y',strtotime($stock['fecha'])),1, 0,'C');
    $pdf->Cell(35, 8,$stock['clave'],1, 0,'C');
    $pdf->Cell(90, 8,$stock['articulo'], 1, 0,'C');
    $pdf->Cell(45, 8,utf8_decode($stock['tipo']), 1, 0,'C');
    $pdf->Cell(20, 8,$stock['cantidad'],1, 0,'C');
    $pdf->Cell(20, 8,$stock['saldoant'],1,0,'C');
    $pdf->Cell(65, 8,$stock['viaje'].' '.$stock['lugar'],1,0,'C');
    $data2 = bcdiv($stock['total'], '1',1);
    $pdf->Cell(20, 8,$data2 ,1,0,'C');
    $pdf->Cell(20, 8,'$'.bcdiv($stock['cantidad']*$stock['preciounit'], '1',2) ,1,0,'C');
    $pdf->Ln(8);
    $gastoTotal=$gastoTotal+($stock['cantidad']*$stock['preciounit']);
}
}
$pdf->Cell(270, 8, '', 0,0,'C');
$pdf->SetFont('Arial', 'B', 10);
$pdf->Cell(20, 8,'TOTALES:', 0,0,'C');
$pdf->SetFillColor(232,232,232);
$pdf->Cell(20, 8,'$'.number_format($gastoTotal,2) ,1,0,'C');

$pdf->Ln(10);
// //CONSULTA DE TOTAL EN STOCK7
// $query2 = "SELECT s.articulo, h.fecha,o.tipo,s.unidad,s.existencia from historial h 
// inner join stock s on s.idstock = h.idstock 
// inner join operacion o on o.idoperacion = h.idoperacion 
// WHERE s.articulo LIKE '%$dato%' AND o.tipo LIKE '%$dato2%' AND h.fecha BETWEEN '$desde' AND '$hasta' 
// group by articulo order by fecha desc ";
// $productos = mysqli_query($mysqli, $query2);
//     $pdf->SetFillColor(232,232,232);
//     $pdf->SetFont('Arial', 'B', 9);
//     $pdf->Cell(40, 8,'',0,0,'C',0);
//     #$pdf->Cell(45, 8,'FECHA',1,0,'C',1);
//     $pdf->Cell(90, 8,'ARTICULO', 1,0,'C',1);
//     $pdf->Cell(50, 8,'TOTAL EN EXISTENCIA', 1,0,'C',1);
//     $pdf->Ln(8);
//     while($compra = mysqli_fetch_array($productos)){
//         $pdf->SetFillColor(232,232,232);
//         $pdf->Cell(40, 8, '', 0,0,'C', 0);
//         #$pdf->Cell(45, 8, $compra['fecha'], 1,0,'C', 0);
//         $pdf->Cell(90, 8, $compra['articulo'] .' '.$compra['unidad'], 1,0,'C', 0);
//         $pdf->Cell(50, 8, $compra['existencia'] .' '.$compra['unidad'], 1,0,'C', 0);
//         $pdf->Ln(8);   
//     }
//     $pdf->Ln(8);   

//Clasificacion por huerta
$query3 = "SELECT h.idstock, s.articulo, h.fecha, h.cantidad, h.idoperacion, o.tipo,
h.saldoant,h.viaje,h.lugar,s.preciounit,sum(h.cantidad*s.preciounit) as gasto,
CASE
    WHEN h.idoperacion  = 1 THEN (cantidad+saldoant)
    WHEN h.idoperacion  = 2 THEN (saldoant-cantidad)
    WHEN h.idoperacion  = 3 THEN (cantidad+saldoant)
    WHEN h.idoperacion  = 4 THEN (cantidad)
    WHEN h.idoperacion  = 5 THEN (saldoant-cantidad)
END as total
FROM historial h
inner join stock s on s.idstock = h.idstock
inner join operacion o on o.idoperacion = h.idoperacion
WHERE s.articulo LIKE '%$dato%' AND o.tipo LIKE '%$dato2%' AND h.fecha BETWEEN '$desde' AND '$hasta' GROUP BY h.lugar ORDER BY h.fecha desc";
$gastoTotal = 0;
$clasif = mysqli_query($mysqli, $query3);
$pdf->SetFillColor(232,232,232);
$pdf->SetFont('Arial', 'B', 9);
$pdf->Cell(40, 8,'',0,0,'C',0);
#$pdf->Cell(45, 8,'FECHA',1,0,'C',1);
$pdf->Cell(90, 8,'LUGAR', 1,0,'C',1);
$pdf->Cell(90, 8,'GASTO', 1,0,'C',1);
$pdf->Ln(8);
$pdf->SetFont('Arial', '', 9);
while ($unidad = mysqli_fetch_array($clasif)) {
    $pdf->Cell(40, 8,'',0,0,'C',0);
    $pdf->Cell(90, 8,$unidad['lugar'], 1,0,'C');
    $pdf->Cell(90, 8,'$ '.number_format($unidad['gasto'],2), 1,0,'C');
    $gastoTotal += $unidad['gasto'];
    $pdf->Ln(8);
}
$pdf->Cell(40, 8,'',0,0,'C',0);
$pdf->Cell(90, 8,'TOTAL', 0,0,'C');
$pdf->Cell(90, 8,'$ '.number_format($gastoTotal,2), 1,0,'C');
$pdf->Ln(8);
$pdf->Output();