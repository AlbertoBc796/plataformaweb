<?php
require('../conexion.php');
require('../../fpdf/fpdf.php');
class PDF extends FPDF
{
function Header()
{
    $this->Image('../../img/logo.jpg',10,5,35);
	$this->SetFont('Courier', 'I', 20);
	$this->Cell(45, 10, '', 0);
	$this->Cell(95, 10, 'GRUPO MURRIETA',0,0,'C',0);
	$this->SetFont('Arial', 'I', 9);
	$this->Cell(40, 10, 'Martinez de la Torre, Ver: '.date('d-m-Y').'', 0);
	$this->Ln(18);
	$this->SetFont('Courier', 'I', 15);
	$this->Cell(45, 10, '', 0);
	$this->Cell(95, 10, 'RESIEMBRA EN HUERTAS',0,0,'C',0);
	$this->Ln(15);
}
function Footer()
{
    $this->SetY(-15);
    $this->SetFont('Arial','I',8);
    $this->Cell(0,10,'Pagina '.$this->PageNo().'/{nb}',0,0,'C');
}
}
$sql=("SELECT @numero:=@numero+1 as total , date_format(A.fecha,'%d-%m-%Y') as fecha,h.nombre as hu,a.tabla,v.nombre as var,a.resiembra,a.mediano,a.produccion,(resiembra+mediano+produccion) as t1 from arboles a
	inner join huerta h on h.idhuerta = a.idhuerta
	inner join variedad v on v.idvariedad = a.idvariedad
	, (SELECT @numero:= 0) AS total ORDER BY fecha desc");
   $query=mysqli_query($mysqli,$sql);
$pdf = new PDF('p','mm','letter');
$pdf->AliasNbPages();
$pdf->SetMargins(5, 5 , 0);
$pdf->AddPage();
$pdf->Ln(10);

$pdf->Ln(15);
$pdf->SetFillColor(232,232,232);
$pdf->SetFont('Arial', 'I', 9);
$pdf->Cell(10, 8,'#', 1,0,'C',1);
$pdf->Cell(20, 8,'FECHA',1,0,'C',1);
$pdf->Cell(35, 8,'HUERTA', 1,0,'C',1);
$pdf->Cell(15, 8,'TABLA', 1,0,'C',1);
$pdf->Cell(30, 8,'VARIEDAD', 1,0,'C',1);
$pdf->Cell(25, 8,'RESIEMBRA', 1,0,'C',1);
$pdf->Cell(25, 8,'MEDIANO', 1,0,'C',1);
$pdf->Cell(25, 8,'PRODUCCION', 1,0,'C',1);
$pdf->Cell(20, 8,'TOTAL', 1,0,'C',1);
$pdf->Ln(8);

while($stock = mysqli_fetch_assoc($query)){
    $pdf->Cell(10, 8,$stock['total'],1, 0,'C');
    $pdf->Cell(20, 8,$stock['fecha'],1, 0,'C');
    $pdf->Cell(35, 8,$stock['hu'], 1, 0,'C');
    $pdf->Cell(15, 8,$stock['tabla'],1, 0,'C');
    $pdf->Cell(30, 8,$stock['var'], 1,0,'C');
    $pdf->Cell(25, 8,$stock['resiembra'], 1,0,'C');
    $pdf->Cell(25, 8,$stock['mediano'], 1,0,'C');
    $pdf->Cell(25, 8,$stock['produccion'], 1,0,'C');
      $pdf->Cell(20, 8,$stock['t1'], 1,0,'C');
      $pdf->Ln(8);
}
$pdf->Output();


?>

select a.fecha,
