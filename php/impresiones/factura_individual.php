<?php
require('../conexion.php');
require('../../fpdf/fpdf.php');



class PDF extends FPDF
{
// Cabecera de página
function Header()
{
    $this->SetFillColor(30, 178, 59  );
    $this->Cell(200, 10, '',0,0,'L',1);
    $this->Ln(15);    
    $this->Cell(200, 40, '',1,0,'L',0);
    $this->Ln(5);    
    // Logo
    $this->Image('../../img/logo.jpg',170,27,35);
    // Arial bold 15
    $this->SetFont('Arial','B',10);
    // Movernos a la derecha
    $this->Cell(85, 10, 'AGRICOLA MEN (Huertas)',0,0,'C',0);
    $this->Ln(10);
    $this->SetFont('Arial', 'I', 7);
    $this->Cell(85, 5, utf8_decode('DIRECCIÓN: NUEVA CENTRAL DE ABASTO #15 '),0,0,'C',0);
    $this->Ln(5); 
    $this->Cell(85, 5,utf8_decode('COLONIA: PLAZA VERDE, MARTÍNEZ DE LA TORRE, VERACRUZ. ') ,0,0,'C',0);
    $this->Ln(5);
    $this->Cell(85, 5, utf8_decode('EMAIL: ANGEL.MURRIETA@HOTMAIL.COM '),0,0,'C',0);
    $this->Ln(5);
    $this->Cell(85, 5,utf8_decode(' TEL: 01(232)3245205 Y 01 (232) 3245206') ,0,0,'C',0);
    $this->Ln(20);  

}


// Pie de página
function Footer()
{
    // Posición: a 1,5 cm del final
    $this->SetY(-15);
    // Arial italic 8
    $this->SetFont('Arial','I',8);
    // Número de página
    $this->Cell(0,10,'Pagina '.$this->PageNo().'/{nb}',0,0,'C');
}
}

// Creación del objeto de la clase heredada
$pdf = new PDF('P','mm','letter');
#$pdf->SetMargins(1, 5 , 0); 
$pdf->AliasNbPages();
$pdf->AddPage();

extract($_GET);
//CONSULTA
$query = "  SELECT @numero:=@numero+1 as total,c.cliente, c.idcompra_venta,c.fecha,c.folio2,
c.kilos_venta,c.precio_venta,c.total_venta, c.facturacion, m.movimiento, pa.pago, v.nombre ,c.folion,
 pr.nombre as pr
 from compra_venta c 
 inner join movimiento m on m.idmovimiento = c.idmovimiento
 inner join pago pa on pa.idpago = c.idpago
 inner join variedad v on v.idvariedad = c.idvariedad
 inner join producto pr on pr.idproducto= c.idproducto ,(SELECT @numero:= 0) AS total where idcompra_venta = $id  ";
$productos = mysqli_query($mysqli, $query);
while($compra = mysqli_fetch_array($productos)){

    $pdf->SetFont('Arial', 'B', 9);
    $pdf->Cell(50, 10,utf8_decode( 'FACTURAR A :'), 1,0,'C',0);
    $pdf->Cell(50, 10,utf8_decode ('REPORTE DE COMPRA '), 1,0,'C',0);
    $pdf->Cell(50, 10,utf8_decode ('N° DE FACTURA'), 1,0,'C',0);
    $pdf->Cell(50, 10, utf8_decode('LUGAR Y FECHA '), 1,0,'C',0);
    $pdf->Ln(10);

    $pdf->SetFont('Arial', 'I', 8);

    $pdf->Cell(50, 10,utf8_decode (' '.$compra['cliente'] .''), 1,0,'C',0);
    $pdf->Cell(50, 10,utf8_decode (''.$compra['folio2'] .''), 1,0,'C',0);
    $pdf->Cell(50, 10,utf8_decode (' '.$compra['folion'].''), 1,0,'C',0);
    $pdf->Cell(50, 10, utf8_decode('Martinez de la Torre, Ver '.date('d-m-Y').''), 1,0,'C',0);
    
    $pdf->Ln(10);
    $pdf->SetFont('Arial', 'B', 9);
    $pdf->Cell(50, 10, ' ', 0,0,'C',0);
    $pdf->Cell(50, 10, '  ', 0,0,'C',0);
    $pdf->Cell(50, 10, utf8_decode(' FACTURACION '), 1,0,'C',0);
    $pdf->Cell(50, 10, utf8_decode('  MOVIMIENTO '), 1,0,'C',0);

    $pdf->Ln(10);
    $pdf->SetFont('Arial', 'I', 8);
    $pdf->Cell(50, 10, ' ', 0,0,'C',0);
    $pdf->Cell(50, 10, '  ', 0,0,'C',0);
    $pdf->Cell(50, 10, utf8_decode('  '.$compra['facturacion'].''), 1,0,'C',0);
    $pdf->Cell(50, 10, utf8_decode('  '.$compra['movimiento'].' '), 1,0,'C',0);

    $pdf->Ln(10);
    $pdf->SetFont('Arial', 'B', 9);
    $pdf->Cell(50, 10, ' ', 0,0,'C',0);
    $pdf->Cell(50, 10, '  ', 0,0,'C',0);
    $pdf->Cell(50, 10, '  ', 0,0,'C',0);
    $pdf->Cell(50, 10, utf8_decode(' PAGO  '), 1,0,'C',0);

    $pdf->Ln(10);
    $pdf->SetFont('Arial', 'I', 8);
    $pdf->Cell(50, 10, ' ', 0,0,'C',0);
    $pdf->Cell(50, 10, '  ', 0,0,'C',0);
    $pdf->Cell(50, 10, ' ', 0,0,'C',0);
    $pdf->Cell(50, 10, utf8_decode('  '.$compra['pago'].' '), 1,0,'C',0);


    $pdf->Ln(30);
    $pdf->SetFont('Arial', 'B', 9);
    $pdf->Cell(10, 10, '#', 1,0,'C',0);
    $pdf->Cell(40, 10,utf8_decode ('PRODUCTO'), 1,0,'C',0);
    $pdf->Cell(50, 10,utf8_decode ('KILOS VENTA '), 1,0,'C',0);
    $pdf->Cell(50, 10,utf8_decode ('PRECIO VENTA'), 1,0,'C',0);
    $pdf->Cell(50, 10,utf8_decode (' TOTAL VENTA'), 1,0,'C',0);
    
    $pdf->Ln(10);
    $pdf->SetFont('Arial', 'I', 8);
    $pdf->Cell(10, 10, utf8_decode($compra['total']), 1,0,'C',0);
    $pdf->Cell(40, 10,utf8_decode($compra['pr'].' ' . $compra['nombre']), 1,0,'C');
    $pdf->Cell(50, 10,utf8_decode($compra['kilos_venta']), 1,0,'C',0);
    $pdf->Cell(50, 10,utf8_decode('$ '.$compra['precio_venta']) , 1,0,'C',0);
    $pdf->Cell(50, 10,utf8_decode('$ '.$compra['total_venta']), 1,0,'C',0);
    $pdf->Ln(20);

    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Cell(200, 4, '-------------------------------------------------------------------------------------------------------------------------------------------------------------------------', 0);
    $pdf->Ln(10);
    $pdf->SetFont('Arial', 'B', 13);
    $pdf->Cell(100, 10, 'TOTAL DE FACTURA   ', 0,0,'L'); 

    $pdf->Cell(100, 10, '$ '. number_format($compra['total_venta'],2).'', 0,0,'R'); #COLOCAR LA VARIABLE DE TOTAL_VENTA
    $pdf->Ln(15);
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Cell(200, 4, '-------------------------------------------------------------------------------------------------------------------------------------------------------------------------', 0);

    $pdf->Ln(20);

    $pdf->SetFillColor(30, 178, 59  );
    $pdf->Cell(200, 10, '',0,0,'L',1);



    $pdf->Ln(100);
}
$pdf->Output('reporte.pdf','D');
