<?php
require('../conexion.php');
require('../../fpdf/fpdf.php');
class PDF extends FPDF
{
function Header()
{
if(strlen($_GET['desde'])>0 and strlen($_GET['hasta'])>0 and strlen($_GET['dato'])>0){
    $dato = $_GET['dato'];
    $desde = $_GET['desde'];
    $hasta = $_GET['hasta'];
    $verDesde = $desde;
    $verHasta = $hasta;
}else{
    $desde = $_GET['desde'];
    $hasta = $_GET['hasta'];
    $dato = 'XXX-XXX-XXX';
    $verDesde = $desde;
    $verHasta = $hasta;
}
    $this->Image('../../img/logo.jpg',10,5,35);
    $this->SetFont('Arial','B',10);
    $this->Cell(85,10,'',0,0,'c');
    $this->Cell(85, 10, 'AGRICOLA MEN (Huertas)',0,0,'C',0);
    $this->Cell(85, 10, 'Martinez de la Torre, Ver '.date('d-m-Y').'', 0,0,'C',0);
    $this->Ln(20);
    $this->Cell(85, 8, '', 0);
    $this->Cell(85, 8, 'REPORTE DE FACTURACION LIMON PERSA ', 0,0,'C',0);
    $this->Cell(85, 8, '', 0);
    $this->Ln(20);
    $this->SetFont('Arial', 'B', 10);
    $this->Cell(6, 8, '', 0);
    $this->Cell(115, 8, 'DE LA FECHA :  '.$verDesde.'  HASTA LA FECHA   '.$verHasta, 0,0,'C',0);
    $this->Ln(15);
}
function Footer()
{
    $this->SetY(-15);
    $this->SetFont('Arial','I',8);
    $this->Cell(0,10,'Pagina '.$this->PageNo().'/{nb}',0,0,'C');
}
}
$pdf = new PDF('l','mm','letter');
#$pdf->SetMargins(1, 5 , 0); 
$pdf->AliasNbPages();
$pdf->AddPage();
    $pdf->SetFont('Arial', 'B', 8);
    $pdf->SetFillColor(232,232,232);
    $pdf->Cell(6, 8, '#', 1,0,'C', 1);
    $pdf->Cell(18, 8, 'Fecha', 1,0,'C', 1);
    $pdf->Cell(28, 8, 'Reporte de Venta', 1,0,'C', 1);
    $pdf->Cell(47, 8, 'Cliente', 1,0,'C', 1);
    $pdf->Cell(20, 8, 'Facturacion', 1,0,'C', 1);
    $pdf->Cell(20, 8, 'Movimiento', 1,0,'C', 1);
    $pdf->Cell(32, 8, 'Pago', 1,0,'C', 1);
    $pdf->Cell(30, 8, 'Producto', 1,0,'C', 1);
    $pdf->Cell(20, 8, 'Kilos Venta', 1,0,'C', 1);
    $pdf->Cell(20, 8, 'Precio Venta', 1,0,'C', 1);
    $pdf->Cell(20, 8, 'Total Venta', 1,0,'C', 1);
$pdf->Ln(8);
$pdf->SetFont('Arial', '', 7);
extract($_GET);



$h2="SELECT count(*) as cuantos from compra_venta_limon c 
 inner join movimiento m on m.idmovimiento = c.idmovimiento
 inner join pago pa on pa.idpago = c.idpago
 inner join variedad v on v.idvariedad = c.idvariedad
 inner join producto pr on pr.idproducto= c.idproducto
  WHERE c.facturacion LIKE '%$dato%' AND c.fecha BETWEEN '$desde' AND '$hasta'";
$r2 = mysqli_query($mysqli,$h2);
while ($row2=mysqli_fetch_assoc($r2)) {
    $a2 = $row2['cuantos']; 
}

$query = "  
SELECT c.fecha,c.cliente, c.idcompra_venta_limon,c.fecha,c.folio2,
c.cajas_venta,c.precio_venta,c.total_venta, c.facturacion, m.movimiento, pa.pago, v.nombre ,
 pr.nombre as pr
 from compra_venta_limon c 
 inner join movimiento m on m.idmovimiento = c.idmovimiento
 inner join pago pa on pa.idpago = c.idpago
 inner join variedad v on v.idvariedad = c.idvariedad
 inner join producto pr on pr.idproducto= c.idproducto
  WHERE c.facturacion LIKE '%$dato%' AND c.fecha BETWEEN '$desde' AND '$hasta' order by c.fecha asc";
  $x=0;
$productos = mysqli_query($mysqli, $query);
while($compra = mysqli_fetch_array($productos)){
    if ( $x <= $a2) {
    $pdf->Cell(6, 8, $x = $x+1, 1,0,'C');
    $pdf->Cell(18, 8,$compra['fecha'], 1,0,'C');
    $pdf->Cell(28, 8, $compra['folio2'], 1,0,'C');
    $pdf->Cell(47, 8,$compra['cliente'], 1,0,'C');
    $pdf->Cell(20, 8,$compra['facturacion'], 1,0,'C');
    $pdf->Cell(20, 8,$compra['movimiento'], 1,0,'C');
    $pdf->Cell(32, 8,$compra['pago'], 1,0,'C');
    $pdf->Cell(30, 8,$compra['pr'].' ' . $compra['nombre'], 1,0,'C');
    $pdf->Cell(20, 8,$compra['cajas_venta'], 1,0,'C');
    $pdf->Cell(20, 8,$compra['precio_venta'], 1,0,'C');
    $pdf->Cell(20, 8, $compra['total_venta'], 1,0,'C');
    $pdf->Ln(8);   
    }   
}
$pdf->Ln(10);
$query2 = "SELECT fecha, facturacion, sum(total_venta)
from compra_venta_limon 
WHERE facturacion LIKE '%$dato'  AND fecha BETWEEN '$desde' AND '$hasta'";
$productos2 = mysqli_query($mysqli, $query2);
while($compra2 = mysqli_fetch_array($productos2)){
    $pdf->Cell(201, 8, '', 0,0,'C');
    $pdf->SetFont('Arial', 'B', 9);
    $pdf->Cell(40, 8,'TOTALES:', 0,0,'C');
    $pdf->SetFillColor(232,232,232);
    $pdf->Cell(20, 8,'$'.$compra2['sum(total_venta)'],1,0,'C',1); 
    $pdf->Ln(8);
}
$pdf->Output('reporte.pdf','D');