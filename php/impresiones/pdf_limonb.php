<?php 
require('../conexion.php');
require('../../fpdf/fpdf.php');


class PDF extends FPDF
{
// Cabecera de página
function Header()
{

    // Logo
    $this->Image('../../img/logo.jpg',10,5,35);
    // Arial bold 15
    $this->SetFont('Arial','B',10);
    // Movernos a la derecha
    $this->Cell(60,10,'',0,0,'c');
    $this->Cell(80, 10, 'AGRICOLA MEN (Huertas)',0,0,'C',0);
    $this->Cell(60, 10, 'Martinez de la Torre, Ver '.date('d-m-Y').'', 0,0,'C',0);
    $this->Ln(20);
    $this->Cell(60, 8, '', 0);
    $this->Cell(80, 8, 'REPORTE DE COMPRA VENTA  ', 0,0,'C',0);
    $this->Cell(60, 8, '', 0);
    $this->Ln(20);
    
    
}

function Footer()
{
    // Posición: a 1,5 cm del final
    $this->SetY(-15);
    // Arial italic 8
    $this->SetFont('Arial','I',8);
    // Número de página
    $this->Cell(0,10,'Pagina '.$this->PageNo().'/{nb}',0,0,'C');
}
}

// Creación del objeto de la clase heredada
$pdf = new PDF('P','mm','letter'); 
$pdf->AliasNbPages();
$pdf->AddPage();
//CONSULTA
extract($_GET);
       require('../conexion.php');
        $sql=("SELECT c.idcompra_venta_limon,c.fecha,h.nombre,h.zona,c.camion,c.marca,c.color,
            c.placas,c.chofer,c.folio1,
            c.cajas_corte,c.precio_corte,c.importe_corte,c.pesaje, 
            c.precio_flete,c.importe_flete,c.egresos,
            c.folio2,c.c_venta,c.merma,c.cajas_venta,c.precio_venta,c.total_venta,c.ingreso_total,c.cliente from compra_venta_limon c inner join huerta h on c.idhuerta=h.idhuerta WHERE c.idcompra_venta_limon = $id ORDER BY fecha");

            $query=mysqli_query($mysqli,$sql);

            $item = 0;

    
    while($compra = mysqli_fetch_array($query)){
    $pdf->SetFillColor(232,232,232);
    $pdf->SetFont('Arial', 'B', 9);
    $pdf->Cell(60, 8, 'Lugar y Fecha de la compra venta:',1,0,'C', 1);
    $pdf->Cell(30, 8,$compra['fecha'] , 0);
    $pdf->Cell(20, 8, 'Huerta:', 0);
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetFont('Arial', '', 10);
    $pdf->Cell(30, 8, $compra['nombre'], 0);
    $pdf->Cell(20, 8, 'Zona:', 0);
    $pdf->SetFont('Arial', '', 10);
    $pdf->Cell(30, 8, $compra['zona'], 0);

    $pdf->ln(10);

     $pdf->Cell(190, 8, '-----------------------------------------------------------------------------------------------------------------------------------------------------------------', 0);

    $pdf->ln(15);
    $pdf->SetFillColor(232,232,232);
     $pdf->Cell(65, 8, '', 0);
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Cell(60, 8, 'Datos del Transporte',1,0,'C',1);
    $pdf->SetFont('Arial', '', 8);
     $pdf->ln(10);


    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Cell(15, 8, 'camion:', 0);
    $pdf->SetFont('Arial', '', 10);
    $pdf->Cell(23, 8,$compra['camion'], 0);
   
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Cell(15, 8, 'Marca:', 0);
    $pdf->SetFont('Arial', '', 10);
    $pdf->Cell(23, 8,$compra['marca'], 0);
    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Cell(15, 8, 'Color:', 0);
    $pdf->SetFont('Arial', '', 10);
    $pdf->Cell(23, 8,$compra['color'], 0);

    $pdf->ln(10);

    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Cell(15, 8, 'Placas:', 0);
    $pdf->SetFont('Arial', '', 10);
    $pdf->Cell(23, 8,$compra['placas'], 0);
   
    $pdf->SetFont('Arial', 'B', 10);
   
    $pdf->Cell(15, 8, 'Chofer:', 0);
    $pdf->SetFont('Arial', '', 10);
    $pdf->Cell(23, 8,$compra['chofer'], 0);
    
$pdf->ln(10);


     $pdf->Cell(190, 8, '-----------------------------------------------------------------------------------------------------------------------------------------------------------------', 0);

     $pdf->ln(15);

  
     
    $pdf->SetFillColor(232,232,232);
    $pdf->Cell(65, 8, '', 0);
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Cell(60, 8, 'Datos Compra Venta',1,0,'C',1);
    $pdf->Cell(45, 8, '', 0);

    $pdf->ln(15);

    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Cell(35, 8, 'CLIENTE', 0);
    $pdf->SetFont('Arial', '', 10);
    $pdf->Cell(30, 8,$compra['cliente'], 0);


    $pdf->ln(10);

    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Cell(20, 8, 'Reporte C.', 0);
    $pdf->SetFont('Arial', '', 10);
    $pdf->Cell(15, 8,$compra['folio1'], 0);
    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Cell(30, 8, 'Cajas corte:', 0);
    $pdf->SetFont('Arial', '', 10);
    $pdf->Cell(15, 8,$compra['cajas_corte'], 0);

    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Cell(30, 8, 'Precio Corte:', 0);
    $pdf->SetFont('Arial', '', 10);
    $pdf->Cell(20, 8,'$'.$compra['precio_corte'], 0);

    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Cell(30, 8, ' Importe Corte:', 0);
    $pdf->SetFont('Arial', '', 10);
    $pdf->Cell(30, 8,'$'.$compra['importe_corte'], 1,0,'C',0);

	$pdf->ln(10);
	$pdf->Cell(130, 8, '', 0);
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Cell(30, 8, ' Gastos Corte:', 0);
    $pdf->SetFont('Arial', '', 10);
    $pdf->Cell(30, 8,'$'.$compra['pesaje'], 1,0,'C',0);

	$pdf->ln(10);

    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Cell(40, 8, '------------------------', 0);
    $pdf->SetFont('Arial', '', 10);
    $pdf->Cell(40, 8,'', 0);

    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Cell(30, 8, 'Precio Flete:', 0);
    $pdf->SetFont('Arial', '', 10);
    $pdf->Cell(20, 8,'$'.$compra['precio_flete'], 0);

    $pdf->SetFont('Arial', 'B', 10);
    $pdf->SetFillColor(232,232,232);
    $pdf->Cell(30, 8, 'Importe Flete:', 0);
    $pdf->SetFont('Arial', '', 10);
    $pdf->Cell(30, 8,'$'.$compra['importe_flete'],1,0,'C', 0);
  $pdf->ln(10);
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->SetFillColor(232,232,232);
    $pdf->Cell(40, 8, '', 0);
    $pdf->SetFont('Arial', '', 10);
    $pdf->Cell(30, 8,'',0,0,'C', 0);

    $pdf->SetFont('Arial', 'B', 10);
    $pdf->SetFillColor(232,232,232);
    $pdf->Cell(30, 8, '', 0);
    $pdf->SetFont('Arial', '', 10);
    $pdf->Cell(30, 8,'',0,0,'C', 0);

    $pdf->SetFont('Arial', 'B', 10);
    $pdf->SetFillColor(232,232,232);
    $pdf->Cell(30, 8, 'EGRESOS:', 0);
    $pdf->SetFont('Arial', '', 10);
    $pdf->Cell(30, 8,'$'.$compra['egresos'],1,0,'C', 1);

   $pdf->ln(10);

    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Cell(20, 8, 'Reporte V.', 0);
    $pdf->SetFont('Arial', '', 10);
    $pdf->Cell(15, 8,$compra['folio2'], 0);

    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Cell(30, 8, 'Cajas Venta:', 0);
    $pdf->SetFont('Arial', '', 10);
    $pdf->Cell(15, 8,$compra['c_venta'], 0);
   
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Cell(30, 8, 'Merma', 0);
    $pdf->SetFont('Arial', '', 10);
    $pdf->Cell(20, 8,$compra['merma'], 0);
  

    $pdf->ln(10);

$pdf->SetFont('Arial', 'B', 10);
    $pdf->Cell(20, 8, '', 0);
    $pdf->SetFont('Arial', '', 10);
    $pdf->Cell(15, 8,'', 0);

  $pdf->SetFont('Arial', 'B', 10);
    $pdf->Cell(30, 8, 'Kilos Venta:', 0);
    $pdf->SetFont('Arial', '', 10);
    $pdf->Cell(15, 8,$compra['cajas_venta'], 0);

    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Cell(30, 8, 'Precio Venta', 0);
    $pdf->SetFont('Arial', '', 10);
    $pdf->Cell(20, 8,'$'.$compra['precio_venta'], 0); 

    $pdf->SetFont('Arial', 'B', 10);
    $pdf->SetFillColor(232,232,232);
    $pdf->Cell(30, 8, 'Total Venta', 0);
    $pdf->SetFont('Arial', '', 10);
    $pdf->Cell(30, 8,'$'.$compra['total_venta'],1,0,'C', 1);

    $pdf->ln(10);

    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Cell(130, 8, '', 0);
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->SetFillColor(232,232,232);
    $pdf->Cell(30, 8, 'INGRESOS', 0);
    $pdf->SetFont('Arial', '', 10);
    $pdf->Cell(30, 8,'$'.$compra['ingreso_total'],1,0,'C', 1);


    $pdf->ln(150);
}

$pdf->Output('reporte_individual_compra_venta_limon.pdf','D');


 ?>

