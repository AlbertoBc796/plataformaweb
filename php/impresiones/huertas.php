<?php
require('../conexion.php');
require('../../fpdf/fpdf.php');
class PDF extends FPDF
{
function Header()
{
    $this->Image('../../img/logo.jpg',10,5,35);
	$this->SetFont('Courier', 'I', 20);
	$this->Cell(45, 10, '', 0);
	$this->Cell(95, 10, 'GRUPO MURRIETA',0,0,'C',0);
	$this->SetFont('Arial', 'I', 9);
	$this->Cell(40, 10, 'Martinez de la Torre, Ver: '.date('d-m-Y').'', 0);
	$this->Ln(18);
	$this->SetFont('Courier', 'I', 15);
	$this->Cell(45, 10, '', 0);
	$this->Cell(95, 10, 'HUERTAS',0,0,'C',0);
	$this->Ln(15);
}
function Footer()
{
    $this->SetY(-15);
    $this->SetFont('Arial','I',8);
    $this->Cell(0,10,'Pagina '.$this->PageNo().'/{nb}',0,0,'C');
}
}
$sql=("SELECT @numero:=@numero+1 as total , idhuerta,date_format(fecha,'%d-%m-%Y') as fecha,clave,nombre,zona,hectareas,producto FROM huerta  , 
	(SELECT @numero:= 0) as total  ");
   $query=mysqli_query($mysqli,$sql);
$pdf = new PDF('P','mm','letter');
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->Ln(10);
$pdf->SetFont('Arial', 'I', 9);
$pdf->Ln(15);
$pdf->SetFillColor(232,232,232);
$pdf->SetFont('Arial', 'B', 9);
$pdf->Cell(10, 8,'#', 1,0,'C',1);
$pdf->Cell(20, 8,'FECHA',1,0,'C',1);
$pdf->Cell(40, 8,'NOMBRE', 1,0,'C',1);
$pdf->Cell(20, 8,'ZONA', 1,0,'C',1);
$pdf->Cell(25, 8,'HA.', 1,0,'C',1);
$pdf->Cell(80, 8,'PRODUCTOS', 1,0,'C',1);
$pdf->Ln(8);
$pdf->SetFont('Arial', 'B', 9);
while($stock = mysqli_fetch_assoc($query)){
    $pdf->Cell(10, 8,$stock['total'],1, 0,'C');
    $pdf->Cell(20, 8,$stock['fecha'],1, 0,'C');
    $pdf->Cell(40, 8,$stock['nombre'], 1, 0,'C');
    $pdf->Cell(20, 8,$stock['zona'],1, 0,'C');
    $pdf->Cell(25, 8,$stock['hectareas'], 1,0,'C');
    $pdf->Cell(80, 8,$stock['producto'], 1,0,'C');
    $pdf->Ln(8);
}
$pdf->Output();