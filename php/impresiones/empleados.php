<?php
require('../conexion.php');
require('../../fpdf/fpdf.php');
setlocale(LC_ALL,"es_ES");
class PDF extends FPDF
{
function Header()
{
    $this->Image('../../img/logo.jpg',10,5,35);
	$this->SetFont('Courier', 'I', 20);
	$this->Cell(90, 10, '', 0);
	$this->Cell(95, 10, 'GRUPO MURRIETA',0,0,'C',0);
    $this->SetFont('Arial', 'I', 9);
    $this->Cell(20, 10, '', 0);
	$this->Cell(40, 10, 'Martinez de la Torre, Ver: '.date('d-m-Y').'', 0);
	$this->Ln(18);
	$this->SetFont('Courier', 'I', 15);
	$this->Cell(90, 10, '', 0);
	$this->Cell(95, 10, 'EMPLEADOS',0,0,'C',0);
	$this->Ln(8);
}
function Footer()
{
    $this->SetY(-15);
    $this->SetFont('Arial','I',8);
    $this->Cell(0,10,'Pagina '.$this->PageNo().'/{nb}',0,0,'C');
}
}


$sql=("SELECT @numero:=@numero+1 as total , em.idempleado, date_format(em.fecha,'%d/%m/%Y')as fecha,p.nombre AS per ,p.apellidop,p.apellidom,
c.cargo,em.huertas AS huerta ,em.salario from empleado em
inner join persona p on p.idpersona = em.idpersona
inner join cargo c on c.idcargo = em.idcargo, 
	(SELECT @numero:= 0) as total order by total");
   $query=mysqli_query($mysqli,$sql);

$pdf = new PDF('l','mm','a4');
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->Ln(10);
$pdf->SetFont('Arial', 'I', 9);
$pdf->SetFillColor(232,232,232);
$pdf->SetFont('Arial', 'B', 9);
$pdf->Cell(10, 8,'#', 1,0,'C',1);
$pdf->Cell(20, 8,'FECHA',1,0,'C',1);
$pdf->Cell(90, 8,'NOMBRE', 1,0,'C',1);
$pdf->Cell(60, 8,'CARGO', 1,0,'C',1);
$pdf->Cell(80, 8,'HUERTA', 1,0,'C',1);
$pdf->Cell(15, 8,'SALARIO', 1,0,'C',1);
$pdf->Ln(8);

$pdf->SetFont('Arial', 'B', 9);
while($stock = mysqli_fetch_assoc($query)){
    $pdf->Cell(10, 8,$stock['total'],1, 0,'C');
    $pdf->Cell(20, 8,$stock['fecha'],1, 0,'C');
    $pdf->Cell(90, 8,utf8_decode($stock['per'].' '.$stock['apellidop'].' '.$stock['apellidom']), 1, 0,'C');
    $pdf->Cell(60, 8,$stock['cargo'],1, 0,'C');
    $pdf->Cell(80, 8,$stock['huerta'], 1,0,'C');
    $pdf->Cell(15, 8,$stock['salario'], 1,0,'C');
    $pdf->Ln(8);
}



$pdf->Output();