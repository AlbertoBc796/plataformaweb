<?php
require('../conexion.php');
require('../../fpdf/fpdf.php');

class PDF extends FPDF
{
// Cabecera de página
function Header()
{
    if (strlen($_GET['desde']) == 0 and strlen($_GET['hasta']) == 0) {
        # code...
        echo '<script>alert("Introduzca los valores de busqueda del reporte\nFecha,huerta,producto...");</script> ';
        echo "<script>window.close()</script>";
    }else {
        $desde = $_GET['desde'];
        $hasta = $_GET['hasta'];
        $verDesde = $desde;
        $verHasta = $hasta;
    }

    if( strlen($_GET['dato'])>0){
        $dato = $_GET['dato'];
    }else{
        $dato = 'HUERTAS';
    }

    if (strlen($_GET['dato2'])>0) {
        $dato2 = $_GET['dato2'];
        $dato56 = $_GET['dato56'];
    }else{
        $dato2 = 'XXX-XXX-XXX';
        $dato56 = 'XXX-XXX-XXX';
    }
    // Logo
    $this->Image('../../img/logo.jpg',10,5,35);

    // Arial bold 15
    $this->SetFont('Arial','B',15);
    // Movernos a la derecha
    $this->Cell(110,10,'',0,0,'c');
    $this->Cell(130, 10, 'AGRICOLA MEN (Huertas)',0,0,'C',0);
    $this->Cell(100, 10, 'Martinez de la Torre, Ver '.date('d-m-Y').'', 0,0,'C',0);
    $this->Ln(20);
    $this->Cell(110, 8, '', 0);
    $this->Cell(130, 8, 'REPORTE DE COSECHA - VENTA DE '.$dato, 0,0,'C',0);
    $this->Cell(100, 8, '', 0);
    $this->Ln(15);
    $this->SetFont('Arial', 'B', 10);
    $this->Cell(10, 8, '', 0);
    $this->Cell(100, 8, 'DE LA FECHA :  '.date('d-m-Y',strtotime($verDesde)).'  HASTA LA FECHA   '.date('d-m-Y',strtotime($verHasta)), 0); #colocar las variables desde y hasta
    $this->Cell(120, 8, '', 0);
    $this->Cell(40, 8, 'PRODUCTO: '.$dato2. ' ' .$dato56, 0,0,'C');
    $this->Cell(45, 8, '', 0);
    $this->Ln(20);
}

// Pie de página
function Footer()
{
    // Posición: a 1,5 cm del final
    $this->SetY(-15);
    // Arial italic 8
    $this->SetFont('Arial','I',8);
    // Número de página
    $this->Cell(0,10,'Pagina '.$this->PageNo().'/{nb}',0,0,'C');
}
}

// Creación del objeto de la clase heredada
$pdf = new PDF('l','mm','legal');

$pdf->AliasNbPages();
$pdf->AddPage();
    $pdf->SetFont('Arial', 'B', 8);
    $pdf->SetFillColor(232,232,232);
    $pdf->Cell(6, 8, '#', 1,0,'C', 1);
    $pdf->Cell(16, 8, 'Fecha', 1,0,'C', 1);
    $pdf->Cell(22, 8, 'Huerta', 1,0,'C', 1);
    $pdf->Cell(32, 8, 'Producto', 1,0,'C', 1);
    $pdf->Cell(14, 8, 'Folio C.', 1,0,'C', 1);
    $pdf->Cell(17, 8, 'Kg corte', 1,0,'C', 1);
    $pdf->Cell(17, 8, '$ Corte', 1,0,'C', 1);
    $pdf->Cell(17, 8, 'Total Corte', 1,0,'C', 1);
    $pdf->Cell(17, 8, 'Gasto Extra', 1,0,'C', 1);
    $pdf->Cell(17, 8, '$ Flete', 1,0,'C', 1);
    $pdf->Cell(17, 8, 'Total Flete', 1,0,'C', 1);
    $pdf->Cell(17, 8, 'Egresos', 1,0,'C', 1);
    $pdf->Cell(14, 8, 'Folio V.', 1,0,'C', 1);
    $pdf->Cell(17, 8, 'Kg Venta', 1,0,'C', 1);
    $pdf->Cell(17, 8, '$ Venta', 1,0,'C', 1);
    $pdf->Cell(20, 8, 'Ingresos', 1,0,'C', 1);
    $pdf->Cell(20, 8, 'Utilidad', 1,0,'C', 1);
    $pdf->Cell(46, 8, 'Cliente', 1,0,'C', 1);
$pdf->Ln(8);
$pdf->SetFont('Arial', '', 7);
extract($_GET);

$h2="SELECT count(*) as cuantos from compra_venta c 
inner join huerta h on c.idhuerta=h.idhuerta
inner join producto p on p.idproducto = c.idproducto
inner join variedad v on v.idvariedad = c.idvariedad
where h.nombre LIKE '%$dato%'  AND p.nombre LIKE '%$dato2%' AND v.nombre LIKE '%$dato56%' AND c.fecha BETWEEN '$desde' AND '$hasta'";
$r2 = mysqli_query($mysqli,$h2);
while ($row2=mysqli_fetch_assoc($r2)) {
    $a2 = $row2['cuantos']; 
}


$query = "SELECT c.fecha,h.nombre AS HUERTITA,p.nombre,p.idproducto ,c.folio1,c.camion,c.marca,c.color,
c.placas,c.chofer,c.kilos_corte,c.precio_corte,c.importe_corte,c.pesaje,p.nombre as producto,v.nombre as variedad,
c.precio_flete,c.importe_flete,c.egresos,
c.kilos_venta,c.precio_venta,c.total_venta,c.ingreso_total,c.folio2,c.cliente,v.nombre 
from  compra_venta c 
    inner join huerta h on c.idhuerta=h.idhuerta
    inner join producto p on p.idproducto = c.idproducto
    inner join variedad v on v.idvariedad = c.idvariedad
    WHERE h.nombre LIKE '%$dato%' AND p.nombre LIKE '%$dato2%' AND v.nombre LIKE '%$dato56%' AND c.fecha BETWEEN '$desde' AND '$hasta' ORDER BY c.fecha";

$x=0;
$crt=0;
$vta=0;

$productos = mysqli_query($mysqli, $query);
while($compra = mysqli_fetch_array($productos)){

    if ( $x <= $a2) {
    $pdf->Cell(6, 8,$x = $x+1,1, 0,'C');
    $pdf->Cell(16, 8,date('d-m-Y',strtotime($compra['fecha'])), 1,0,'C');
    $pdf->Cell(22, 8, $compra['HUERTITA'], 1,0,'C');
    $pdf->Cell(32, 8, $compra['producto'].' '.$compra['variedad'], 1,0,'C');
    $pdf->Cell(14, 8, $compra['folio1'], 1,0,'C');
    $pdf->Cell(17, 8,$compra['kilos_corte'], 1,0,'C');
    if ($compra['precio_corte'] > 0) {
        $crt = $crt+1;
    }
    $pdf->Cell(17, 8,'$'.$compra['precio_corte'], 1,0,'C');
    $pdf->Cell(17, 8,'$'.number_format($compra['importe_corte'],2), 1,0,'C');
    $pdf->Cell(17, 8,'$'.$compra['pesaje'], 1,0,'C');
    $pdf->Cell(17, 8,'$'.$compra['precio_flete'], 1,0,'C');
    $pdf->Cell(17, 8,'$'.$compra['importe_flete'],1,0,'C');
    $pdf->SetFillColor(255,110,110);#Rojo
    $pdf->Cell(17, 8,'$'.number_format($compra['egresos'],2),1,0,'C',true);
    $pdf->Cell(14, 8, $compra['folio2'], 1,0,'C');
    $pdf->Cell(17, 8,$compra['kilos_venta'], 1,0,'C');
    if ($compra['precio_venta'] > 0) {
        $vta = $vta+1;
    }
    $pdf->Cell(17, 8,'$'.$compra['precio_venta'], 1,0,'C');
    $pdf->SetFillColor(110,255,110);#Verde
    $pdf->Cell(20, 8,'$'.number_format($compra['total_venta'],2),1,0,'C',true);
    $pdf->Cell(20, 8,'$'.number_format($compra['ingreso_total'],2),1,0,'C');
    $pdf->Cell(46, 8,$compra['cliente'],1,0,'C');
    $pdf->Ln(8);
        
    } 
}
    if ($crt == 0) {
        $crt=1;
    }
    if ($vta == 0) {
        $vta=1;
    }
    if ($x == 0) {
        $x=1;
    }
    $pdf->Ln(8);

//CONSULTA DE TOTAL 
$query = "SELECT h.nombre ,p.nombre,p.idproducto,v.nombre, sum(kilos_corte),sum(precio_corte),sum(importe_corte),sum(pesaje),
sum(precio_flete),sum(importe_flete), sum(egresos),sum(kilos_venta),sum(precio_venta),sum(total_venta), sum(ingreso_total) from compra_venta c 
inner join huerta h on c.idhuerta=h.idhuerta 
inner join producto p on p.idproducto = c.idproducto
inner join variedad v on v.idvariedad = c.idvariedad 
WHERE h.nombre LIKE '%$dato%' AND  p.nombre LIKE '%$dato2' AND v.nombre LIKE '%$dato56%' AND c.fecha BETWEEN '$desde' AND '$hasta'";

$productos = mysqli_query($mysqli, $query);
while($compra = mysqli_fetch_array($productos)){

    $pdf->Cell(73, 8, '', 0,0,'C');
    $pdf->SetFont('Arial', 'B', 7);
    $pdf->Cell(17, 8,'TOTALES:', 0,0,'C');
    $pdf->SetFillColor(232,232,232);
    $pdf->Cell(17, 8,$compra['sum(kilos_corte)'].' Kg', 1,0,'C',1);
    $pdf->Cell(17, 8,'$'.number_format($compra['sum(precio_corte)']/$crt,2), 1,0,'C',1);
    $pdf->Cell(17, 8,'$'.number_format($compra['sum(importe_corte)'],2), 1,0,'C',1);
    $pdf->Cell(17, 8,'$'.$compra['sum(pesaje)'], 1,0,'C',1);
    $pdf->Cell(17, 8,'$'.number_format($compra['sum(precio_flete)']/$x,2), 1,0,'C',1);
    $pdf->Cell(17, 8,'$'.$compra['sum(importe_flete)'],1,0,'C',1);
    $pdf->Cell(17, 8,'$'.number_format($compra['sum(egresos)'],2),1,0,'C',1);
    $pdf->Cell(14, 8,'',0,0,'C',0);
    $pdf->Cell(17, 8,$compra['sum(kilos_venta)'], 1,0,'C',1);
    $pdf->Cell(17, 8,'$'.number_format($compra['sum(precio_venta)']/$vta,2), 1,0,'C',1);
    $pdf->Cell(20, 8,'$'.number_format($compra['sum(total_venta)'],2),1,0,'C',1);
    $pdf->Cell(20, 8,'$'.number_format($compra['sum(ingreso_total)'],2),1,0,'C',1);
    $pdf->Cell(24, 8,'', 0,0,'C',0);
}
    $pdf->Ln(8);
    $pdf->Cell(107, 8, '', 0,0,'C');
    $pdf->SetFont('Arial', 'B', 7);
    $pdf->Cell(17, 8,'$ Promedio', 0,0,'C');
    $pdf->Cell(34, 8, '', 0,0,'C');
    $pdf->Cell(17, 8,'$ Promedio', 0,0,'C');
    $pdf->Cell(65, 8, '', 0,0,'C');
    $pdf->Cell(17, 8,'$ Promedio', 0,0,'C');
    $pdf->Ln(8);

    //Resumen de huertas
    $query3 = "SELECT c.fecha, c.idhuerta, h.nombre as huerta, sum(kilos_corte) as kilosC,
    round(avg(precio_corte),2) as pcorte, sum(egresos) as egreso, sum(kilos_venta) as kilosV, 
    round(avg(precio_venta),2) as pventa, sum(total_venta) as ingreso, sum(ingreso_total) as utilidad,
    c.cliente, c.idproducto, c.idvariedad, p.nombre as fruta, v.nombre as variedad
    from compra_venta c
    inner join huerta h on c.idhuerta=h.idhuerta
    inner join producto p on p.idproducto = c.idproducto
    inner join variedad v on v.idvariedad = c.idvariedad
    WHERE  h.nombre LIKE '%$dato%' AND  p.nombre LIKE '%$dato2' AND v.nombre LIKE '%$dato56%' AND c.fecha BETWEEN '$desde' AND '$hasta'
    GROUP BY h.nombre,p.nombre,v.nombre ORDER BY p.nombre,v.nombre,h.nombre";
    
    $resumen = mysqli_query($mysqli, $query3);
    $variedadAct='';
    $variedadAnt='';
    $count=0;
    $totalCorte=0;
    $totalVenta=0;
    $ingreso=0;
    $egreso=0;
    $utilidad=0;
    $registros =mysqli_num_rows($resumen);
    while($compra3 = mysqli_fetch_array($resumen)){
        $variedadAct = $compra3['variedad'];
        if ($variedadAct != $variedadAnt) {
            if ($count > 0) {
                $pdf->Cell(35, 10,'', 0);
                $pdf->Cell(55, 10, '', 0);
                $pdf->Cell(35, 10, number_format($totalCorte), 1,0,'C', 1);
                $pdf->Cell(35, 10, '', 0);
                $pdf->Cell(35, 10, number_format($totalVenta), 1,0,'C', 1);
                $pdf->Cell(35, 10, '', 0);
                $pdf->Cell(35, 10, '$'.number_format($ingreso,2), 1,0,'C', 1);
                $pdf->Cell(35, 10, '$'.number_format($egreso,2), 1,0,'C', 1);
                $pdf->Cell(35, 10, '$'.number_format($utilidad,2), 1,0,'C', 1);
            }
            $pdf->AddPage();
            $pdf->SetFillColor(232,232,232);
            $pdf->SetFont('Arial', 'B', 14);
            $pdf->Cell(35, 10, 'Huerta', 1,0,'C', 1);
            $pdf->Cell(55, 10, 'Producto', 1,0,'C', 1);
            if ($compra3['fruta'] == 'LIMON') {
                $pdf->Cell(35, 10, 'Cajas', 1,0,'C', 1);
            }else{
                $pdf->Cell(35, 10, 'Kg. corte', 1,0,'C', 1);
            }
            $pdf->Cell(35, 10, '$ Corte', 1,0,'C', 1);
            $pdf->Cell(35, 10, 'Kg. venta', 1,0,'C', 1);
            $pdf->Cell(35, 10, '$ Venta', 1,0,'C', 1);
            $pdf->Cell(35, 10, 'Ingresos', 1,0,'C', 1);
            $pdf->Cell(35, 10, 'Egresos', 1,0,'C', 1);
            $pdf->Cell(35, 10, 'Utilidad', 1,0,'C', 1);
            $pdf->Ln(10);
            $totalCorte=0;
            $totalVenta=0;
            $ingreso=0;
            $egreso=0;
            $utilidad=0;
        }
        $pdf->SetFont('Arial', 'B', 12);
        $pdf->Cell(35, 10,$compra3['huerta'], 1,0,'C');
        $pdf->Cell(55, 10,$compra3['fruta'].' '.$compra3['variedad'], 1,0,'C');
        $pdf->Cell(35, 10,$compra3['kilosC'], 1,0,'C');
        $pdf->Cell(35, 10,$compra3['pcorte'], 1,0,'C');
        $pdf->Cell(35, 10,$compra3['kilosV'], 1,0,'C');
        $pdf->Cell(35, 10,$compra3['pventa'], 1,0,'C');
        $pdf->Cell(35, 10,'$'.number_format($compra3['ingreso'],2), 1,0,'C');
        $pdf->Cell(35, 10,'$'.number_format($compra3['egreso'],2), 1,0,'C');
        $pdf->Cell(35, 10,'$'.number_format($compra3['utilidad'],2), 1,0,'C');
        $pdf->Ln(10);
        $variedadAnt = $variedadAct;
        $totalCorte += $compra3['kilosC'];
        $totalVenta += $compra3['kilosV'];
        $ingreso += $compra3['ingreso'];
        $egreso += $compra3['egreso'];
        $utilidad += $compra3['utilidad'];
        $count++;
        if ($count >= $registros) {
            $pdf->Cell(35, 10,'', 0);
            $pdf->Cell(55, 10, '', 0);
            $pdf->Cell(35, 10, number_format($totalCorte), 1,0,'C', 1);
            $pdf->Cell(35, 10, '', 0);
            $pdf->Cell(35, 10, number_format($totalVenta), 1,0,'C', 1);
            $pdf->Cell(35, 10, '', 0);
            $pdf->Cell(35, 10, '$'.number_format($ingreso,2), 1,0,'C', 1);
            $pdf->Cell(35, 10, '$'.number_format($egreso,2), 1,0,'C', 1);
            $pdf->Cell(35, 10, '$'.number_format($utilidad,2), 1,0,'C', 1);
        }
    }
    $pdf->Output('reporte.pdf','D');
