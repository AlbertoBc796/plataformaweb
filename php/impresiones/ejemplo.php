<?php
require('../conexion.php');
require('../../fpdf/fpdf.php');

class PDF extends FPDF
{
// Cabecera de página
function Header()
{

    // Logo
    $this->Image('../../img/logo.jpg',10,8,33);
    // Arial bold 15
    $this->SetFont('Arial','B',15);
    // Movernos a la derecha
    $this->Cell(80);
    // Título
    $this->Cell(30,10,'Title',1,0,'C');
    // Salto de línea
    $this->Ln(20);
}




// Pie de página
function Footer()
{
    // Posición: a 1,5 cm del final
    $this->SetY(-15);
    // Arial italic 8
    $this->SetFont('Arial','I',8);
    // Número de página
    $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
}
}



if(strlen($_GET['desde'])>0 and strlen($_GET['hasta'])>0 and strlen($_GET['dato'])>0){
    $dato = $_GET['dato'];
    $desde = $_GET['desde'];
    $hasta = $_GET['hasta'];
    $verDesde = date('d/m/Y', strtotime($desde));
    $verHasta = date('d/m/Y', strtotime($hasta));
}else{

    $desde = $_GET['desde'];
    $hasta = $_GET['hasta'];
    $dato = 'XXX-XXX-XXX';
    $verDesde = date('d/m/Y', strtotime($desde));
    $verHasta = date('d/m/Y', strtotime($hasta));
}

$pdf = new FPDF('l','mm','legal');
$pdf->SetMargins(1, 5 , 0);
$pdf->AliasNbPages(); 
$pdf->AddPage();


 $pdf->SetFont('Arial', 'B', 8);
    $pdf->SetFillColor(232,232,232);

    $pdf->Cell(6, 8, 'Fol', 1,0,'C', 1);
    $pdf->Cell(15, 8, 'Fecha', 1,0,'C', 1);
    $pdf->Cell(18, 8, 'Huerta', 1,0,'C', 1);
   

    $pdf->Cell(18, 8, 'Camion', 1,0,'C', 1);
    $pdf->Cell(18, 8, 'Marca', 1,0,'C', 1);
    $pdf->Cell(18, 8, 'Color', 1,0,'C', 1);
    $pdf->Cell(18, 8, 'Placas', 1,0,'C', 1);
    $pdf->Cell(40, 8, 'Chofer', 1,0,'C', 1);

    $pdf->Cell(14, 8, 'R. Corte', 1,0,'C', 1);

    $pdf->Cell(15, 8, 'K. corte', 1,0,'C', 1);
    $pdf->Cell(15, 8, 'P. Corte', 1,0,'C', 1);
    $pdf->Cell(15, 8, 'Imp. Corte', 1,0,'C', 1);

    $pdf->Cell(15, 8, 'P. Flete', 1,0,'C', 1);
    $pdf->Cell(15, 8, 'Imp. Flete', 1,0,'C', 1);
    $pdf->Cell(15, 8, 'Egresos', 1,0,'C', 1);


    $pdf->Cell(14, 8, 'R. Venta', 1,0,'C', 1);
    $pdf->Cell(15, 8, 'K. Venta', 1,0,'C', 1);
    $pdf->Cell(15, 8, 'P. Venta', 1,0,'C', 1);
    $pdf->Cell(15, 8, 'T. Venta', 1,0,'C', 1);

    $pdf->Cell(15, 8, 'Ingresos', 1,0,'C', 1);

    $pdf->Cell(24, 8, 'Cliente', 1,0,'C', 1);


$pdf->Ln(8);


$pdf->SetFont('Arial', '', 7);

extract($_GET);



//CONSULTA
//
// EL @numero:=@numero+1 

$query = "  SELECT @numero:=@numero+1 as total ,c.fecha,h.nombre,c.folio1,c.camion,c.marca,c.color,c.placas,c.chofer,c.kilos_corte,c.precio_corte,c.importe_corte, c.precio_flete,c.importe_flete,c.egresos,c.kilos_venta,c.precio_venta,c.total_venta,c.ingreso_total,c.folio2,c.cliente from  compra_venta c inner join huerta h on c.idhuerta=h.idhuerta , (SELECT @numero:= 0) AS total WHERE h.nombre LIKE '%$dato%' AND c.fecha BETWEEN '$desde' AND '$hasta'";


$productos = mysqli_query($mysqli, $query);

while($compra = mysqli_fetch_array($productos)){
    
    $pdf->Cell(6, 8, $compra['total'], 1,0,'C');
    $pdf->Cell(15, 8, date('d/m/Y', strtotime($compra['fecha'])), 1,0,'C');
    $pdf->Cell(18, 8, $compra['nombre'], 1,0,'C');
    $pdf->Cell(18, 8,$compra['camion'], 1,0,'C');
    $pdf->Cell(18, 8,$compra['marca'], 1,0,'C');
    $pdf->Cell(18, 8,$compra['color'], 1,0,'C');
    $pdf->Cell(18, 8,$compra['placas'], 1,0,'C');
    $pdf->Cell(40, 8,$compra['chofer'], 1,0,'C');
    $pdf->Cell(14, 8, $compra['folio1'], 1,0,'C');

    $pdf->Cell(15, 8,$compra['kilos_corte'], 1,0,'C');
    $pdf->Cell(15, 8,'$'.$compra['precio_corte'], 1,0,'C');
    $pdf->Cell(15, 8,'$'.$compra['importe_corte'], 1,0,'C');
    $pdf->Cell(15, 8,'$'.$compra['precio_flete'], 1,0,'C');
    $pdf->Cell(15, 8,'$'.$compra['importe_flete'],1,0,'C');
    $pdf->Cell(15, 8,'$'.$compra['egresos'],1,0,'C');


    $pdf->Cell(14, 8, $compra['folio2'], 1,0,'C');
    $pdf->Cell(15, 8,$compra['kilos_venta'], 1,0,'C');
    $pdf->Cell(15, 8,'$'.$compra['precio_venta'], 1,0,'C');
    $pdf->Cell(15, 8,'$'.$compra['total_venta'],1,0,'C');

    $pdf->Cell(15, 8,'$'.$compra['ingreso_total'],1,0,'C');

    $pdf->Cell(24, 8,$compra['cliente'],1,0,'C');
    
    $pdf->Ln(8);
    
}

    $pdf->SetFillColor(232,232,232);
    $pdf->Cell(18, 8, '', 0,0,'C', 0);
    $pdf->Cell(18, 8, '', 0,0,'C', 0);
    $pdf->Cell(18, 8, '', 0,0,'C', 0);
    $pdf->Cell(18, 8, '', 0,0,'C', 0);
    $pdf->Cell(18, 8, '', 0,0,'C', 0);
    $pdf->Cell(18, 8, '', 0,0,'C', 0);

    $pdf->Cell(40, 8, '', 0,0,'C', 0);

    $pdf->Cell(14, 8, '', 0,0,'C', 0);
    $pdf->Cell(15, 8, '', 0,0,'C', 0);
    $pdf->Cell(15, 8, '', 0,0,'C', 0);
    $pdf->Cell(15, 8, '', 0,0,'C', 0);
    $pdf->Cell(15, 8, '', 0,0,'C', 0);
    $pdf->Cell(15, 8, '', 0,0,'C', 0);
    $pdf->Cell(15, 8, '', 0,0,'C', 0);

    $pdf->Cell(14, 8, '', 0,0,'C', 0);

    $pdf->Cell(15, 8, '', 0,0,'C', 0);
    $pdf->Cell(15, 8, '', 0,0,'C', 0);

    $pdf->Cell(15, 8, '', 0,0,'C', 0);
    $pdf->Cell(15, 8, '', 0,0,'C', 0);
    $pdf->Cell(24, 8,'', 0,0,'C',0);

      $pdf->Ln(8);
    $pdf->SetFont('Arial', '', 7);


//CONSULTA DE TOTAL 
$query = "SELECT h.nombre, sum(kilos_corte),sum(precio_corte),sum(importe_corte),sum(precio_flete),sum(importe_flete), 
sum(egresos),sum(kilos_venta),sum(precio_venta),sum(total_venta), sum(ingreso_total) from compra_venta c inner join huerta h on c.idhuerta=h.idhuerta WHERE h.nombre LIKE '%$dato%' AND c.fecha BETWEEN '$desde' AND '$hasta'";


$productos = mysqli_query($mysqli, $query);

while($compra = mysqli_fetch_array($productos)){

    $pdf->Cell(18, 8, '', 0,0,'C');
    $pdf->Cell(18, 8, '', 0,0,'C');
    $pdf->Cell(18, 8,'', 0,0,'C');
    $pdf->Cell(18, 8,'', 0,0,'C');
    $pdf->Cell(18, 8,'', 0,0,'C');
    $pdf->Cell(18, 8,'', 0,0,'C');

    $pdf->SetFont('Arial', 'B', 7);
    $pdf->Cell(40, 8,'TOTALES:', 0,0,'C');

    $pdf->SetFillColor(232,232,232);
    $pdf->Cell(14, 8,'', 0,0,'C');

    
    $pdf->Cell(15, 8,$compra['sum(kilos_corte)'], 1,0,'C',1);
    $pdf->Cell(15, 8,'$'.$compra['sum(precio_corte)'], 1,0,'C',1);
    $pdf->Cell(15, 8,'$'.$compra['sum(importe_corte)'], 1,0,'C',1);

    $pdf->Cell(15, 8,'$'.$compra['sum(precio_flete)'], 1,0,'C',1);
    $pdf->Cell(15, 8,'$'.$compra['sum(importe_flete)'],1,0,'C',1);
    $pdf->Cell(15, 8,'$'.$compra['sum(egresos)'],1,0,'C',1);

    $pdf->Cell(14, 8,'',0,0,'C',0);

    $pdf->Cell(15, 8,$compra['sum(kilos_venta)'], 1,0,'C',1);
    $pdf->Cell(15, 8,'$'.$compra['sum(precio_venta)'], 1,0,'C',1);
    $pdf->Cell(15, 8,'$'.$compra['sum(total_venta)'],1,0,'C',1);

    $pdf->Cell(15, 8,'$'.$compra['sum(ingreso_total)'],1,0,'C',1);

    $pdf->Cell(24, 8,'', 0,0,'C',0);





    $pdf->Ln(8);
}


$pdf->Output('reporte2.pdf','D');


?>