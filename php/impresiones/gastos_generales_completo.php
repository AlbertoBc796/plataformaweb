<?php
setlocale(LC_TIME,"es_ES");
require('../conexion.php');
require('../../fpdf/fpdf.php');

class PDF extends FPDF
{
// Cabecera de página
    function Header()
    {
        if (strlen($_GET['desde']) == 0 and strlen($_GET['hasta']) == 0) {
            # code...
            echo '<script>alert("Introduzca los valores de busqueda del reporte\nFecha,huerta,producto...");</script> ';
            echo "<script>window.close()</script>";
        }else{
            $folio = $_GET['dato2'];
            $busqueda = $_GET['dato56'];
            $desde = $_GET['desde'];
            $hasta = $_GET['hasta'];
            $verDesde = $desde;
            $verHasta = $hasta;
        }
        // Logo
        $this->Image('../../img/logo.jpg',10,5,35);
        // Arial bold 15
        $this->SetFont('Arial','B',10);
        // Movernos a la derecha
        $this->Cell(110,10,'',0,0,'c');
        $this->Cell(110, 10, 'AGRICOLA MEN (Compras)',0,0,'C',0);
        $this->Cell(85, 10, 'Martinez de la Torre, Ver '.date('d-m-Y').'', 0,0,'C',0);
        $this->Ln(20);
        $this->Cell(110, 8, '', 0);
        $this->Cell(110, 8, 'REPORTE DE GASTOS ', 0,0,'C',0);
        if ($folio != 0) {
            $this->Ln(8);
            $this->Cell(110, 8, '', 0);
            $this->Cell(110, 8, 'FACTURA: '.$folio.' '.$busqueda, 0,0,'C',0);
        }else{
            $this->Ln(8);
            $this->Cell(110, 8, '', 0);
            $this->Cell(110, 8,$busqueda, 0,0,'C',0);
        }
        $this->Cell(85, 8, '', 0);
        $this->Ln(10);
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(6, 8, '', 0);
        $this->Cell(115, 8, 'DE : '.$verDesde.' HASTA '.$verHasta, 0,0,'C',0); #colocar las variables desde y hasta
        $this->Ln(15);
    }

    // Pie de página
    function Footer()
    {
        // Posición: a 1,5 cm del final
        $this->SetY(-15);
        // Arial italic 8
        $this->SetFont('Arial','I',8);
        // Número de página
        $this->Cell(0,10,'Pagina '.$this->PageNo().'/{nb}',0,0,'C');
    }
}
// Creación del objeto de la clase heredada
    $pdf = new PDF('l','mm','legal');
    #$pdf->SetMargins(1, 5 , 0); 
    $pdf->AliasNbPages();
    $pdf->AddPage();
    $pdf->SetFont('Arial', 'B', 8);
    $pdf->SetFillColor(232,232,232);
    $pdf->Cell(6, 8, '#', 1,0,'C', 1);
    $pdf->Cell(18, 8, 'Fecha', 1,0,'C', 1);
    $pdf->Cell(20, 8, 'Factura', 1,0,'C', 1);
    $pdf->Cell(40, 8, 'Pago', 1,0,'C', 1);
    $pdf->Cell(48, 8, 'Proveedor', 1,0,'C', 1);
    $pdf->Cell(70, 8, 'Concepto', 1,0,'C', 1);
    $pdf->Cell(25, 8, 'Rubro', 1,0,'C', 1);
    $pdf->Cell(20, 8, 'Cantidad', 1,0,'C', 1);
    $pdf->Cell(25, 8, 'Gasto', 1,0,'C', 1);
    $pdf->Cell(18, 8, 'Segmento', 1,0,'C', 1);
    $pdf->Cell(35, 8, 'Huerta', 1,0,'C', 1);
    $pdf->Ln(8);
    $pdf->SetFont('Arial', '', 7);
    extract($_GET);
    if (strlen($_GET['dato56']) == 0) {
        $ext='';
    }else{
        $ext = "AND (t.clave LIKE '%$dato56%' or h.nombre LIKE '%$dato56%')";
    }
$h2="SELECT count(g.idgasto) as cuantos, date_format(g.fecha , '%d-%m-%Y') as fecha, m.movimiento, 
    g.semana, pa.pago, g.idmovimiento, g.idfactura,g.proveedor,g.cantidad,g.precio, g.concepto,
    g.idtransporte,t.clave,h.nombre as huerta,g.idhuerta 
    FROM gastos g
    inner join movimiento m on m.idmovimiento = g.idmovimiento
    left join huerta h on h.idhuerta = g.idhuerta
    left join transporte t on t.idtransporte = g.idtransporte
    inner join pago pa on pa.idpago = g.idpago
    WHERE g.fecha BETWEEN '$desde' AND '$hasta'
    AND g.idfactura LIKE '%$dato2%'
    ".$ext;
    $r2 = mysqli_query($mysqli,$h2);
    while ($row2=mysqli_fetch_assoc($r2)) {
        $a2 = $row2['cuantos']; 
    }

$query = "SELECT g.idgasto, date_format(g.fecha , '%d-%m-%Y') as fecha, m.movimiento, 
    pa.pago, g.idmovimiento, g.idfactura,g.proveedor,g.cantidad,g.precio,g.semana,g.detalle,
    g.idtransporte,t.clave,h.nombre as huerta,g.idhuerta, g.concepto,
    CASE
        WHEN g.idconcepto  = 1 THEN 'MAQUINARIA'
        WHEN g.idconcepto  = 2 THEN 'HUERTA'
        WHEN g.idconcepto  = 3 THEN 'GSTO. IND. ADMIN'
        WHEN g.idconcepto  = 4 THEN 'GSTO. IND. OPER'
        WHEN g.idconcepto  = 5 THEN 'ALMACEN'
        WHEN g.idconcepto  = 6 THEN 'BIOFABRICA'
    END as rubro
    FROM gastos g
    inner join movimiento m on m.idmovimiento = g.idmovimiento
    left join huerta h on h.idhuerta = g.idhuerta
    left join transporte t on t.idtransporte = g.idtransporte
    inner join pago pa on pa.idpago = g.idpago
    WHERE g.fecha BETWEEN '$desde' AND '$hasta'
    AND g.idfactura LIKE '%$dato2%'".$ext."order by fecha asc";
$x=0;
$productos = mysqli_query($mysqli, $query);
while($compra = mysqli_fetch_array($productos)){

    if ($x <= $a2) {
    $pdf->Cell(6, 8, $x = $x+1, 1,0,'C');
    $pdf->Cell(18, 8,$compra['fecha'], 1,0,'C');
    $pdf->Cell(20, 8, $compra['idfactura'], 1,0,'C');
    $pdf->Cell(40, 8, $compra['movimiento'].' '.$compra['pago'], 1,0,'C');
    $pdf->Cell(48, 8,$compra['proveedor'], 1,0,'C');
    $pdf->Cell(70, 8,$compra['concepto'], 1,0,'C');
    $pdf->Cell(25, 8,$compra['rubro'], 1,0,'C');
    $pdf->Cell(20, 8,$compra['cantidad'], 1,0,'C');
    $pdf->Cell(25, 8,'$'.number_format($compra['precio']*$compra['cantidad'],2), 1,0,'C');
    $pdf->Cell(18, 8,$compra['clave'], 1,0,'C');
    $pdf->Cell(35, 8,$compra['huerta'], 1,0,'C');
    $pdf->Ln(8);
    }  
}
$pdf->Ln(10);
    //CONSULTA DE TOTAL
    $pdf->SetFont('Arial', 'B', 9);
$pdf->Cell(40, 8,'RESUMEN POR RUBRO', 0,0,'C');
$pdf->Ln(8);
$pdf->SetFont('Arial', 'B', 8);
$pdf->SetFillColor(232,232,232);
$pdf->Cell(40, 8, 'Rubro', 1,0,'C', 1);
$pdf->Cell(25, 8, 'No. de gastos', 1,0,'C', 1);
$pdf->Cell(32, 8, 'Total', 1,0,'C', 1);
$pdf->Ln(8);
$query2="SELECT g.fecha,count(g.idgasto) AS oper,g.idconcepto, 
ROUND(sum(g.cantidad*g.precio),2) as tot,t.clave,h.nombre as huerta,
CASE
        WHEN g.idconcepto  = 1 THEN 'MAQUINARIA'
        WHEN g.idconcepto  = 2 THEN 'HUERTA'
        WHEN g.idconcepto  = 3 THEN 'GSTO. IND. ADMIN'
        WHEN g.idconcepto  = 4 THEN 'GSTO. IND. OPER'
        WHEN g.idconcepto  = 5 THEN 'ALMACEN'
        WHEN g.idconcepto  = 6 THEN 'BIOFABRICA'
    END as rubro 
from gastos g
left join huerta h on h.idhuerta = g.idhuerta
left join transporte t on t.idtransporte = g.idtransporte
WHERE g.fecha BETWEEN '$desde' AND '$hasta'
AND g.idfactura LIKE '%$dato2%'".$ext."group by g.idconcepto order by g.fecha asc";
$productos2 = mysqli_query($mysqli, $query2);
while($compra2 = mysqli_fetch_array($productos2)){
    $pdf->SetFont('Arial', '', 9);
    $pdf->Cell(40, 8,$compra2['rubro'], 1,0,'C');
    $pdf->Cell(25, 8,$compra2['oper'], 1,0,'C');
    $pdf->SetFont('Arial', 'B', 9);
    $pdf->SetFillColor(232,232,232);
    $pdf->Cell(32, 8,'$'.number_format($compra2['tot'],2),1,0,'C',1);
    $pdf->Ln(8);
}
$pdf->Ln(8);
$pdf->Output();