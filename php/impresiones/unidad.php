<?php
require('../conexion.php');
require('../../fpdf/fpdf.php');

class PDF extends FPDF
{
// Cabecera de página
    function Header()
    {
        // Logo
        $this->Image('../../img/logo.jpg',10,5,35);
        // Arial bold 15
        $this->SetFont('Arial','B',10);
        // Movernos a la derecha
        $this->Cell(60,10,'',0,0,'c');
        $this->Cell(80, 10, 'AGRICOLA MEN (Huertas)',0,0,'C',0);
        $this->Cell(60, 10, 'Martinez de la Torre, Ver '.date('d-m-Y').'', 0,0,'C',0);
        $this->Ln(20);
        $this->Cell(60, 8, '', 0);
        $this->Cell(80, 8, 'REPORTE DE MANTENIMIENTO', 0,0,'C',0);
        $this->Cell(60, 8, '', 0);
        $this->Ln(20);
    }

    function Footer()
    {
        // Posición: a 1,5 cm del final
        $this->SetY(-15);
        // Arial italic 8
        $this->SetFont('Arial','I',8);
        // Número de página
        $this->Cell(0,10,'Pagina '.$this->PageNo().'/{nb}',0,0,'C');
    }
}

$pdf = new PDF('P','mm','letter'); 
$pdf->AliasNbPages();
$pdf->AddPage();

//CONSULTA
extract($_GET);

    $sql="SELECT m.folio, date_format(m.fecha,'%d/%m/%Y') as fecha, m.unidad, t.clave, t.nombre,
    t.marca, t.color, t.placas, m.medicion, 
    CASE
        WHEN m.idtipo = 1 THEN 'PREVENTIVO'
        WHEN m.idtipo = 2 THEN 'CORRECTIVO'
    END AS tipo, m.chofer,
    CASE
        WHEN m.idservicio = 1 THEN 'INTERNO'
        WHEN m.idservicio = 2 THEN 'EXTERNO'
    END AS servicio,m.proveedor,m.gasto, m.notas
    FROM mantenimientos m
    INNER JOIN transporte t ON t.idtransporte = m.unidad
    WHERE m.folio = $id
    GROUP BY m.folio ORDER BY m.folio DESC";

    $query=mysqli_query($mysqli,$sql);
    while($compra = mysqli_fetch_array($query)){
        $pdf->SetFillColor(232,232,232);
        $pdf->SetFont('Arial', 'B', 9);
        $pdf->Cell(30, 8, 'Fecha:',1,0,'C', 1);
        $pdf->Cell(40, 8,$compra['fecha'],0,0,'C');

        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Cell(40, 8, 'Datos del Transporte',1,0,'C',1);
        $pdf->SetFont('Arial', 'B', 9);
        $pdf->Cell(35, 8,$compra['nombre'],0,0,'C');
        $pdf->Cell(20, 8,$compra['marca'],0,0,'C');
        $pdf->Cell(25, 8,$compra['color'], 0,0,'C');

        $pdf->ln(10);
        $pdf->Cell(190, 8, '-----------------------------------------------------------------------------------------------------------------------------------------------------------------', 0);
        $pdf->ln(15);

            $pdf->SetFillColor(232,232,232);
            $pdf->Cell(65, 8, '', 0);
            $pdf->SetFont('Arial', 'B', 10);
            $pdf->Cell(60, 8, 'Datos del Servicio',1,0,'C',1);
            $pdf->SetFont('Arial', '', 8);
            $pdf->ln(10);

            $pdf->SetFont('Arial', 'B', 10);
            $pdf->Cell(20, 8, 'Camion:', 0);
            $pdf->SetFont('Arial', '', 10);
            $pdf->Cell(25, 8,$compra['clave'], 0);
            
            $pdf->SetFont('Arial', 'B', 10);
            $pdf->Cell(20, 8, 'Placas:', 0);
            $pdf->SetFont('Arial', '', 10);
            $pdf->Cell(25, 8,$compra['placas'], 0);
            
            $pdf->SetFont('Arial', 'B', 10);
            $pdf->Cell(20, 8, 'Chofer:', 0);
            $pdf->SetFont('Arial', '', 10);
            $pdf->Cell(30, 8,$compra['chofer'], 0);

        $pdf->ln(10);

            $pdf->SetFont('Arial', 'B', 10);
            $pdf->Cell(25, 8, 'Km || Hrs:', 0);
            $pdf->SetFont('Arial', '', 10);
            $pdf->Cell(20, 8,number_format($compra['medicion']), 0);
            $pdf->SetFont('Arial', 'B', 10);
            $pdf->Cell(20, 8, 'Servicio:', 0);
            $pdf->SetFont('Arial', '', 10);
            $pdf->Cell(30, 8,$compra['tipo'], 0);
            $pdf->Cell(20, 8,$compra['servicio'], 0);
            $pdf->Cell(10, 8,'', 0);
            if ($compra['servicio'] == 'EXTERNO') {
                $pdf->SetFont('Arial', 'B', 10);
                $pdf->Cell(30, 8, 'Proveedor:', 0);
                $pdf->SetFont('Arial', '', 10);
                $pdf->Cell(40, 8,$compra['proveedor'], 0);
            }
            
        $pdf->ln(10);
        $pdf->Cell(190, 8, '-----------------------------------------------------------------------------------------------------------------------------------------------------------------', 0);
        $pdf->ln(15);
    
    $query3 ="SELECT m.folio, m.refacciones, s.articulo,m.cantidad, s.preciounit
        from mantenimientos m
        inner join stock s on s.idstock = m.refacciones
        where m.folio = $id ORDER BY m.folio DESC";
        $registro2 = mysqli_query($mysqli,$query3);
        $pdf->SetFillColor(232,232,232);
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Cell(15, 8, 'No.',1,0,'C', 1);
        $pdf->Cell(90, 8, 'Articulo',1,0,'C', 1);
        $pdf->Cell(20, 8, 'Cantidad',1,0,'C', 1);
        $pdf->Cell(35, 8, 'Precio',1,0,'C', 1);
        $pdf->Cell(35, 8, 'Sub-total',1,0,'C', 1);
        $pdf->ln(8);
        $pdf->SetFont('Arial', '', 10);
        $item = 1;
        $sub_total = 0;
        while ($articulos = mysqli_fetch_array($registro2)) {
            $pdf->Cell(15, 8, $item,1,0,'C');
            $pdf->Cell(90, 8,$articulos['articulo'],1,0,'C');
            $pdf->Cell(20, 8,$articulos['cantidad'],1,0,'C');
            $pdf->Cell(35, 8,'$ '.number_format($articulos['preciounit'],2),1,0,'C');
            $pdf->Cell(35, 8,'$ '.number_format($articulos['cantidad']*$articulos['preciounit'],2),1,0,'C');
            $pdf->ln(8);
            $sub_total += $articulos['cantidad']*$articulos['preciounit'];
            $item++;
        }
        $pdf->Cell(125, 8,'',0,0,'C');
        $pdf->Cell(35, 8,'Sub total ',0,0,'C');
        $pdf->Cell(35, 8,'$ '.number_format($sub_total,2),1,0,'C');
        $pdf->ln(20);

        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Cell(125, 8, '', 0);
        $pdf->Cell(35, 8, 'Servicio',1,0,'C',1);
        $pdf->SetFont('Arial', '', 10);
        $pdf->Cell(35, 8,'$ '.number_format($compra['gasto'],2),1,0,'C');

        $pdf->ln(10);
        $pdf->Cell(190, 8, '-----------------------------------------------------------------------------------------------------------------------------------------------------------------', 0);
        $pdf->ln(15);

        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Cell(125, 8, '', 0);
        $pdf->Cell(35, 8, 'TOTAL',1,0,'C',1);
        $pdf->SetFont('Arial', '', 10);
        $pdf->Cell(35, 8,'$ '.number_format($compra['gasto']+$sub_total,2),1,0,'C');
        $pdf->ln(9);
        
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Cell(35, 8, 'Notas',1,0,'C',1);
        $pdf->ln(15);
        $pdf->SetFont('Arial', '', 10);
        $pdf->Ln(-2); # Regreso hacia arriba
        
        $pdf->MultiCell(200,4,$compra['notas'],0,'L'); # Descripcion
        $pdf->ln(9);
    }
$pdf->Output();
?>