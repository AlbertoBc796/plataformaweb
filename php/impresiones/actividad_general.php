<?php
require('../conexion.php');
require('../../fpdf/fpdf.php');

class PDF extends FPDF
{
function Header()

{
if(strlen($_GET['desde'])==0 and strlen($_GET['hasta'])==0 and strlen($_GET['dato'])==0 AND strlen($_GET['dato2'])==0){
    echo '<script>alert("Introduzca los valores de busqueda del reporte\nFecha,huerta,producto...");</script> ';
    echo "<script>window.close()</script>";
}else{
    $desde = $_GET['desde'];
    $hasta = $_GET['hasta'];
    $dato2 = $_GET['dato2'];
    $dato = 'XXX-XXX-XXX';
    $verDesde = $desde;
    $verHasta = $hasta;
}
    $this->Image('../../img/logo.jpg',10,5,35);
    $this->SetFont('Arial','B',15);
    $this->Cell(120,10,'',0,0,'c');
    $this->Cell(100, 10, 'AGRICOLA MEN (Huertas)',0,0,'C',0);
    $this->SetFont('Arial','B',12);
    $this->Cell(120, 10, 'Martinez de la Torre, Ver '.date('d-m-Y').'', 0,0,'C',0);
    $this->Ln(15);
    $this->SetFont('Arial','B',15);
    $this->Cell(120, 8, '', 0);
    $this->Cell(100, 8, 'REPORTE DE ACTIVIDADES HUERTA', 0,0,'C',0);
    $this->Cell(120, 8, '', 0);
    $this->Ln(15);
    $this->SetFont('Arial', 'B', 10);
    $this->Cell(45, 8, '', 0);
    $this->Cell(100, 8, 'DE LA '.$verDesde.' HASTA LA '.$verHasta, 0); #colocar las variables desde y hasta
    $this->Ln(10);
}
function Footer()
{
    $this->SetY(-15);
    $this->SetFont('Arial','I',8);
    $this->Cell(0,10,'Pagina '.$this->PageNo().'/{nb}',0,0,'C');
}
}
$pdf = new PDF('l','mm','legal');
$pdf->AliasNbPages();
#$pdf->SetMargins(1, 5 , 0); 
$pdf->AddPage();
extract($_GET);

$pdf->Ln(3);

$pdf->Ln(8);
$pdf->SetFont('Arial', '', 8);
$pdf->SetFillColor(232,232,232);
$pdf->Cell(18, 8, 'Fecha', 1,0,'C', 1);
$pdf->Cell(57, 8, 'Nombre', 1,0,'C', 1);
$pdf->Cell(50, 8, 'Cargo', 1,0,'C', 1);
$pdf->Cell(18, 8, 'Salario', 1,0,'C', 1);
$pdf->Cell(18, 8, 'D/Trabajo', 1,0,'C', 1);
$pdf->Cell(18, 8, 'Pago/Dia', 1,0,'C', 1);
$pdf->Cell(55, 8, 'Categoria', 1,0,'C', 1);
$pdf->Cell(55, 8, 'Sub-categoria', 1,0,'C', 1);
$pdf->Cell(24, 8, 'Producto', 1,0,'C', 1);
$pdf->Cell(30, 8, 'Huerta', 1,0,'C', 1);
$pdf->Ln(8);
$pdf->SetFont('Arial', '', 8);

$query = " SELECT a.idactividades_general,date_format(a.fecha,'%d/%m/%Y') as fecha,a.semana,
    a.mes,a.rubro,concat(p.nombre,' ',p.apellidop,' ',p.apellidom) as ncp,
    a.cargo,a.salario,a.diat,a.pago_dia,a.idherramienta_segmento,
    c.nombre as nc,s.nombre as ns,
    a.detalle,a.producto,a.cantidad,a.precio,a.importe,
    h.nombre as h_nombre from actividades_general a
    inner join persona p on p.idpersona = a.idpersona
    inner join categoria c on c.idcategoria = a.idcategoria
    inner join subcategoria s on s.idsubcategoria = a.idsubcategoria
    inner join huerta h on h.idhuerta = a.idhuerta 
    WHERE  (concat(p.nombre,' ',p.apellidop,' ',p.apellidom) LIKE '%$dato%' or c.nombre LIKE '%$dato%')
    AND a.fecha BETWEEN '$desde' AND '$hasta' order by fecha,cargo ASC";
    $productos = mysqli_query($mysqli, $query);
while($compra = mysqli_fetch_array($productos)){
        $pdf->Cell(18, 8, $compra['fecha'], 1,0,'C');
        $pdf->SetFont('Arial', '', 7);
        $pdf->Cell(57, 8,utf8_decode($compra['ncp']) , 1,0,'C');
        $pdf->SetFont('Arial', '', 8);
        $pdf->Cell(50, 8,$compra['cargo'], 1,0,'C');
        $pdf->Cell(18, 8,$compra['salario'], 1,0,'C');
        $pdf->Cell(18, 8,$compra['diat'], 1,0,'C');
        $pdf->Cell(18, 8,'$'.$compra['pago_dia'], 1,0,'C');
        $pdf->Cell(55, 8,$compra['nc'], 1,0,'C');
        $pdf->Cell(55, 8,$compra['ns'],1,0,'C');
        $pdf->Cell(24, 8,$compra['producto'], 1,0,'C');
        $pdf->Cell(30, 8,$compra['h_nombre'],1,0,'C');
        $pdf->Ln(8);
}
    $pdf->Ln(8);
    $pdf->SetFont('Arial', '', 8);
//TOTAL 
$query = "SELECT a.fecha, concat(p.nombre,' ',p.apellidop,' ',p.apellidom) as ncp,round(sum(a.diat),2) as t1,
round(sum(a.pago_dia),2) as t2, c.nombre as nc from actividades_general a 
inner join persona p on p.idpersona = a.idpersona
inner join categoria c on c.idcategoria = a.idcategoria
WHERE a.fecha BETWEEN '$desde' AND '$hasta'
AND (concat(p.nombre,' ',p.apellidop,' ',p.apellidom) LIKE '%$dato%' or c.nombre LIKE '%$dato%')";
$productos = mysqli_query($mysqli, $query);
while($compra = mysqli_fetch_array($productos)){
    $pdf->SetFillColor(232,232,232);
    $pdf->Cell(75, 8, '', 0,0,'C', 0);
    $pdf->Cell(68, 8, 'TOTAL', 1,0,'C', 1);    
    $pdf->Cell(18, 8, $compra['t1'], 1,0,'C', 1);
    $pdf->Cell(18, 8, '$'.number_format($compra['t2'],2), 1,0,'C', 1);
}
$pdf->Ln(16);

$pdf->AddPage();
$pdf->SetFont('Arial', 'B', 12);
$pdf->Cell(57, 8, 'CARGO A HUERTAS', 0,0,'C', 0); 
$pdf->Ln(8);

//Header de productos
$sqlP = "SELECT concat(p.nombre,' ',p.apellidop,' ',p.apellidom) as ncp,
a.fecha,a.producto, h.nombre , c.nombre as nc
from actividades_general a 
inner join persona p on p.idpersona = a.idpersona
inner join categoria c on c.idcategoria = a.idcategoria
inner join huerta h on h.idhuerta = a.idhuerta
WHERE a.fecha BETWEEN '$desde' AND '$hasta'
AND (concat(p.nombre,' ',p.apellidop,' ',p.apellidom) LIKE '%$dato%' or c.nombre LIKE '%$dato%') group by a.producto ";
$frutas = mysqli_query($mysqli, $sqlP);
$nroProductos = mysqli_num_rows($frutas);
$pdf->SetFillColor(232,232,232);
$pdf->SetFont('Arial', 'B', 10);
$pdf->Cell(40, 8, 'Huerta', 1,0,'C', 1);
while($compra = mysqli_fetch_array($frutas)){   
    $pdf->Cell(40, 8, $compra['producto'], 1,0,'C', 1);
    $frutas2[]=$compra['producto'];
}
$pdf->Cell(40, 8, 'Ingresos', 1,0,'C', 1);
$pdf->Ln(8);

//Numero huertas
$sqlH = "SELECT concat(p.nombre,' ',p.apellidop,' ',p.apellidom) as ncp,
a.fecha,a.producto, h.nombre , c.nombre as nc
from actividades_general a 
inner join persona p on p.idpersona = a.idpersona
inner join categoria c on c.idcategoria = a.idcategoria
inner join huerta h on h.idhuerta = a.idhuerta
WHERE a.fecha BETWEEN '$desde' AND '$hasta'
AND (concat(p.nombre,' ',p.apellidop,' ',p.apellidom) LIKE '%$dato%' or c.nombre LIKE '%$dato%') group by h.nombre ";
$huertas = mysqli_query($mysqli,$sqlH);
$nroHuertas = mysqli_num_rows($huertas);
$pdf->SetFillColor(232,232,232);
$pdf->SetFont('Arial', '', 8);
while($compra = mysqli_fetch_array($huertas)){   
    $huertas2[]=$compra['nombre'];
}

//Inner table
$resultados = array();
$totalGral = 0;
for ($z=0; $z < $nroProductos; $z++) { 
    $resultados[$z] = 0;
}
for ($i=0; $i < $nroHuertas; $i++) { 
    $pdf->Cell(40, 8, $huertas2[$i], 1,0,'C');
    $lugar = $huertas2[$i];
    $total = 0;
    for ($j=0; $j < $nroProductos; $j++) { 
        $siembra=$frutas2[$j];
        $sql = "SELECT concat(p.nombre,' ',p.apellidop,' ',p.apellidom) as ncp, a.fecha, a.pago_dia,a.producto, 
        h.nombre ,round(sum(a.pago_dia),2) as pago_dia, c.nombre as nc
        from actividades_general a 
        inner join persona p on p.idpersona = a.idpersona
        inner join categoria c on c.idcategoria = a.idcategoria
        inner join huerta h on h.idhuerta = a.idhuerta
        WHERE a.fecha BETWEEN '$desde' AND '$hasta' AND h.nombre = '$lugar' AND a.producto = '$siembra'
        AND (concat(p.nombre,' ',p.apellidop,' ',p.apellidom) LIKE '%$dato%' or c.nombre LIKE '%$dato%') ";
        $gasto=mysqli_query($mysqli,$sql);
        $pdf->SetFont('Arial', '', 8);
        while($compra = mysqli_fetch_array($gasto)){   
            if ($compra['pago_dia'] != 0) {
                $pdf->Cell(40, 8, '$'.number_format($compra['pago_dia'],2), 1,0,'C', 0);
            }else {
                $pdf->Cell(40, 8, '$ - ', 1,0,'C', 0);
            }
            $total += $compra['pago_dia'];
            $resultados[$j]=$resultados[$j]+$compra['pago_dia'];
        }
    }
    $pdf->Cell(40, 8, '$ '.number_format($total,2), 1,0,'C', 0);
    $totalGral +=  $total;
    $pdf->Ln(8);
}
$pdf->SetFont('Arial', 'B', 10);
$pdf->Cell(40, 8, 'TOTAL', 0,0,'C', 0);
for ($k=0; $k < count($resultados); $k++) { 
    $pdf->Cell(40, 8, '$ '.number_format($resultados[$k],2), 1,0,'C', 0);
}
$pdf->Cell(40, 8, '$ '.number_format($totalGral,2), 1,0,'C', 0);
$pdf->Output();
?>