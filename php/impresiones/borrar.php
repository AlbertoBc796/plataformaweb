<?php
require('../conexion.php');
require('../../fpdf/fpdf.php');

class PDF extends FPDF
{
// Cabecera de página
function Header()
{

if(strlen($_GET['desde'])>0 and strlen($_GET['hasta'])>0 and strlen($_GET['dato'])>0){
    $dato = $_GET['dato'];
    $desde = $_GET['desde'];
    $hasta = $_GET['hasta'];
    $verDesde = $desde;
    $verHasta = $hasta;
}else{

    $desde = $_GET['desde'];
    $hasta = $_GET['hasta'];
    $dato = 'XXX-XXX-XXX';
    $verDesde = $desde;
    $verHasta = $hasta;
}
    // Logo
    $this->Image('../../img/logo.jpg',10,5,35);
    // Arial bold 15
    $this->SetFont('Arial','B',10);
    // Movernos a la derecha
    $this->Cell(85,10,'',0,0,'c');
    $this->Cell(85, 10, 'AGRICOLA MEN (Huertas)',0,0,'C',0);
    $this->Cell(85, 10, 'Martinez de la Torre, Ver '.date('d-m-Y').'', 0,0,'C',0);
    $this->Ln(20);
    $this->Cell(85, 8, '', 0);
    $this->Cell(85, 8, 'REPORTE DE FACTURACION ', 0,0,'C',0);
    $this->Cell(85, 8, '', 0);
    $this->Ln(20);
    $this->SetFont('Arial', 'B', 10);
    $this->Cell(6, 8, '', 0);
    $this->Cell(115, 8, 'DE LA FECHA :  '.$verDesde.'  HASTA LA FECHA   '.$verHasta, 0,0,'C',0); #colocar las variables desde y hasta
    $this->Ln(15);
}

// Pie de página
function Footer()
{
    // Posición: a 1,5 cm del final
    $this->SetY(-15);
    // Arial italic 8
    $this->SetFont('Arial','I',8);
    // Número de página
    $this->Cell(0,10,'Pagina '.$this->PageNo().'/{nb}',0,0,'C');
}
}
// Creación del objeto de la clase heredada
$pdf = new PDF('l','mm','letter');
#$pdf->SetMargins(1, 5 , 0); 
$pdf->AliasNbPages();
$pdf->AddPage();




$pdf->SetFillColor(232,232,232);

$pdf->SetFont('Arial', 'B', 9);
$pdf->Cell(15, 8,'', 0,0,'C',0);
$pdf->Cell(15, 8,'FOLIO', 1,0,'C',1);
$pdf->Cell(30, 8,'FECHA',1,0,'C',1);
$pdf->Cell(30, 8,'OPERACION ', 1,0,'C',1);
$pdf->Cell(100, 8,'ARTICULO', 1,0,'C',1);
$pdf->Cell(20, 8,'CANTIDAD', 1,0,'C',1);
$pdf->Cell(40, 8,'SALDO ANTERIOR', 1,1,'C',1);

$pdf->SetFont('Arial', 'B', 9);

extract($_GET);
$sql=("SELECT h.idhistorial,h.saldoant,s.articulo,h.fecha,h.cantidad,o.tipo,s.unidad from historial h inner join stock s on s.idstock = h.idstock inner join operacion o on o.idoperacion = h.idoperacion WHERE h.fecha BETWEEN '$desde' AND '$hasta' order by articulo asc");
            $query=mysqli_query($mysqli,$sql);

while($stock = mysqli_fetch_assoc($query)){
    $pdf->Cell(15, 8,'',0, 0,'C');
    $pdf->Cell(15, 8,$stock['idhistorial'],1, 0,'C');
    $pdf->Cell(30, 8,$stock['fecha'],1, 0,'C');
    $pdf->Cell(30, 8,$stock['tipo'], 1, 0,'C');
    $pdf->Cell(100, 8,$stock['articulo'], 1, 0,'C');
    $pdf->Cell(20, 8,$stock['cantidad'].' '.$stock['unidad'],1, 0,'C');
    $pdf->Cell(40, 8,$stock['saldoant'].' '.$stock['unidad'], 1,1,'C');

}

$pdf->Ln(20);

//CONSULTA DE TOTAL EN STOCK
$query2 = "SELECT s.articulo,h.fecha,o.tipo,s.unidad,s.existencia from historial h inner join stock s on s.idstock = h.idstock inner join operacion o on o.idoperacion = h.idoperacion WHERE h.fecha BETWEEN '$desde' AND '$hasta' group by articulo order by articulo asc ";


$productos = mysqli_query($mysqli, $query2);
    $pdf->SetFillColor(232,232,232);
    $pdf->SetFont('Arial', 'B', 9);
    $pdf->Cell(15, 8,'',0,0,'C',0);
    $pdf->Cell(45, 8,'FECHA',1,0,'C',1);
    $pdf->Cell(130, 8,'ARTICULO', 1,0,'C',1);
    $pdf->Cell(60, 8,'TOTAL EN EXISTENCIA', 1,0,'C',1);
    $pdf->SetFont('Arial', 'B', 9);
    $pdf->Ln(8);


    while($compra = mysqli_fetch_array($productos)){

    $pdf->SetFillColor(232,232,232);
    $pdf->Cell(15, 8, '', 0,0,'C', 0);
    $pdf->Cell(45, 8, $compra['fecha'], 1,0,'C', 0);
    $pdf->Cell(130, 8, $compra['articulo'] .' '.$compra['unidad'], 1,0,'C', 0);
    $pdf->Cell(60, 8, $compra['existencia'] .' '.$compra['unidad'], 1,0,'C', 0);
$pdf->Ln(8);
   
}

$pdf->Output('reporte.pdf','D');
