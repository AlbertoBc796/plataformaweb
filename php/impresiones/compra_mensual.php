<?php
require('../conexion.php');
require('../../fpdf/fpdf.php');
setlocale(LC_TIME,"es_ES");
$dato = date("m");
$dato2 = date("Y");
class PDF extends FPDF
{
// Cabecera de página
function Header()
{  
    setlocale(LC_TIME,"es_ES");
    $dato = strftime("%B");
    $dato2 = date("Y");
    // Logo
    $this->Image('../../img/logo.jpg',10,5,35);

    // Arial bold 15
    $this->SetFont('Arial','B',12);
    // Movernos a la derecha
    $this->Cell(80,10,'',0,0,'c');
    $this->Cell(100, 10, 'AGRICOLA MEN (Huertas)',0,0,'C',0);
    $this->Cell(85, 10, 'Martinez de la Torre, Ver '.date('d-m-Y').'', 0,0,'C',0);
    $this->Ln(20);
    $this->Cell(80, 8, '', 0);
    $this->Cell(100, 8, 'REPORTE DE COSECHA - VENTA MENSUAL', 0,0,'C',0);
    $this->Cell(100, 8, '', 0);
    $this->Ln(15);
    $this->SetFont('Arial', 'B', 10);
    $this->Cell(10, 8, '', 0);
    $this->Cell(100, 8, 'MES : '.$dato.' '.$dato2, 0); #colocar las variables desde y hasta
    $this->Cell(80, 8, '', 0);
    $this->Ln(10);
}

// Pie de página
function Footer()
{
    // Posición: a 1,5 cm del final
    $this->SetY(-15);
    // Arial italic 8
    $this->SetFont('Arial','I',8);
    // Número de página
    $this->Cell(0,10,'Pagina '.$this->PageNo().'/{nb}',0,0,'C');
}
}

// Creación del objeto de la clase heredada
$pdf = new PDF('l','mm','letter');

$pdf->AliasNbPages();
$pdf->AddPage();
    $pdf->SetFont('Arial', 'B', 8);
    $pdf->SetFillColor(232,232,232);
    $pdf->Cell(6, 8, '#', 1,0,'C', 1);
    $pdf->Cell(16, 8, 'Fecha', 1,0,'C', 1);
    $pdf->Cell(22, 8, 'Huerta', 1,0,'C', 1);
    $pdf->Cell(32, 8, 'Producto', 1,0,'C', 1);
    $pdf->Cell(17, 8, 'Kg corte', 1,0,'C', 1);
    $pdf->Cell(17, 8, '$ Corte', 1,0,'C', 1);
    $pdf->Cell(17, 8, 'Total Corte', 1,0,'C', 1);
    $pdf->Cell(17, 8, 'Egresos', 1,0,'C', 1);
    $pdf->Cell(17, 8, 'Kg Venta', 1,0,'C', 1);
    $pdf->Cell(17, 8, '$ Venta', 1,0,'C', 1);
    $pdf->Cell(20, 8, 'Ingresos', 1,0,'C', 1);
    $pdf->Cell(20, 8, 'Utilidad', 1,0,'C', 1);
    $pdf->Cell(46, 8, 'Cliente', 1,0,'C', 1);
$pdf->Ln(8);
$pdf->SetFont('Arial', '', 7);
extract($_GET);

$h2="SELECT count(*) as cuantos from compra_venta c 
inner join huerta h on c.idhuerta=h.idhuerta
inner join producto p on p.idproducto = c.idproducto
inner join variedad v on v.idvariedad = c.idvariedad
where month(c.fecha)=$dato and year(c.fecha)=$dato2";
$r2 = mysqli_query($mysqli,$h2);
while ($row2=mysqli_fetch_assoc($r2)) {
    $a2 = $row2['cuantos']; 
}


$query = "  
SELECT c.fecha,h.nombre AS HUERTITA,p.nombre,p.idproducto ,c.folio1,c.camion,c.marca,c.color,
c.placas,c.chofer,c.kilos_corte,c.precio_corte,c.importe_corte,c.pesaje,p.nombre as producto,v.nombre as variedad,
c.precio_flete,c.importe_flete,c.egresos,
c.kilos_venta,c.precio_venta,c.total_venta,c.ingreso_total,c.folio2,c.cliente,v.nombre 
from  compra_venta c 
    inner join huerta h on c.idhuerta=h.idhuerta
    inner join producto p on p.idproducto = c.idproducto
    inner join variedad v on v.idvariedad = c.idvariedad
    where month(c.fecha)=$dato and year(c.fecha)=$dato2";

$x=0;
$crt=0;
$vta=0;
$multi=1;

$productos = mysqli_query($mysqli, $query);
while($compra = mysqli_fetch_array($productos)){
    if ($compra['producto'] == 'LIMON') {
        $multi=25;
    }else {
        $multi=1;
    }
    if ( $x <= $a2) {
    $pdf->Cell(6, 8,$x = $x+1,1, 0,'C');
    $pdf->Cell(16, 8,date('d-m-Y',strtotime($compra['fecha'])), 1,0,'C');
    $pdf->Cell(22, 8, $compra['HUERTITA'], 1,0,'C');
    $pdf->Cell(32, 8, $compra['producto'].' '.$compra['variedad'], 1,0,'C');
    $pdf->Cell(17, 8,$compra['kilos_corte']*$multi, 1,0,'C');
    if ($compra['precio_corte'] > 0) {
        $crt = $crt+1;
    }
    $pdf->Cell(17, 8,'$'.$compra['precio_corte'], 1,0,'C');
    $pdf->Cell(17, 8,'$'.number_format($compra['importe_corte'],2), 1,0,'C');
    $pdf->SetFillColor(255,110,110);#Rojo
    $pdf->Cell(17, 8,'$'.number_format($compra['egresos'],2),1,0,'C',true);
    $pdf->Cell(17, 8,$compra['kilos_venta'], 1,0,'C');
    if ($compra['precio_venta'] > 0) {
        $vta = $vta+1;
    }
    $pdf->Cell(17, 8,'$'.$compra['precio_venta'], 1,0,'C');
    $pdf->SetFillColor(110,255,110);#Verde
    $pdf->Cell(20, 8,'$'.number_format($compra['total_venta'],2),1,0,'C',true);
    $pdf->Cell(20, 8,'$'.number_format($compra['ingreso_total'],2),1,0,'C');
    $pdf->Cell(46, 8,$compra['cliente'],1,0,'C');
    $pdf->Ln(8);
        
    } 
}
    if ($crt == 0) {
        $crt=1;
    }
    if ($vta == 0) {
        $vta=1;
    }
    if ($x == 0) {
        $x=1;
    }

//CONSULTA DE TOTAL 
$query = "SELECT h.nombre ,p.nombre,p.idproducto,v.nombre, c.fecha,sum(kilos_corte),sum(precio_corte),
sum(importe_corte),sum(pesaje),sum(precio_flete),sum(importe_flete), sum(egresos),sum(kilos_venta),
sum(precio_venta),sum(total_venta),sum(ingreso_total) from compra_venta c 
inner join huerta h on c.idhuerta=h.idhuerta 
inner join producto p on p.idproducto = c.idproducto
inner join variedad v on v.idvariedad = c.idvariedad 
where month(c.fecha)=$dato and year(c.fecha)=$dato2";

$productos = mysqli_query($mysqli, $query);
while($compra = mysqli_fetch_array($productos)){

    $pdf->Cell(59, 8, '', 0,0,'C');
    $pdf->SetFont('Arial', 'B', 7);
    $pdf->Cell(17, 8,'TOTALES:', 0,0,'C');
    $pdf->SetFillColor(232,232,232);
    $pdf->Cell(17, 8,$compra['sum(kilos_corte)'].' Kg', 1,0,'C',1);
    $pdf->Cell(17, 8,'',0);
    $pdf->Cell(17, 8,'$'.number_format($compra['sum(importe_corte)'],2), 1,0,'C',1);
    $pdf->Cell(17, 8,'$'.number_format($compra['sum(egresos)'],2),1,0,'C',1);
    $pdf->Cell(17, 8,$compra['sum(kilos_venta)'].' Kg', 1,0,'C',1);
    $pdf->Cell(17, 8,'',0);
    $pdf->Cell(20, 8,'$'.number_format($compra['sum(total_venta)'],2),1,0,'C',1);
    $pdf->Cell(20, 8,'$'.number_format($compra['sum(ingreso_total)'],2),1,0,'C',1);
    $pdf->Cell(24, 8,'', 0,0,'C',0);
    $pdf->Ln(8);
}

    $pdf->Ln(5);

    $query3 = "SELECT COUNT(c.idcompra_venta) as ventas, avg(c.precio_corte) as pc, c.fecha, v.idvariedad,
    c.idproducto,c.idvariedad,avg(c.precio_venta) as pv, p.nombre as fruta,v.nombre as valor,p.idproducto
    from compra_venta c
    inner join producto p on p.idproducto = c.idproducto
    inner join variedad v on v.idvariedad = c.idvariedad 
    WHERE month(c.fecha)=$dato and year(c.fecha)=$dato2 group by p.nombre, v.nombre";
    $productos3 = mysqli_query($mysqli, $query3);
        $pdf->SetFillColor(232,232,232);
        $pdf->Cell(20, 8, 'Producto', 1,0,'C', 1);
        $pdf->Cell(28, 8, 'Variedad', 1,0,'C', 1);
        $pdf->Cell(20, 8, 'No. ventas', 1,0,'C', 1);
        $pdf->Cell(20, 8, '$ Prom. C', 1,0,'C', 1);
        $pdf->Cell(20, 8, '$ Prom. V', 1,0,'C', 1);
        $pdf->Ln(8);
    while($compra3 = mysqli_fetch_array($productos3)){
        
        $pdf->SetFont('Arial', 'B', 9);
        $pdf->Cell(20, 8,$compra3['fruta'], 1,0,'C');
        $pdf->Cell(28, 8,$compra3['valor'],1,0,'C'); 
        $pdf->Cell(20, 8,$compra3['ventas'], 1,0,'C');
        $pdf->Cell(20, 8,'$'.number_format($compra3['pc'],2),1,0,'C');
        $pdf->Cell(20, 8,'$'.number_format($compra3['pv'],2),1,0,'C');
    
        $pdf->Ln(8);
    }
$pdf->Output('reporte_mensual.pdf','D');
?>