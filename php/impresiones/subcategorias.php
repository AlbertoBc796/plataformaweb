<?php 
require('../conexion.php');
require('../../fpdf/fpdf.php');
class PDF extends FPDF
{
function Header()
{
    $this->Image('../../img/logo.jpg',10,5,35);
	$this->SetFont('Courier', 'I', 20);
	$this->Cell(45, 10, '', 0);
	$this->Cell(95, 10, 'GRUPO MURRIETA',0,0,'C',0);
	$this->SetFont('Arial', 'I', 9);
	$this->Cell(40, 10, 'Martinez de la Torre, Ver: '.date('d-m-Y').'', 0);
	$this->Ln(18);
	$this->SetFont('Courier', 'I', 15);
	$this->Cell(45, 10, '', 0);
	$this->Cell(95, 10, 'ACTIVIDADES GENERALALES',0,0,'C',0);
	$this->SetFont('Arial', 'I', 9);
	$this->Ln(15);
}
function Footer()
{
    $this->SetY(-15);
    $this->SetFont('Arial','I',8);
    $this->Cell(0,10,'Pagina '.$this->PageNo().'/{nb}',0,0,'C');
}
}
$pdf = new PDF('p','mm','letter');
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->Ln(10);
$sql=("SELECT  @numero:=@numero+1 as total ,  s.nombre as sub,c.nombre from subcategoria s
inner join categoria c on c.idcategoria = s.idcategoria, 
(SELECT @numero:= 0) as total order by c.nombre");
$query=mysqli_query($mysqli,$sql);
$pdf->SetFillColor(232,232,232);
$pdf->SetFont('Arial', 'B', 9);
$pdf->Cell(20, 8,'', 0,0,'C',0);
$pdf->Cell(20, 8,'#', 1,0,'C',1);
$pdf->Cell(70, 8,'CATEGORIA',1,0,'C',1);
$pdf->Cell(70, 8,'SUBCATEGORIA',1,0,'C',1);
$pdf->Ln(8);
while($stock = mysqli_fetch_assoc($query)){
	$pdf->Cell(20, 8,'', 0,0,'C',0);
    $pdf->Cell(20, 8,$stock['total'],1, 0,'C');
    $pdf->Cell(70, 8,$stock['nombre'],1, 0,'C');
    $pdf->Cell(70, 8,$stock['sub'],1, 0,'C');
	$pdf->Ln(8);
}
$pdf->Output();
 ?>