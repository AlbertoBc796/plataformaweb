<?php
require('../conexion.php');
require('../../fpdf/fpdf.php');
class PDF extends FPDF
{
function Header()
{
    $this->Image('../../img/logo.jpg',10,5,35);
	$this->SetFont('Courier', 'I', 20);
	$this->Cell(45, 10, '', 0);
	$this->Cell(95, 10, 'GRUPO MURRIETA',0,0,'C',0);
	$this->SetFont('Arial', 'I', 9);
	$this->Cell(40, 10, 'Martinez de la Torre, Ver: '.date('d-m-Y').'', 0);
	$this->Ln(18);
	$this->SetFont('Courier', 'I', 15);
	$this->Cell(45, 10, '', 0);
	$this->Cell(95, 10, 'INVENTARIO GENERAL',0,0,'C',0);
	$this->Ln(15);
}
function Footer()
{
    $this->SetY(-15);
    $this->SetFont('Arial','I',8);
    $this->Cell(0,10,'Pagina '.$this->PageNo().'/{nb}',0,0,'C');
}
}
$sql=("SELECT @numero:=@numero+1 as total , date_format(fecha,'%d-%m-%Y') as fecha,articulo,unidad,existencia FROM stock
	, (SELECT @numero:= 0) AS total ORDER BY fecha desc");
$query=mysqli_query($mysqli,$sql);
$pdf = new PDF('p','mm','letter');
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->Ln(10);
$pdf->SetFont('Arial', 'I', 9);
$pdf->Ln(15);
$pdf->SetFillColor(232,232,232);
$pdf->SetFont('Arial', 'B', 9);
$pdf->Cell(20, 8,'FOLIO', 1,0,'C',1);
$pdf->Cell(30, 8,'FECHA',1,0,'C',1);
$pdf->Cell(90, 8,'ARTICULO', 1,0,'C',1);
$pdf->Cell(20, 8,'UNIDAD', 1,0,'C',1);
$pdf->Cell(30, 8,'EXISTENCIA', 1,1,'C',1);
$pdf->SetFont('Arial', 'B', 9);
while($stock = mysqli_fetch_assoc($query)){
    $pdf->Cell(20, 8,$stock['total'],1, 0,'C');
    $pdf->Cell(30, 8,$stock['fecha'],1, 0,'C');
    $pdf->Cell(90, 8,$stock['articulo'], 1, 0,'C');
    $pdf->Cell(20, 8,$stock['unidad'],1, 0,'C');
    $pdf->Cell(30, 8,$stock['existencia'], 1,1,'C');
}
$pdf->Output();


?>