<?php
require('../conexion.php');
require('../../fpdf/fpdf.php');



class PDF extends FPDF
{
// Cabecera de página
function Header()
{
    $this->SetFillColor(30, 178, 59  );
    $this->Cell(200, 10, '',0,0,'L',1);
    $this->Ln(15);    
    $this->Cell(200, 40, '',1,0,'L',0);
    $this->Ln(5);    
    // Logo
    $this->Image('../../img/logo.jpg',170,27,35);
    // Arial bold 15
    $this->SetFont('Arial','B',10);
    // Movernos a la derecha
    $this->Cell(85, 10, 'AGRICOLA MEN (Orden de compra)',0,0,'C',0);
    $this->Ln(10);
    $this->SetFont('Arial', 'I', 7);
    $this->Cell(85, 5, utf8_decode('DIRECCIÓN: NUEVA CENTRAL DE ABASTO #15 '),0,0,'C',0);
    $this->Ln(5); 
    $this->Cell(85, 5,utf8_decode('COLONIA: PLAZA VERDE, MARTÍNEZ DE LA TORRE, VERACRUZ. ') ,0,0,'C',0);
    $this->Ln(5);
    $this->Cell(85, 5, utf8_decode('EMAIL: ANGEL.MURRIETA@HOTMAIL.COM '),0,0,'C',0);
    $this->Ln(5);
    $this->Cell(85, 5,utf8_decode(' TEL: 01(232)3245205 Y 01 (232) 3245206') ,0,0,'C',0);

}


// Pie de página
function Footer()
{
    // Posición: a 1,5 cm del final
    $this->SetY(-15);
    // Arial italic 8
    $this->SetFont('Arial','I',8);
    // Número de página
    $this->Cell(0,10,'Pagina '.$this->PageNo().'/{nb}',0,0,'C');
}
}

//CONSULTA
$sql=("SELECT @numero:=@numero+1 as total , date_format(fecha,'%d-%m-%Y') as fecha,articulo,unidad,existencia,limite,proveedor,preciounit FROM stock
	, (SELECT @numero:= 0) AS total ORDER BY fecha desc");
   $query=mysqli_query($mysqli,$sql);
$pdf = new PDF('p','mm','letter');
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->Ln(10);
$pdf->SetFont('Arial', 'I', 9);
$pdf->Ln(15);
$pdf->SetFillColor(232,232,232);
$pdf->SetFont('Arial', 'B', 9);
$pdf->Cell(20, 8,'FOLIO', 1,0,'C',1);
$pdf->Cell(30, 8,'PROVEEDOR',1,0,'C',1);
$pdf->Cell(90, 8,'ARTICULO', 1,0,'C',1);
$pdf->Cell(20, 8,'UNIDAD', 1,0,'C',1);
$pdf->Cell(30, 8,'PRECIO', 1,1,'C',1);
$pdf->SetFont('Arial', 'B', 9);
$gastoTotal = 0;
while($stock = mysqli_fetch_assoc($query)){

    if ($stock['existencia'] <= $stock['limite']-1) {
        # code...
        $pdf->Cell(20, 8,$stock['total'],1, 0,'C');
        $pdf->Cell(30, 8,$stock['proveedor'],1, 0,'C');
        $pdf->Cell(90, 8,$stock['articulo'], 1, 0,'C');
        $pdf->Cell(20, 8,($stock['limite']-$stock['existencia']).' '.$stock['unidad'],1, 0,'C');
        $pdf->Cell(30, 8,$stock['preciounit'], 1,1,'C');
        $gastoTotal=$gastoTotal+($stock['preciounit']*($stock['limite']-$stock['existencia']));
    }
}
    $pdf->Ln(10);    
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Cell(200, 4, '-------------------------------------------------------------------------------------------------------------------------------------------------------------------------', 0);
    $pdf->Ln(10);
    $pdf->SetFont('Arial', 'B', 13);
    $pdf->Cell(100, 10, 'TOTAL DE FACTURA   ', 0,0,'L'); 

    $pdf->Cell(100, 10, '$ '. $gastoTotal.'  ', 0,0,'R'); #COLOCAR LA VARIABLE DE TOTAL_VENTA
    $pdf->Ln(15);
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Cell(200, 4, '-------------------------------------------------------------------------------------------------------------------------------------------------------------------------', 0);

    $pdf->Ln(20);

    $pdf->SetFillColor(30, 178, 59  );
    $pdf->Cell(200, 10, '',0,0,'L',1);
$pdf->Output('orden_de_compra.pdf','D');
