<?php
	include('../conexion.php');
	$paginaActual = $_POST['partida'];
    $query = "SELECT c.cliente, c.idcompra_venta,date_format(c.fecha,'%d-%m-%Y') as fecha,
    h.nombre as HUERTITA, c.kilos_corte,c.precio_corte,c.importe_corte, c.precio_flete,
    c.importe_flete,c.egresos,c.kilos_venta,c.precio_venta,c.total_venta,
    c.ingreso_total, p.nombre as producto, v.nombre as variedad
    from compra_venta c
    inner join huerta h on c.idhuerta=h.idhuerta
    inner join producto p on p.idproducto = c.idproducto
    inner join variedad v on v.idvariedad = c.idvariedad
    ORDER BY c.fecha";
    $nroProductos = mysqli_num_rows(mysqli_query($mysqli, $query));
    $nroLotes = 20;
    $nroPaginas = ceil($nroProductos/$nroLotes);
    $lista = '';
    $tabla = '';

    $x = $nroPaginas ;

    if($paginaActual > 1){
        $lista = $lista.' <li class="page-item"><a class="page-link"  href="javascript:pagination('.($paginaActual-1).');">Anterior</a></li>';
    }
    for($i=1; $i<=$nroPaginas; $i++){
        if($i == $paginaActual){
            $lista = $lista.'<li class="page-item active"><a class="page-link"  href="javascript:pagination('.$i.');">'.$i.'</a></li>';
        }else{
            $lista = $lista.'<li class="page-item"><a class="page-link"  href="javascript:pagination('.$i.');">'.$i.'</a></li>';
        }
    }

    #$lista = $lista.'<li class="page-item"><a class="page-link"  href="javascript:pagination('.$i.');">'.$i.'</a></li>
   //<li class="disabled"><a  >...</a></li>
   //<li class="page-item"><a class="page-link" href=""  >'.$nroPaginas.'</a></li>';

    if($paginaActual < $nroPaginas){
        $lista = $lista.'<li class="page-item"><a class="page-link"  href="javascript:pagination('.($paginaActual+1).');">Siguiente</a></li>';
    }
    if($paginaActual <= 1){
        $limit = 0;
    }else{
        $limit = $nroLotes*($paginaActual-1);
    }

    $query2 = "SELECT c.cliente, c.idcompra_venta,date_format(c.fecha,'%d-%m-%Y') as fecha,h.nombre as HUERTITA,
c.kilos_corte,c.precio_corte,c.importe_corte, c.precio_flete,
c.importe_flete,c.egresos,c.kilos_venta,c.precio_venta,c.total_venta,
c.ingreso_total, p.nombre as producto, v.nombre as variedad
    from compra_venta c 
    inner join huerta h on c.idhuerta=h.idhuerta
    inner join producto p on p.idproducto = c.idproducto
    inner join variedad v on v.idvariedad = c.idvariedad
    ORDER BY c.fecha  desc LIMIT $limit, $nroLotes  ";
        $registro = mysqli_query($mysqli, $query2);
        $tabla = $tabla.'<table  class="table table-striped table-bordered text-center">
                    <thead>
                    <tr>
                        <th>FECHA</th>
                        <th>CLIENTE</th>
                        <th>HUERTA</th>
                        <th>PRODUCTO</th>
                        <th>CORTE</th>
                        <th>TOTAL CORTE</th>
                        <th>TOTAL FLETE</th>
                        <th>GASTOS</th>
                        <th>VENTA</th>
                        <th>TOTAL VENTA</th>
                        <th>IMPORTE NETO</th>
                        <th>EDITAR</th>
                        <th>ELIMINAR</th>
                        <th>IMPRIMIR</th>
                    </tr>
                    </thead>';
				
	while($registro2 = mysqli_fetch_array($registro)){
        if ($registro2['producto'] == 'LIMON') {
            $unidad = 'Cajas';
        }else{
            $unidad = 'Kg';
        }
		$tabla = $tabla.'<tr>
        <td>'.$registro2['fecha'].'</td>
        <td>'.$registro2['cliente'].'</td>
        <td>'.$registro2['HUERTITA'].'</td>
        <td>'.$registro2['producto'].' '.$registro2['variedad'].'</td>
        <td>'.$registro2['kilos_corte'].' '.$unidad.'</td>
        <td>'.'$'.number_format($registro2['importe_corte'],2).'</td>
        <td>'.'$'.number_format($registro2['importe_flete'],2).'</td>
        <td>'.'$'.number_format($registro2['egresos'],2).'</td>
        <td>'.$registro2['kilos_venta'].' Kg'.'</td>
        <td>'.'$'.number_format($registro2['total_venta'],2).'</td>
        <td>'.'$'.number_format($registro2['ingreso_total'],2).'</td>
        <td><a href=act_compra_venta.php?id='.$registro2['idcompra_venta'].'&idborrar=2"><img src="../img/act.png" alt="ACTUALIZAR" class="img-rounded"></a></td>

        <td><a href=../php/eliminaciones/eliminar_cv.php?id='.$registro2['idcompra_venta'].'&idborrar=2"><img src="../img/del.png" alt="ACTUALIZAR" class="img-rounded"></a></td>

        <td><a href=../php/impresiones/pdf.php?id='.$registro2['idcompra_venta'].'&idborrar=2"><img src="../img/imp.png" alt="ACTUALIZAR" class="img-rounded"></a></td>
						</tr>';		
	}
        
    $tabla = $tabla.'</table>';

    $array = array(0 => $tabla,1 => $lista);

    echo json_encode($array);
?>