<?php
	include('../conexion.php');
	$paginaActual = $_POST['partida'];
    session_start();
    $query = " SELECT h.idhistorial, h.fecha,s.articulo,o.tipo,h.saldoant,h.cantidad,h.viaje, 
    CASE
    WHEN h.idoperacion  = 1 THEN (cantidad+saldoant)
    WHEN h.idoperacion  = 2 THEN (saldoant-cantidad)
    WHEN h.idoperacion  = 3 THEN (cantidad+saldoant)
    WHEN h.idoperacion  = 4 THEN (cantidad)
    WHEN h.idoperacion  = 5 THEN (saldoant-cantidad)
    END as total
    FROM historial h 
    inner join stock s on s.idstock = h.idstock
    inner join operacion o on o.idoperacion = h.idoperacion order by h.fecha desc";

    $nroProductos = mysqli_num_rows(mysqli_query($mysqli, $query));
    $nroLotes = 20;
    $nroPaginas = ceil($nroProductos/$nroLotes);
    $lista = '';
    $tabla = '';
    if($paginaActual > 1){
        $lista = $lista.' <li class="page-item"><a class="page-link"  href="javascript:pagination('.($paginaActual-1).');">Anterior</a></li>';
    }
    for($i=1; $i<=$nroPaginas; $i++){
        if($i == $paginaActual){
            $lista = $lista.'<li class="page-item active"><a class="page-link"  href="javascript:pagination('.$i.');">'.$i.'</a></li>';
        }
    }
    if($paginaActual < $nroPaginas){
        $lista = $lista.'<li class="page-item"><a class="page-link"  href="javascript:pagination('.($paginaActual+1).');">Siguiente</a></li>';
    }

    if($paginaActual <= 1){
        $limit = 0;
    }else{
        $limit = $nroLotes*($paginaActual-1);
    }
    $query2 = "SELECT h.idhistorial, h.fecha,s.articulo,o.tipo,h.saldoant,h.cantidad,h.lugar,h.viaje, 
CASE
    WHEN h.idoperacion  = 1 THEN (cantidad+saldoant)
    WHEN h.idoperacion  = 2 THEN (saldoant-cantidad)
    WHEN h.idoperacion  = 3 THEN (cantidad+saldoant)
    WHEN h.idoperacion  = 4 THEN (cantidad)
    WHEN h.idoperacion  = 5 THEN (saldoant-cantidad)
END as total
FROM historial h
    inner join stock s on s.idstock = h.idstock
    inner join operacion o on o.idoperacion = h.idoperacion order by h.fecha desc, h.idhistorial desc
    LIMIT $limit, $nroLotes   ";
    $registro = mysqli_query($mysqli, $query2);

    if($_SESSION['tipo'] == 1){
        $var1='<th>ELIMINAR</th>';
    }else {
        $var1 = '';
    }
    $tabla = $tabla.'<table  class="table table-striped table-bordered  text-center">
                    <thead>
                        <tr>
                        <th>FECHA</th>
                        <th>ARTICULO</th>
                        <th>OPERACION</th>
                        <th>CANTIDAD</th>
                        <th>EXISTENCIA</th>  
                        <th>ACTUAL</th>   
                        <th>DESTINO</th>'.$var1.
                        '</tr>
                    </thead>';
	while($registro2 = mysqli_fetch_array($registro)){
        if($_SESSION['tipo'] == 1){
            $var2 = '<td><a href=../php/eliminaciones/eliminar_historial.php?id='.$registro2['idhistorial'].'&idborrar=2"><img src="../img/del.png" alt="ELIMINAR" class="img-rounded"></a></td>';
        }else {
            $var2 = '';
        }
        if ($registro2['viaje'] == '') {
            $var3 = '';
        }else {
            $var3 = '<br>';
        }
		$tabla = $tabla.'<tr>
            <td>'.date('d-m-Y',strtotime($registro2['fecha'])).'</td>
            <td><a href=act_historial_almacen.php?id='.$registro2['idhistorial'].'>'.$registro2['articulo'].'</a></td>
            <td>'.$registro2['tipo'].'</td>          
            <td>'.$registro2['cantidad'].'</td>
            <td>'.$registro2['saldoant'].' </td>
            <td>'.$registro2['total'].'</td>
            <td>'.$registro2['viaje'].$var3.$registro2['lugar'].'</td>'.
            $var2.
            '</tr>';		
	}    
    $tabla = $tabla.'</table>';
    $array = array(0 => $tabla,1 => $lista);
    echo json_encode($array);
?>