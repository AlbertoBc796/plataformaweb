<?php 
    include('../conexion.php');
    date_default_timezone_set("America/Mexico_City");
    $paginaActual = $_POST['partida'];
    $query = "SELECT * from historial_users";
    $nroProductos = mysqli_num_rows(mysqli_query($mysqli, $query));
    $nroLotes = 10;
    $nroPaginas = ceil($nroProductos/$nroLotes);
    $lista = '';
    $tabla = '';

    if($paginaActual > 1){
        $lista = $lista.' <li class="page-item"><a class="page-link"  href="javascript:pagination('.($paginaActual-1).');">Anterior</a></li>';
    }
    for($i=1; $i<=$nroPaginas; $i++){
        if($i == $paginaActual){
            $lista = $lista.'<li class="page-item active"><a class="page-link"  href="javascript:pagination('.$i.');">'.$i.'</a></li>';
        }else{
            $lista = $lista.'<li class="page-item"><a class="page-link"  href="javascript:pagination('.$i.');">'.$i.'</a></li>';
        }
    }
    if($paginaActual < $nroPaginas){
        $lista = $lista.'<li class="page-item"><a class="page-link"  href="javascript:pagination('.($paginaActual+1).');">Siguiente</a></li>';
    }

    if($paginaActual <= 1){
        $limit = 0;
    }else{
        $limit = $nroLotes*($paginaActual-1);
    }

    $query2 = "SELECT date_format( h.fecha,'%d/%m/%Y <br> %H:%i:%s') as fecha,h.idusuario,
    h.actividad,h.equipo, u.usuario as username from historial_users h
    inner join usuarios u on u.idusuarios = h.idusuario
    ORDER BY h.fecha DESC
    LIMIT $limit,$nroLotes";
    $registro = mysqli_query($mysqli,$query2);

    $tabla = $tabla.'<table class="table table-striped table-bordered text-center">
    <thead>
        <tr>
            <th>FECHA</th>
            <th>USUARIO</th>
            <th>ACTIVIDAD</th>
            <th>EQUIPO</th>
        </tr>
    </thead>';

    while ($a = mysqli_fetch_array($registro)) {
        $tabla = $tabla.'<tr>
        <td>'.$a['fecha'].'</td>
        <td>'.$a['username'].'</td>
        <td>'.$a['actividad'].'</td>
        <td>'.$a['equipo'].'</td>
    </tr>';
    }
    $tabla = $tabla.'</table>';
    $array = array(0 => $tabla, 1 => $lista);

    echo json_encode($array);