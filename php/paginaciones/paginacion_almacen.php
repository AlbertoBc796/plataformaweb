<?php
	include('../conexion.php');
	$paginaActual = $_POST['partida'];
    $query = "SELECT idstock,fecha,clave,articulo,unidad,existencia,limite FROM stock order by limite> existencia desc   ";
    $nroProductos = mysqli_num_rows(mysqli_query($mysqli, $query));
    $nroLotes = 20;
    $nroPaginas = ceil($nroProductos/$nroLotes);
    $lista = '';
    $tabla = '';
    if($paginaActual > 1){
        $lista = $lista.' <li class="page-item"><a class="page-link"  href="javascript:pagination('.($paginaActual-1).');">Anterior</a></li>';
    }
    for($i=1; $i<=$nroPaginas; $i++){
        if($i == $paginaActual){
            $lista = $lista.'<li class="page-item active"><a class="page-link"  href="javascript:pagination('.$i.');">'.$i.'</a></li>';
        }
    }
    if($paginaActual < $nroPaginas){
        $lista = $lista.'<li class="page-item"><a class="page-link"  href="javascript:pagination('.($paginaActual+1).');">Siguiente</a></li>';
    }
    if($paginaActual <= 1){
        $limit = 0;
    }else{
        $limit = $nroLotes*($paginaActual-1);
    }
    $query2 = "SELECT idstock, fecha , clave, articulo,unidad,existencia,limite FROM stock order by limite>existencia  desc LIMIT $limit, $nroLotes  ";
    $registro = mysqli_query($mysqli, $query2);
    $tabla = $tabla.'<table  class="table table-striped table-bordered text-center">
                    <thead>
                        <tr>
                            <th>FECHA</th>
                            <th>CLAVE</th>
                            <th>ARTICULO</th>
                            <th>UNIDAD</th>
                            <th>EXISTENCIA</th>
                            <th>LIMITE</th>
                            <th>EDITAR</th>
                            <th>ELIMINAR</th>
                        </tr>
                    </thead>';
                    while($registro2 = mysqli_fetch_array($registro)){
                        $tabla = $tabla.
                        ' <tr>
                            <td>'.date('d-m-Y',strtotime($registro2['fecha'])).'</td>
                            <td>'.$registro2['clave'].'</td>';
                            if ($registro2['existencia'] <= $registro2['limite']-1) {
                                $tabla = $tabla.'<td scope="row">'.$registro2['articulo'].'</td>';
                            }else{
                                $tabla = $tabla.'<td>'.$registro2['articulo'].'</td>';
                            }
                            $tabla = $tabla.'<td>'.$registro2['unidad'].'</td>
                            <td>'.$registro2['existencia'].'</td>
                            <td>'.$registro2['limite'].'</td>
                            <td><a href=actualizar_stock.php?id='.$registro2['idstock'].'&idborrar=2"><img src="../img/act.png" alt="ACTUALIZAR" class="img-rounded"></a></td>
                            <td><a href=../php/eliminaciones/eliminar_almacen.php?id='.$registro2['idstock'].'&idborrar=2"><img src="../img/del.png" alt="ELIMINAR" class="img-rounded"></a></td>
                        </tr>';
                    } 
    $tabla = $tabla.'</table>';
    $array = array(0 => $tabla,1 => $lista);
    echo json_encode($array);
?>
