<?php
    include('../conexion.php');
    $paginaActual = $_POST['partida'];
    $nombre_Huerta = $_POST['dato'];
    $query = "SELECT idempleado, fecha, idpersona, idcargo,salario,huertas 
    from empleado where huertas like '%$nombre_Huerta%'";

    $nroProductos = mysqli_num_rows(mysqli_query($mysqli, $query));
    $nroLotes = 5;
    $nroPaginas = ceil($nroProductos/$nroLotes);
    $lista = '';
    $tabla = '';

    if($paginaActual > 1){
        $lista = $lista.' <li class="page-item"><a class="page-link"  href="javascript:pagination('.($paginaActual-1).');">Anterior</a></li>';
    }
    for($i=1; $i<=$nroPaginas; $i++){
        if($i == $paginaActual){
            $lista = $lista.'<li class="page-item active"><a class="page-link"  href="javascript:pagination('.$i.');">'.$i.'</a></li>';
        }else{
            $lista = $lista.'<li class="page-item"><a class="page-link"  href="javascript:pagination('.$i.');">'.$i.'</a></li>';
        }
    }
    if($paginaActual < $nroPaginas){
        $lista = $lista.'<li class="page-item"><a class="page-link"  href="javascript:pagination('.($paginaActual+1).');">Siguiente</a></li>';
    }

    if($paginaActual <= 1){
        $limit = 0;
    }else{
        $limit = $nroLotes*($paginaActual-1);
    }

    $query2 = "SELECT e.idempleado, date_format(e.fecha,'%d/%m/%Y') as fecha, e.idpersona, e.idcargo,
    e.salario, e.huertas, p.nombre as nombre, p.apellidop as apell1, p.apellidom as apell2, 
    c.cargo as labor
    from empleado e 
    inner join persona p on e.idpersona = p.idpersona
    inner join cargo c on e.idcargo = c.idcargo where e.huertas like '%$nombre_Huerta%' 
    order by labor ASC LIMIT $limit, $nroLotes";
    $registro = mysqli_query($mysqli, $query2);
    $s="$";

    $tabla = $tabla.'<table  class="table table-striped table-bordered text-center">
    <thead>
        <tr>
            <th>FECHA</th>
            <th>NOMBRE</th>
            <th>CARGO</th>
            <th>HUERTAS</th>
            <th>SALARIO</th>
            <th>EDITAR</th>
            <th>ELIMINAR</th>
        </tr>
    </thead>';

	while($registro2 = mysqli_fetch_array($registro)){
		$tabla = $tabla.'<tr>
            <td>'.$registro2['fecha'].'</td>
            <td>'.$registro2['nombre'].' '.$registro2['apell1'].' '.$registro2['apell2'].'</td>
            <td>'.$registro2['labor'].'</td>
            <td>'.$registro2['huertas'].'</td>
            <td> '.$s.$registro2['salario'].'</td>
            <td><a href=act_personal.php?id='.$registro2['idempleado'].'&idborrar=2"><img src="../img/act.png" alt="ACTUALIZAR" class="img-rounded"></a></td>
            <td><a href=../php/eliminaciones/eliminar_empleado.php?id='.$registro2['idempleado'].'&idborrar=2"><img src="../img/del.png" alt="ELIMINAR" class="img-rounded"></a></td>
		</tr>';		
	}
    $tabla = $tabla.'</table>';
    $array = array(0 => $tabla,1 => $lista);
    echo json_encode($array);
?>