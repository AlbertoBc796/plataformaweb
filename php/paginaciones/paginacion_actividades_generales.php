<?php
	include('../conexion.php');
	$paginaActual = $_POST['partida'];
    $query = "SELECT a.idactividades_general,a.fecha,a.semana,a.mes,a.rubro,
    concat(p.nombre,' ',p.apellidop,' ', p.apellidom) as nombrecompleto,cargo,salario,diat,pago_dia,
    c.idcategoria,c.nombre as nc,s.idsubcategoria,s.nombre as ns,a.detalle,a.idherramienta_segmento,
    a.producto as pp,a.cantidad,a.precio,a.importe,h.idhuerta,h.nombre as h_nombre from actividades_general a
    inner join persona p on p.idpersona = a.idpersona
    inner join huerta h on h.idhuerta = a.idhuerta
    inner join subcategoria s on s.idsubcategoria = a.idsubcategoria
    inner join categoria c on c.idcategoria = s.idcategoria  order by a.fecha desc ";
    $nroProductos = mysqli_num_rows(mysqli_query($mysqli, $query));
    $nroLotes = 20;
    $nroPaginas = ceil($nroProductos/$nroLotes);
    $lista = '';
    $tabla = '';
    if($paginaActual > 1){
        $lista = $lista.' <li class="page-item"><a class="page-link"  href="javascript:pagination('.($paginaActual-1).');">Anterior</a></li>';
    }
    for($i=1; $i<=$nroPaginas; $i++){
        if($i == $paginaActual){
            $lista = $lista.'<li class="page-item active"><a class="page-link"  href="javascript:pagination('.$i.');">'.$i.'</a></li>';
        }
    }
    if($paginaActual < $nroPaginas){
        $lista = $lista.'<li class="page-item"><a class="page-link"  href="javascript:pagination('.($paginaActual+1).');">Siguiente</a></li>';
    }
    if($paginaActual <= 1){
        $limit = 0;
    }else{
        $limit = $nroLotes*($paginaActual-1);
    }
    $query2 = "SELECT a.idactividades_general,date_format(a.fecha,'%d/%m/%Y') as 
	fecha,a.semana,a.mes,a.rubro,
    concat(p.nombre,' ',p.apellidop,' ', p.apellidom) as nombrecompleto,cargo,salario,diat,pago_dia,
    c.idcategoria,c.nombre as nc,s.idsubcategoria,s.nombre as ns,a.detalle,a.idherramienta_segmento,
    a.producto as pp,a.cantidad,a.precio,a.importe,h.idhuerta,h.nombre as h_nombre from actividades_general a
    inner join persona p on p.idpersona = a.idpersona
    inner join huerta h on h.idhuerta = a.idhuerta
    inner join subcategoria s on s.idsubcategoria = a.idsubcategoria
    inner join categoria c on c.idcategoria = s.idcategoria  order by a.fecha desc  LIMIT $limit, $nroLotes ";
    $registro = mysqli_query($mysqli, $query2);
    $tabla = $tabla.'<table  class="table table-striped table-bordered  table-responsive-md text-center">
                    <thead>
                    <tr>
                        <th class="oculto"  style="display:none;">FOLIO</th>
                        <th>FECHA</th>
                        <th>SEMANA</th>
                        <th>MES</th>
                        <th class="oculto"  style="display:none;">RUBRO</th>
                        <th>EMPLEADO</th>
                        <th>CARGO</th>
                        <th>SALARIO</th>
                        <th>DIA/T.</th>
                        <th>PAGO</th>
                        <th>ACTIVIDAD</th>
                        <th>SUB. ACTIVIDAD</th>
                        <th class="oculto"  style="display:none;">DETALLE</th>
                        <th class="oculto"  style="display:none;">HERRAMIENTA/SEGMENTO</th>
                        <th class="oculto"  style="display:none;">PRODUCTO</th>
                        <th class="oculto"  style="display:none;">CANTIDAD</th>
                        <th class="oculto"  style="display:none;">PRECIO</th>
                        <th class="oculto"  style="display:none;">IMPORTE</th>
                        <th>HUERTA</th>
                        <th>EDITAR</th>
                        <th>ELIMINAR</th>
                    </tr>
                    </thead>';
	while($registro2 = mysqli_fetch_array($registro)){
		$tabla = $tabla.'<tr>
		<td class="oculto"  style="display:none;">'.$registro2['idactividades_general'].'</td>
        <td>'.$registro2['fecha'].'</td>
        <td>'.$registro2['semana'].'</td>
        <td>'.$registro2['mes'].'</td>
        <td  class="oculto"  style="display: none;">'.$registro2['rubro'].'</td>

        <td>'.$registro2['nombrecompleto'].' </td>
        <td>'.$registro2['cargo'].'</td>
        <td>'.$registro2['salario'].'</td>
        <td>'.$registro2['diat'].'</td>
        <td>'.$registro2['pago_dia'].'</td>

        <td>'.$registro2['nc'].'</td>
        <td>'.$registro2['ns'].'</td>

        <td class="oculto"  style="display:none;">'.$registro2['detalle'].'</td>
        <td class="oculto"  style="display:none;">'.$registro2['idherramienta_segmento'].'</td>
        <td class="oculto"  style="display:none;">'.$registro2['pp'].'</td>
        <td class="oculto"   style="display:none;">'.$registro2['cantidad'].'</td>
        <td class="oculto"  style="display:none;">'.$registro2['precio'].'</td>
        <td class="oculto"  style="display:none;">'.$registro2['importe'].'</td>
        <td>'.$registro2['h_nombre'].'</td>

        <td><a href=actualizar_actividades_general.php?id='.$registro2['idactividades_general'].'&idborrar=2"><img src="../img/act.png" alt="ACTUALIZAR" class="img-rounded"></a></td>

        <td><a href=../php/eliminaciones/eliminar_actividades_general.php?id='.$registro2['idactividades_general'].'&idborrar=2"><img src="../img/del.png" alt="ACTUALIZAR" class="img-rounded"></a></td>';		
	}
    $tabla = $tabla.'</table>';
    $array = array(0 => $tabla,1 => $lista);
    echo json_encode($array);
?>