<?php 
    include('../conexion.php');
    $paginaActual = $_POST['partida'];
    $query = "SELECT * from mantenimientos";
    $nroProductos = mysqli_num_rows(mysqli_query($mysqli, $query));
    $nroLotes = 15;
    $nroPaginas = ceil($nroProductos/$nroLotes);
    $lista = '';
    $tabla = '';

    if($paginaActual > 1){
        $lista = $lista.' <li class="page-item"><a class="page-link"  href="javascript:pagination('.($paginaActual-1).');">Anterior</a></li>';
    }
    for($i=1; $i<=$nroPaginas; $i++){
        if($i == $paginaActual){
            $lista = $lista.'<li class="page-item active"><a class="page-link"  href="javascript:pagination('.$i.');">'.$i.'</a></li>';
        }else{
            $lista = $lista.'<li class="page-item"><a class="page-link"  href="javascript:pagination('.$i.');">'.$i.'</a></li>';
        }
    }
    if($paginaActual < $nroPaginas){
        $lista = $lista.'<li class="page-item"><a class="page-link"  href="javascript:pagination('.($paginaActual+1).');">Siguiente</a></li>';
    }

    if($paginaActual <= 1){
        $limit = 0;
    }else{
        $limit = $nroLotes*($paginaActual-1);
    }
    $query2 = "SELECT m.folio, date_format(m.fecha , '%d/%m/%Y') as fecha, m.unidad, m.medicion,
    m.idtipo, m.chofer, t.clave, m.idservicio, m.proveedor,m.gasto
    FROM mantenimientos m
    INNER JOIN transporte t ON t.idtransporte = m.unidad
    GROUP BY m.folio ORDER BY m.folio DESC
    LIMIT $limit, $nroLotes";
    $registro = mysqli_query($mysqli,$query2);

    $tabla = $tabla.'<table  class="table table-striped table-bordered  text-center">
    <thead>
        <tr>
            <th>FECHA</th>
            <th>FOLIO</th>
            <th>UNIDAD</th>
            <th>KM / HRS</th>
            <th>CHOFER</th>
            <th>PROVEEDOR</th>
            <th>GASTO</th>
            <th>ARTICULOS</th>
            <th>EDITAR</th>
            <th>ELIMINAR</th>
            <th>IMPRIMIR</th>
        </tr>
    </thead>';
    
    while ($a = mysqli_fetch_array($registro)) {
        $factura = $a['folio'];
        $query3 ="SELECT m.folio, m.refacciones, s.articulo,
        m.cantidad, s.preciounit
        from mantenimientos m
        inner join stock s on s.idstock = m.refacciones
        where m.folio = $factura";
        $registro2 = mysqli_query($mysqli,$query3);
        $articulos='';
        $gasto=0;
        while ($b = mysqli_fetch_array($registro2)) {
            $articulos .= $b['articulo'].', ';
            $gasto += $b['cantidad']*$b['preciounit'];
        }
        $articulos = substr($articulos,0,-2);
        if ($a['proveedor'] == '') {
            $proveedor = 'INTERNO';
        }else {
            $proveedor = $a['proveedor'];
        }
        if ($articulos == '') {
            $articulos = 'SERVICIO EXTERNO';
        }
        $factura = str_pad($factura, 4, "0", STR_PAD_LEFT);

        $tabla = $tabla.'<tr>
            <td>'.$a['fecha'].'</td>
            <td>'.$factura.'</td>
            <td>'.$a['clave'].'</td>
            <td>'.$a['medicion'].'</td>
            <td>'.$a['chofer'].'</td>
            <td>'.$proveedor.'</td>
            <td>$'.($a['gasto']+$gasto).'</td>
            <td>'.$articulos.'</td>
            <td><a href=act_mantenimiento.php?dato='.$a['folio'].'&idborrar=2"><img src="../img/act.png" alt="ACTUALIZAR" class="img-rounded"></a></td>
            <td><a href=../php/eliminaciones/eliminar_mantenimiento.php?dato='.$a['folio'].'&idborrar=2"><img src="../img/del.png" alt="ACTUALIZAR" class="img-rounded"></a></td>
            <td><a href=../php/impresiones/unidad.php?id='.$a['folio'].'&idborrar=2"><img src="../img/imp.png" alt="ACTUALIZAR" class="img-rounded"></a></td>
        </tr>';
    }
    $tabla = $tabla.'</table>';
    $array = array(0 => $tabla, 1 => $lista);

    echo json_encode($array);