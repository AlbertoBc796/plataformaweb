<?php
	include('../conexion.php');
	$paginaActual = $_POST['partida'];
    $query = "SELECT s.idsubcategoria,c.idcategoria,s.nombre as s,c.nombre as c from subcategoria s inner join categoria c on c.idcategoria = s.idcategoria  order by c.idcategoria ";
    $nroProductos = mysqli_num_rows(mysqli_query($mysqli, $query));
    $nroLotes = 20;
    $nroPaginas = ceil($nroProductos/$nroLotes);
    $lista = '';
    $tabla = '';

    if($paginaActual > 1){
        $lista = $lista.' <li class="page-item"><a class="page-link"  href="javascript:pagination('.($paginaActual-1).');">Anterior</a></li>';
    }
    for($i=1; $i<=$nroPaginas; $i++){
        if($i == $paginaActual){
            $lista = $lista.'<li class="page-item active"><a class="page-link"  href="javascript:pagination('.$i.');">'.$i.'</a></li>';
        }
    }
    if($paginaActual < $nroPaginas){
        $lista = $lista.'<li class="page-item"><a class="page-link"  href="javascript:pagination('.($paginaActual+1).');">Siguiente</a></li>';
    }
  
  	if($paginaActual <= 1){
  		$limit = 0;
  	}else{
  		$limit = $nroLotes*($paginaActual-1);
  	}

    $query2 = "SELECT s.idsubcategoria,c.idcategoria,s.nombre as s,c.nombre AS c from subcategoria s inner join categoria c on c.idcategoria = s.idcategoria  order by c.idcategoria LIMIT $limit, $nroLotes  ";

  	$registro = mysqli_query($mysqli, $query2);


  	$tabla = $tabla.'<table  class="table table-striped table-bordered text-center">
                    <thead>
			         <tr>
                    <th>CATEGORIA</th>
                    <th>SUBCATEGORIA</th>
                    <th>EDITAR</th>
                    <th>ELIMINAR</th>
			            </tr>
                        </thead>';
			
	while($registro2 = mysqli_fetch_array($registro)){

$tabla = $tabla.
		
        ' <tr>
            <td>'.$registro2['c'].'</td>
            <td>'.$registro2['s'].'</td>
      
        <td><a href=act_actividades_huertas.php?id='.$registro2['idsubcategoria'].'&idborrar=2"><img src="../img/act.png" alt="ACTUALIZAR" class="img-rounded"></a></td>

            <td><a href=../php/eliminaciones/eliminar_actividades_huertas.php?id='.$registro2['idsubcategoria'].'&idborrar=2"><img src="../img/del.png" alt="ELIMINAR" class="img-rounded"></a></td>
		 </tr>';		
	}
        
    $tabla = $tabla.'</table>';

    

    $array = array(0 => $tabla,
    			   1 => $lista);

    echo json_encode($array);
?>
