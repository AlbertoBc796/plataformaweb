<?php
    include('../conexion.php');
    $paginaActual = $_POST['partida'];
    $query = "SELECT g.idgasto, date_format(g.fecha , '%d/%m/%Y') as fecha, m.movimiento, 
    pa.pago, g.idmovimiento, g.idfactura,g.proveedor,g.cantidad,g.precio,
    g.idtransporte,t.clave,h.nombre as huerta,g.idhuerta, g.concepto,r.rubro
    FROM gastos g
    inner join movimiento m on m.idmovimiento = g.idmovimiento
    inner join rubro r on r.idrubro = g.idconcepto
    left join huerta h on h.idhuerta = g.idhuerta
    left join transporte t on t.idtransporte = g.idtransporte
    inner join pago pa on pa.idpago = g.idpago
    order by g.fecha DESC ";
    $nroProductos = mysqli_num_rows(mysqli_query($mysqli, $query));
    $nroLotes = 20;
    $nroPaginas = ceil($nroProductos/$nroLotes);
    $lista = '';
    $tabla = '';

    if($paginaActual > 1){
        $lista = $lista.' <li class="page-item"><a class="page-link"  href="javascript:pagination('.($paginaActual-1).');">Anterior</a></li>';
    }
    for($i=1; $i<=$nroPaginas; $i++){
        if($i == $paginaActual){
            $lista = $lista.'<li class="page-item active"><a class="page-link"  href="javascript:pagination('.$i.');">'.$i.'</a></li>';
        }else{
            $lista = $lista.'<li class="page-item"><a class="page-link"  href="javascript:pagination('.$i.');">'.$i.'</a></li>';
        }
    }
    if($paginaActual < $nroPaginas){
        $lista = $lista.'<li class="page-item"><a class="page-link"  href="javascript:pagination('.($paginaActual+1).');">Siguiente</a></li>';
    }

    if($paginaActual <= 1){
        $limit = 0;
    }else{
        $limit = $nroLotes*($paginaActual-1);
    }

    $query2 = "SELECT g.idgasto, date_format(g.fecha , '%d/%m/%Y') as fecha, m.movimiento, 
    pa.pago, g.idmovimiento, g.idfactura,g.proveedor,g.cantidad,g.precio,
    g.idtransporte,t.clave,h.nombre as huerta,g.idhuerta, g.concepto,r.rubro
    FROM gastos g
    inner join movimiento m on m.idmovimiento = g.idmovimiento
    inner join rubro r on r.idrubro = g.idconcepto
    left join huerta h on h.idhuerta = g.idhuerta
    left join transporte t on t.idtransporte = g.idtransporte
    inner join pago pa on pa.idpago = g.idpago
    order by g.fecha DESC LIMIT $limit, $nroLotes";
    $registro = mysqli_query($mysqli, $query2);
    
    $tabla = $tabla.'<table  class="table table-striped table-bordered  text-center">
    <thead>
        <tr>
            <th>FECHA</th>
            <th>FACTURA</th>
            <th>MOVIMIENTO</th>
            <th>PROVEEDOR</th>
            <th>CONCEPTO</th>
            <th>RUBRO</th>
            <th>CANTIDAD</th>
            <th>TOTAL</th>
            <th>DESTINO</th>
            <th>EDITAR</th>
            <th>ELIMINAR</th>
        </tr>
    </thead>';
    
    while($registro2 = mysqli_fetch_array($registro)){
        if ($registro2['clave']=='') {
            $brinco = '';
        }else{
            $brinco = '<br>';
        }
        $tabla = $tabla.'<tr>
        <td>'.$registro2['fecha'].'</td>
        <td>'.$registro2['idfactura'].'</td>
        <td>'.$registro2['movimiento'].'<br>'.$registro2['pago'].'</td>
        <td>'.$registro2['proveedor'].'</td>
        <td>'.$registro2['concepto'].'</td>
        <td>'.$registro2['rubro'].'</td>
        <td>'.$registro2['cantidad'].'</td>
        <td>'.'$'.number_format($registro2['precio']*$registro2['cantidad'],2).'</td>
        <td>'.$registro2['clave'].$brinco.$registro2['huerta'].'</td>
        <td><a href=act_gastos.php?id='.$registro2['idgasto'].'&idborrar=2"><img src="../img/act.png" alt="ACTUALIZAR" class="img-rounded"></a></td>
        <td><a href=../php/eliminaciones/eliminar_gasto.php?id='.$registro2['idgasto'].'&idborrar=2"><img src="../img/del.png" alt="ACTUALIZAR" class="img-rounded"></a></td>
        </tr>';       
    }
    $tabla = $tabla.'</table>';
    $array = array(0 => $tabla,1 => $lista);

    echo json_encode($array);
?>