<?php
    include('../conexion.php');
    $paginaActual = $_POST['partida'];
    $query = "SELECT  c.idcompra_venta,date_format(c.fecha , '%d/%m/%Y') as fecha,c.folio2,
    c.facturacion, m.movimiento, pa.pago,v.nombre,
    c.kilos_venta,c.precio_venta,c.total_venta,c.cliente
    from compra_venta c 
    inner join movimiento m on m.idmovimiento = c.idmovimiento
    inner join pago pa on pa.idpago = c.idpago
    inner join variedad v on v.idvariedad = c.idvariedad
    ORDER BY c.fecha DESC";
    $nroProductos = mysqli_num_rows(mysqli_query($mysqli, $query));
    $nroLotes = 20;
    $nroPaginas = ceil($nroProductos/$nroLotes);
    $lista = '';
    $tabla = '';

    if($paginaActual > 1){
        $lista = $lista.' <li class="page-item"><a class="page-link"  href="javascript:pagination('.($paginaActual-1).');">Anterior</a></li>';
    }
    for($i=1; $i<=$nroPaginas; $i++){
        if($i == $paginaActual){
            $lista = $lista.'<li class="page-item active"><a class="page-link"  href="javascript:pagination('.$i.');">'.$i.'</a></li>';
        }else{
            $lista = $lista.'<li class="page-item"><a class="page-link"  href="javascript:pagination('.$i.');">'.$i.'</a></li>';
        }
    }
    if($paginaActual < $nroPaginas){
        $lista = $lista.'<li class="page-item"><a class="page-link"  href="javascript:pagination('.($paginaActual+1).');">Siguiente</a></li>';
    }

    if($paginaActual <= 1){
        $limit = 0;
    }else{
        $limit = $nroLotes*($paginaActual-1);
    }

    $query2 = "SELECT c.idcompra_venta,date_format(c.fecha, '%d/%m/%Y') as fecha,c.folio2,
    c.facturacion, m.movimiento, pa.pago,v.nombre,
    c.kilos_venta,c.precio_venta,c.total_venta,c.cliente
    from compra_venta c 
    inner join movimiento m on m.idmovimiento = c.idmovimiento
    inner join pago pa on pa.idpago = c.idpago
    inner join variedad v on v.idvariedad = c.idvariedad
    ORDER BY c.fecha  desc LIMIT $limit, $nroLotes  ";
    $registro = mysqli_query($mysqli, $query2);

    $tabla = $tabla.'<table  class="table table-striped table-bordered  text-center">
    <thead>
        <tr>
            <th>FECHA</th>
            <th>REPORTE VENTA</th>
            <th>FACTURACION</th>
            <th>MOVIMIENTO </th>
            <th>PAGO</th>
            <th>PRODUCTO </th>
            <th>KILOS VENTA</th>
            <th>PRECIO VENTA</th>
            <th>TOTAL VENTA</th>
            <th>CLIENTE</th>                    
            <th>EDITAR</th>
            <th>ELIMINAR</th>
            <th>IMPRIMIR</th>
        </tr>
    </thead>';
    
    while($registro2 = mysqli_fetch_array($registro)){
        $tabla = $tabla.'<tr>
        <td>'.$registro2['fecha'].'</td>
        <td>'.$registro2['folio2'].'</td>
        <td>'.$registro2['facturacion'].'</td>
        <td>'.$registro2['movimiento'].'</td>
        <td>'.$registro2['pago'].'</td>
        <td>'.$registro2['nombre'].'</td>
        <td>'.number_format($registro2['kilos_venta']).'</td>
        <td>'.'$'.$registro2['precio_venta'].'</td>
        <td>'.'$'.number_format($registro2['total_venta'],2).'</td>
        <td>'.$registro2['cliente'].'</td>

        <td><a href=act_facturacion.php?id='.$registro2['idcompra_venta'].'&idborrar=2"><img src="../img/act.png" alt="ACTUALIZAR" class="img-rounded"></a></td>
        <td><a href=../php/eliminaciones/eliminar_factura.php?id='.$registro2['idcompra_venta'].'&idborrar=2"><img src="../img/del.png" alt="ACTUALIZAR" class="img-rounded"></a></td>
        <td><a href=../php/impresiones/factura_individual.php?id='.$registro2['idcompra_venta'].'&idborrar=2"><img src="../img/imp.png" alt="ACTUALIZAR" class="img-rounded"></a></td>
        </tr>';       
    }
    $tabla = $tabla.'</table>';
    $array = array(0 => $tabla,1 => $lista);

    echo json_encode($array);
?>