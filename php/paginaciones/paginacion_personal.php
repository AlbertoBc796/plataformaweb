<?php
	include('../conexion.php');
    $paginaActual = $_POST['partida'];
    
    $query = "SELECT e.idempleado,date_format( e.fecha,'%d/%m/%Y') as fecha,
    p.nombre as per,p.idpersona, 
    p.apellidop, p.apellidom, c.cargo,e.salario 
    FROM empleado e 
    inner join persona p on e.idpersona = p.idpersona 
    inner join cargo c on e.idcargo = c.idcargo ";
    $nroProductos = mysqli_num_rows(mysqli_query($mysqli, $query));
    $nroLotes = 20;
    $nroPaginas = ceil($nroProductos/$nroLotes);
    $lista = '';
    $tabla = '';

    if($paginaActual > 1){
        $lista = $lista.' <li class="page-item"><a class="page-link"  href="javascript:pagination('.($paginaActual-1).');">Anterior</a></li>';
    }
    for($i=1; $i<=$nroPaginas; $i++){
        if($i == $paginaActual){
            $lista = $lista.'<li class="page-item active"><a class="page-link"  href="javascript:pagination('.$i.');">'.$i.'</a></li>';
        }else{
            $lista = $lista.'<li class="page-item"><a class="page-link"  href="javascript:pagination('.$i.');">'.$i.'</a></li>';
        }
    }
    if($paginaActual < $nroPaginas){
        $lista = $lista.'<li class="page-item"><a class="page-link"  href="javascript:pagination('.($paginaActual+1).');">Siguiente</a></li>';
    }

    if($paginaActual <= 1){
        $limit = 0;
    }else{
        $limit = $nroLotes*($paginaActual-1);
    }

    $query2 = "SELECT e.idempleado, e.fecha, e.huertas,p.nombre as per,p.idpersona, 
    p.apellidop, p.apellidom, c.cargo, e.salario FROM empleado e 
    inner join persona p on e.idpersona = p.idpersona 
    inner join cargo c on e.idcargo = c.idcargo
    ORDER BY e.fecha ASC
    LIMIT $limit, $nroLotes ";
    $registro = mysqli_query($mysqli, $query2);
    $s="$";

    $tabla = $tabla.'<table  class="table table-striped table-bordered text-center">
    <thead>
        <tr>
            <th>FECHA</th>
            <th>NOMBRE</th>
            <th>CARGO</th>
            <th>HUERTAS</th>
            <th>SALARIO</th>
            <th>EDITAR</th>
            <th>ELIMINAR</th>
        </tr>
    </thead>';

	while($registro2 = mysqli_fetch_array($registro)){
        

		$tabla = $tabla.'<tr>
            <td>'.date('d-m-Y',strtotime($registro2['fecha'])).'</td>
            <td>'.$registro2['per'].' '.$registro2['apellidop'].' '.$registro2['apellidom'].'</td>
            <td>'.$registro2['cargo'].'</td>
            <td>'.$registro2['huertas'].'</td>
            <td> '.$s.$registro2['salario'].'</td>
            <td><a href=act_personal.php?id='.$registro2['idempleado'].'&idborrar=2"><img src="../img/act.png" alt="ACTUALIZAR" class="img-rounded"></a></td>
            <td><a href=../php/eliminaciones/eliminar_empleado.php?id='.$registro2['idempleado'].'&idborrar=2"><img src="../img/del.png" alt="ELIMINAR" class="img-rounded"></a></td>
		</tr>';		
	}
    $tabla = $tabla.'</table>';
    $array = array(0 => $tabla,1 => $lista);
    echo json_encode($array);
?>