<?php
	include('../conexion.php');
	$paginaActual = $_POST['partida'];
    $query = "SELECT idhuerta,date_format(fecha,'%d-%m-%Y') as fecha,clave,nombre,zona,hectareas,producto FROM huerta order by clave,nombre asc ";
    $nroProductos = mysqli_num_rows(mysqli_query($mysqli, $query));
    $nroLotes = 20;
    $nroPaginas = ceil($nroProductos/$nroLotes);
    $lista = '';
    $tabla = '';
    if($paginaActual > 1){
        $lista = $lista.' <li class="page-item"><a class="page-link"  href="javascript:pagination('.($paginaActual-1).');">Anterior</a></li>';
    }
    for($i=1; $i<=$nroPaginas; $i++){
        if($i == $paginaActual){
            $lista = $lista.'<li class="page-item active"><a class="page-link"  href="javascript:pagination('.$i.');">'.$i.'</a></li>';
        }else{
            $lista = $lista.'<li class="page-item"><a class="page-link"  href="javascript:pagination('.$i.');">'.$i.'</a></li>';
        }
    }
    if($paginaActual < $nroPaginas){
        $lista = $lista.'<li class="page-item"><a class="page-link"  href="javascript:pagination('.($paginaActual+1).');">Siguiente</a></li>';
    }
    if($paginaActual <= 1){
        $limit = 0;
    }else{
        $limit = $nroLotes*($paginaActual-1);
    }
    $query2 = "SELECT idhuerta,date_format(fecha,'%d-%m-%Y') as fecha,clave,nombre,zona,hectareas,producto FROM huerta order by clave,nombre asc LIMIT $limit, $nroLotes  ";
    $registro = mysqli_query($mysqli, $query2);
    $tabla = $tabla.'<table  class="table table-striped table-bordered text-center">
                <thead>
                    <tr>
                        <th>FECHA</th>
                        <th>CLAVE</th>
                        <th>NOMBRE</th>
                        <th>ZONA</th>
                        <th>HECTAREAS</th>
                        <th>PRODUCTO</th>
                        <th>EDITAR</th>
                        <th>ELIMINAR</th>
                    </tr>
                </thead>';
				
	while($registro2 = mysqli_fetch_array($registro)){
		$tabla = $tabla.'<tr>
        <td>'.$registro2['fecha'].'</td>
        <td>'.$registro2['clave'].'</td>
        <td>'.$registro2['nombre'].'</td>
        <td>'.$registro2['zona'].'</td>
        <td>'.$registro2['hectareas'].'</td>
        <td>'.$registro2['producto'].'</td>
        <td><a href=actualizar_huertas.php?id='.$registro2['idhuerta'].'&idborrar=2"><img src="../img/act.png" alt="ACTUALIZAR" class="img-rounded"></a></td>
        <td><a href=../php/eliminaciones/eliminar_huerta.php?id='.$registro2['idhuerta'].'&idborrar=2"><img src="../img/del.png" alt="ELIMINAR" class="img-rounded"></a></td>
		</tr>';		
	}  
    $tabla = $tabla.'</table>';
    $array = array(0 => $tabla,1 => $lista);
    echo json_encode($array);
?>
