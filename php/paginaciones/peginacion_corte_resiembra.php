<?php
	include('../conexion.php');
	$paginaActual = $_POST['partida'];

    $query = "SELECT hi.idhistorial_arboles, hi.fecha as f, h.nombre as n, hi.tabla as t,hi.cantidad as c from historial_arboles hi
inner join  arboles a on a.idarboles = hi.idarboles
inner join huerta h on h.idhuerta = a.idhuerta";

    $nroProductos = mysqli_num_rows(mysqli_query($mysqli, $query));
    $nroLotes = 20;
    $nroPaginas = ceil($nroProductos/$nroLotes);
    $lista = '';
    $tabla = '';
    if($paginaActual > 1){
        $lista = $lista.' <li class="page-item"><a class="page-link"  href="javascript:pagination('.($paginaActual-1).');">Anterior</a></li>';
    }
    for($i=1; $i<=$nroPaginas; $i++){
        if($i == $paginaActual){
            $lista = $lista.'<li class="page-item active"><a class="page-link"  href="javascript:pagination('.$i.');">'.$i.'</a></li>';
        }
    }
    if($paginaActual < $nroPaginas){
        $lista = $lista.'<li class="page-item"><a class="page-link"  href="javascript:pagination('.($paginaActual+1).');">Siguiente</a></li>';
    }
  
  	if($paginaActual <= 1){
  		$limit = 0;
  	}else{
  		$limit = $nroLotes*($paginaActual-1);
  	}
    $query2 = "
    SELECT hi.idhistorial_arboles, hi.fecha as f, h.nombre as n, hi.tabla as t,hi.cantidad as c from historial_arboles hi
inner join  arboles a on a.idarboles = hi.idarboles
inner join huerta h on h.idhuerta = a.idhuerta

  LIMIT $limit, $nroLotes   ";
  	$registro = mysqli_query($mysqli, $query2);

  	$tabla = $tabla.'<table  class="table table-striped table-bordered  text-center">
                    <thead>
			            <tr>
                    <th>FECHA</th>
                     <th>HUERTA</th>
                    <th>TABLA</th>
                    <th>CANTIDAD</th>  
                    <th>ELIMINAR</th>
			            </tr>
                        </thead>';
				
	while($registro2 = mysqli_fetch_array($registro)){
		$tabla = $tabla.'<tr>
          <td>'.$registro2['f'].'</td>
          <td>'.$registro2['n'].'</td>
          <td>'.$registro2['t'].'</td>
          <td>'.$registro2['c'].' </td>
         
            <td><a href=../php/eliminaciones/eliminar_historial_corte_resiembra.php?id='.$registro2['idhistorial_arboles'].'&idborrar=2"><img src="../img/del.png" alt="ELIMINAR" class="img-rounded"></a></td>
						  </tr>';		
	}    
    $tabla = $tabla.'</table>';
    $array = array(0 => $tabla,
    			   1 => $lista);
    echo json_encode($array);
?>