<?php
include('../conexion.php');
$paginaActual = $_POST['partida'];
$dato = $_POST['dato'];

$query = "SELECT c.cliente, c.idcompra_venta,date_format(c.fecha,'%d-%m-%Y') as fecha,
h.nombre as HUERTITA, c.kilos_corte,c.precio_corte,c.importe_corte, c.precio_flete,
c.importe_flete,c.egresos,c.kilos_venta,c.precio_venta,c.total_venta,
c.ingreso_total, p.nombre as producto, v.nombre as variedad
from compra_venta c
inner join huerta h on c.idhuerta=h.idhuerta
inner join producto p on p.idproducto = c.idproducto
inner join variedad v on v.idvariedad = c.idvariedad
WHERE h.idhuerta = $dato
ORDER BY c.fecha";
$nroProductos = mysqli_num_rows(mysqli_query($mysqli, $query));
$nroLotes = 5;
$nroPaginas = ceil($nroProductos/$nroLotes);
$lista = '';
$tabla = '';

if($paginaActual > 1){
    $lista = $lista.' <li class="page-item"><a class="page-link"  href="javascript:paginationCompra('.($paginaActual-1).');">Anterior</a></li>';
}
for($i=1; $i<=$nroPaginas; $i++){
    if($i == $paginaActual){
        $lista = $lista.'<li class="page-item active"><a class="page-link"  href="javascript:paginationCompra('.$i.');">'.$i.'</a></li>';
    }else{
        $lista = $lista.'<li class="page-item"><a class="page-link"  href="javascript:paginationCompra('.$i.');">'.$i.'</a></li>';
    }
}
if($paginaActual < $nroPaginas){
    $lista = $lista.'<li class="page-item"><a class="page-link"  href="javascript:paginationCompra('.($paginaActual+1).');">Siguiente</a></li>';
}
if($paginaActual <= 1){
    $limit = 0;
}else{
    $limit = $nroLotes*($paginaActual-1);
}

$sql = " SELECT c.idcompra_venta,date_format(c.fecha,'%d/%m/%Y') as 
	fecha,h.nombre,c.kilos_corte,c.precio_corte,c.importe_corte, c.precio_flete,c.idproducto,c.idvariedad,
	c.importe_flete,c.egresos,c.kilos_venta,c.precio_venta,c.total_venta,c.ingreso_total, p.nombre as producto, v.nombre as variedad
from compra_venta c 
inner join huerta h on c.idhuerta=h.idhuerta
inner join producto p on c.idproducto=p.idproducto
inner join variedad v on v.idvariedad = c.idvariedad

WHERE h.idhuerta = $dato
ORDER BY idcompra_venta DESC LIMIT $limit, $nroLotes";
$registro = mysqli_query($mysqli, $sql);

$tabla = $tabla.'<table class="table table-striped table-bordered text-center">
		<tr>
            <th class="text-center">FECHA</th>
            <th class="text-center">PRODUCTO</th>
            <th class="text-center">CORTE</th>
            <th class="text-center">$ CORTE</th>
            <th class="text-center">GASTO CORTE</th>
            <th class="text-center">EGRESOS</th>
            <th class="text-center">VENTA</th>
            <th class="text-center">$ VENTA</th>
            <th class="text-center">IMPORTE</th>
            <th class="text-center">IMPORTE NETO</th>
        </tr>';

	while($registro2 = mysqli_fetch_array($registro)){
        if ($registro2['producto'] == 'LIMON') {
            $unidad = 'Cajas';
        }else{
            $unidad = 'Kg';
        }
        $tabla = $tabla. '<tr>
                <td>'.$registro2['fecha'].'</td>
                <td>'.$registro2['producto'].' '.$registro2['variedad'].'</td>
				<td>'.$registro2['kilos_corte'].' '.$unidad.'</td>
				<td>'.'$'.$registro2['precio_corte'].'</td>
				<td>'.'$'.number_format($registro2['importe_corte'],2).'</td>
				<td>'.'$'.number_format($registro2['egresos'],2).'</td>
				<td>'.$registro2['kilos_venta'].' Kg'.'</td>
				<td>'.'$'.$registro2['precio_venta'].'</td>
				<td>'.'$'.number_format($registro2['total_venta'],2).'</td>
				<td>'.'$'.number_format($registro2['ingreso_total'],2).'</td>
				</tr>';
	}
    $tabla = $tabla. '</table>';
    $array = array(0 => $tabla,1 => $lista);
    echo json_encode($array);
?>