<?php
	include('../conexion.php');
	$paginaActual = $_POST['partida'];
    $query = "SELECT * FROM categoria";
    $nroProductos = mysqli_num_rows(mysqli_query($mysqli, $query));
    $nroLotes = 20;
    $nroPaginas = ceil($nroProductos/$nroLotes);
    $lista = '';
    $tabla = '';
    if($paginaActual > 1){
        $lista = $lista.' <li class="page-item"><a class="page-link"  href="javascript:pagination('.($paginaActual-1).');">Anterior</a></li>';
    }
    for($i=1; $i<=$nroPaginas; $i++){
        if($i == $paginaActual){
            $lista = $lista.'<li class="page-item active"><a class="page-link"  href="javascript:pagination('.$i.');">'.$i.'</a></li>';
        }
    }
    if($paginaActual < $nroPaginas){
        $lista = $lista.'<li class="page-item"><a class="page-link"  href="javascript:pagination('.($paginaActual+1).');">Siguiente</a></li>';
    }
  
  	if($paginaActual <= 1){
  		$limit = 0;
  	}else{
  		$limit = $nroLotes*($paginaActual-1);
  	}
    $query2 = "SELECT * FROM categoria LIMIT $limit, $nroLotes  ";
  	$registro = mysqli_query($mysqli, $query2);
    $s="$";
  	$tabla = $tabla.'<table  class="table table-striped table-bordered text-center">
                    <thead>
			            <tr>
			             <th>ACTIVIDAD</th>
                        <th>EDITAR</th>
                        <th>ELIMINAR</th>
			            </tr>
                        </thead>';
	while($registro2 = mysqli_fetch_array($registro)){
		$tabla = $tabla.'<tr>
        <td>'.$registro2['nombre'].'</td>
        <td><a href=act_actividad_general.php?id='.$registro2['idcategoria'].'&idborrar=2"><img src="../img/act.png" alt="ACTUALIZAR" class="img-rounded"></a></td>

            <td><a href=../php/eliminaciones/eliminar_actividad_general.php?id='.$registro2['idcategoria'].'&idborrar=2"><img src="../img/del.png" alt="ELIMINAR" class="img-rounded"></a></td>
						  </tr>';		
	}
    $tabla = $tabla.'</table>';
    $array = array(0 => $tabla,
    			   1 => $lista);
    echo json_encode($array);
?>
