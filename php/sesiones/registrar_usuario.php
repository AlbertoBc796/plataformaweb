<?php
if (isset($_POST['reg-submit'])) {
	require("../conexion.php");
	$usuario = $_POST['usuario'];
	$password = $_POST['password'];
	$password2 = $_POST['password2'];
	if (empty($usuario) || empty($password) || empty($password2)){
		header("Location: ../../vistas/registrar_usuario.php?error=1&usuario=".$usuario); //1 = Empty fields
		exit();
	}elseif ($password !== $password2) {
		header("Location: ../../vistas/registrar_usuario.php?error=2&usuario=".$usuario); //2 = Incorrect pass
		exit();
	}else {
		$consulta="SELECT usuario FROM usuarios WHERE usuario = ?";
		$sen = mysqli_stmt_init($mysqli);
		if (!mysqli_stmt_prepare($sen,$consulta)) {
			header("Location: ../../vistas/registrar_usuario.php?error=3"); //3 = SQL problem
			exit();
		}else{
			mysqli_stmt_bind_param($sen,"s",$usuario);
			mysqli_stmt_execute($sen);
			mysqli_stmt_store_result($sen);
			$resultCheck = mysqli_stmt_num_rows($sen);
			if ($resultCheck > 0) {
				header("Location: ../../vistas/registrar_usuario.php?error=4"); //4 = User taken
				exit();
			}
			else {
				$proc="INSERT into usuarios (usuario,pass) values (?,?)";
				$sentencia = mysqli_stmt_init($mysqli);
				if (!mysqli_stmt_prepare($sentencia,$proc)) {
					header("Location: ../../vistas/registrar_usuario.php?error=3"); //3
					exit();
				}else {
					$hashedPwd = password_hash($password,PASSWORD_DEFAULT);
					mysqli_stmt_bind_param($sentencia,"ss",$usuario,$hashedPwd);
					mysqli_stmt_execute($sentencia);
					header("Location: ../../vistas/registrar_usuario.php?signup=succes"); //
					exit();
				}
			}
		}
	}
	mysqli_stmt_close($sentencia);
	mysqli_close($mysqli);
}else {
	header("Location: ../../vistas/registrar_usuario.php");
	exit();
}
?>