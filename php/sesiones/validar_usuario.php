<?php
date_default_timezone_set("America/Mexico_City");
if (isset($_POST['login-sbmt'])) {
	require("../conexion.php");
	$username=$_POST['user'];
	$password=$_POST['password'];
	if (empty($username) || empty($password)) {
		header("Location: ../../index.php?error=1");//1
		exit();
	}else {
		$query="SELECT * FROM usuarios WHERE usuario = ? ";
		$sentencia=mysqli_stmt_init($mysqli);
		if (!mysqli_stmt_prepare($sentencia,$query)) {
			header("Location: ../../index.php?error=3");//3
			exit();
		}else {
			mysqli_stmt_bind_param($sentencia,"s",$username);
			mysqli_stmt_execute($sentencia);
			$resultado = mysqli_stmt_get_result($sentencia);
			if ($row = mysqli_fetch_assoc($resultado)) {
				$pwdCheck = password_verify($password,$row['pass']);
				if ($pwdCheck == false) {
					header("Location: ../../index.php?error=2");//2
					exit();
				}elseif ($pwdCheck == true) {
					if(!isset($_SESSION)){
						session_start();
						if ($row['tipousuario'] > 0) {
							$_SESSION['id']=$row['idusuarios'];
							$_SESSION['tipo']=$row['tipousuario'];
							$_SESSION['user']=$row['usuario'];

							$fecha_hoy = date("Y-m-d H:i:s");
							$maquina = gethostbyaddr($_SERVER['REMOTE_ADDR']);
							$proc=" INSERT INTO historial_users (fecha,idusuario,actividad,equipo) values ('$fecha_hoy','".$_SESSION['id']."','Inicio sesión','$maquina')";
							$sentencia=mysqli_query($mysqli,$proc);

							echo "<script>location.href='../../vistas/compra_venta.php'</script>";
						}else{
							header("Location: ../../index.php?error=5");//5
						}
						
					}
				}else {
					header("Location: ../../index.php?error=2");
					exit();
				}
			}else {
				header("Location: ../../index.php?error=6");//6
				exit();
			}
		}
	}
}else{
	header("Location: ../../index.php");
	exit();
}
?>