<?php
function errorCase($caso)
{
    $mensaje = '';
    switch($caso){
        case 1:
            $mensaje = 'Por favor llene todos los espacios.';
            break;
        case 2:
            $mensaje = 'Contraseña no coinciden.';
            break;
        case 3:
            $mensaje = 'Problema con la base de datos...';
            break;
        case 4:
            $mensaje = 'Usuario existente...';
            break;
        case 5:
            $mensaje = 'Usuario no autorizado';
            break;
        case 6:
            $mensaje = 'Ingrese un usuario valido';
            break;
    }
    return $mensaje;
}

function createNotification($usuario1,$nombreUser,$notificaciones,$tipoN)
{
    $mensaje = '';
    switch($tipoN){
        case 1:
            $mensaje = "<a class=\"dropdown-item\" href=\"compra_venta.php\" onclick='update( \"".$usuario1."\",\"".$tipoN."\")' ><i class=\"far fa-money-bill-alt\"></i>".' '.$nombreUser.' registro '.$notificaciones." Compra-Venta</a>";
            break;
        case 2:
            $mensaje = "<a class=\"dropdown-item\" href=\"actividades_general.php\" onclick='update( \"".$usuario1."\",\"".$tipoN."\")' ><i class=\"fas fa-user-tag\"></i>".' '.$nombreUser.' registro '.$notificaciones." Actividades generales</a>";
            break;
        case 3:
            $mensaje = "<a class=\"dropdown-item\" href=\"historial.php\" onclick='update( \"".$usuario1."\",\"".$tipoN."\")' ><i class=\"fas fa-toolbox\"></i>".' '.$nombreUser.' registro '.$notificaciones." Productos en el almacen</a>";
            break;
        case 4:
            $mensaje = "<a class=\"dropdown-item\" href=\"historial.php\" onclick='update( \"".$usuario1."\",\"".$tipoN."\")' ><i class=\"fas fa-toolbox\"></i>".' '.$nombreUser.' registro '.$notificaciones." Entrada al almacen</a>";
            break;
        case 5:
            $mensaje = "<a class=\"dropdown-item\" href=\"historial.php\" onclick='update( \"".$usuario1."\",\"".$tipoN."\")' ><i class=\"fas fa-toolbox\"></i>".' '.$nombreUser.' registro '.$notificaciones." Salida del almacen</a>";
            break;
        case 6:
            $mensaje = "<a class=\"dropdown-item\" href=\"mantenimiento.php\" onclick='update( \"".$usuario1."\",\"".$tipoN."\")' ><i class=\"fas fa-wrench\"></i>".' '.$nombreUser.' registro '.$notificaciones." Mantenimiento</a>";
            break;
        case 7:
            $mensaje = "<a class=\"dropdown-item\" href=\"gastos_generales.php\" onclick='update( \"".$usuario1."\",\"".$tipoN."\")' ><i class=\"fas fa-clipboard\"></i>".' '.$nombreUser.' registro '.$notificaciones." Gasto indirecto</a>";
            break;
    }
    return $mensaje;
    //<i class="far fa-money-bill-alt"></i>
}