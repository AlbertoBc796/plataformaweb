

CREATE TABLE `actividades_general` (
  `idactividades_general` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` date NOT NULL,
  `semana` varchar(45) COLLATE utf8_spanish2_ci NOT NULL,
  `mes` varchar(45) COLLATE utf8_spanish2_ci NOT NULL,
  `rubro` varchar(45) COLLATE utf8_spanish2_ci NOT NULL,
  `idpersona` int(11) NOT NULL,
  `cargo` varchar(45) COLLATE utf8_spanish2_ci NOT NULL,
  `salario` float(10,2) NOT NULL,
  `diat` float(10,2) NOT NULL,
  `pago_dia` float(10,2) NOT NULL,
  `idcategoria` int(11) NOT NULL,
  `idsubcategoria` int(11) NOT NULL,
  `detalle` varchar(45) COLLATE utf8_spanish2_ci NOT NULL,
  `idherramienta_segmento` int(11) NOT NULL,
  `area` varchar(45) COLLATE utf8_spanish2_ci NOT NULL,
  `producto` varchar(45) COLLATE utf8_spanish2_ci NOT NULL,
  `cantidad` float NOT NULL,
  `precio` float NOT NULL,
  `importe` float NOT NULL,
  `idhuerta` int(11) NOT NULL,
  PRIMARY KEY (`idactividades_general`),
  KEY `fk_persona_idx` (`idpersona`),
  KEY `fk_per_id_idx` (`idpersona`),
  KEY `fk_herr_id_idx` (`idherramienta_segmento`),
  KEY `fk_subcat_id_idx` (`idsubcategoria`),
  KEY `fk_hut_idx` (`idhuerta`),
  CONSTRAINT `fk_herr_id` FOREIGN KEY (`idherramienta_segmento`) REFERENCES `herramienta_segmento` (`idherramienta_segmento`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_hut` FOREIGN KEY (`idhuerta`) REFERENCES `huerta` (`idhuerta`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_per_id` FOREIGN KEY (`idpersona`) REFERENCES `persona` (`idpersona`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_subcat_id` FOREIGN KEY (`idsubcategoria`) REFERENCES `subcategoria` (`idsubcategoria`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

INSERT INTO actividades_general VALUES("10","2018-11-24","SEMANA 47","NOVIEMBRE ","NO","1","INGENIERO","300.00","1.67","501.00","3","1","NO","1","1","1","1","1","1","15");
INSERT INTO actividades_general VALUES("11","2018-11-25","SEMANA 47","NOVIEMBRE ","NO","1","INGENIERO","300.00","0.33","99.00","3","1","NO","3","1","NARANJA","1000","2000","30000","20");
INSERT INTO actividades_general VALUES("13","2018-12-03","SEMANA 49","DICIEMBRE","NO","1","INGENIERO","300.00","1.00","300.00","3","1","NA","1","2","2","2","2","2","10");
INSERT INTO actividades_general VALUES("14","2018-11-26","SEMANA 48","NOVIEMBRE ","NA","1","INGENIERO","300.00","2.00","600.00","3","1","NO","3","NO","NARANJA","1","1","1","23");



CREATE TABLE `cargo` (
  `idcargo` int(11) NOT NULL AUTO_INCREMENT,
  `cargo` varchar(45) COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`idcargo`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

INSERT INTO cargo VALUES("2","INGENIERO");



CREATE TABLE `categoria` (
  `idcategoria` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`idcategoria`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

INSERT INTO categoria VALUES("3","CONTROL FITOSANITARIO");



CREATE TABLE `compra_venta` (
  `idcompra_venta` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` date NOT NULL,
  `idhuerta` int(11) NOT NULL,
  `cuadrilla` varchar(45) CHARACTER SET latin1 NOT NULL,
  `tabla` varchar(45) CHARACTER SET latin1 NOT NULL,
  `camion` varchar(45) CHARACTER SET latin1 NOT NULL,
  `marca` varchar(45) CHARACTER SET latin1 NOT NULL,
  `color` varchar(45) CHARACTER SET latin1 NOT NULL,
  `placas` varchar(45) CHARACTER SET latin1 NOT NULL,
  `chofer` varchar(45) CHARACTER SET latin1 NOT NULL,
  `folio1` varchar(45) COLLATE utf8_spanish2_ci NOT NULL,
  `kilos_corte` float(10,0) NOT NULL,
  `precio_corte` float(10,2) NOT NULL,
  `importe_corte` float(10,2) NOT NULL,
  `pesaje` float(10,2) NOT NULL,
  `precio_flete` float(10,2) NOT NULL,
  `importe_flete` float(10,2) NOT NULL,
  `egresos` float(10,2) NOT NULL,
  `folio2` varchar(45) COLLATE utf8_spanish2_ci NOT NULL,
  `kilos_venta` float(10,0) NOT NULL,
  `precio_venta` float(10,2) NOT NULL,
  `total_venta` float(10,2) NOT NULL,
  `ingreso_total` float(10,2) NOT NULL,
  `cliente` varchar(45) COLLATE utf8_spanish2_ci NOT NULL,
  `idproducto` int(11) NOT NULL,
  `idvariedad` int(11) NOT NULL,
  `folion` varchar(45) COLLATE utf8_spanish2_ci NOT NULL,
  `facturacion` varchar(45) COLLATE utf8_spanish2_ci NOT NULL,
  `idmovimiento` int(11) NOT NULL,
  `idpago` int(11) NOT NULL,
  PRIMARY KEY (`idcompra_venta`),
  KEY `compra_venta_huerta_idx` (`idhuerta`),
  KEY `fk_variedad_idx` (`idvariedad`),
  KEY `fk_movi_idx` (`idmovimiento`),
  KEY `fk_pagoo_idx` (`idpago`),
  KEY `fk_producto1_idx` (`idproducto`),
  CONSTRAINT `compra_venta_huerta` FOREIGN KEY (`idhuerta`) REFERENCES `huerta` (`idhuerta`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_movi` FOREIGN KEY (`idmovimiento`) REFERENCES `movimiento` (`idmovimiento`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_pagoo` FOREIGN KEY (`idpago`) REFERENCES `pago` (`idpago`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_producto1` FOREIGN KEY (`idproducto`) REFERENCES `producto` (`idproducto`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_variedad` FOREIGN KEY (`idvariedad`) REFERENCES `variedad` (`idvariedad`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

INSERT INTO compra_venta VALUES("8","2018-10-03","11","JOSE HERNANDEZ BELLO","NA","CAMIONETA","FORD","BLANCA","XU34283","ALFONSO MENDOZA GIL","1733","4500","0.35","1575.00","0.00","0.00","0.00","1575.00","1021","3830","2.50","9575.00","8000.00","MERCADO NACIONAL","15","32","NA","NA","5","8");
INSERT INTO compra_venta VALUES("9","2018-10-03","11","NA","NA","CAMIONETA","FORD","BLANCA","XU34283","ALFONSO MENDOZA GIL","1733","151","1.00","151.00","0.00","0.00","0.00","151.00","1021","350","0.80","280.00","129.00","MERCADO NACIONAL","15","32","NA","NA","5","8");
INSERT INTO compra_venta VALUES("10","2018-10-15","11","JOSE HERNANDEZ BELLO","NA","CAMIONETA","FORD","BLANCA","XU34283","ALFONSO MENDOZA GIL","1752","3670","0.40","1468.00","0.00","0.00","0.00","1468.00","1045","3230","2.70","8721.00","7253.00","MERCADO NACIONAL","15","32","NA","NA","5","8");
INSERT INTO compra_venta VALUES("11","2018-10-16","12","NA","NA","CAMIONETA","FORD","BLANCA","XU34283","ALFONSO MENDOZA GIL","NA","0","0.00","0.00","0.00","0.00","0.00","0.00","1046","2520","1.00","2520.00","2520.00","MERCADO NACIONAL","15","33","NA","NA","5","8");
INSERT INTO compra_venta VALUES("12","2018-10-12","12","ISMAEL MARTINEZ","NA","TORTON","INTERNACIONAL","BLANCO-AZUL","XU75283","MATEO","NA","0","0.00","0.00","0.00","0.00","0.00","0.00","1041","19880","1.30","25844.00","25844.00","MERCADO NACIONAL","15","33","NA","NA","5","8");
INSERT INTO compra_venta VALUES("13","2018-10-04","12","NA","NA","TORTON","DODGE","ROJO","HK5151F","MATEO MARIN","NA","19860","0.00","0.00","0.00","0.00","0.00","0.00","NA","19860","1.30","25818.00","25818.00","JESUS SALVADOR HDZ. TORRE","15","33","NA","MEN","4","7");
INSERT INTO compra_venta VALUES("14","2018-09-29","12","NA","NA","TORTON","DINA","BCO-AMARILLO","HH0734F","ANGEL CAZARES MORA","NA","0","0.00","0.00","0.00","0.00","0.00","0.00","1015","19660","1.30","25558.00","25558.00","MATEO MARIN","15","33","463","MEN","4","6");
INSERT INTO compra_venta VALUES("15","2018-10-02","22","NA","NA","TORTON","DODGE","ROJO","HK5151F","JOSE ANTONIO","1","10","1.00","10.00","1.00","1.00","10.00","21.00","1","10","1.00","10.00","-11.00","MERCADO NACIONAL","16","35","1","NA","4","6");



CREATE TABLE `compra_venta_limon` (
  `idcompra_venta_limon` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` date NOT NULL,
  `idhuerta` int(11) NOT NULL,
  `cuadrilla` varchar(45) COLLATE utf8_spanish2_ci NOT NULL,
  `tabla` varchar(45) COLLATE utf8_spanish2_ci NOT NULL,
  `camion` varchar(45) COLLATE utf8_spanish2_ci NOT NULL,
  `marca` varchar(45) COLLATE utf8_spanish2_ci NOT NULL,
  `color` varchar(45) COLLATE utf8_spanish2_ci NOT NULL,
  `placas` varchar(45) COLLATE utf8_spanish2_ci NOT NULL,
  `chofer` varchar(45) COLLATE utf8_spanish2_ci NOT NULL,
  `folio1` varchar(45) COLLATE utf8_spanish2_ci NOT NULL,
  `cajas_corte` float(10,0) NOT NULL,
  `precio_corte` float(10,2) NOT NULL,
  `importe_corte` float(10,2) NOT NULL,
  `pesaje` float(10,2) NOT NULL,
  `precio_flete` float(10,2) NOT NULL,
  `importe_flete` float(10,2) NOT NULL,
  `egresos` float(10,2) NOT NULL,
  `folio2` varchar(45) COLLATE utf8_spanish2_ci NOT NULL,
  `c_venta` float(10,1) NOT NULL,
  `merma` float(10,1) NOT NULL,
  `cajas_venta` float(10,0) NOT NULL,
  `precio_venta` float(10,2) NOT NULL,
  `total_venta` float(10,2) NOT NULL,
  `ingreso_total` float(10,2) NOT NULL,
  `cliente` varchar(45) COLLATE utf8_spanish2_ci NOT NULL,
  `idproducto` int(11) NOT NULL,
  `idvariedad` int(11) NOT NULL,
  `folion` varchar(45) COLLATE utf8_spanish2_ci NOT NULL,
  `facturacion` varchar(45) COLLATE utf8_spanish2_ci NOT NULL,
  `idmovimiento` int(11) NOT NULL,
  `idpago` int(11) NOT NULL,
  PRIMARY KEY (`idcompra_venta_limon`),
  KEY `compra_venta_huerta2_idx` (`idhuerta`),
  KEY `fk_movimiento2_idx` (`idmovimiento`),
  KEY `fk_pago2_idx` (`idpago`),
  KEY `fk_producto2_idx` (`idproducto`),
  KEY `fk_variedad2_idx` (`idvariedad`),
  CONSTRAINT `compra_venta_huerta2` FOREIGN KEY (`idhuerta`) REFERENCES `huerta` (`idhuerta`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_movimiento2` FOREIGN KEY (`idmovimiento`) REFERENCES `movimiento` (`idmovimiento`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_pago2` FOREIGN KEY (`idpago`) REFERENCES `pago` (`idpago`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_producto2` FOREIGN KEY (`idproducto`) REFERENCES `producto` (`idproducto`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_variedad2` FOREIGN KEY (`idvariedad`) REFERENCES `variedad` (`idvariedad`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

INSERT INTO compra_venta_limon VALUES("2","2018-10-01","10","NA","NA","TORTON","DINA","AZUL","XU34281","ODILON VENTURA MACIAS","1728","170","30.00","5100.00","0.00","0.00","0.00","5100.00","1016","170.0","5.0","4215","0.00","0.00","-5100.00","CITRICOS CADILLO SA DE CV","14","34","NA","AG","4","6");
INSERT INTO compra_venta_limon VALUES("3","2018-10-05","10","ANTONIO PULIDO UTRERA","NA","RABON","FORD","ROJO","XU34282","CANDIDO MURRIETA LEON","1736","94","30.00","2820.00","100.00","0.00","0.00","2920.00","1025","65.0","29.0","1610","0.00","0.00","-2920.00","CITRICOS CADILLO SA DE CV","14","34","NA","AG","4","6");
INSERT INTO compra_venta_limon VALUES("4","2018-10-04","10","ANTONIO PULIDO UTRERA","NA","RABON","FORD","ROJO","XU34282","CANDIDO MURRIETA LEON","1734","239","30.00","7170.00","100.00","0.00","0.00","7270.00","1022","235.0","4.0","5855","0.00","0.00","-7270.00","CITRICOS CADILLO SA DE CV","14","34","NA","AG","4","6");
INSERT INTO compra_venta_limon VALUES("5","2018-10-11","10","NA","NA","CAMIONETA","FORD","BLANCA","XU34283","ALFONSO MENDOZA GIL","NA","0","0.00","0.00","0.00","0.00","0.00","0.00","1027","42.0","0.0","1050","1.60","1680.00","1680.00","MERCADO NACIONAL","14","34","NA","NA","5","8");
INSERT INTO compra_venta_limon VALUES("6","2018-10-15","10","ANTONIO PULIDO UTRERA","NA","RABON","FORD","ROJO","XU34282","CANDIDO MURRIETA LEON","1753","48","30.00","1440.00","100.00","0.00","0.00","1540.00","1043","48.0","1169.0","0","0.00","0.00","-1540.00","CITRICOS CADILLO","14","34","NA","AG","4","6");
INSERT INTO compra_venta_limon VALUES("7","2018-10-15","10","ELPIDIO VENTURA VALENCIA","NA","TORTON","DINA","AZUL","XU34281","ODILON VENTURA MACIAS","1754","107","30.00","3210.00","100.00","0.00","0.00","3310.00","1044","105.0","2.0","20630","0.00","0.00","-3310.00","CITRICOS CADILLO SA DE CV","14","34","NA","AG","4","6");
INSERT INTO compra_venta_limon VALUES("8","2018-10-02","13","NA","NA","RABON","FORD","ROJO","XU34282","CANDIDO MURRIETA LEON","1729","90","30.00","2700.00","60.00","0.00","0.00","2760.00","1017","89.0","1.0","2209","0.00","0.00","-2760.00","CITRICOS CADILLO SA DE CV","14","34","NA","AG","4","6");
INSERT INTO compra_venta_limon VALUES("9","2018-10-03","13","ANTONIO PULIDO UTRERA","NA","RABON","FORD","ROJO","XU34282","CANDIDO MURRIETA LEON","1731","15","30.00","450.00","0.00","0.00","0.00","450.00","1019","137.0","0.0","373","0.00","0.00","-450.00","CITRICOS CADILLO SA DE CV","14","34","NA","AG","4","6");
INSERT INTO compra_venta_limon VALUES("10","2018-10-02","14","ANTONIO PULIDO UTRERA","NA","RABON","FORD","ROJO","XU34282","CANDIDO MURRIETA LEON","1729","8","30.00","240.00","10.00","0.00","0.00","250.00","1017","8.0","0.0","198","0.00","0.00","-250.00","CITRICOS CADILLO SA DE CV","14","34","NA","AG","4","6");
INSERT INTO compra_venta_limon VALUES("11","2018-10-02","15","CANDIDO MURRIETA LEON","NA","RABON","FORD","ROJO","XU34282","CANDIDO MURRIETA LEON","1729","16","30.00","480.00","30.00","0.00","0.00","510.00","1017","16.0","0.0","397","0.00","0.00","-510.00","CITRICOS CADILLO SA DE CV","14","34","NA","AG","4","6");
INSERT INTO compra_venta_limon VALUES("12","2018-10-02","16","ELPIDIO VENTURA VALENCIA","NA","TORTON","DINA","AZUL","XU34281","ODILON VENTURA MACIAS","1730","216","30.00","6480.00","0.00","0.00","0.00","6480.00","1018","213.0","3.0","5454","0.00","0.00","-6480.00","CITRICOS CADILLO SA DE CV","14","34","NA","AG","4","6");
INSERT INTO compra_venta_limon VALUES("13","2018-10-03","16","ANTONIO PULIDO UTRERA","NA","RABON","FORD","ROJO","XU34282","CANDIDO MURRIETA LEON","1731","123","30.00","3690.00","100.00","0.00","0.00","3790.00","1019","122.0","1.0","3038","0.00","0.00","-3790.00","CITRICOS CADILLO SA DE CV","14","34","NA","AG","4","6");
INSERT INTO compra_venta_limon VALUES("14","2018-10-03","16","ELPIDIOTVENTURA VALENCIA","NA","TORTON","DINA","AZUL","XU34281","ODILON VENTURA MACIAS","1732","215","30.00","6450.00","100.00","0.00","0.00","6550.00","1020","213.0","2.0","5534","0.00","0.00","-6550.00","CITRICOS CADILLO SA DE CV","14","34","NA","AG","4","6");
INSERT INTO compra_venta_limon VALUES("15","2018-10-11","20","ANTONIO PULIDO UTRERA","NA","RABON","FORD","ROJO","XU34282","CANDIDO MURRIETA LEON","1746","91","30.00","2730.00","55.00","0.00","0.00","2785.00","1038","89.0","2.0","0","0.00","0.00","-2785.00","CITRICOS CADILLO SA DE CV","14","34","NA","AG","4","6");
INSERT INTO compra_venta_limon VALUES("16","2018-10-11","20","ELPIDIO VENTURA VALENCIA","NA","TORTON","DINA","AZUL","XU34281","ODILON VENTURA MACIAS","1749","125","30.00","3750.00","100.00","0.00","0.00","3850.00","1039","125.0","0.0","3135","0.00","0.00","-3850.00","CITRICOS CADILLO SA DE CV","14","34","NA","AG","4","6");
INSERT INTO compra_venta_limon VALUES("17","2018-10-12","20","ANTONIO PULIDO UTRERA","NA","RABON","FORD","ROJO","XU34282","CANDIDO MURRIETA LEON","1750","65","30.00","1950.00","50.00","0.00","0.00","2000.00","1040","64.0","1.0","1559","0.00","0.00","-2000.00","CITRICOS CADILLO SA DE CV","14","34","NA","AG","4","6");
INSERT INTO compra_venta_limon VALUES("18","2018-10-11","21","ANTONIO PULIDO UTRERA","NA","CAMIONETA","FORD","BLANCA","XU34283","ALFONSO MENDOZA GIL","1748","7","30.00","210.00","0.00","0.00","0.00","210.00","1039","7.0","0.0","170","0.00","0.00","-210.00","CITRICOS CADILLO SA DE CV","14","34","NA","AG","4","6");
INSERT INTO compra_venta_limon VALUES("19","2018-10-12","22","ANTONIO PULIDO UTRERA","NA","RABON","FORD","ROJO","XU34282","CANDIDO MURRIETA LEON","1750","58","30.00","1740.00","50.00","0.00","0.00","1790.00","1040","58.0","0.0","1412","0.00","0.00","-1790.00","CITRICOS CADILLO SA DE CV","14","34","NA","AG","4","6");
INSERT INTO compra_venta_limon VALUES("20","2018-10-13","24","ANTONIO PULIDO UTRERA","NA","RABON","FORD","ROJO","XU34282","CANDIDO MURRIETA LEON","1751","25","30.00","750.00","50.00","0.00","0.00","800.00","1042","25.0","0.0","688","0.00","0.00","-800.00","CITRICOS CADILLO SA DE CV","14","34","NA","AG","4","6");
INSERT INTO compra_venta_limon VALUES("21","2018-10-13","23","ANTONIO PULIDO UTRERA","NA","RABON","FORD","ROJO","XU34282","CANDIDO MURRIETA LEON","1751","54","30.00","1620.00","50.00","0.00","0.00","1670.00","1042","53.0","1.0","1291","0.00","0.00","-1670.00","CITRICOS CADILLO SA DE CV","14","34","NA","AG","4","6");
INSERT INTO compra_venta_limon VALUES("22","2018-10-05","16","ELPIDIO VENTURA VALENCIA","NA","TORTON","DINA","AZUL","XU34281","ODILON VENTURA MACIAS","1737","47","30.00","1410.00","30.00","0.00","0.00","1440.00","1026","46.0","0.0","1175","0.00","0.00","-1440.00","CITRICOS CADILLO SA DE CV","14","34","NA","AG","4","6");
INSERT INTO compra_venta_limon VALUES("23","2018-10-04","16","ELPIDIO VENTURA VALENCIA","NA","TORTON","DINA","AZUL","XU34281","ODILON VENTURA MACIAS","1735","227","30.00","6810.00","100.00","0.00","0.00","6910.00","210123","225.0","2.0","5775","0.00","0.00","-6910.00","CITRICOS CADILLO SA DE CV","14","34","NA","AG","4","6");
INSERT INTO compra_venta_limon VALUES("24","2018-10-25","16","ANTONIO PULIDO UTRERA","NA","RABON","FORD","ROJO","XU34282","CANDIDO MURRIETA LEON","1763","98","30.00","2940.00","100.00","0.00","0.00","3040.00","1056","97.0","1.0","2411","0.00","0.00","-3040.00","CITRICOS CADILLO SA DE CV","14","34","NA","NA","4","6");
INSERT INTO compra_venta_limon VALUES("25","2018-10-25","16","ELPIDIO VENTURA VALENCIA","NA","TORTON","DINA","AZUL","XU34281","ODILON VENTURA MACIAS","1764","129","30.00","3870.00","100.00","0.00","0.00","3970.00","1057","127.0","2.0","3216","0.00","0.00","-3970.00","CITRICIOS CADILLO SA DE CV","14","34","NA","NA","4","6");
INSERT INTO compra_venta_limon VALUES("26","2018-10-26","16","ANTONIO PULIDO UTRERA","NA","RABON","FORD","ROJO","XU34282","CANDIDO MURRIETA LEON","1767","132","30.00","3960.00","100.00","0.00","0.00","4060.00","1060","130.0","2.0","3210","0.00","0.00","-4060.00","CITRICOS CADILLO SA DE CV","14","34","NA","NA","4","6");
INSERT INTO compra_venta_limon VALUES("27","2018-10-26","16","ELPIDIO VENTURA VALENCIA","NA","TORTON","DINA","AZUL","XU34281","ODILON VENTURA MACIAS","1768","123","30.00","3690.00","100.00","0.00","0.00","3790.00","1061","121.0","2.0","2998","0.00","0.00","-3790.00","CITIRCOS CADILLO SA DE CV","14","34","NA","NA","4","6");
INSERT INTO compra_venta_limon VALUES("28","2018-10-05","17","ELPIDIO VENTURA VALENCIA","NA","TORTON","DINA","AZUL","XU34281","ODILON VENTURA MACIAS","1737","95","30.00","2850.00","70.00","0.00","0.00","2920.00","1026","93.0","2.0","2377","0.00","0.00","-2920.00","CITRICOS CADILLO SA DE CV","14","34","NA","AG","4","6");
INSERT INTO compra_venta_limon VALUES("29","2018-10-06","17","ELPIDIO VENTURA VALENCIA","NA","TORTON","DINA","AZUL","XU34281","ODILON VENTURA MACIAS","1738","90","30.00","2700.00","100.00","0.00","0.00","2800.00","1028","88.0","2.0","2269","0.00","0.00","-2800.00","CITRICOS CADILLO SA DE CV","14","34","NA","AG","4","6");
INSERT INTO compra_venta_limon VALUES("30","2018-10-06","17","ANTONIO PULIDO UTRERA","NA","RABON","FORD","ROJO","XU34282","CANDIDO MURRIETA LEON","1739","95","30.00","2850.00","100.00","0.00","0.00","2950.00","1029","94.0","1.0","2282","0.00","0.00","-2950.00","CITRICOS CADILLO SA DE CV","14","34","NA","AG","4","6");
INSERT INTO compra_venta_limon VALUES("31","2018-10-08","17","ANTONIO PULIDO UTRERA","NA","RABON","FORD","ROJO","XU34282","CANDIDO MURRIETA LEON","1740","114","30.00","3420.00","100.00","0.00","0.00","3520.00","1032","114.0","0.0","2797","0.00","0.00","-3520.00","CITRICOS CADILLO SA DE CV","14","34","NA","AG","4","6");
INSERT INTO compra_venta_limon VALUES("32","2018-10-08","17","ELPIDIO VENTURA VALENCIA","NA","TORTON","DINA","AZUL","XU34281","ODILON VENTURA MACIAS","1741","95","30.00","2850.00","100.00","0.00","0.00","2950.00","1033","91.0","4.0","2328","0.00","0.00","-2950.00","CITRICOS CADILLO SA DE CV","14","34","NA","AG","4","6");
INSERT INTO compra_venta_limon VALUES("33","2018-10-09","17","ANTONIO PULIDO UTRERA","NA","RABON","FORD","ROJO","XU34282","CANDIDO MURRIETA LEON","1742","141","30.00","4230.00","100.00","0.00","0.00","4330.00","1034","140.0","1.0","3420","0.00","0.00","-4330.00","CITRICOS CADILLO SA DE CV","14","34","NA","AG","4","6");
INSERT INTO compra_venta_limon VALUES("34","2018-10-09","17","ELPIDIO VENTURA VALENCIA","NA","TORTON","DINA","AZUL","XU34281","ODILON VENTURA MACIAS","1743","69","30.00","2070.00","100.00","0.00","0.00","2170.00","1035","67.0","2.0","1696","0.00","0.00","-2170.00","CITRICOS CADILLO SA DE CV","14","34","NA","AG","4","6");
INSERT INTO compra_venta_limon VALUES("35","2018-10-10","17","ELPIDIO VENTURA VALENCIA","NA","TORTON","DINA","AZUL","XU34281","ODILON VENTURA MACIAS","1744","103","30.00","3090.00","100.00","0.00","0.00","3190.00","1036","102.0","1.0","2576","0.00","0.00","-3190.00","CITRICOS CADILLO SA DE CV","14","34","NA","AG","4","6");
INSERT INTO compra_venta_limon VALUES("36","2018-10-10","17","ANTONIO PULIDO UTRERA ","NA","RABON","FORD","ROJO","XU34282","CANDIDO MURRIETA LEON","1745","95","30.00","2850.00","100.00","0.00","0.00","2950.00","1037","95.0","0.0","2340","0.00","0.00","-2950.00","CITRICOS CADILLO SA DE CV","14","34","NA","AG","4","6");
INSERT INTO compra_venta_limon VALUES("37","2018-10-11","17","ANTONIO PULIDO UTRERA","NA","RABON","FORD","ROJO","XU34282","CANDIDO MURRIETA LEON","1746","68","30.00","2040.00","55.00","0.00","0.00","2095.00","1038","67.0","1.0","1623","0.00","0.00","-2095.00","CITRICOS CADILLO SA DE CV","14","34","NA","AG","4","6");



CREATE TABLE `empleado` (
  `idempleado` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` date NOT NULL,
  `idpersona` int(11) NOT NULL,
  `idcargo` int(11) NOT NULL,
  `idhuerta` int(11) NOT NULL,
  `salario` float(10,2) NOT NULL,
  PRIMARY KEY (`idempleado`),
  KEY `compra_venta_cargo_idx` (`idcargo`),
  KEY `compra_venta_persona_idx` (`idpersona`),
  KEY `compra_venta_huerta_idx` (`idhuerta`),
  CONSTRAINT `compra_venta_cargo` FOREIGN KEY (`idcargo`) REFERENCES `cargo` (`idcargo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `compra_venta_persona` FOREIGN KEY (`idpersona`) REFERENCES `persona` (`idpersona`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

INSERT INTO empleado VALUES("3","2018-11-23","1","2","19","300.00");



CREATE TABLE `herramienta_segmento` (
  `idherramienta_segmento` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` date NOT NULL,
  `clave` varchar(45) COLLATE utf8_spanish2_ci NOT NULL,
  `descripcion` varchar(45) COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`idherramienta_segmento`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

INSERT INTO herramienta_segmento VALUES("1","2018-11-21","HE001","MACHETE");
INSERT INTO herramienta_segmento VALUES("3","2018-11-23","HE002","PINZAS");



CREATE TABLE `historial` (
  `idhistorial` int(11) NOT NULL AUTO_INCREMENT,
  `idstock` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `cantidad` float NOT NULL,
  `idoperacion` int(11) NOT NULL,
  `saldoant` float NOT NULL,
  PRIMARY KEY (`idhistorial`),
  KEY `fk_stock_idx` (`idstock`),
  KEY `fk_operacion_idx` (`idoperacion`),
  CONSTRAINT `fk_operacion` FOREIGN KEY (`idoperacion`) REFERENCES `operacion` (`idoperacion`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_stock` FOREIGN KEY (`idstock`) REFERENCES `stock` (`idstock`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

INSERT INTO historial VALUES("22","6","2017-12-29","2","2","4");
INSERT INTO historial VALUES("23","6","2018-04-16","2","2","2");
INSERT INTO historial VALUES("24","41","2017-12-29","2","2","8");
INSERT INTO historial VALUES("25","41","2018-01-03","1","2","6");
INSERT INTO historial VALUES("26","41","2018-01-13","1","1","5");
INSERT INTO historial VALUES("27","41","2018-04-16","2","2","6");
INSERT INTO historial VALUES("28","41","2018-09-20","2","2","4");
INSERT INTO historial VALUES("29","43","2018-01-11","0","2","4");
INSERT INTO historial VALUES("30","42","2018-01-18","1","2","2");
INSERT INTO historial VALUES("31","15","2018-08-30","1","2","2");
INSERT INTO historial VALUES("32","17","0108-09-20","0","2","2");
INSERT INTO historial VALUES("33","26","2018-05-16","3","2","44");
INSERT INTO historial VALUES("34","26","2018-06-18","2","2","41");
INSERT INTO historial VALUES("35","26","2018-06-19","3","2","39");
INSERT INTO historial VALUES("36","26","2018-06-29","8","2","36");
INSERT INTO historial VALUES("37","26","2018-06-29","1","2","28");
INSERT INTO historial VALUES("38","26","2018-07-27","1","2","27");
INSERT INTO historial VALUES("39","28","2018-03-20","1","2","5");
INSERT INTO historial VALUES("40","28","2018-08-08","1","2","4");
INSERT INTO historial VALUES("41","28","2018-10-17","1","2","3");
INSERT INTO historial VALUES("42","29","2018-03-28","1","2","5");
INSERT INTO historial VALUES("43","29","2018-08-08","4","2","4");
INSERT INTO historial VALUES("44","27","2018-04-05","2","2","25");
INSERT INTO historial VALUES("45","27","2018-04-07","1","2","23");
INSERT INTO historial VALUES("46","27","2018-05-02","1","2","22");
INSERT INTO historial VALUES("47","27","2018-05-12","2","2","21");
INSERT INTO historial VALUES("48","27","2018-03-17","1","2","19");
INSERT INTO historial VALUES("49","27","2018-05-12","1","2","18");
INSERT INTO historial VALUES("50","27","2018-05-30","1","2","17");
INSERT INTO historial VALUES("51","27","2018-06-15","1","2","16");
INSERT INTO historial VALUES("52","27","2018-07-28","1","2","15");
INSERT INTO historial VALUES("53","27","2018-07-31","1","2","14");
INSERT INTO historial VALUES("54","27","2018-08-01","5","2","13");
INSERT INTO historial VALUES("55","27","2018-09-01","3","2","8");



CREATE TABLE `huerta` (
  `idhuerta` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` date NOT NULL,
  `clave` varchar(45) CHARACTER SET latin1 NOT NULL,
  `nombre` varchar(45) CHARACTER SET latin1 NOT NULL,
  `zona` varchar(45) CHARACTER SET latin1 NOT NULL,
  `hectareas` decimal(10,0) NOT NULL,
  `producto` varchar(45) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`idhuerta`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

INSERT INTO huerta VALUES("10","2018-09-01","1005","EL ZAPOTAL III","MARTINEZ","10","LIMON");
INSERT INTO huerta VALUES("11","2018-09-01","1010","SARABIA","MISANTLA","15","NARANJA");
INSERT INTO huerta VALUES("12","2018-09-01","1012","LA LUZ 2","COXQUIHUI","35","NARANJA");
INSERT INTO huerta VALUES("13","2018-09-01","1002","EL DIAMANTE I","MARTINEZ","2","LIMON");
INSERT INTO huerta VALUES("14","2018-09-01","1002","EL DIAMANTE II","MARTINEZ","2","LIMON");
INSERT INTO huerta VALUES("15","2018-09-01","1002","EL DIAMANTE III","MARTINEZ","4","LIMON");
INSERT INTO huerta VALUES("16","2018-09-01","1007","SANTA ISABEL","MARTINEZ","22","LIMON");
INSERT INTO huerta VALUES("17","2018-09-01","1006","SAN ANGEL I","MARTINEZ","20","NARANJA LIMON");
INSERT INTO huerta VALUES("18","2018-09-01","1015","EL MEZCLERO","EL MEZCLERO","15","NARANJA");
INSERT INTO huerta VALUES("19","2018-09-01","1012","LA LUZ 1","COXQUIHUI","15","NARANJA TORONJA");
INSERT INTO huerta VALUES("20","2018-09-01","1006","SAN ANGEL II","MARTINEZ","35","LIMON");
INSERT INTO huerta VALUES("21","2018-09-01","1009","EMILIANO ZAPATA","MARTINEZ","1","NARANJA LIMON");
INSERT INTO huerta VALUES("22","2018-09-01","1016","IXTACUACO","TLAPACOYAN","3","LIMON");
INSERT INTO huerta VALUES("23","2018-09-01","1004","EL MIRADOR","MARTINEZ","21","NARANJA LIMON");
INSERT INTO huerta VALUES("24","2018-09-01","1008","GAMEZ I","MARTINEZ","10","NARANJA LIMON");



CREATE TABLE `movimiento` (
  `idmovimiento` int(11) NOT NULL AUTO_INCREMENT,
  `movimiento` varchar(45) COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`idmovimiento`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

INSERT INTO movimiento VALUES("4","BANCOS");
INSERT INTO movimiento VALUES("5","EFECTIVO");



CREATE TABLE `operacion` (
  `idoperacion` int(11) NOT NULL AUTO_INCREMENT,
  `tipo` varchar(45) COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`idoperacion`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

INSERT INTO operacion VALUES("1","ENTRADA");
INSERT INTO operacion VALUES("2","SALIDA");



CREATE TABLE `pago` (
  `idpago` int(11) NOT NULL AUTO_INCREMENT,
  `pago` varchar(45) CHARACTER SET big5 NOT NULL,
  `idmovimiento` int(11) NOT NULL,
  PRIMARY KEY (`idpago`),
  KEY `fk_movimiento_idx` (`idmovimiento`),
  CONSTRAINT `fk_movimiento` FOREIGN KEY (`idmovimiento`) REFERENCES `movimiento` (`idmovimiento`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

INSERT INTO pago VALUES("6","TRANSFERENCIA","4");
INSERT INTO pago VALUES("7","DEPOSITO","4");
INSERT INTO pago VALUES("8","CAJA","5");



CREATE TABLE `persona` (
  `idpersona` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) COLLATE utf8_spanish2_ci NOT NULL,
  `apellidop` varchar(45) COLLATE utf8_spanish2_ci NOT NULL,
  `apellidom` varchar(45) COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`idpersona`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

INSERT INTO persona VALUES("1","MIGUEL �NGEL","MART�NEZ","HERN�NDEZ");



CREATE TABLE `producto` (
  `idproducto` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`idproducto`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

INSERT INTO producto VALUES("14","LIMON");
INSERT INTO producto VALUES("15","NARANJA");
INSERT INTO producto VALUES("16","TORONJA");



CREATE TABLE `stock` (
  `idstock` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` date NOT NULL,
  `articulo` varchar(45) COLLATE utf8_spanish2_ci NOT NULL,
  `unidad` varchar(45) COLLATE utf8_spanish2_ci NOT NULL,
  `existencia` float NOT NULL,
  `limite` float NOT NULL,
  PRIMARY KEY (`idstock`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

INSERT INTO stock VALUES("3","2017-11-09","RETEN 60 110 10","PZ","10","5");
INSERT INTO stock VALUES("4","2017-11-09","BALERO 2212 2RS TV FAG","PZ","8","5");
INSERT INTO stock VALUES("5","2017-11-09","BALERO 2212 TV C3 FAG","PZ","2","1");
INSERT INTO stock VALUES("6","2018-11-09","RETEN 60 110 12","PZ","0","2");
INSERT INTO stock VALUES("7","2017-12-05","MOTOSIERRA STHILL 250","PZ","2","1");
INSERT INTO stock VALUES("8","2017-12-07","TRANSMISION 1:1 90 HP","PZ","2","1");
INSERT INTO stock VALUES("9","2017-12-07","TRANSMISION REDON OG75","PZ","5","1");
INSERT INTO stock VALUES("10","2017-12-07","FLECHA CARDAN CAT A5 EST Z-6 120 MM","PZ","4","1");
INSERT INTO stock VALUES("11","2017-12-07","YUGO ESTRIADO CAT . 5","PZ","20","5");
INSERT INTO stock VALUES("12","2017-12-07","CRUCETA CAT 5 30.0 X 80.0 ","PZ","10","1");
INSERT INTO stock VALUES("13","2017-12-22","REFACCIONES RETRO COMPLETA FACT.84877","PZ","1","1");
INSERT INTO stock VALUES("14","2018-01-13","REMACHADORA BELLOTA","PZ","1","1");
INSERT INTO stock VALUES("15","2018-01-13","CAJAS DE GRAPAS ALLIGATOR","PZ","1","1");
INSERT INTO stock VALUES("16","2018-01-13","BALEROS 3 206","PZ","2","1");
INSERT INTO stock VALUES("17","2018-01-13","BALEROS 32 209-A","PZ","2","1");
INSERT INTO stock VALUES("18","2018-01-13","MEDIOS CANDADOS P-60","PZ","10","5");
INSERT INTO stock VALUES("19","2018-01-13","CANDADOS COMPLETOS P-60","PZ","10","5");
INSERT INTO stock VALUES("20","2018-01-30","TRANSMISION","PZ","4","2");
INSERT INTO stock VALUES("21","2018-01-30","RETEN 2208 TVH FAG","PZ","2","1");
INSERT INTO stock VALUES("22","2018-01-31","LAINA 84261891","PZ","2","1");
INSERT INTO stock VALUES("23","2018-01-31","LAINA 84261890","PZ","2","1");
INSERT INTO stock VALUES("24","2018-01-31","PERNO 84249274","PZ","1","1");
INSERT INTO stock VALUES("25","2018-01-31","HUASA 84224845","PZ","6","2");
INSERT INTO stock VALUES("26","2018-03-12","GUANTES CARNAZA","PZ","26","5");
INSERT INTO stock VALUES("27","2018-03-12","DISCOS CORTE  4 1/2" NORTON","PZ","5","5");
INSERT INTO stock VALUES("28","2018-03-12","DISCO CORTE 7" EASY CUT","PZ","2","2");
INSERT INTO stock VALUES("29","2018-03-12","DISCO DESGASTE 7" WEILER","PZ","0","2");
INSERT INTO stock VALUES("30","2018-10-20","FILTRO P 550387","PZ","1","1");
INSERT INTO stock VALUES("31","2018-10-20","FILTRO P550779","PZ","1","1");
INSERT INTO stock VALUES("32","2018-10-20","FILTRO P 551423","PZ","2","1");
INSERT INTO stock VALUES("33","2018-10-20","FILTRO P550127","PZ","1","1");
INSERT INTO stock VALUES("34","2018-10-20","FILTRO BF587D","PZ","1","1");
INSERT INTO stock VALUES("35","2018-10-20","FILTRO 33638","PZ","1","1");
INSERT INTO stock VALUES("36","2018-10-20","FILTRO 46562 PRIMARIO","PZ","1","1");
INSERT INTO stock VALUES("37","2018-10-20","FILTRO 46562 SECUNDARIO","PZ","1","1");
INSERT INTO stock VALUES("38","2018-10-20","FILTRO B7328-P551100","PZ","1","1");
INSERT INTO stock VALUES("39","2018-10-20","FILTRO P 550587","PZ","1","1");
INSERT INTO stock VALUES("40","2018-10-20","FILTRO P 1793 42","PZ","1","1");
INSERT INTO stock VALUES("41","2017-11-09","RETEN 70 100 10","PZ","2","2");
INSERT INTO stock VALUES("42","2018-01-03","BALERO 2208 STEY R","PZ","1","1");
INSERT INTO stock VALUES("43","2018-01-08","BALERO 2208 TVH FAG","PZ","4","1");
INSERT INTO stock VALUES("44","2017-12-20","BOMBAS INSULFADORAS NUEVAS","PZ","8","4");
INSERT INTO stock VALUES("45","2018-03-09","CABLE DE ACERO","MTS","80","40");



CREATE TABLE `subcategoria` (
  `idsubcategoria` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) COLLATE utf8_spanish2_ci NOT NULL,
  `idcategoria` int(11) NOT NULL,
  PRIMARY KEY (`idsubcategoria`),
  KEY `fk_sub_cat_idx` (`idcategoria`),
  CONSTRAINT `fk_sub_cat` FOREIGN KEY (`idcategoria`) REFERENCES `categoria` (`idcategoria`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

INSERT INTO subcategoria VALUES("1","CONTROL FITOSANITARIO","3");



CREATE TABLE `transporte` (
  `idtransporte` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) COLLATE utf8_spanish2_ci NOT NULL,
  `marca` varchar(45) COLLATE utf8_spanish2_ci NOT NULL,
  `color` varchar(45) COLLATE utf8_spanish2_ci NOT NULL,
  `placas` varchar(45) COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`idtransporte`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

INSERT INTO transporte VALUES("5","CAMIONETA","FORD","BLANCA","XU34283");
INSERT INTO transporte VALUES("6","TORTON","DODGE","ROJO","HK5151F");
INSERT INTO transporte VALUES("7","TORTON","DINA","BCO-AMARILLO","HH0734F");
INSERT INTO transporte VALUES("8","TORTON","INTERNACIONAL","BLANCO-AZUL","XU75283");
INSERT INTO transporte VALUES("9","TORTON","DINA","AZUL","XU34281");
INSERT INTO transporte VALUES("10","RABON","FORD","ROJO","XU34282");



CREATE TABLE `usuarios` (
  `idusuarios` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(45) COLLATE utf8_spanish2_ci NOT NULL,
  `pass` varchar(200) COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`idusuarios`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

INSERT INTO usuarios VALUES("12","MIGUEL","1234");
INSERT INTO usuarios VALUES("14","ALDO","1234");
INSERT INTO usuarios VALUES("15","ANGEL","1234");



CREATE TABLE `variedad` (
  `idvariedad` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) COLLATE utf8_spanish2_ci NOT NULL,
  `idproducto` int(11) NOT NULL,
  PRIMARY KEY (`idvariedad`),
  KEY `fk_prodf_idx` (`idproducto`),
  KEY `fk_prod456_idx` (`idproducto`),
  CONSTRAINT `fk_prodf` FOREIGN KEY (`idproducto`) REFERENCES `producto` (`idproducto`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

INSERT INTO variedad VALUES("32","FREMONT","15");
INSERT INTO variedad VALUES("33","MARCHS","15");
INSERT INTO variedad VALUES("34","PERSA","14");
INSERT INTO variedad VALUES("35","DOBLE ROSA","16");

