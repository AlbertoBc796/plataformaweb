<?php
	include 'function.php';
	if(isset($_POST['backup'])){
		//Datos enviados desde el formulario por el metodo post ($_POST = nombre de los inputs del form)
		$server = $_POST['server'];
		$username = $_POST['username'];
		$password = $_POST['password'];
		$dbname = $_POST['dbname'];
		//Creacion de respaldo mediante la funcion backDb
		backDb($server, $username, $password, $dbname);
		exit();
	}
	else{
		echo 'Rellena las credenciales de la base de datos';
	}
?>