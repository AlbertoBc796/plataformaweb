<?php include('include.php'); 
?>
<title>Compra_Venta</title>
<meta charset="UTF-8">
<main>
<div class="container-fluid"> 
    <div class="row">
        <div class="col-md-12"    id='animacion2'>
            <form action="../php/inserciones/insertar_compra_venta.php"  method="post">            
            <hr>
                <div class="form-row">
                    <div class="form-group col-md-2 text-center">
                        <label for="fecha">FECHA</label>
                        <input type="date" class="form-control" name="fecha" placeholder="DD/MM/AAAA"  required  >
                    </div>
                    <!--CON ESTO PUEDO HACER VALIDAR LA FECHA pattern="(0[1-9]|1[0-9]|2[0-9]|3[01]).(0[1-9]|1[012]).[0-9]{4}"--> 
                    <?php 
                        require("../php/conexion.php");
                        $consulta="SELECT idhuerta,clave, nombre FROM huerta ORDER BY clave ASC;";              
                        $ress=mysqli_query($mysqli,$consulta);
                        $huertas ="";
                        while ($arreglo=mysqli_fetch_array($ress)) {
                            $huertas=$huertas."<option value=\"".$arreglo['idhuerta']."\">".$arreglo['nombre']." </option>";
                        }
                    ?>

                    <div class="form-group col-md-2 text-center">
                        <label for="huerta" class="text-center"> HUERTA </label>
                        <select  name="huerta" class="form-control browser-default"  required><?php echo $huertas; ?></select>
                    </div>

                    <div class="form-group col-md-2 text-center">
                        <label for="producto" class="text-center">PRODUCTO </label>
                            <select name="producto" id="producto" class="form-control browser-default"  required onchange="JavaScript:showMsg()" onchange="JavaScript:yesnoCheck()">
                                <option value="0">Seleccione</option>
                            </select>
                    </div>

                    <div class="form-group col-md-2 text-center">
                        <label for="variedad" class="text-center">VARIEDAD </label>
                        <select name="variedad" id="variedad" class="form-control browser-default"  required>
                            <option value="0">Seleccione</option>
                        </select>
                    </div>

                    <div class="form-group col-md-4 text-center">
                        <label for="cliente">CLIENTE</label>
                            <input type="text" onKeyUp="this.value = this.value.toUpperCase();" class="form-control" name="cliente">
                    </div>

                </div>


                <div class="form-row ui-widget">
                    <div class="form-group col-md-2 text-center">
                        <label for="ApellidoP">CAMION</label>
                            <input onkeypress="return soloLetras(event);" id="camion" onKeyUp="this.value = this.value.toUpperCase();" type="text" class="form-control"  name="camion" required>
                            <input onKeyUp="this.value = this.value.toUpperCase();" id="idtransporte" name="idtransporte" class="form-control" type="hidden">
                    </div>

                    <div class="form-group col-md-2 text-center">
                        <label for="marca">MARCA</label>
                            <input onkeypress="return soloLetras(event);"  onKeyUp="this.value = this.value.toUpperCase();" type="text" class="form-control text-uppercase" id="marca" name="marca" required>
                    </div>

                    <div class="form-group col-md-2 text-center">
                        <label for="color">COLOR</label>
                            <input type="text" onkeypress="return soloLetras(event);"  onKeyUp="this.value = this.value.toUpperCase();"  class="form-control" id="colors" name="color" required>
                    </div>

                    <div class="form-group col-md-2 text-center">
                        <label for="placas">PLACAS</label>
                            <input onKeyUp="this.value = this.value.toUpperCase();" type="text" class="form-control text-uppercase" id="placas" name="placas" required>
                    </div>

                    <div class="form-group col-md-4 text-center">
                        <label for="ApellidoM">CHOFER</label>
                            <input onkeypress="return soloLetras(event);"  onKeyUp="this.value = this.value.toUpperCase();" type="text" class="form-control text-uppercase" id="chofer" name="chofer" required>
                    </div>
                    
                    <div id="multi" class="form-group col-md-12  col-sm-12 text-center" style="display: none;">
							<label id="clave" for="clave">VENTA</label><br>
							<div class="form-check form-check-inline">
								<input class="form-check-input" type="radio" name="servicio" id="serv1" value="1" onclick="show1();">
								<label class="form-check-label" for="serv1">
									Kg
								</label>
							</div>
							<div class="form-check form-check-inline">
								<input class="form-check-input" type="radio" name="servicio" id="serv2" value="2" onclick="show2();">
								<label class="form-check-label" for="serv2">
									Cajas
								</label>
							</div>
						</div>

                </div>
                <hr>
                <div class="form-row">
                    
                    <div class="form-group col-md-2 text-center">
                        <label for="folio1">RECIBO CORTE</label>
                        <input type="text" class="form-control"  name="folio1" >
                    </div>

                    <div class="form-group col-md-1 text-center">
                        <label id="unidad">KG</label>
                        <input type="text" class="form-control" id="multiplicando"  onkeypress="return filterFloat(event,this);" name="kilo_corte" required >
                    </div>

                    <div class="form-group col-md-1 text-center">
                        <label for="ApellidoM">PRECIO</label>
                        <input type="text" class="form-control" id="multiplicador"  onkeyup="multiplicacion();" onkeypress="return filterFloat(event,this);" name="precio_corte" required >
                    </div>

                    <div class="form-group col-md-2 text-center">
                        <label for="ApellidoM">IMPORTE</label>
                        <input type="text" class="form-control " id="resultado"  onkeypress="return filterFloat(event,this);" name="importe_corte"  readonly >
                    </div>

                    <div class="form-group col-md-1 text-center">
                        <label for="gastos_corte">GASTO</label>
                        <input type="text" class="form-control " id="pes"  onkeypress="return filterFloat(event,this);" name="pesaje" >
                    </div>

                    <div class="form-group col-md-2 text-center">
                        <label for="ApellidoM">PRECIO FLETE</label>
                        <input type="text"  class="form-control" onkeyup="mul2(); reesultado();" name="precio_flete" id="multiplicador2" onkeypress="return filterFloat(event,this);" required>
                    </div>

                    <div class="form-group col-md-1 text-center">
                        <label for="resultado">$ FLETE</label>
                        <input type="text" class="form-control" id="res" onkeypress="return filterFloat(event,this);" readonly name="importe_flete" >
                    </div>

                    <div class="form-group col-md-2 text-center">
                        <label for="resultado">EGRESOS</label>
                        <input type="text" class="form-control" id="egresos"  name="egresos"  onkeypress="return filterFloat(event,this);" readonly="" required >
                    </div>
                    
                </div>


                <div class="form-row">

                    <div class="form-group col-md-2 text-center">
                        <label for="folio2">RECIBO VENTA </label>
                        <input  type="text" class="form-control"   name="folio2">
                    </div>

                    <div class="form-group col-md-2 text-center">
                        <label for="kilo_venta" id="kilo_venta">KG VENTA </label>
                        <input  type="text" class="form-control" id="kilosventa" onkeyup="mul3(); resMerma();"  name="kilo_venta" onkeypress="return filterFloat(event,this);" required >
                    </div>

                    <div id="kg_limon" class="form-group col-md-1 text-center" style="display: none;" >
                        <label for="kg_limon">CAJAS</label>
                        <input  type="text" id="kilo_limon" onkeypress="return filterFloat(event,this);" onkeyup="resMerma(); cajaProm();" class="form-control" name="kg_limon">
                    </div>

                    <div id="kg_caja" class="form-group col-md-1 text-center" style="display: none;">
                        <label for="kg_caja" id="kg_caja">KG CAJA</label>
                        <input  type="text" class="form-control" id="kilo_caja" name="kilo_caja" onkeypress="return filterFloat(event,this);" readonly required >
                    </div>

                    <div class="form-group col-md-2 text-center">
                        <label for="kilo_merma" id="kilo_merma">KG MERMA</label>
                        <input  type="text" class="form-control" id="merma" name="merma" onkeypress="return filterFloat(event,this);" readonly required >
                    </div>

                    <div class="form-group col-md-2 text-center">
                        <label for="precio_venta">PRECIO VENTA</label>
                        <input type="text" class="form-control" id="precioventa" onkeyup="mul3(); reesultado2();" onkeypress="return filterFloat(event,this);"  name="precio_venta" required  >
                    </div>

                    <div class="form-group col-md-2 text-center">
                        <label for="total_venta">INGRESOS</label>
                        <input  type="text" class="form-control" id="totalventa" name="total_venta" onkeypress="return filterFloat(event,this);" readonly required >
                    </div>

                    
                </div>


                <div class="form-row ui-widget">

                    <div class="form-group col-md-2 text-center">
                        <label for="nuevof"> FOLIO</label>
                        <input type="text" class="form-control text-center" onKeyUp="this.value = this.value.toUpperCase();"  name="nuevof" id="nuevof">          
                    </div>

                    <div class="form-group col-md-2 text-center">
                        <label for=""> FACTURACION</label>
                        <select name="facturacion" id="facturacion" class="form-control browser-default text-center">
                            <option value="NA">NA</option>
                            <option value="MEN">MEN</option>
                            <option value="AG">AG</option>
                            <option value="NME">NME</option>
                            <option value="PMV">PMV</option>
                            <option value="CAMV">CAMV</option>
                            <option value="ANMV">ANMV</option>
                            <option value="GMV">GMV</option>
                            <option value="CMP">CMP</option>
                            <option value="IMP">IMP</option>
                        </select>
                    </div>

                    <div class="form-group col-md-2 text-center">
                        <label for="movimiento" class=" text-center">MOVIMIENTOS </label>
                        <select name="movimiento" id="movimiento" class="form-control browser-default text-center"  required>
                            <option value="">Seleccione</option>
                        </select>
                    </div>

                    <div class="form-group col-md-2 text-center">
                        <label for="pago" class="text-center"> PAGO </label>
                        <select name="pago" id="pago" class="form-control browser-default text-center"  required>
                                <option value="">Seleccione</option>
                        </select>
                    </div>

                    <div class="form-group col-md-2 text-center">
                        <label for="ingresos">UTILIDAD</label>
                        <input  type="text" onkeypress="return filterFloat(event,this);" class="form-control" id="ingresos" name="ingresos"  readonly required>
                    </div>

                    <div class="form-group text-center col-md-2 pt-3">
                        <button type="submit" class="btn btn-primary" name="submit" value="submit"><i class="fa fa-save"></i></button>
                    </div>
                </div>
            </form>
        </div>
    </div> 


    <hr>


    <div class="row d-flex text-center" >

        <div class="col-sm-2 mr-auto p-2" >
            <a id="btnSlide" class="btn-floating  btn-comm ">
                <i class="far fa-money-bill-alt"></i> </a>
        </div>

        <div class="col-sm-2 p-2" >
            <a href="javascript:reporteMes();" title="Reporte mensual" type="button" class="btn-floating btn-comm"><i class="fas fa-calendar-check"></i></a>
        </div>

        <div class="col-sm-2 p-2" >
            <a href="javascript:reporteSemana();" title="Reporte semanal" type="button" class="btn-floating btn-comm"><i class="fas fa-calendar-alt"></i></a>
        </div>

        <div class="col-sm-2 p-2" >
            <a href="javascript:reportePDF();" title="Reporte general" class="btn-floating btn-comm"><i class="fas fa-file-import"></i></a>
        </div>

        <div class="col-sm-2 p-2" >
            <a href="javascript:reportePDF2();" title="Reporte completo" class="btn-floating btn-comm"><i class="far fa-file-pdf"></i></a>
        </div>
    </div>


    <div class="row text-center">

        <div class="col-sm-2 text-center p-3"  >
            <input class="form-control  text-uppercase text-center " placeholder="DD/MM/AAA" type="date" id="bd-desde"/>
        </div>
        <div class="col-sm-1 text-center pt-4 text-center" >
                <h6>HASTA</h6>
        </div>

        <div class="col-sm-2 text-center p-3"  >
            <input class="form-control  text-uppercase text-center"  placeholder="DD/MM/AAA"  type="date" id="bd-hasta"/>
        </div>
            
        <div class="col-sm-2 text-center p-3" >
            <input  class="form-control text-center" onKeyUp="this.value = this.value.toUpperCase();"  type="text" placeholder=" Huerta" id="bs-prod"/> 
        </div>

        <div class="col-sm-2 text-center p-3" >
            <input  class="form-control text-center" onKeyUp="this.value = this.value.toUpperCase();"  type="text" placeholder=" Producto " id="bs-producto"/> 
        </div>

        <div class="col-sm-2 text-center p-3" >
            <input  class="form-control text-center" onKeyUp="this.value = this.value.toUpperCase();"   type="text" placeholder=" Variedad " id="bs-vari"/>
        </div>

        <div class="col-md-1 col-sm-1 text-center" >
            <a type="button" class="btn-floating btn-comm" onclick="buscadorcito();"><i class="fas fa-search"></i></a>
        </div>
        
    </div> <br>
    <div class=" row ">
        <div class="registros col-md-12 table table-responsive" id="agrega-registros2"></div>
    </div>
    <center><nav aria-label="..."><ul class="pagination justify-content-center" id="pagination2"></ul></nav></center>
</div>

</main>

    <script src="../js/jquery-ui.js"></script>  
    <script "text/javascript" src="../js/multiplicacion2.js"></script>
    <script "text/javascript" src="../js/busqueda_compra_venta.js"></script>
    <script "text/javascript" src="../js/funcion_combo.js"></script>

<!-- ESCRIPT PARA OCULTAR EL FORMULARIO DE AGREGAR-->
<script>
    var toogle= false;
    $("#btnSlide").click(function()
    {
        if (!toogle)
        {
            toogle = true;
            $("#animacion2").slideUp("slow");
            $("#btnSlide").html('<i class="far fa-money-bill-alt"></i>')
        }
        else
        {
            toogle = false;
            $("#animacion2").slideDown("slow");
            $("#btnSlide").html('<i class="fas fa-minus"></i>')
        }
    })
</script>

<script type="text/javascript">
    $(function() {
            $("#camion").autocomplete({
                source: "../php/autocompletadores/transporte.php",
                minLength: 2,
                select: function(event, ui) {
                event.preventDefault();
                    $('#camion').val(ui.item.camion);       
                    $('#marca').val(ui.item.marca);
                    $('#colors').val(ui.item.colors);
                    $('#placas').val(ui.item.placas);
                    $('#idtransporte').val(ui.item.idtransporte);
            }
            });
    });    
</script>

<script>
    $(document).ready(function (){
        $("#bs-producto").typeahead({
            source: function(query,resultado)
        });
    })
</script>