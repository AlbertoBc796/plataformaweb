<?php 
	include('include.php');
	include('../php/conexion.php');
	$huerta = "SELECT * FROM huerta order by nombre";
	$result = mysqli_query($mysqli,$huerta);	
?>
<title>Huertas</title>
<html lang="en">
	<meta charset="UTF-8">
	<main>
		<div class="container-fluid" >
			<div class="row " >
		<?php 
			while ( $row = mysqli_fetch_assoc($result)) {
			?>
				<div class="col-md-3 mt-3 text-left">
					<div class="card">
						<img src="../img/na_lim.jpg" class="card-img-top " alt="">
							<div class="card-body">
								<div class="alert-success"><h4 class="card-title text-center" style="color: red; font-family: Times New Roman;"><?php echo $row['nombre'];  ?></h4></div>
								<p style="font-size: 12.5px">Zona: <?php echo $row['zona'] ?></p>
								<p style="font-size: 12.5px">Hectareas: <?php echo $row['hectareas'] ?></p>
								<p style="font-size: 12.5px">Productos: <?php echo $row['producto'] ?></p>
									<a href="cuadrillas.php?id=<?php echo $row['idhuerta']; ?>">Ver Mas...</a>
							</div>	
					</div>
				</div>
		<?php } ?>
			</div>
		</div>
	</main>