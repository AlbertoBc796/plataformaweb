<?php 
include('include.php');
session_start(); ?>
<main>
	<div class="container-fluid">
		<div class="row justify-content-center">
			<div class="col-sm-6">
				<?php
					if(isset($_SESSION['error'])){
						?>
						<div class="alert alert-danger text-center">
							<?php echo $_SESSION['error']; ?>
						</div>
						<?php

						unset($_SESSION['error']);
					}

					if(isset($_SESSION['success'])){
						?>
						<div class="alert alert-success text-center">
							<?php echo $_SESSION['success']; ?>
						</div>
						<?php

						unset($_SESSION['success']);
					}
				?>
				<div class="card">
					<div class="card-body">
						<h3 class="text-center">Credenciales de la base de datos</h3>
						<br>
						<form method="POST" action="../php/backup/restauracion.php" enctype="multipart/form-data">
						    <div class="form-group row">
						     	<label for="server" class="col-sm-3 col-form-label">Servidor</label>
						      	<div class="col-sm-9">
						        	<input type="text" class="form-control" id="server" name="server" value="localhost" readonly required>
						      	</div>
						    </div>
						    <div class="form-group row">
						      	<label for="username" class="col-sm-3 col-form-label">Usuario</label>
						      	<div class="col-sm-9">
						        	<input type="text" class="form-control" id="username" name="username" 
						        	value="root" readonly required>
						      	</div>
						    </div>
						    <div class="form-group row">
						      	<label for="password" class="col-sm-3 col-form-label">Contraseña</label>
						      	<div class="col-sm-9">
						        	<input type="text" class="form-control" id="password" name="password" placeholder="********************" readonly="">
						      	</div>
						    </div>
						    <div class="form-group row">
						      	<label for="dbname" class="col-sm-3 col-form-label">Dase de datos</label>
						      	<div class="col-sm-9">
						        	<input type="text" class="form-control" id="dbname" name="dbname" value="gm_cv" readonly="" required>
						      	</div>
						    </div>
						    <div class="form-group row">
						      	<label for="sql" class="col-sm-3 col-form-label">Archivo</label>
						      	<div class="col-sm-9">
						        	<input type="file" class="form-control-file" id="sql" name="sql" placeholder="base de datos que deseas restaurar" required>
						      	</div>
						    </div>
						    <button type="submit" class="btn btn-primary" name="restore">Restaurar</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>

	