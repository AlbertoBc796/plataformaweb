<?php
    include('include.php'); 
    $sql="SELECT idrubro,rubro FROM rubro";
    $sql2="SELECT idsubgasto,subgasto FROM subgasto";
    $registro = mysqli_query($mysqli, $sql);
    $registro2 = mysqli_query($mysqli, $sql2);
    $gasto='';
    while ($a=mysqli_fetch_array($registro)) {
        $gasto .= '<option value="'.$a['idrubro'].'">'.$a['rubro'].'</option>';
    }
?>
<title>Gastos Indirectos</title>
<main>
    <div class="container-flui">
        <div class="row text-center">
            <div class="col-md-6">
                <a href="#factura" class="inf">FACTURA</a>
            </div>
            <div class="col-md-6">
                <a href="#cheque" class="inf">CHEQUE</a>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-12 oculto" id="factura">
                <form action="../php/inserciones/insertar_gasto.php" method="post">
                    <div class="form-row text-center">
                        <div class="form-group col-md-2">
                            <label for="">FECHA</label>
                            <input type="date" class="form-control text-center"  name="fecha"  required >
                        </div>
                        <div class="form-group col-md-2">
							<label for="">SEMANA</label>
							<input type="text" class="form-control text-center" onKeyUp="this.value = this.value.toUpperCase();" value="<?php echo 'SEMANA '.date("W");?>" name="semana" required>
                        </div>
                        <div class="form-group col-md-2 text-center">
                            <label for="movimiento" class=" text-center">MOVIMIENTOS </label>
                            <select name="movimiento" id="movimiento" class="form-control browser-default text-center"  required>
                                <option value="">Seleccione</option>
                            </select>
                        </div>
                        <div class="form-group col-md-2 text-center">
                            <label for="pago" class="text-center"> PAGO </label>
                            <select name="pago" id="pago" class="form-control browser-default text-center"  onchange="JavaScript:showMsg()" required>
                                <option value="">Seleccione</option>
                            </select>
                        </div>
                        <div class="form-group col-md-2">
							<label for="">No. FACTURA</label>
							<input type="text" class="form-control text-center" onKeyUp="this.value = this.value.toUpperCase();" name="factura" required>
                        </div>
                        <div id="num_cheque" class="form-group col-md-2" style="display: none;">
							<label for="">No. CHEQUE</label>
							<input type="text" class="form-control text-center" onKeyUp="this.value = this.value.toUpperCase();" name="cheque" required>
                        </div>
                    </div>
                    <div class="form-row text-center">
                        <div class="form-group col-md-4">
							<label for="">PROVEEDOR</label>
							<input type="text" class="form-control text-center" onKeyUp="this.value = this.value.toUpperCase();" name="proveedor" required>
                        </div>
                        <div class="form-group col-md-1 p-3">
                            <div id="agrego"><button type="button" id="agregar_campo" class="btn btn-success" onClick="addCancion()" name="submit" value="submit"><i class="fa fa-plus"></i></button></div>                            
                        </div>
                        <div class="form-group col-md-1 p-3">
                            <div id = "elimino"><button type="button" id="remover_campo" class="btn btn-danger" name="submit" value="submit"><i class="fa fa-minus"></i></button></div>
                        </div>
                        <div class="form-group col-md-2 p-3">
                            <div><button type="submit" class="btn btn-primary" name="submit" value="submit"><i class="fa fa-save"></i></button></div>
                        </div>
                        <div class="form-group col-md-2">
							<label for="">TOTAL</label>
							<input type="number" min=0 class="form-control text-center" id='total' name="total" required readonly>
                        </div>
                        <div id="multi" class="form-group col-md-2 text-center">
							<label id="clave" for="clave">IVA</label><br>
							<div class="form-check form-check-inline">
								<input class="form-check-input" type="radio" name="iva" id="serv1" value="1" onclick="show1();">
								<label class="form-check-label" for="serv1">
									Si
								</label>
							</div>
							<div class="form-check form-check-inline">
								<input class="form-check-input" type="radio" name="iva" id="serv2" value="2" onclick="show2();">
								<label class="form-check-label" for="serv2">
									No
								</label>
							</div>
						</div>
                    </div>
                    <div class="form-row text-center">
                        <div class="form-group col-md-3">
							<label for="">CONCEPTO</label>
							<input name="concepto[]" type="text" class="form-control text-center" onKeyUp="this.value = this.value.toUpperCase();" required>
                        </div>
                        <div class="form-group col-md-1">
							<label for="">CANTIDAD</label>
							<input type="number" min=0 step="0.01" class="form-control text-center"  name="cantidad[]" required>
                        </div>
                        <div class="form-group col-md-1">
							<label for="">PRECIO U.</label>
							<input type="number" min="0" step="0.01" class="form-control text-center" onKeyUp="reesultado6(); resultado7()" name="precio[]" required>
                        </div>
                        <div class="form-group col-md-1">
							<label for="">TOTAL</label>
							<input type="number" min=0 step='0.0000001' class="form-control text-center" name="precio_total[]" required readonly>
                        </div>
                        <div class="form-group col-md-2">
							<label for="cargo">RUBRO</label>
							<select name="cargo[]" id="destino" class="form-control browser-default text-center">
                            <?php
                                echo $gasto;
                            ?>
                            </select>
                        </div>
                        <div class="form-group col-md-2">
							<label for="cargo">SUB-CARGO</label>
							<select name="subcargo[]" id="subcargo" class="form-control browser-default text-center">
                            <?php
                                
                            ?>
                            </select>
                        </div>
                        <div class="form-group col-md-1 text-center">
                            <label for="camion">CAMION</label>
                            <input name="clave" id="clave_0" autocomplete="off" type="text" onKeyUp="this.value = this.value.toUpperCase();" class="form-control text-center camion">
                            <input name="idtransporte[]" id="idtransporte_0" onKeyUp="this.value = this.value.toUpperCase();" class="form-control" type="hidden">
                        </div>
                        <div id="destino_salida" class="form-group col-md-1 ">
							<label for="destino" >HUERTA</label>
							<input  name="destino" id="buscador_0" autocomplete="off" type="text" onKeyUp="this.value = this.value.toUpperCase();" class="form-control text-center huerta">
							<input  name="idhuerta[]" id="idhuerta_0" onKeyUp="this.value = this.value.toUpperCase();" class="form-control text-center" type="hidden">
						</div>
                    </div>
                    <div id="canciones"></div>
                    <div class="form-group">
                        <label for="exampleFormControlTextarea1">NOTAS:</label>
                        <textarea class="form-control" id="exampleFormControlTextarea1" name="notas" rows="3"></textarea>
                    </div>
                </form>
            </div>

            <div class="col-md-12 oculto" id="cheque">
                <form action="../php/inserciones/insertar_gasto_cheque.php" method="post">
                    <div class="form-row text-center">
                        <div class="form-group col-md-2">
                            <label for="">FECHA</label>
                            <input type="date" class="form-control text-center"  name="fecha"  required >
                        </div>
                        <div class="form-group col-md-2">
							<label for="">SEMANA</label>
							<input type="text" class="form-control text-center" onKeyUp="this.value = this.value.toUpperCase();" value="<?php echo 'SEMANA '.date("W");?>" name="semana" required>
                        </div>
                        <div class="form-group col-md-2 text-center">
                            <label for="movimiento" class=" text-center">MOVIMIENTOS </label>
                            <input type="text" class="form-control text-center" onKeyUp="this.value = this.value.toUpperCase();" value="BANCO" name="nombre_mov" readonly="">
                            <input type="hidden" class="form-control text-center" value="4" name="movimiento" required>
                        </div>
                        <div class="form-group col-md-2 text-center">
                            <label for="pago" class="text-center"> PAGO </label>
                            <input type="text" class="form-control text-center" onKeyUp="this.value = this.value.toUpperCase();" value="CHEQUE" name="nombre_pago" readonly="">
                            <input type="hidden" class="form-control text-center" value="9" name="pago" required>
                        </div>
                        <div id="num_cheque" class="form-group col-md-2">
							<label for="">No. CHEQUE</label>
							<input type="text" class="form-control text-center" onKeyUp="this.value = this.value.toUpperCase();" name="cheque" required>
                        </div>
                        <div class="form-group col-md-1 p-3">
                            <div id="agrego_factura"><button type="button" id="agregar_campo" class="btn btn-success" name="submit" value="submit"><i class="fa fa-plus"></i></button></div>
                        </div>
                        <div class="form-group col-md-1 p-3">
                            <div id = "elimino_factura"><button type="button" id="remover_campo" class="btn btn-danger" name="submit" value="submit"><i class="fa fa-minus"></i></button></div>
                        </div>
                    </div>
                    <div id="folio0" class="form-row text-center">
                        <div class="form-group col-md-2" >
							<label for="">FACTURA</label>
							<input type="text" class="form-control text-center" onKeyUp="this.value = this.value.toUpperCase();" name="factura[]" required>
                        </div>
                        <div class="form-group col-md-4">
							<label for="">PROVEEDOR</label>
							<input type="text" class="form-control text-center" onKeyUp="this.value = this.value.toUpperCase();" name="proveedor[]" required>
                        </div>
                        <div class="form-group col-md-2">
							<label for="">TOTAL</label>
							<input type="number" min=0 class="form-control text-center total" id='total0' name="total" required readonly>
                        </div>
                        <div id="multi" class="form-group col-md-2 text-center">
							<label id="clave" for="clave">IVA</label><br>
							<div class="form-check form-check-inline">
								<input class="form-check-input" type="radio" name="ivac" id="serv3" value="1">
								<label class="form-check-label" for="serv3">
									Si
								</label>
							</div>
							<div class="form-check form-check-inline">
								<input class="form-check-input" type="radio" name="ivac" id="serv4" value="0">
								<label class="form-check-label" for="serv4">
									No
								</label>
							</div>
						</div>
                        <div id="agrego_cheque" class="form-group-col-md-1 p-4">
                            <a href="#"><img id="agregar_campo" src="../img/add_bs.png" style="width: 40px; height: 40px;"alt="ELIMINAR" class="img-rounded"></a>
                        </div>
                        <div id="elimino_cheque" class="form-group-col-md-1 p-4">
                            <a href="#" ><img id="remover_campo" src="../img/del_s.png" alt="ELIMINAR" style="width: 40px; height: 40px;" class="img-rounded"></a>
                        </div>
                        <div class="form-group col-md-2" style="display:none;">
                            <label for="">LIMITE</label>
                            <input type="number" min=0 class="form-control text-center" id="limite0" name="limite[]" required readonly>
                        </div>
                    </div>
                    <div id="addFactura"></div>
                    <div class="form-group text-center">
                        <label for="exampleFormControlTextarea1">NOTAS:</label>
                        <textarea class="form-control" id="exampleFormControlTextarea1" name="notas" rows="1"></textarea>
                        <div class="form-group col-md-12 p-3">
                            <div><button type="submit" class="btn btn-primary" name="submit" value="submit"><i class="fa fa-save"></i></button></div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <hr>
        <div class="row d-flex text-center" >
            <div class="col-sm-2 mr-auto" >
                <a id="btnSlide" class="btn-floating  btn-comm "><i class="fas fa-shopping-basket"></i> </a>
            </div>

            <div class="col-md-1 col-sm-1 text-center">
				<a type="button" class="btn-floating btn-comm" onclick="buscadorcito2();">
					<i class="fas fa-search"></i>
				</a>
			</div>

            <div class="col-sm-2" >
                <a href="javascript:reporteGastos();" title="Reporte general" class="btn-floating btn-comm"><i class="fas fa-file-import"></i></a>
            </div>

            <div class="col-sm-2" >
                <a href="javascript:reporteGC();" title="Reporte completo" class="btn-floating btn-comm"><i class="far fa-file-pdf"></i></a>
            </div>
        </div>

        <div class="row">
			<div class="col-md-2 text-center p-3">
				<input class="form-control text-center " type="date" id="bd-desde" />
			</div>

			<div class="col-md-1 text-center p-3">
				<h6 class="pt-3 ">HASTA</h6>
			</div>

			<div class="col-md-2 text-center p-3">
				<input class="form-control text-center " type="date" id="bd-hasta" />
			</div>

			<div class="col-md-3 text-center p-3">
				<input onKeyUp="this.value = this.value.toUpperCase();" class="form-control text-center " type="text" placeholder="FACTURA, UNIDAD, HUERTA..." id="bs-prod"/>
			</div>

            <div class="col-md-2 text-center p-3">
				<input onKeyUp="this.value = this.value.toUpperCase();" class="form-control text-center " type="text" placeholder="MOVIMIENTO" id="bs-mov"/>
			</div>

            <div class="col-md-2 text-center p-3">
				<input onKeyUp="this.value = this.value.toUpperCase();" class="form-control text-center " type="text" placeholder="PROVEEDOR" id="bs-prov"/>
			</div>

		</div>
        <div class=" row ">
            <div class="registros col-md-12 table table-responsive" id="agrega-registros-gastos"></div>
        </div>
        <center>
            <nav aria-label="..."><ul class="pagination justify-content-center" id="pagination-gastos"></ul></nav>
        </center>
    </div>
</main>

<script "text/javascript" src="../js/funcion_combo.js"></script>
<script "text/javascript" src="../js/compra_general.js"></script> 
<script src="../js/jquery-ui.js"></script> 

<script>
	jQuery(document).ready(function(){
		$(".oculto").hide();              
		$(".inf").click(function(){
			var nodo = $(this).attr("href");  
			if ($(nodo).is(":visible")){
				$(nodo).hide("slow");
				return false;
			}else{
				$(".oculto").hide("slow");                             
				$(nodo).fadeToggle("slow");
				return false;
			}
		});
	}); 
	</script>

<script>
	var toogle= true;
	$("#btnSlide").click(function()
	{
		if (!toogle)
		{
			toogle = true;
			$("#animacion").slideUp("slow");
			$("#btnSlide").html('<i class="fas fa-shopping-basket"></i>')
		}
		else
		{
			toogle = false;
			$("#animacion").slideDown("slow");
			$("#btnSlide").html('<i class="fas fa-minus"></i>')
		}
	})
</script>