<?php include('include.php'); ?>

<main>
	<div class="container-fluid">

		<div class="row  text-center"  >

			<div class="col-sm-2 col-md-3 text-center pt-3">
				<input  class="form-control text-center" placeholder="DD/MM/AAA" type="date" id="bd-desde"/>
			</div>

			<div class="col-sm-2 col-md-2 text-center pt-3">
				 <h6 class=" pt-2">HASTA</h6>
			</div>

			<div class="col-sm-2 col-md-3 text-center pt-3">
				<input class="form-control  text-center" type="date" placeholder="DD/MM/AAAA" id="bd-hasta"/>
			</div>

			<div class="col-sm-2 col-md-2 text-center pt-3">
				<input  class="form-control text-center" type="text" onKeyUp="this.value = this.value.toUpperCase();" placeholder="Busqueda" id="bs-prod"/> 
			</div>

			<div class="col-sm-2 col-md-2">
				<a href="javascript:reportePDF3();" class="btn-floating btn-comm"><i class="fas fa-file-import"></i></a>
				
			</div>

		</div> <br>
		
	    <div class="row" >
	     	<div class="registros col-md-12 col-sm-12 pt-3  table-responsive-sm  " id="registros9"></div>
		</div> 

	</div><br>

	<center>
   		<nav aria-label="..."><ul class="pagination justify-content-center" id="paginacion9"></ul></nav>
   	</center>
</main>

<script  src="../js/busqueda_historial_corte_resiembra.js"></script>

