<?php   include('include.php'); 
    $var =$_GET['id'];
    require("../php/conexion.php");
    $consulta="SELECT idhuerta,clave,nombre,zona,hectareas,producto, clave
    FROM huerta where idhuerta = $var ORDER BY clave ASC;";
    $ress=mysqli_query($mysqli,$consulta);
?>

<html lang="en">
<title>Cuadrillas</title>
<meta charset="UTF-8">
    <main>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                <form action="" method="post" class="text-center">
                    <div class="form-row pt-3">
                        <div class="form-group col-md-2">
                            <label for="huerta">HUERTA</label>
                            <input class="form-control" id="id-huerta" name="huerta" value="<?php echo $var; ?> "type="hidden">
                            <?php 
                                while ($valores = mysqli_fetch_array($ress)) 
                                { ?>
                                    <input type="text" class="form-control" id="nombre-huerta" name="huerta" value="<?php echo $valores['nombre']; ?>"readonly>
                        </div>

                        <div class="form-group col-md-2">
                            <label for="zona">ZONA</label>
                            <input type="text" class="form-control" id="zona-huerta" name="zona" value="<?php echo $valores['zona'].""; ?>"readonly>
                        </div>

                        <div class="form-group col-md-2">
                            <label for="hectareas">HECTAREAS</label>
                            <input type="text" class="form-control" name="hectareas" value="<?php echo $valores['hectareas']." "; ?>"readonly>
                        </div>

                        <div class="form-group col-md-3">
                            <label for="producto">PRODUCTOS</label>
                            <input type="text" class="form-control" name="producto" value="<?php echo $valores['producto']." "; ?>"readonly>
                        </div>

                        <div class="form-group col-md-2">
                            <label for="clave">CLAVE</label>
                            <input type="text" class="form-control" name="clave" value="<?php echo $valores['clave']." "; ?>"readonly>
                        </div>

                        <?php } ?>
                    </div>
                </form>
                </div>
            </div>
            <hr>

		<div class="row">
			<div class="form-group col-md-12">
				<div class="registros table-responsive-sm" id="agrega-registropersonal"></div>
                <nav aria-label="..."><ul class="pagination justify-content-center" id="paginationpersonal"></ul></nav>
            </div>
		</div>
        <hr>

        <div class="row">
			<div class="col-md-12">
				<div class="registros table-responsive-sm" id="agrega-registros2"></div>
                <nav aria-label="..."><ul class="pagination justify-content-center" id="pagination2"></ul></nav>
            </div>
		</div>
        <!-- https://www.highcharts.com/forum/viewtopic.php?t=20707#p81670 -->
        </div>


    </main>

<script  src="../js/busca_cuadrilla.js"></script>
<script src="../js/jquery-ui.js"></script> 