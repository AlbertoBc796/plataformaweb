<?php include('include.php'); 
	$mysqli = new mysqli("localhost","root","","gm_cv");
	$almacen ="SELECT articulo,existencia, limite from stock where existencia <= limite-1";
	$alerta2=mysqli_query($mysqli,$almacen);		
	#https://www.youtube.com/watch?v=MQFHfFyssH4
	#https://support.awesome-table.com/hc/en-us/articles/115001399529-Use-CSS-to-change-the-style-of-each-row-depending-on-the-content
	$articulos="";
	while ($a=mysqli_fetch_array($alerta2)) {
		$articulos=$articulos." ".$a['articulo']."<br>";
	}
	$row_cnt = mysqli_num_rows($alerta2);
?>

<title>Productos Almacen</title>
<script>
	$( function() {$( "#dialog" ).dialog();} );
</script>

<style>
	td[scope="row"] {
    background-color: #ffa1a1;
	}
</style>

<main>
	<div style="background-color:#ffa1a1;" id="dialog" title="<?php echo $row_cnt; ?> productos por abastecer...">
		<p><?php echo $articulos; ?></p>
	</div>

	<div class="container-fluid" >
		<div class="row pt-3">
			<div class="col-md-12 text-center" id="animacion"  >
				<p class="text-center h3 ">ALTA DE PRODUCTOS DE ALMACEN </p>
				<form action="../php/inserciones/insertar_almacen.php" method="POST" >
					<div class="form-row text-center mt-5">
						<div class="form-group col-md-2 text-center">
							<label for="Fecha">FECHA</label>
							<input  type="date"   class="form-control text-center" name="fecha" required>
						</div>

						<div class="form-group col-md-2 text-center">
							<label for="">CLAVE</label>
							<input type="text" onKeyUp="this.value = this.value.toUpperCase();"  class="form-control text-center" name="clave" id="clave" required>
						</div>

						<div class="form-group col-md-5 text-center">
							<label for="articulo" >ARTICULO</label>
							<input type="text"  onKeyUp="this.value = this.value.toUpperCase();"  class="form-control text-center" name="articulo" id="articulo" required>
						</div>

						<div class="form-group col-md-2 text-center">
							<label for="">UNIDADES</label>
							<input type="text" title="Unidad de medicion" onKeyUp="this.value = this.value.toUpperCase();"  class="form-control text-center"  onkeypress="return soloLetras(event);"  name="unidad" id="unidad" required>
						</div>

						<div class="form-group col-md-2 text-center">
							<label for="">CANTIDAD</label>
							<input type="text" class="form-control text-center" name="existencia" id="existencia" required>	
						</div>

						<div class="form-group col-md-2 text-center">
							<label for="">LIMITE</label>
							<input type="text" class="form-control text-center" name="limite" id="limite" required>	
						</div>

						<div class="form-group col-md-3 text-center">
							<label for="">PROVEEDOR</label>
							<input type="text" class="form-control text-center" name="proveedor" id="proveedor" required>	
						</div>

						<div class="form-group col-md-2 text-center">
							<label for="">PRECIO UNIT</label>
							<input type="number" step="0.01" class="form-control text-center" name="preciounit" id="preciounit" required>	
						</div>

						<div class="form-group col-sm-2 text-center pt-3">
							<button type="submit" class="btn btn-primary" name="submit" value="submit"><i class="fa fa-save"></i></button>
						</div>
					</div>
				</form>
			</div>	
		</div>

	<hr>

		<div class="row" > <!--  ROW QUE CONTINE LAS ENTRADAS Y SALIDAS DEL ALMACEN-->
			<div class="col-md-12 ">
				<p class="text-center h3">ENTRADA / SALIDA DE PRODUCTOS </p>
				<form action="../php/inserciones/insertar_historial_almacen.php" class="mt-5" method="POST">
					<div class="form-row ui-widget text-center ">
						<div class="form-group col-md-2 text-center">
							<label for="fecha " >FECHA</label>
							<input type="date"  class="form-control text-center"   name="fecha"  required>
							<!--pattern="(0[1-9]|1[0-9]|2[0-9]|3[01]).(0[1-9]|1[012]).[0-9]{4}"-->
						</div>

						<div class="form-group col-md-3 ">
							<label for="articulo2" >ARTICULO</label>
							<input type="text" class="form-control" name="articulo" id="articulo2" required>
							<input  id="idstock" onKeyUp="this.value = this.value.toUpperCase();"  name="idstock" class="form-control text-center" type="hidden" required>
						</div>

						<div class="form-group col-md-2 text-center">
							<label for="existencia2">ACTUAL</label>
							<input type="text" class="form-control text-center" name="existencia" id="existencia2" onKeyUp="this.value = this.value.toUpperCase();"  readonly="">
						</div>

						<div class="form-group col-md-2">
							<?php
								require("../php/conexion.php");
								$consulta="SELECT * FROM operacion"; 				
								$ress=mysqli_query($mysqli,$consulta);
							?>
							<label for="operacion">OPERACION</label>
							<select  name="operacion" class="form-control browser-default text-center" id="operacion"  required>
									<option  value="">Selecciona</option>
									<?php 
										while($arreglo=mysqli_fetch_array($ress))
										{
											?>
											<option value="<?php echo $arreglo['idoperacion'] ?>">
											<?php echo $arreglo['tipo']." ";?>	
											</option><?php  } ?>
							</select>
						</div>

						<div class="form-group col-md-2 text-center">
							<label for="cantidad">CANTIDAD</label>
							<input class="form-control text-center" name="cantidad" id="cantidad" onkeyup="almacen();" required>	
						</div>

						<div class="col-md-2">
							<label for="">EXISTENCIA</label> 
							<input type="text" class="form-control text-center" name="saldoNuevo" id="saldoNuevo" readonly="">
							<input class="form-control text-center" name="saldoant" id="saldoant" type="hidden" readonly="">
						</div>

						<div class="form-group col-md-2 text-center">
                            <label for="camion">CAMION</label>
                            <input name="unidad" id="camion" autocomplete="off" type="text" onKeyUp="this.value = this.value.toUpperCase();" class="form-control camion text-center">
                        </div>

						<div id="destino_salida" class="form-group col-md-2">
							<label for="destino" >HUERTA</label>
							<input  autocomplete="off" type="text" onKeyUp="this.value = this.value.toUpperCase();" class="form-control text-center" name="destino" id="buscador" onkeyup="autocompletar()" >
						</div>

						<div id="vale" class="form-group col-md-2">
							<label for="destino" >VALE</label>
							<input  autocomplete="off" type="text" onKeyUp="this.value = this.value.toUpperCase();" class="form-control text-center" name="vale" onkeyup="autocompletar()" >
						</div>

						<div class="form-group col-md-2 text-center">
							<label for="producto">PRODUCTO</label>
							<select name="producto" id="producto" class="form-control browser-default"  required onchange="JavaScript:showMsg()" onchange="JavaScript:yesnoCheck()">
                                <option value="0">Seleccione</option>
                            </select>
						</div>

						<div class="form-group text-center col-sm-1 pt-3">
							<button type="submit" class="btn btn-primary" name="submit" value="submit"><i class="fa fa-save"></i></button>
						</div>
					</div>
				</form>
			</div>
		</div> <!-- TERMINA ROW DE ENTRADAS Y SALIDAS DEL ALMACEN-->
	<hr><br>

		<div class="row">
			<div class="col-md-4 col-sm-4 text-center">
				<a id="btnSlide" class="btn-floating  btn-comm ">
				<i class="fas fa-toolbox"></i> </a>
			</div>

			<div class="col-md-4 col-sm-4 pt-3 text-center">
				<input  class="form-control" type="text" placeholder="Busqueda...."  onKeyUp="this.value = this.value.toUpperCase();" id="bs-prod"/>
			</div>

			<div class="col-md-2 col-sm-2 text-center">
				<a  href="../php/impresiones/orden_compra.php" class="btn-floating btn-comm "><i class="fas fa-shopping-basket"></i></a>
			</div>

			<div class="col-md-2 col-sm-2 text-center">
				<a  href="../php/impresiones/almacen.php" class="btn-floating btn-comm "><i class="fas fa-file-import"></i></a>
			</div>
		</div> <br>

		<div class="row" >
			<div class="col-md-12">
				<div class="registros table table-responsive-sm" id="agrega-registros4"></div>
			</div> 
		</div>
	</div> <!--CONTAINER FLUID--> 		

<br>

	<center>
		<nav aria-label="..."><ul class="pagination justify-content-center" id="pagination2"></ul></nav>
	</center>
</main>


<script src="../js/jquery-ui.js"></script> 
<script src="../js/busqueda_almacen.js"></script>
<script "text/javascript" src="../js/funcion_combo.js"></script>

<!-- ESCRIPT PARA OCULTAR EL FORMULARIO DE AGREGAR-->

<script>
	var toogle= false;
	$("#btnSlide").click(function()
	{
		if (!toogle)
		{
			toogle = true;
			$("#animacion").slideUp("slow");
			$("#btnSlide").html('<i class="fas fa-toolbox"></i>')
		}
		else
		{
			toogle = false;
			$("#animacion").slideDown("slow");
			$("#btnSlide").html('<i class="fas fa-minus"></i>')
		}
	})
</script>

<script type="text/javascript">
	$(function() {
        $("#articulo2").autocomplete({
        source: "../php/autocompletadores/autocomplete_almacen.php",
        minLength: 1,
        select: function(event, ui) {
        event.preventDefault();
        $('#articulo2').val(ui.item.articulo2);
		$('#existencia2').val(ui.item.existencia2);
		$('#idstock').val(ui.item.idstock);
		$('#saldoant').val(ui.item.saldoant);
        }
        });
    });
</script>

<script type="text/javascript">
    $(function() {
            $("#buscador").autocomplete({
                source: "../php/autocompletadores/autocomplete_almacen_destino.php",
                minLength: 1,
                select: function(event, ui) {
                event.preventDefault();
                    $('#buscador').val(ui.item.destino);       
                    $('#iddestino').val(ui.item.iddestino);
            }
            });
    });
</script>

<script type="text/javascript">
	$(function() {
        $("#camion").autocomplete({
            source: "../php/autocompletadores/transporte.php",
            minLength: 1,
            select: function(event, ui) {
            event.preventDefault();
                $('#camion').val(ui.item.clave);
                $('#marca').val(ui.item.marca);
                $('#colors').val(ui.item.colors);
                $('#placas').val(ui.item.placas);
                $('#idtransporte').val(ui.item.idtransporte);
            }
        });
    });
</script>