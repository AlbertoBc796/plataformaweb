<?php include('include.php');  ?>


<main>
	<div class="container-fluid">
	<br>
		<div class="row justify-content-center" >
			<div class="col-sm-6"  >
				<div class="card">
					<div class="card-body">
						<h3 class="text-center">Credenciales de la base de datos</h3>
						<br>
						<form method="POST" action="../php/backup/backup.php">
						    <div class="form-group row ">
						     	<label for="server" class="col-sm-3 col-form-label">Servidor</label>
						      	<div class="col-sm-9">
						        	<input type="text" class="form-control" id="server" name="server" value="localhost"  readonly="">
						      	</div>
						    </div>
						    <div class="form-group row">
						      	<label for="username" class="col-sm-3 col-form-label">Usuario</label>
						      	<div class="col-sm-9">
						        	<input type="text" class="form-control" id="username" name="username" value="root"  readonly="">
						      	</div>
						    </div>
						    <div class="form-group row">
						      	<label for="password" class="col-sm-3 col-form-label">Contraseña</label>
						      	<div class="col-sm-9">
						        	<input type="text" class="form-control" id="password" name="password" placeholder="********************" readonly="">
						      	</div>
						    </div>
						    <div class="form-group row">
						      	<label for="dbname" class="col-sm-3 col-form-label">Base de datos</label>
						      	<div class="col-sm-9">
						        	<input type="text" class="form-control" id="dbname" name="dbname" value="gm_cv" readonly="">
						      	</div>
						    </div>

						    <div class="form-group row text-center">
						    	<div class="col-sm-12">
						    		<button type="submit" class="btn btn-primary" name="backup">Respaldo</button>
						    	</div>
						    </div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>
