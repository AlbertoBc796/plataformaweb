<?php include('include.php');  ?>
<head>
    <script src="../libreria/code/highcharts.js"></script>
    <script src="../libreria/code/highcharts-3d.js"></script>
    <script src="../libreria/code/modules/exporting.js"></script>
    <script src="../libreria/code/modules/export-data.js"></script>
</head>
<main>  
    <div class="container-fluid">
        <div class="row " >
            <div class="col-md-12 col-sm-12 " >
                <form action="" class="text-center" >
                    <div class="form-row text-center  d-flex justify-content-around" >
                        <div class="form-group col-md-4 col-sm-4" >
                            <input type="date" class="form-control text-center">
                        </div>

                        <div class="form-group col-md-4 col-sm-4" >
                            <input type="date" class="form-control text-center">
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="row" >
             <div id="cont" style="height: 100%;width: 100%; border: 1px solid #6A908D"></div>
        </div>
    </div>
</main>
        <script type="text/javascript">

            Highcharts.chart('cont', {
                chart: {
                    type: 'pie',
                    options3d: {
                        enabled: true,
                        alpha: 45,
                        beta: 0
                    }
                },
                title: {
                    text: 'COMPRA VENTA'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        depth: 35,
                        dataLabels: {
                            enabled: true,
                            format: '{point.name}'
                        }
                    }
                },
                series: [{
                    type: 'pie',
                    name: 'PORCENTAJES',
                    data: [

<?php
    include ('../php/conexion.php');
   $inicio = "2018-01-01";
   $final ="2018-12-31";
    $sql ="SELECT  c.idcompra_venta_limon,c.fecha,h.nombre ,c.ingreso_total from compra_venta_limon c  
    inner join huerta h on c.idhuerta=h.idhuerta
    where c.fecha between '$inicio' and '$final' order by fecha";
    $result = mysqli_query ($mysqli,$sql);

    while ($registros = mysqli_fetch_array($result)) {
        ?>
     ['<?php echo $registros ["nombre"]; ?> ', <?php echo $registros ['ingreso_total']; ?>  ],   

    <?php 
    }
    ?>     ]
                }]
            });
        </script>