<?php 
	include('include.php');
	require("../php/conexion.php");
	$consulta="SELECT idcargo,cargo FROM cargo ORDER BY cargo ASC;"; 				
	$ress=mysqli_query($mysqli,$consulta);
	$revisa="SELECT e.idpersona, e.idcargo, concat(p.nombre,' ',p.apellidop,' ',p.apellidom) as ncp, c.cargo, c.idcargo 
	from empleado e 
	inner join cargo c on c.idcargo = e.idcargo
	inner join persona p on p.idpersona = e.idpersona where c.cargo like '%JEFE%'";
	$cuadrilla=mysqli_query($mysqli,$revisa);
?>

<title>Registro empleado</title>
<meta charset="utf8">
<main>
	<div class="container-fluid">
		<div class="row pt-3" id="ocultar">
			<div class="col-md-12 pt-3" >
				<form action="../php/inserciones/insertar_personal.php" method="post">			
					<div class="form-row ui-widget">
						<div class="form-group col-md-2 text-center">
							<label id="label" for="">FECHA</label>
							<input type="date"  class="form-control text-center"  name="fecha"  required  placeholder="DD/MM/AAAA">
						</div>

						<div class="form-group col-md-6 text-center">
							<label id="label" for="">NOMBRE</label>
							<input  type="text" class="form-control text-center"   name="nombre" id="nombre">
							<input onKeyUp="this.value = this.value.toUpperCase();" id="idpersona" name="idpersona" class="form-control" type="hidden">
						</div>
						
						<!-- ==========00SELECCION DE CARGO================== -->
						<div class="form-group col-md-4 text-center">
							<label for="clave" class="text-center"> CARGO </label>
							<select id="cargo" name="idcargo" onchange="JavaScript:showMsg()" class="form-control browser-default text-center" onKeyUp="this.value = this.value.toUpperCase();"  required>
							<?php
								while($arreglo=mysqli_fetch_array($ress))
								{
									?>
									<option value="<?php echo $arreglo['idcargo'] ?>">
									<?php echo $arreglo['cargo']." ";?>
									</option><?php  
								} ?>
							</select>
						</div>
					</div>

					<div class="form-row">
						<div class="form-group col-md-4 text-center">
							<label id="label">HUERTAS</label>
							<div align="left" name="huertas" id="huertas" class="container-fluid" style="overflow-y: scroll;max-height: 100px;"></div>
						</div>

						<div id="cuadrilla" class="form-group col-md-3 text-center">
							<label for="clave" class="text-center">CUADRILLA</label>
							<select name="idcuadrilla" id="idcuadrilla" class="form-control browser-default text-center" onKeyUp="this.value = this.value.toUpperCase();">
									<option value="0">SIN CUADRILLA</option>
								<?php
									while ($a=mysqli_fetch_array($cuadrilla)) {?>
										<option value="<?php echo $a['idpersona'] ?>">
									<?php echo $a['ncp']." ";?>
									</option><?php  
									}
								?>
							</select>
						</div>

						<div class="form-group col-md-3 text-center">
							<label  for="">SALARIO</label>
							<input type="text"  class="form-control text-center"
							name="salario" onKeyUp="this.value = this.value.toUpperCase();"  required placeholder="$">
						</div>

						<div class="form-group col-md-1 text-center pt-3">
							<button type="submit" class="btn btn-primary" name="submit" value="submit"><i class="fa fa-save"></i></button>
						</div>
					</div>
				</form>
				<br>
			</div>
			<br>
		</div> 

		<hr>
		<br>

		<div class="row" >
			<div class="col-md-4 col-sm-4 text-center">
				<a id="btnSlide" class="btn-floating  btn-comm ">
				<i class="fas fa-id-card-alt"></i> </a>
			</div>

			<div class="col-md-4 col-sm-4 text-center pt-3">
				<input  class="form-control " type="text" placeholder="Busqueda...." id="bs-prod"  onKeyUp="this.value = this.value.toUpperCase();"/>
			</div>

			<div class="col-md-4 col-sm-4 text-center">
				<a  href="../php/impresiones/empleados.php" class="btn-floating btn-comm "><i class="fas fa-file-import"></i></a>
			</div>
		</div> <br>

		<div class="row">
			<div class="col-md-12">
				<div class="registros table-responsive-sm" id="agrega-registropersonal">
				</div>
			</div> 
		</div>
    </div> <br>


    <center>
        <nav aria-label="..."><ul class="pagination justify-content-center" id="paginationpersonal"></ul></nav>
    </center>

</main><!--/Main layout-->

<script  src="../js/busca_personal.js"></script>
<script src="../js/jquery-ui.js"></script> 

<script type="text/javascript">
	$(function() {
            $("#nombre").autocomplete({
                source: "../php/autocompletadores/autocompleteper.php",
                minLength: 1,
                select: function(event, ui) {
					event.preventDefault();
					$('#nombre').val(ui.item.nombre);
					$('#idpersona').val(ui.item.idpersona);
				}
            });
    });
</script>

<script>
	var toogle= false;
    $("#btnSlide").click(function()
	{
		if (!toogle)
		{
			toogle = true;
			$("#ocultar").slideUp("slow");
			$("#btnSlide").html('<i class="fas fa-id-card-alt"></i>')
		}
		else
		{
			toogle = false;
			$("#ocultar").slideDown("slow");
			$("#btnSlide").html('<i class="fas fa-minus"></i>')
		}
	})
</script>
