<?php include('include.php');
?>
<html lang="en">
	<meta charset="UTF-8">
	<main>
		<div class="container-fluid" >
			<div class="row" >
				<div class="col-md-12">
					<form action="../php/inserciones/insertar_arboles.php" method="post" class="text-center">
						<div class="form-row pt-3">
							<div class="form-group col-md-2">
								<label for="fecha"> FECHA</label>
								<input type="date" class="form-control "  name="fecha" >		 						 
							</div>

							<?php 
                                require("../php/conexion.php");
                                    $consulta="SELECT idhuerta,clave, nombre FROM huerta ORDER BY clave ASC;";              
									$ress=mysqli_query($mysqli,$consulta);
							?>

							<div class="form-group col-md-2 text-center">
								<label for="huerta" class="text-center"> HUERTA </label>
								<select  name="huerta" class="form-control browser-default"  required>
								<?php
									while($arreglo=mysqli_fetch_array($ress))
									{
								?>
									<option selected="selected" value="<?php echo $arreglo['idhuerta'] ?>">
									<?php echo $arreglo['nombre']." ";?>
									</option><?php  } ?>
									</select>
							</div>

							<div class="col-md-2">
								<label for="fecha">TABLA</label>
								<input type="text" class="form-control " name="tabla" >
							</div>

							<?php 
                                require("../php/conexion.php");
                                    $consulta="SELECT idvariedad, nombre FROM variedad ;";              
									$ress=mysqli_query($mysqli,$consulta);
							?>

                            <div class="form-group col-md-2 text-center">
                                <label for="huerta" class="text-center"> VARIEDAD </label>
                                    <select  name="idvariedad" class="form-control browser-default"  required>
                                    <?php 
                                        while($arreglo=mysqli_fetch_array($ress))
                                        {
                                    ?>
                                        <option selected="selected" value="<?php echo $arreglo['idvariedad'] ?>">                  
                                        <?php echo $arreglo['nombre']." ";?>
                                        </option><?php  } ?>
                                    </select>
                            </div>

                            <div class="form-group col-md-2">
								<label >ARBOL RESIEMBRA</label>
								<input type="text" class="form-control" name="arbolc" id="arbolch" >
							</div>

							<div class="form-group col-md-2" >
								<label >ARBOL MEDIANO</label>
								<input type="text" class="form-control" name="arbolm" id="arbolme" >
							</div>
						</div>

						<div class="form-row">
							<div class="form-group col-md-2" >
								<label >ARBOL PRODUCCION</label>
								<input type="text" class="form-control" name="arbolp" id="arbolpr">
							</div>

							<div class="form-group col-md-2" >
								<label >TOTAL</label>
								<input type="text" class="form-control" name="total" id="totalt" onkeyup="total_arboles()">
							</div>

							<div class=" form-group col-md-1  pt-3 " >
								<button type="submit" class="btn btn-primary" name="submit" value="submit"><i class="fa fa-save"></i></button> 
							</div>
						</div>
					</form>
				</div>
			</div> 
		<hr> 
			<div class="row text-center">
				<div class="col-sm-4 col-md-4  ">
					<a href="#info1" class="inf">ARBOLES CHICOS</a>
				</div>
				<div class="col-sm-4 col-md-4  ">
					<a href="#info2" class="inf">ARBOLES MEDIANOS</a>
				</div>
				<div class="col-sm-4 col-md-4  ">
					<a href="#info3" class="inf">ARBOLES EN PRODUCCION</a>
				</div>
			</div>

<!--alert alert-secondary-->
		<br><br>
		
			<div class="row text-center "> <br><br><br>
				<div id="info1" class="col-xs-12  oculto">
					<form action="../php/inserciones/insertar_historial_arboles.php" method="POST">
						<div class="form-row ui-widget text-center ">
							<div class="form-group col-md-2 text-center">
								<label for="fecha " >FECHA</label>
								<input type="date" required class="form-control text-center"   name="fecha"  >
							</div>

							<div class="form-group col-md-2 ">
								<label for="articulo2" >TABLA</label>
								<input  id="idarboles" onKeyUp="this.value = this.value.toUpperCase();"  name="idarboles" class="form-control text-center" type="hidden">
								<input type="text" class="form-control" name="tabla" id="tabla">
							</div>

							<div class="form-group col-md-2 text-center">
								<label for="arbolc">ARB. CH</label>
								<input type="text" class="form-control text-center" name="arbolc" id="arbolc" onKeyUp="this.value = this.value.toUpperCase();"  readonly>
							</div>

							<div class="form-group col-md-2">
								<?php 
									require("../php/conexion.php");
									$consulta="SELECT * FROM operacion"; 				
									$ress=mysqli_query($mysqli,$consulta);
								?>
								<label for="operacion">ACTIVIDAD</label>
									<select  name="operacion" class="form-control browser-default text-center"  required>
										<option  value="">Selecciona</option>
										<?php 
											while($arreglo=mysqli_fetch_array($ress))
											{
										?>
											<option value="<?php echo $arreglo['idoperacion'] ?>">
											<?php echo $arreglo['tipo']." ";?>	
											</option><?php  } ?>
								</select>
							</div>

							<div class="form-group col-md-2 text-center">
								<label for="cantidad">CANTIDAD</label>
								<input type="text" class="form-control text-center" required  name="cantidad" id="cantidad">		
							</div>

							<div class="form-group text-center col-sm-2 pt-3">
								<button type="submit" class="btn btn-primary" name="submit" value="submit"><i class="fa fa-save"></i></button>
							</div>
						</div>
					</form>
				</div>

				<div id="info2" class="col-xs-12  oculto">
					<form action="../php/inserciones/insertar_historial_arboles_medianos.php" method="POST">
						<div class="form-row ui-widget text-center ">

							<div class="form-group col-md-2 text-center">
								<label for="fecha" >FECHA</label>
								<input type="date"  class="form-control text-center"   name="fecham">
							</div>

							<div class="form-group col-md-2 ">
								<label >TABLA</label>
								<input type="text" class="form-control" name="tablam" id="tablam">
								<input  id="idarbolesm"  name="idarbolesm" class="form-control text-center" type="hidden">
							</div>

							<div class="form-group col-md-2 text-center">
								<label for="arbolesm">ARB. MED</label>
									<input  id="arbolesm"   name="arbolesm" class="form-control text-center" type="text" readonly="">
							</div>

							<div class="form-group col-md-2">
								<?php
									require("../php/conexion.php");
										$consulta="SELECT * FROM operacion";
										$ress=mysqli_query($mysqli,$consulta);
								?>
								<label for="operacion">ACTIVIDAD</label>
									<select  name="operacionm" class="form-control browser-default text-center"  required>
										<option  value="">Selecciona</option>
										<?php 
											while($arreglo=mysqli_fetch_array($ress))
											{
										?>
										<option value="<?php echo $arreglo['idoperacion'] ?>">
										<?php echo $arreglo['tipo']." ";?>	
										</option><?php  } ?>
								</select>
							</div>

							<div class="form-group col-md-2 text-center">
								<label for="cantidad">CANTIDAD</label>
								<input type="text" class="form-control text-center" name="cantidadm" id="cantidadm">		
							</div>

							<div class="form-group text-center col-sm-2 pt-3">
								<button type="submit" class="btn btn-primary" name="submit" value="submit"><i class="fa fa-save"></i></button>
							</div>
						</div>
					</form>
				</div>

				<div id="info3" class="col-xs-12  oculto">
					<form action="../php/inserciones/insertar_historial_arboles_produccion.php" method="POST">
						<div class="form-row ui-widget text-center ">
							<div class="form-group col-md-2 text-center">
								<label for="fecha " >FECHA</label>
								<input type="date"  class="form-control text-center"   name="fechap"  >
							</div>

							<div class="form-group col-md-2 ">
								<label for="articulo2" >TABLA</label>
								<input type="text" class="form-control" name="tablap" id="tablap">
								<input  id="idarbolesp" onKeyUp="this.value = this.value.toUpperCase();"  name="idarbolesp" class="form-control text-center" type="hidden">
							</div>

							<div class="form-group col-md-2 text-center">
								<label for="existencia2">ARB. PROD</label>
								<input type="text" class="form-control text-center" name="arbolcp" id="arbolcp" onKeyUp="this.value = this.value.toUpperCase();"  readonly="">
							</div>

						
							<div class="form-group col-md-2">
								<?php
									require("../php/conexion.php");
									$consulta="SELECT * FROM operacion"; 				
									$ress=mysqli_query($mysqli,$consulta);
								?>
									<label for="operacion">ACTIVIDAD</label>
									<select  name="operacionp" class="form-control browser-default text-center"  required>
									<option  value="">Selecciona</option>
								<?php 
									while($arreglo=mysqli_fetch_array($ress))
									{
								?>
									<option value="<?php echo $arreglo['idoperacion'] ?>">
									<?php echo $arreglo['tipo']." ";?>	
									</option><?php  } ?>
									</select>
							</div>

							<div class="form-group col-md-2 text-center">
								<label for="cantidad">CANTIDAD</label>
								<input type="text" class="form-control text-center" name="cantidadp" id="cantidad">		
							</div>

							<div class="form-group text-center col-sm-2 pt-3">
								<button type="submit" class="btn btn-primary" name="submit" value="submit"><i class="fa fa-save"></i></button>
							</div>
						</div>
					</form>
				</div>
			</div>
		<hr>
			<div class="row" >
				<div class="col-md-4 col-sm-4 text-center"></div>
				<div class="col-md-4 col-sm-4 text-center pt-3">
					<input  class="form-control text-center  " type="text" placeholder="Busqueda...." id="bs-prod"  onKeyUp="this.value = this.value.toUpperCase();"/>
				</div>
				
				<div class="col-md-4 col-sm-4 text-center">
					<a  href="../php/impresiones/arboles.php" class="btn-floating btn-comm "><i class="fas fa-file-import"></i></a>
				</div>
			</div> <br>

			<div class="row">
				<div class="col-md-12">
					<div class="registros table table-responsive-sm" id="agrega-registroarbol"></div>
				</div>
			</div>

		</div> <br><br>
		<center>
			<nav aria-label="..."><ul class="pagination justify-content-center" id="paginationarbol">
				</ul>
			</nav>
		</center>

<script src="../js/jquery-ui.js"></script> 
<script  src="../js/busca_arbol.js"></script>

<script>
	jQuery(document).ready(function(){
		$(".oculto").hide();              
		$(".inf").click(function(){
			var nodo = $(this).attr("href");  
			if ($(nodo).is(":visible")){
				$(nodo).hide();
				return false;
			}else{
				$(".oculto").hide("slow");                             
				$(nodo).fadeToggle("fast");
				return false;
			}
		});
	}); 
	</script>
</main>

	<script>
		function total_arboles(){
        n1 = document.getElementById("arbolch").value;
        n2 = document.getElementById("arbolme").value;
        n3 = document.getElementById("arbolpr").value;
        n4 = parseFloat(n1) + parseFloat(n2) + parseFloat(n3);
        document.getElementById("totalt").value = n4;
    }
	</script>


	<script >
		$(function() {
			$("#tabla").autocomplete({
				source: "../php/autocompletadores/autocomplete_arboles.php",
				minLength: 1,
				select: function(event, ui) {
					event.preventDefault();
					$('#tabla').val(ui.item.tabla);
					$('#arbolc').val(ui.item.arbolc);
					$('#idarboles').val(ui.item.idarboles);
				}
			});
		});

		$(function() {
			$("#tablam").autocomplete({
				source: "../php/autocompletadores/autocomplete_arboles_medianos.php",
				minLength: 1,
				select: function(event, ui) {
					event.preventDefault();
					$('#tablam').val(ui.item.tablam);
					$('#arbolesm').val(ui.item.arbolesm);
					$('#idarbolesm').val(ui.item.idarbolesm);
				}
			});
		});

		$(function() {
			$("#tablap").autocomplete({
				source: "../php/autocompletadores/autocomplete_arboles_produccion.php",
				minLength: 1,
				select: function(event, ui) {
					event.preventDefault();
					$('#tablap').val(ui.item.tablap);
					$('#arbolcp').val(ui.item.arbolcp);
					$('#idarbolesp').val(ui.item.idarbolesp);
				}
			});
		});
	</script>