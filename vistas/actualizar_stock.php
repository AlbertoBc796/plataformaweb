<?php include('include.php'); 
    extract($_GET);
		require("../php/conexion.php");
			$sql="SELECT idstock,fecha,clave,articulo,unidad,existencia,limite,proveedor,preciounit FROM stock where idstock=$id";
			$ressql=mysqli_query($mysqli,$sql);	
				while ($fila=mysqli_fetch_row ($ressql)){
					$id=$fila[0];
					$fecha=$fila[1];
					$clave=$fila[2];
					$articulo=$fila[3];
					$unidad=$fila[4];
					$existencia=$fila[5];
					$limite=$fila[6];
					$proveedor=$fila[7];
					$precio=$fila[8];
				}
?> 
	<title>Actualizar almacen</title>
	<main>	
		<div class="container-fluid">
			
			<div class="row col-md-12">
				<form action="../php/actualizaciones/actualizar_stock.php" class="pt-3" method="POST">

					<div class="form-row text-center">
						<div class="form-group col-sm-2 col-md-2">
							<input class="form-control text-center" type="hidden" name="id" value= "<?php echo $id ?>" readonly="readonly" >
							<label for="">FECHA</label>
							<input class="form-control text-center "  onKeyUp="this.value = this.value.toUpperCase();" type="text"  name="fecha" placeholder=""  value="<?php echo $fecha?>">
						</div>

						<div class="form-group text-center col-sm-3 col-md-3">
							<label for="">CLAVE</label>
							<input class="form-control text-center"  onKeyUp="this.value = this.value.toUpperCase();" type="text" name="clave"  value="<?php echo $clave?>">
						</div>

						<div class="form-group text-center col-sm-3 col-md-3">
							<label for="">ARTICULO</label>
							<input class="form-control text-center"  onKeyUp="this.value = this.value.toUpperCase();" type="text" name="articulo"  value='<?php echo $articulo?>'>
						</div>

						<div class="form-group text-center col-sm-2 col-md-2">
							<label for="">UNIDAD</label>
							<input class="form-control text-center"  onKeyUp="this.value = this.value.toUpperCase();" type="text" name="unidad" value="<?php echo $unidad?>">	
						</div>

						<div class="form-group text-center col-sm-2 col-md-2">
							<label for="">EXISTENCIA</label>
							<input class="form-control text-center" type="text" name="existencia"  value="<?php echo $existencia?>"  onKeyUp="this.value = this.value.toUpperCase();">
						</div>

						<div class="form-group text-center col-sm-2 col-md-2">
							<label for="">LIMITE</label>
							<input class="form-control text-center" type="text" name="limite"  value="<?php echo $limite?>">
						</div>

						<div class="form-group text-center col-sm-2 col-md-4">
							<label for="">PROVEEDOR</label>
							<input class="form-control text-center" type="text" name="proveedor"  value="<?php echo $proveedor?>"  onKeyUp="this.value = this.value.toUpperCase();">
						</div>

						<div class="form-group text-center col-sm-2 col-md-2">
							<label for="">PRECIO UNIT.</label>
							<input class="form-control text-center" type="text" name="precio"  value="<?php echo $precio?>"  onKeyUp="this.value = this.value.toUpperCase();">
						</div>

						<div class="form-group text-center col-sm-3 pt-3">
							<input  type="submit" value="Guardar" class="btn btn-success btn-primary">
						</div>
					</div>
				</form>
			</div>
		</div>	
	</main>
