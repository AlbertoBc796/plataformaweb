<?php include('include.php');  ?>
<meta charset="UTF-8">
 <main>
 	<div class="container-fluid"  >
	 	<div class="row text-center">
			
			<div class="col-sm-2 pt-3 text-center">
				<input  class="form-control text-uppercase text-center" placeholder="DD/MM/AAA" type="date" id="bd-desde"/>
			</div>
			<div class="col-sm-2 pt-3 text-center">
				<h6 class="mt-2 pl-4 text-center">HASTA</h6>
			</div>
			<div class="col-sm-2 pt-3  text-center">
				<input class="form-control  text-uppercase text-center"  placeholder="DD/MM/AAA"  type="date" id="bd-hasta"/>
			</div>
			<div class="col-sm-2 pt-3 text-center">
				<input  class="form-control  text-center" onKeyUp="this.value = this.value.toUpperCase();"  type="text" placeholder=" # FACTURA" id="bs-prod"/>  
			</div>
			<div class="col-sm-2  text-center">
				<a type="button" class="btn-floating btn-comm text-center" onclick="buscadorfactura();"><i class="fas fa-search"></i></a>
			</div>
			<div class="col-sm-2">
				<a  href="javascript:reportePDF();" class="btn-floating btn-comm text-center"><i class="fas fa-file-import"></i></a>
			</div>
	 	</div>
	 	<br>


	 	<div class="row">
	 		<div class="registros col-sm-12 col-md-12 pt-3" id="agrega-registrosfacturalimon"></div>
	 	</div>

 	</div>

 	<center>
        <nav aria-label="..."><ul class="pagination justify-content-center" id="paginationfacturalimon"></ul></nav>
    </center>
 	

 </main>

 <script src="../js/busqueda_compra_venta_facturacion_limon.js"></script>