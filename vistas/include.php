<?php 
    include ('../php/conexion.php');
    require_once("../php/sesiones/error_handle.php");
    if (session_start()) {
    $sql = "SET GLOBAL event_scheduler=on"; #ESTA CONSULTA ES PARA ENCENDER 
    #EL PLANIFICADOR DE EVENTOS DE PHPMYADMIN EL CUAL 
    #LO USO PARA QUE SE EJECUTEN MIS EVENTOS PROGRAMADOS....
    $ejecutar = mysqli_query($mysqli,$sql);
    }
    if(!isset($_SESSION['user'])){
        header("Location: ../index.php");
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/mdb.min.css">
    <link rel="stylesheet" href="../css/font-awesome/css/all.css">
    <link rel="stylesheet" href="../css/jquery-ui.css">
    <link rel="stylesheet" href="../css/modal_sesion.css">
    <link rel="stylesheet" href="../css/jquery.lwMultiSelect.css" />
    <!-- include the style -->
<link rel="stylesheet" href="../sas/css/alertify.min.css" />
<!-- include a theme -->
<link rel="stylesheet" href="../sas/css/themes/default.min.css" />

</head>
<body class="fixed-sn cyan-skin">
	<!--Double navigation-->
    <header>
        <!-- Sidebar navigation -->
        <div id="slide-out" class="side-nav sn-bg-4 fixed">
            <ul class="custom-scrollbar">
                <li>
                    <div class="logo-wrapper waves-light">
                        <a href="#"><img src="../img/logob2.png" style="width: 251px; height: 140px;" class="img-fluid flex-center"></a>
                    </div>
                </li>

                <li>
                    <ul class="collapsible collapsible-accordion">
                        <li><a class="collapsible-header waves-effect arrow-r"><i class="fab fa-gg"></i> Compra Venta<i class="fa fa-angle-down rotate-icon"></i></a>
                            <div class="collapsible-body">
                                <ul class="list-unstyled">
                                    <li><a href="compra_venta.php" class="waves-effect">Compra Venta</a></li>
                                    <li><a href="facturacion2.php" class="waves-effect">Facturacion</a></li>
                                    <li><a href="gastos_generales.php" class="waves-effect">Gastos generales</a></li>
                                </ul>
                            </div>
                        </li>

                        <li>
                            <a class="collapsible-header waves-effect arrow-r"><i class="fab fa-gg">
                            </i> Personal <i class="fa fa-angle-down rotate-icon"></i></a>
                            <div class="collapsible-body">
                            <ul class="list-unstyled">
                                    <li><a href="actividades_general.php" class="waves-effect">Actividades General</a></li>
                                </ul>
                            </div>
                        </li>

                        <li>
                            <a class="collapsible-header waves-effect arrow-r"><i class="fab fa-gg">
                            </i> Huertas <i class="fa fa-angle-down rotate-icon"></i></a>
                            <div class="collapsible-body">
                            <ul class="list-unstyled">
                                <li><a href="huertas.php" class="waves-effect">Registro Huertas</a></li>
                                <li><a href="cards.php" class="waves-effect">Informacion huertas</a></li>
                            </ul>
                            </div>
                        </li>

                        <li>
                            <a class="collapsible-header waves-effect arrow-r"><i class="fab fa-gg">
                            </i> Almacen <i class="fa fa-angle-down rotate-icon"></i></a>
                            <div class="collapsible-body">
                            <ul class="list-unstyled">
                                    <li><a href="almacen.php" class="waves-effect">Productos</a></li>
                                    <li><a href="historial.php" class="waves-effect">Historial</a></li>
                                </ul>
                            </div>
                        </li>

                        <li><a class="collapsible-header waves-effect arrow-r"><i class="fab fa-gg"></i> Vehiculos<i class="fa fa-angle-down rotate-icon"></i></a>
                            <div class="collapsible-body">
                                <ul class="list-unstyled">
                                    <li><a href="mantenimiento.php" class="waves-effect">Mantenimientos</a></li>
                                </ul>
                            </div>
                        </li>

                        <li>
                            <a class="collapsible-header waves-effect arrow-r"><i class="fab fa-gg">
                            </i> Arboles <i class="fa fa-angle-down rotate-icon"></i></a>
                            <div class="collapsible-body">
                            <ul class="list-unstyled">
                                <li><a href="form_cards.php" class="waves-effect">Corte y Resiembra</a></li>
                                <li><a href="historial_corte_resiembra.php" class="waves-effect">Historial</a></li>
                                </ul>
                            </div>
                        </li>

                        <li>
                            <a class="collapsible-header waves-effect arrow-r"><i class="fab fa-gg">
                            </i> Seccion A/B <i class="fa fa-angle-down rotate-icon"></i></a>
                            <div class="collapsible-body">
                                <ul class="list-unstyled">
                                    <li><a href="productos_huertas.php" class="waves-effect">Productos</a></li>
                                    <li><a href="variedades_huertas.php" class="waves-effect">Variedades</a></li>
                                    <li><a href="cargo.php" class="waves-effect">Cargo</a></li>
                                    <li><a href="persona.php" class="waves-effect">Persona</a></li>
                                    <li><a href="trabajador.php" class="waves-effect">Empleado</a></li>
                                    <li><a href="actividad_general.php" class="waves-effect">Actividad Gral.</a></li>
                                    <li><a href="subcategoria_general.php" class="waves-effect">Subactividad Gral.</a></li>
                                    <li><a href="movimiento.php" class="waves-effect">Movimiento</a></li>
                                    <li><a href="pago.php" class="waves-effect">Pago</a></li>
                                    <li><a href="transporte_de_carga.php" class="waves-effect">Transporte de carga</a></li>
                                </ul>
                            </div>
                        </li>

                        <li><a class="collapsible-header waves-effect arrow-r"><i class="fab fa-gg"></i> ESTADISTICAS<i class="fa fa-angle-down rotate-icon"></i></a>
                            <div class="collapsible-body">
                                <ul class="list-unstyled">
                                    <li><a href="graficaphp.php" class="waves-effect">Graficos Compra Citricos </a></li>
                                    <li><a href="grafica2php.php" class="waves-effect">Graficos Compra Limon </a></li>
                                    <li><a href="grafica2php.php" class="waves-effect">Graficos2</a></li>
                                </ul>
                            </div>
                        </li>

                        <li><a class="collapsible-header waves-effect arrow-r"><i class="fab fa-gg"></i> Sobre Nosotros<i class="fa fa-angle-down rotate-icon"></i></a>
                            <div class="collapsible-body">
                                <ul class="list-unstyled">
                                    <li><a href="inf.php" class="waves-effect">Grupo Murrieta</a></li>
                                </ul>
                            </div>
                        </li>

                        <?php 
                            if($_SESSION['tipo'] == 1){
                                echo '<li><a class="collapsible-header waves-effect arrow-r"><i class="fab fa-gg"></i>Administrador<i class="fa fa-angle-down rotate-icon"></i></a>
                                    <div class="collapsible-body">
                                        <ul class="list-unstyled">
                                            <li><a href="sesiones_usuarios.php" class="waves-effect">Sesiones de usuarios.</a></li>
                                            <li><a href="control_usuarios.php" class="waves-effect">Validacion de usuarios.</a></li>
                                        </ul>
                                    </div>
                                </li>';
                            }
                        ?>

                    </ul>
                </li>
                <!--/. Side navigation links -->
            </ul>
            <div class="sidenav-bg mask-strong"></div>
        </div>
        <!--/. Sidebar navigation -->
        <!-- Navbar -->
        <nav class="navbar fixed-top navbar-toggleable-md navbar-expand-lg scrolling-navbar double-nav">
            <!-- SideNav slide-out button -->
            <div class="float-left">
                <a href="#" data-activates="slide-out" class="button-collapse"><i class="fa fa-bars"></i></a>
            </div>

            <!-- =============================INICIO DE TOP MENU==============================-->
            <!-- Breadcrumb-->
            <div class="breadcrumb-dn mr-auto">
                <p>Grupo Murrieta</p>
            </div>

            <ul class="nav navbar-nav nav-flex-icons ml-auto">
                <li class="nav-item mr-2">
                    <a class="nav-link"><i class="fa fa-user"></i> 
                    <span class="clearfix d-none d-sm-inline-block" >
                        Bienvenido (a): 
                        <strong style="color:red; font-family: serif; font-style: italic; font-size: 15px; font-weight: bold;">
                        <?php echo $_SESSION['user'];?></strong>
                    </span></a>
                </li>

                <li class="nav-item dropdown mr-2">
                <?php
                    $revision="SELECT COUNT(n.idnotif) as numNot, n.fecha, n.user1, n.user2, n.tipo, u.usuario FROM notificaciones n
                    inner join usuarios u on u.idusuarios = n.user1
                    where user2 = ".$_SESSION['id']." AND estado = 0  GROUP BY user1,tipo ORDER BY fecha DESC";
                    $noti = mysqli_query($mysqli,$revision);
                    $nroNotif = mysqli_num_rows($noti);
                ?>
                    <a href="#" class="nav-link dropdown toggle waves-effect waves-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bell"></i>
                    <span class="badge badge-danger"><?php if ($nroNotif > 0) {echo $nroNotif;}?></span></a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <?php
                            echo '<label class=\"header\">Tienes '.$nroNotif.' notificaciones</label>';
                            while ($check = mysqli_fetch_array($noti)) {
                                echo createNotification($check['user1'],$check['usuario'],$check['numNot'],$check['tipo']);
                                } ?>
                    </div>
                </li>

                <li class="nav-item dropdown mr-2">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Ajustes</a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="act_usua.php?id=<?php echo $_SESSION ['id'] ?>"  ><strong style="display: none;"><?php echo $_SESSION['id'];?></strong> Ajustes de Usuario</a>
                        <a class="dropdown-item" href="respaldo_vista.php">Respaldo BD</a>
                        <a class="dropdown-item" href="restauracion_vista.php">Restauracion BD</a>
                        <a class="dropdown-item" href="../php/sesiones/cerrar_sesion.php">Cerrar Sesion</a>
                </li>
            </ul>
        </nav>
        <!-- /.Navbar -->
    </header>
    <!--/.Double navigation-->
    <!-- SCRIPTS -->
    <!-- JQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script src="../js/jquery-1.12.1.min.js"></script>
    <script src="../js/jquery.lwMultiSelect.js"></script>
    <script "text/javascrip" src="../js/popper.min.js"></script>
    <script "text/javascript" src="../js/bootstrap.min.js"></script>
    <script "text/javascript" src="../js/mdb.min.js"></script>
    <script "text/javascript" src="../js/myjava.js"></script>
    <script "text/javascript" src="../js/validaciones.js"></script>
    <script src="../js/raphael.min.js"> </script>
    <script src="../sas/alertify.min.js"></script>
    
    <script>
        // SideNav Initialization
        $(".button-collapse").sideNav();
        new WOW().init();
    </script>
    <script>
        function update(iduser, tipo)
        {
            $.ajax({
                url: '../php/sesiones/notificaciones.php',
                type: 'POST',
                data: 'iduser='+iduser+'&tipo='+tipo,
            });
            return false;
        }
    </script>
</body>
</html>