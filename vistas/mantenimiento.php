<?php include('include.php');
	require("../php/conexion.php");
	$country = '';
	$query = "SELECT clave FROM stock WHERE clave NOT IN ('','10171701','BIOFABRICA','CINTA','COPLES','CORNETA','CABLE','COMBUSTIBLE',
	'CONTENEDOR','DIAFRAGMA','DISCOS','EMBUDOS','ESPONJA','ESTACAS','FLECHAS','FOCOS','GATOS','GRAPAS','HERRAMIENTA','LENTES','LLAVES',
	'MAQUINARIA','RIEGO','PERNO','PERNOS','PISTOLAS','PLANTA','POLEA','REDUCCIONES','TENSORES','TOMAS DE FUERZA','YUGOS','ZAPATILLAS')
	GROUP BY clave ORDER BY clave ASC";
	$statement = mysqli_query($mysqli,$query);
	while($row=mysqli_fetch_array($statement))
	{
		$country .= '<option value="'.$row["clave"].'">'.$row["clave"].'</option>';
	}
	$folioNuevo = "SELECT folio from mantenimientos group by folio order by folio DESC limit 1";
	$statement2 = mysqli_query($mysqli,$folioNuevo);
	$folio = 0;
	while ($a=mysqli_fetch_array($statement2)) {
		$folio = $a['folio'];
	}
	$folio = str_pad($folio + 1, 4, "0", STR_PAD_LEFT);
?>
<title>Mantenimientos</title>
<main>
	<div class="container-fluid pt-3" >
		<div class="row">
			<div class="col-md-12 col-sm-12"  id='animacion'>
				<form action="../php/inserciones/insertar_mantenimiento.php" method="post">
					<div class="form-row">
						<div class="form-group col-md-1 col-sm-3 text-center">
							<label for="fecha">FOLIO</label>
							<input type="text" class="form-control text-center"  name="folio" value="<?php echo $folio; ?>" required readonly>
						</div>

						<div class="form-group col-md-2 col-sm-3 text-center">
							<label for="fecha">FECHA</label>
							<input type="date" class="form-control text-center"  name="fecha"  required >
						</div>

						<div class="form-group col-md-3  col-sm-3 text-center">
							<label  for="clave">UNIDAD</label>
							<input  onKeyUp="this.value = this.value.toUpperCase();" id="camion" type="text" class="form-control" name="camion"  required >
							<input  onKeyUp="this.value = this.value.toUpperCase();" id="idcamion" type="hidden" class="form-control" name="idcamion"   >
						</div>

						<div class="form-group col-md-2 text-center">
							<label for="diat">KM / HRS</label>
							<input  type="number" min="0" step="0.1" class="form-control text-center diat"   name="km_hrs" id="km_hrs">
						</div>

						<div class="form-group col-md-4  col-sm-3 text-center">
							<label for="clave">MANTENIMIENTO</label><br>
							<div class="form-check form-check-inline">
								<input class="form-check-input" type="radio" name="mantenimiento" id="radio1" value="1">
								<label class="form-check-label" for="radio1">
									Preventivo
								</label>
							</div>
							<div class="form-check form-check-inline">
								<input class="form-check-input" type="radio" name="mantenimiento" id="radio2" value="2">
								<label class="form-check-label" for="radio2">
									Correctivo
								</label>
							</div>
						</div>
					</div>

					<div class="form-row ui-widget">
						<div class="form-group col-md-1 col-sm-5 text-center">
							<label for="placas">PLACAS</label>
							<input onKeyUp="this.value = this.value.toUpperCase();" id="placas" type="text" class="form-control " name="placas">
						</div>

						<div class="form-group col-md-4  col-sm-3 text-center">
							<label  for="clave">CHOFER</label>
							<input  onKeyUp="this.value = this.value.toUpperCase();" id="chofer" type="text" class="form-control" name="chofer"  required >
						</div>

						<div class="form-group col-md-3  col-sm-3 text-center">
							<label id="clave" for="clave">I/E</label><br>
							<div class="form-check form-check-inline">
								<input class="form-check-input" type="radio" name="servicio" id="serv1" value="1" onclick="show1();">
								<label class="form-check-label" for="serv1">
									Interno
								</label>
							</div>
							<div class="form-check form-check-inline">
								<input class="form-check-input" type="radio" name="servicio" id="serv2" value="2" onclick="show2();">
								<label class="form-check-label" for="serv2">
									Externo
								</label>
							</div>
						</div>

						<div id="proveedor" class="form-group col-md-3 text-center" style="display: none;">
							<label for="">PROVEEDOR</label>
							<input type="text" onKeyUp="this.value = this.value.toUpperCase();" class="form-control text-center" name="proveedor" id="proveedor">	
						</div>

						<div id="gasto" class="form-group col-md-1 text-center" style="display: none;">
							<label for="">GASTO</label>
							<input type="number" onKeyUp="this.value = this.value.toUpperCase();" class="form-control text-center" name="gasto" id="gasto">	
						</div>
					</div>

					<div class="form-row">
						<div class="form-group col-md-5">
							<div class="container" style="width:500px;">
								<h6 align="center">Almacen</h6>
								<select name="country" id="country" class="form-control browser-default text-center action">
									<option value="">Seleccione componentes</option>
									<?php echo $country; ?>
								</select>
								<br />
								<select name="city" id="city" multiple class="form-control"></select>
								<br />
								<input type="hidden" name="hidden_city" id="hidden_city" />
								<center><button type="button" id="agregar" class="btn btn-info" name="insert" value="">Agregar</button></center>
							</div>
						</div>

						<div id="articulos" class="form-group col-md-3  col-sm-3 text-center">
							<label id="unidades" for="clave">UNIDAD</label>
						</div>

						<div class="form-group col-md-1  col-sm-1 text-center" id="cantidades">
							<label  for="clave">CANTIDAD</label>
						</div>

						<div class="form-group col-md-1  col-sm-1 text-center" id="actuales">
							<label  for="clave">ACTUAL</label>
						</div>

						<div class="form-group col-md-1  col-sm-1 text-center" id="precios">
							<label  for="clave">PRECIO</label>
						</div>

						<div class="form-group col-md-1  col-sm-1 text-center" id="eliminaciones">
							<label  for="clave">ELIMINAR</label>
						</div>

						<div class="form-group col-md-9 col-sm-3 text-center pt-3">
							<label for="exampleFormControlTextarea1">NOTAS:</label>
							<textarea class="form-control" id="exampleFormControlTextarea1" name="notas" rows="3"></textarea>
						</div>

						<div class="form-group col-md-1 col-sm-3 text-center pt-3">
							<button type="submit" class="btn btn-primary" name="submit" value="submit"><i class="fa fa-save"></i></button>
						</div>
					</div>
				</form>
			</div>
		</div>
		<hr>
		<div class="row">
			<div class="col-sm-3  text-center">
				<a id="btnSlide" class="btn-floating  btn-comm ">
				<i class="fas fa-wrench"></i> </a>
			</div>

			<div class="col-md-3 text-center p-3">
				<input onKeyUp="this.value = this.value.toUpperCase();" class="form-control text-center " type="text" placeholder="FOLIO" id="bs-folio"/>
			</div>

			<div class="col-sm-3 pt-3 text-center">
				<input  class="form-control text-center" onKeyUp="this.value = this.value.toUpperCase();" type="text" placeholder="UNIDAD" id="bs-unidad"/>
			</div>

			<div class="col-sm-3 text-center">
				<a  href="../php/impresiones/.php" class="btn-floating btn-comm ">
				<i class="fas fa-file-import"></i></a>
			</div>			
		</div> <br>

		<div class="row">
			<div class="col-md-12">
				<div class="registros table table-responsive-sm" id="agrega-mantenimientos"></div>
			</div> 
		</div>
	</div>

	<center>
		<nav aria-label="..."><ul class="pagination justify-content-center" id="paginationM"></ul></nav>
	</center>
</main>

<script src="../js/jquery-ui.js"></script>
<script "text/javascript" src="../js/busqueda_mantenimiento.js"></script>

<script>
	var toogle= true;
	$("#btnSlide").click(function()
	{
		if (toogle)
		{
			toogle = false;
			$("#animacion").slideUp("slow");
			$("#btnSlide").html('<i class="fas fa-wrench"></i>')
		}
		else
		{
			toogle = true;
			$("#animacion").slideDown("slow");
			$("#btnSlide").html('<i class="fas fa-minus"></i>')
		}
	})
</script>

<script type="text/javascript">
    $(function() {
            $("#camion").autocomplete({
                source: "../php/autocompletadores/transporte.php",
                minLength: 2,
                select: function(event, ui) {
                event.preventDefault();
                    $('#camion').val(ui.item.clave);       
                    $('#placas').val(ui.item.placas);
                    $('#idcamion').val(ui.item.idtransporte);
            }
            });
    });
</script>