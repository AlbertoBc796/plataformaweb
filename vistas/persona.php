<?php  include('include.php')?>
<title>Registro personal</title>
<main>
	<div class="container-fluid" >
		<div class="row pt-3" id="ocultar" >
			
			<div class="col-md-12 pt-3" >

				<form action="../php/inserciones/insertar_persona.php" method="post">
							
					<div class="form-row">
						<div class="form-group col-md-2 text-center">
							<label id="label" for="">NOMBRE</label>
							<input type="text" onKeyUp="this.value = this.value.toUpperCase();"  class="form-control text-center" name="nombre"  required>
						</div>

						<div class="form-group col-md-2 text-center">
							<label id="label" for="">APELLIDO P.</label>
							<input type="text" onKeyUp="this.value = this.value.toUpperCase();"  class="form-control text-center"  name="apellidop"  required >
						</div>

						<div class="form-group col-md-2 text-center">
							<label id="label" for="">APELLIDO M.</label>
							<input type="text" onKeyUp="this.value = this.value.toUpperCase();"  class="form-control text-center" name="apellidom"  required>
						</div>

						<div class="form-group col-sm-1 offset-1 text-center pt-3">
							<button type="submit" class="btn btn-primary" name="submit" value="submit"><i class="fa fa-save"></i></button>
						</div>
					</div>
				</form>
			</div>
		</div> <hr>
		<div class="row" >
			<div class="col-sm-3 text-center col-md-6" >
					<a id="btnSlide" class="btn-floating  btn-comm ">
					<i class="fas fa-id-card-alt"></i> </a>
			</div>

			<div class="col-sm-3 pt-3 text-center col-md-6">
				<input  class="form-control " type="text" placeholder="Busqueda...." id="bs-prod"  onKeyUp="this.value = this.value.toUpperCase();"/>
			</div>
		</div> <br>
		
		<div class="row" >
			<div class="registros col-sm-12 pt-3 table table-responsive-sm" id="agrega-registropersona" ></div>
		</div> 
	</div> <!--CONTAINER FLUID-->
	<br>
    <center>
        <nav aria-label="..."><ul class="pagination justify-content-center" id="paginationpersona">
			</ul>
		</nav>
    </center>

</main><!--/Main layout-->
    
<script  src="../js/busqueda_persona.js"></script>
	
<script>
	var toogle= false;
	$("#btnSlide").click(function(){
		if (!toogle)
		{
			toogle = true;
			$("#ocultar").slideUp("slow");
			$("#btnSlide").html('<i class="fas fa-id-card-alt"></i>')
		}else
		{
			toogle = false;
			$("#ocultar").slideDown("slow");
			$("#btnSlide").html('<i class="fas fa-minus"></i>')
		}
	})
</script>
