<?php include('include.php');  ?>
<head>
    <script src="../libreria/code/highcharts.js"></script>
    <script src="../libreria/code/highcharts-3d.js"></script>
    <script src="../libreria/code/modules/exporting.js"></script>
    <script src="../libreria/code/modules/export-data.js"></script>
</head>
<main>  
    <div class="container-fluid">
        <div class="row " >

        </div>

        <div class="row" >
             <div id="container" style="height: 100%;width: 100%; border: 1px solid #6A908D"></div>
        </div>
    </div>
</main>
        <script >

                Highcharts.chart('container', {

                    title: {
                        text: '2016'
                    },

                    subtitle: {
                        text: 'Source: thesolarfoundation.com'
                    },

                    yAxis: {
                        title: {
                            text: 'Number of Employees'
                        }
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle'
                    },

                    plotOptions: {
                        series: {
                            label: {
                                connectorAllowed: false
                            },
                            pointStart: 2014
                        }
                    },


                    <?php 
                         include ('../php/conexion.php');
                        $sql ="SELECT  c.idcompra_venta,c.fecha,h.idhuerta,h.nombre ,sum(c.ingreso_total) as total  from compra_venta  c  inner join huerta h on c.idhuerta=h.idhuerta group by h.idhuerta ";
                         $result = mysqli_query ($mysqli,$sql);
                         while ($registros = mysqli_fetch_array($result)) {
                     ?>

                    series: [{
                        name: '<?php echo $registros["nombre"]; ?>',
                        data: [<?php echo $registros["total"]; ?>]
                    }],
<?php } ?>

                    responsive: {
                        rules: [{
                            condition: {
                                maxWidth: 500
                            },
                            chartOptions: {
                                legend: {
                                    layout: 'horizontal',
                                    align: 'center',
                                    verticalAlign: 'bottom'
                                }
                            }
                        }]
                    }

                });

        </script>