<?php include('include.php'); ?>
<main>
	<div class="container-fluid pt-3" >
		<div class="row">
			<div class="col-md-12 col-sm-12"  id='animacion'>
				<form action="../php/inserciones/insertar_herramienta_segmento.php" id="" method="post">
					<div class="form-row">
						
						<div class="form-group col-md-2 col-sm-2 text-center">
							<label >FECHA</label>
				      			<input  type="date" class="form-control text-center" id="fecha"  name="fecha"  required>
						</div>

				    	<div class="form-group col-md-2  col-sm-2 text-center">
				      		<label  for="clave">CLAVE</label>
				      		<input  onKeyUp="this.value = this.value.toUpperCase();" id="clave" type="text" class="form-control " name="clave"  required >
				    	</div>

				    	<div class="form-group col-md-5 col-sm-5 text-center">
				      		<label for="descripcion">DESCRIPCION</label>
				      		<input onKeyUp="this.value = this.value.toUpperCase();" id="descripcion" type="text" class="form-control "  name="descripcion"  required>
				    	</div>

				    	<div class="form-group col-md-3 col-sm-3 text-center pt-3">
				    		<button type="submit" class="btn btn-primary" name="submit" value="submit"><i class="fa fa-save"></i></button>
				    	</div>

				  	</div>
				</form>

				
			</div>
						 
		</div>

		<hr>
					
		
		<div class="row">

			<div class="col-sm-4  text-center">
				<a id="btnSlide" class="btn-floating  btn-comm ">
					<i class="fas fa-wrench"></i> </a>
			</div>
			<div class="col-sm-4 pt-3 text-center">
				<input  class="form-control text-center" onKeyUp="this.value = this.value.toUpperCase();" type="text" placeholder="Herramienta...." id="bs-prod"/>
			</div>
			<div class="col-sm-4 text-center">
				<a  href="../php/impresiones/herramientas.php" class="btn-floating btn-comm "><i class="fas fa-file-import"></i></a>
			</div>
			
		</div> <br>

		
		<div class="row">
			<div class="col-md-12">
         		<div class="registros table table-responsive-sm" id="agrega-registrosherramienta"></div>
   			</div> 
		</div>

   			



							
	</div> <!-- TERMINA EL FLUID -->


				<!-- ======= PAGINACION ========-->
				<center>
           			 <nav aria-label="..."><ul class="pagination justify-content-center" id="pagination7"></ul></nav>
         		</center>

        		<!-- ====== TERMINA PAGINACION ====-->		
</main>

<!--ESCRIPT PARA PAGINACION Y BUSQUEDAS-->
<script  src="../js/busqueda_herramienta_segmento.js"></script>


<!-- ESCRIPT PARA OCULTAR EL FORMULARIO DE AGREGAR-->

<script>
var toogle= false;

			$("#btnSlide").click(function()
{
	if (!toogle)
	{
		toogle = true;

		$("#animacion").slideUp("slow");

		
		$("#btnSlide").html('<i class="fas fa-wrench"></i>')
	}
	else
	{
		toogle = false;

		$("#animacion").slideDown("slow");

		$("#btnSlide").html('<i class="fas fa-minus"></i>')
	}
})
</script>