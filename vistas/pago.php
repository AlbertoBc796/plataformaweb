<?php include('include.php'); ?> 
<title>Registrar forma de pago</title>
<main>
	<div class="container-fluid" >
			<div class="row" id="animacion" >
				<div class="col-md-12 col-sm-12">
				<form name="miformulario" action="../php/inserciones/insertar_pago.php" id="" method="post">
					<div class="form-row">
			
				    	<div class="form-group  col-md-4 col-sm-4 text-center">
				      		<label id="label" for="">PAGO</label>
				      		<input type="text" class="form-control text-uppercase" name="pago"  onKeyUp="this.value = this.value.toUpperCase();" required >
				    	</div>

				    	<?php 
                            require("../php/conexion.php");
                            $consulta="SELECT * FROM movimiento ORDER BY movimiento ASC;";    
                            $ress=mysqli_query($mysqli,$consulta); ?>   

				    	<div class="form-group col-md-4 col-sm-4 text-center">		
                        	
	                           <label for="idmovimiento" class="text-center"> MOVIMIENTO </label>
				     			 <select id="idmovimiento" name="idmovimiento" class="form-control browser-default text-center" onKeyUp="this.value = this.value.toUpperCase();"  required>
				        			<?php 
									while($arreglo=mysqli_fetch_array($ress))
									{
									?>
									<option value="<?php echo $arreglo['idmovimiento'] ?>">

										<?php echo $arreglo['movimiento']." ";?>
										
									</option><?php  } ?>
				      			</select>
                    	</div>

                    	
				    	<div class="form-group col-md-4 col-sm-4 pt-3 text-center">
					    	<button type="submit" class="btn btn-primary" name="submit" value="submit"><i class="fa fa-save"></i></button>
					    </div>
				   	</div>
				</form>
			</div>
				
			</div> <!-- TERMINA EL ROW--> 
			<hr>
				<div class="row"> 
					<div class="col-sm-4 col-md-4 text-center">
						<a id="btnSlide" class="btn-floating  btn-comm ">
								<i class="fas fa-credit-card"></i> </a>
					</div>

					<!--<div class="col-sm-4 col-md-4 pt-3 text-center">
						<input  class="form-control text-center"  onKeyUp="this.value = this.value.toUpperCase();" type="text" placeholder="Busqueda...." id="bs-prod"/>
					</div>-->

				</div>
				<br>

				<div class="row">
					<div class="col-md-12 col-sm-12">
		             		<div class="registros table table-responsive-sm" id="agrega-registropago"></div>
		       		</div> 
				</div>
	</div> 
				<center>
           			 <nav aria-label="..."><ul class="pagination justify-content-center" id="paginationpago"></ul></nav>
         		</center>
</main>

<!--ESCRIPT PARA PAGINACION Y BUSQUEDAS-->
<script  src="../js/busca_pago.js"></script>
<!-- ESCRIPT PARA OCULTAR EL FORMULARIO DE AGREGAR-->
<script>
var toogle= false;
			$("#btnSlide").click(function()
{
	if (!toogle)
	{
		toogle = true;
		$("#animacion").slideUp("slow");
		$("#btnSlide").html('<i class="fas fa-credit-card"></i>')
	}
	else
	{
		toogle = false;
		$("#animacion").slideDown("slow");
		$("#btnSlide").html('<i class="fas fa-minus"></i>')
	}
})
</script>