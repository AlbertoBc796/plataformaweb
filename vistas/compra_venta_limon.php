<?php include('include.php');  ?>
<meta charset="UTF-8">
    <header>HERRAMIENTAS</header>

<main>
    <div class="container-fluid">
    <div class="row">
        <div class="col-md-12"   id='animacion2'>
            <form action="../php/inserciones/insertar_compra_venta_limon.php"  method="post">            
                    <div class="form-row">
                        <div class="form-group col-md-2 text-center">
                            <label for="fecha">FECHA</label>
                                <input type="date" class="form-control" name="fecha"   required  >
                        </div>

                            <?php 
                                require("../php/conexion.php");
                                    $consulta="SELECT idhuerta,clave, nombre FROM huerta ORDER BY clave ASC;";              
                                    $ress=mysqli_query($mysqli,$consulta); ?>   
                                <div class="form-group col-md-2 text-center">
                                    <label for="huerta" class="text-center"> HUERTA </label>
                                         <select  name="huerta" class="form-control browser-default"  required>
                                            <?php 
                                            while($arreglo=mysqli_fetch_array($ress))
                                            {
                                            ?>
                                            <option selected="selected" value="<?php echo $arreglo['idhuerta'] ?>">                  
                                                <?php echo $arreglo['nombre']." ";?>
                                            </option><?php  } ?>
                                        </select>
                                </div>

                                <div class="form-group col-md-4 text-center">
                                    <label for="puesto">CUADRILLA</label>
                                        <input type="text" class="form-control " onkeypress="return soloLetras(event);"  onKeyUp="this.value = this.value.toUpperCase();" name="cuadrilla" required>
                                </div>

                                 <div class="form-group col-md-4 text-center">
                                    <label for="Nombre">TABLA</label>
                                    <input type="text" onKeyUp="this.value = this.value.toUpperCase();"  class="form-control" name="tabla" required="">
                                 </div>
                    </div>

                    <div class="form-row ui-widget">
                                
                        <div class="form-group col-md-2 text-center">
                            <label for="ApellidoP">CAMION</label>
                                 <input onkeypress="return soloLetras(event);"  onKeyUp="this.value = this.value.toUpperCase();" type="text" class="form-control" id="camion" name="camion" required>

                                  <input onKeyUp="this.value = this.value.toUpperCase();" id="idtransporte" name="idtransporte" class="form-control" type="hidden">
                        </div>

                        <div class="form-group col-md-2 text-center">
                            <label for="marca">MARCA</label>
                                 <input onkeypress="return soloLetras(event);"  onKeyUp="this.value = this.value.toUpperCase();" type="text" class="form-control text-uppercase" id="marca" name="marca" required>
                        </div>

                        <div class="form-group col-md-2 text-center">
                            <label for="color">COLOR</label>
                                 <input type="text" onkeypress="return soloLetras(event);"  onKeyUp="this.value = this.value.toUpperCase();"  class="form-control" id="colors" name="color" required>
                        </div>

                        <div class="form-group col-md-2 text-center">
                            <label for="placas">PLACAS</label>
                                 <input onKeyUp="this.value = this.value.toUpperCase();" type="text" class="form-control text-uppercase" id="placas" name="placas" required >
                        </div>

                            <!--SIRVE PARA LEGALIZAR PLACAS-->
                            <!--pattern="[A-Z]{3}[-][A-Z-0-9]{3}[-][A-Z-0-9]{1,1}"-->


                        <div class="form-group col-md-4 text-center">
                            <label for="ApellidoM">CHOFER</label>
                                 <input onkeypress="return soloLetras(event);"  onKeyUp="this.value = this.value.toUpperCase();" type="text" class="form-control text-uppercase" id="chofer" name="chofer" required>
                        </div>

                    </div>
                            
                    <div class="form-row">

                            <div class="form-group col-md-1 text-center">
                                <label for="folio1">RE. CORTE</label>
                                <input type="text" class="form-control"  name="folio1" >
                            </div>

                            <div class="form-group col-md-2 text-center">
                                <label for="">CAJAS CORTE</label>
                                <input type="text" class="form-control" id="multiplicando" onkeypress="return filterFloat(event,this);" name="caja_corte" required >
                            </div>
                            
                            <div class="form-group col-md-2 text-center">
                                <label for="">PRE. CORTE</label>
                                <input type="text" class="form-control" id="multiplicador"  onkeyup="multiplicacion();" onkeypress="return filterFloat(event,this);" name="precio_corte" required >
                            </div>
                            
                            <div class="form-group col-md-1 text-center">
                                <label for="">IMP.CORTE</label>
                                <input type="text" class="form-control " id="resultado" onkeypress="return filterFloat(event,this);" name="importe_corte"  readonly >
                            </div>

                             <div class="form-group col-md-1 text-center">
                                    <label for="">GTO.CORTE</label>
                                    <input type="text" class="form-control" id="pes" name="pesaje">
                            </div>

                             <div class="form-group col-md-2 text-center">
                                <label for="">PRE. FLETE</label>
                                    <input type="text"  class="form-control" onkeyup="mul2(); " name="precio_flete" id="multiplicador2" onkeypress="return filterFloat(event,this);" required>
                            </div>

                            <div class="form-group col-md-1 text-center">
                                    <label for="">IMP. FLETE</label>
                                    <input type="text" class="form-control" id="res" onkeyup="reesultado();" onkeypress="return filterFloat(event,this);" readonly name="importe_flete" >
                            </div>
                             
                            <div class="form-group col-md-2 text-center">
                                    <label for="">EGRESOS</label>
                                     <input type="text" class="form-control" id="egresos"  name="egresos"  onkeypress="return filterFloat(event,this);" readonly="" required >
                            </div>
                            
                    </div>

                    <div class="form-row">

                        <div class="form-group col-md-1 text-center">
                                <label for="folio2">RE. VENTA </label>
                            <input  type="text" class="form-control"   name="folio2">
                        </div>

                        <div class="form-group col-md-2 text-center">
                                <label for="c_venta">CAJA VENTA </label>
                            <input  type="text" class="form-control"   name="c_venta">
                        </div>  

                          <div class="form-group col-md-2 text-center">
                                <label for="merma">CAJA MERMA </label>
                            <input  type="text" class="form-control" id="cajas_merma"   name="merma" required >
                        </div>

                        <div class="form-group col-md-2 text-center">
                                <label for="kilo_venta">KG VENTA </label>
                            <input  type="text" class="form-control" id="kilosventa"   name="caja_venta" onkeypress="return filterFloat(event,this);" required >
                        </div>


                        <div class="form-group col-md-2 text-center">
                                <label for="precio_venta">PRECIO VENTA</label>
                                <input type="text" class="form-control" id="precioventa" onchange="mul3();" onkeypress="return filterFloat(event,this);"  name="precio_venta" required  >
                        </div>

                        <div class="form-group col-md-3 text-center">
                                <label for="total_venta">TOTAL VENTA</label>
                                <input  type="text" class="form-control" id="totalventa" name="total_venta" onkeypress="return filterFloat(event,this);" onkeyup="reesultado2();" readonly required >
                        </div>

                    </div>

                    <div class="form-row ui-widget">
                         <div class="form-group col-md-3 text-center">
                                <label for="ingresos">ING.TOTAL</label>
                            <input  type="text" onkeypress="return filterFloat(event,this);" class="form-control" id="ingresos" name="ingresos"  readonly required>
                        </div>

                        <div class="form-group col-md-3 text-center">
                            <label for="cliente">CLIENTE   </label>
                                <input type="text" onKeyUp="this.value = this.value.toUpperCase();" class="form-control" name="cliente">  
                        </div>



                        <div class="form-group col-md-3 text-center">
                            <label for="producto" class="text-center">PRODUCTO </label>
                                <select name="producto" id="productol" class="form-control browser-default"  required>
                                    <option value="0">Seleccione</option>
                                </select>
                        </div>

                        <div class="form-group col-md-3 text-center">
                            <label for="variedad" class="text-center">VARIEDAD </label>
                                <select name="variedad" id="variedadl" class="form-control browser-default"  required>
                                  <option value="0">Seleccione</option>
                                </select>
                        </div>
                    </div>

                    <div class="form-row">

                        <div class="form-group col-md-2 text-center">
                             <label for="nuevof"> FOLIO</label>
                                <input type="text" class="form-control text-center" onKeyUp="this.value = this.value.toUpperCase();"  name="nuevof" id="nuevof">          
                        </div>

                        <div class="form-group col-md-2 text-center">
                            <label for=""> FACTURACION</label>
                            <select name="facturacion" id="facturacion" class="form-control browser-default text-center">
                                <option value="NA">NA</option>
                                <option value="MEN">MEN</option>
                                <option value="AG">AG</option>
                                <option value="NME">NME</option>
                                <option value="PMV">PMV</option>
                                <option value="CAMV">CAMV</option>
                                <option value="ANMV">ANMV</option>
                                <option value="GMV">GMV</option>
                                <option value="CMP">CMP</option>
                                <option value="IMP">IMP</option>
                            </select>
                        </div>

                        <div class="form-group col-md-2 text-center">
                            <label for="movimiento" class=" text-center">MOVIMIENTOS </label>
                            <select name="movimiento" id="movimiento" class="form-control browser-default text-center"  required>
                                <option value="">Seleccione</option>
                            </select>
                        </div>

                        <div class="form-group col-md-2 text-center">
                            <label for="pago" class="text-center"> PAGO </label>
                            <select name="pago" id="pago" class="form-control browser-default text-center"  required>
                                    <option value="">Seleccione</option>
                            </select>
                        </div>
           
                             <div class="form-group text-center col-md-4 pt-3">
                            <button type="submit" class="btn btn-primary" name="submit" value="submit"><i class="fa fa-save"></i></button>  
                        </div>  
                    </div>
            </form>
        </div>

    </div> <hr>

    <div class="row d-flex text-center" >

        <div class="col-sm-2 mr-auto p-2" >
            <a id="btnSlide" class="btn-floating  btn-comm ">
                <i class="far fa-money-bill-alt"></i> </a>
        </div>

        <div class="col-sm-2 p-2" >
            <a type="button" class="btn-floating btn-comm" onclick="buscadorcito();"><i class="fas fa-search"></i></a>
        </div>

        <div class="col-sm-2 p-2" >
            <a target="_blank" href="javascript:reportePDF();" class="btn-floating btn-comm"><i class="fas fa-file-import"></i></a>
        </div>  
        
    </div> 

    <div class="row text-center" > 

        <div class="col-sm-2 text-center"  >
             <input class="form-control  text-uppercase text-center " placeholder="DD/MM/AAA" type="date" id="bd-desde"/>
        </div>
                 
        <div class="col-sm-2 text-center pt-2 text-center" >
                <h6>HASTA</h6>
        </div>

        <div class="col-sm-2 text-center"  >
            <input class="form-control  text-uppercase text-center"  placeholder="DD/MM/AAA"  type="date" id="bd-hasta"/>
        </div>
            
        <div class="col-sm-2 text-center" >
            <input  class="form-control text-center" onKeyUp="this.value = this.value.toUpperCase();"  type="text" placeholder=" Huerta" id="bs-prod"/> 
        </div>

        <div class="col-sm-2 text-center" >
            <input  class="form-control text-center" onKeyUp="this.value = this.value.toUpperCase();"  type="text" placeholder=" Producto "
             id="bs-producto"/> 
        </div>

        <div class="col-sm-2 text-center" >
            <input  class="form-control text-center" onKeyUp="this.value = this.value.toUpperCase();"   type="text" placeholder=" Variedad "
             id="bs-vari"/>
        </div>
      
    </div> <br>


        <div class=" row ">
             <div class="registros col-md-12 table table-responsive" id="agrega-registroslimon"></div>
        </div>

</div>

        <center>
            <nav aria-label="..."><ul class="pagination justify-content-center" id="paginationlimon"></ul></nav>
         </center>
</main>

    <script src="../js/jquery-ui.js"></script>   
    <script "text/javascript" src="../js/multiplicacion.js"></script>
    <script "text/javascript" src="../js/busqueda_compra_venta_limon.js"></script>
    <script "text/javascript" src="../js/funcion_combol.js"></script>
<!-- ESCRIPT PARA OCULTAR EL FORMULARIO DE AGREGAR-->
<script>
var toogle= false;

            $("#btnSlide").click(function()
{
    if (!toogle)
    {
        toogle = true;

        $("#animacion2").slideUp("slow");

        
        $("#btnSlide").html('<i class="far fa-money-bill-alt"></i>')
    }
    else
    {
        toogle = false;

        $("#animacion2").slideDown("slow");

        $("#btnSlide").html('<i class="fas fa-minus"></i>')
    }
})
</script>

 <script type="text/javascript">

    $(function() {
            $("#camion").autocomplete({
                source: "../php/autocompletadores/transporte.php",
                minLength: 2,
                select: function(event, ui) {
                event.preventDefault();
                    $('#camion').val(ui.item.camion);       
                    $('#marca').val(ui.item.marca);
                    $('#colors').val(ui.item.colors);
                     $('#placas').val(ui.item.placas);
                    $('#idtransporte').val(ui.item.idtransporte);
           }
            });
    });
    
</script>