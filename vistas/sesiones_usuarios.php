<?php 
    include('include.php');
?>
<title>Sesiones Usuarios</title>
<main>
    <div class="container-fluid pt-3">
        <div class="row text-center">
            <div class="col-sm-2 text-center pt-3">
                <input  class="form-control text-uppercase text-center" placeholder="DD/MM/AAA" type="date" id="bd-desde"/>
            </div>

            <div class="col-sm-1 text-center pt-3">
                <h6 class="mt-2 pl-4 text-center">HASTA</h6>
            </div>

            <div class="col-sm-2 text-center pt-3">
                <input class="form-control  text-uppercase text-center"  placeholder="DD/MM/AAA"  type="date" id="bd-hasta"/>
            </div>

            <div class="col-sm-3 text-center pt-3">
                <input  class="form-control  text-center" onKeyUp="this.value = this.value.toUpperCase();"  type="text" placeholder="USUARIO" id="bs-user"/> 
            </div>

            <div class="col-sm-3 text-center">
                <a type="button" class="btn-floating btn-comm text-center" onclick="buscaUsuario();"><i class="fas fa-search"></i></a>
            </div>
        </div>
        <br>
        <div class="row text-center">
            <div class="registros col-sm-12 col-md-12 table table-responsive" id="agrega-sesiones"></div>
        </div>
        <center><nav aria-label="..."><ul class="pagination justify-content-center" id="pagination-sesiones"></ul></nav></center>
    </div>
</main>

<script  src="../js/busqueda_usuarios.js"></script>