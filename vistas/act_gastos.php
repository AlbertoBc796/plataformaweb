<?php include('include.php');
    extract($_GET);
    require("../php/conexion.php");
    $sql="SELECT g.idgasto, g.fecha, m.movimiento, g.semana,
    pa.pago, g.idmovimiento, g.idfactura,g.proveedor,g.cantidad,g.precio,
    g.idtransporte,t.clave,h.nombre as huerta,g.idhuerta, g.idpago, 
    g.idconcepto, g.concepto,g.detalle,
    CASE
        WHEN g.idconcepto  = 1 THEN 'MAQUINARIA'
        WHEN g.idconcepto  = 2 THEN 'HUERTA'
        WHEN g.idconcepto  = 3 THEN 'GSTO. IND. ADMIN'
        WHEN g.idconcepto  = 4 THEN 'GSTO. IND. OPER'
        WHEN g.idconcepto  = 5 THEN 'ALMACEN'
        WHEN g.idconcepto  = 6 THEN 'BIOFABRICA'
    END as rubro
    FROM gastos g
    inner join movimiento m on m.idmovimiento = g.idmovimiento
    left join huerta h on h.idhuerta = g.idhuerta
    left join transporte t on t.idtransporte = g.idtransporte
    inner join pago pa on pa.idpago = g.idpago 
    WHERE g.idgasto=$id";
    $ressql=mysqli_query($mysqli,$sql);
    while ($fila=mysqli_fetch_row ($ressql))
    {
        $id=$fila[0];
        $fecha=$fila[1];
        $movimiento=$fila[2];
        $semana=$fila[3];
        $pago=$fila[4];
        $idmovimiento=$fila[5];
        $idfactura=$fila[6];
        $proveedor=$fila[7];
        $cantidad=$fila[8];
        $precio=$fila[9];
        $idTransporte=$fila[10];
        $claveTransporte=$fila[11];
        $huerta=$fila[12];
        $idheurta=$fila[13];
        $idpago=$fila[14];
        $idconcepto=$fila[15];
        $concepto=$fila[16];
        $detalle=$fila[17];
        $rubro=$fila[18];
        
    }

	$consulta="SELECT idmovimiento,movimiento FROM movimiento ";
	$ress=mysqli_query($mysqli,$consulta);
	$movimientos="";
	while($arreglo=mysqli_fetch_array($ress))
	{
        $movimientos=$movimientos."<option value=\"".$arreglo['idmovimiento']."\">".$arreglo['movimiento']."  </option>";
    }

	$consultapa="SELECT idpago,pago FROM pago ";
    $respa=mysqli_query($mysqli,$consultapa);
    $pagos="";
	while($arreglo=mysqli_fetch_array($respa))
	{
        $pagos=$pagos."<option value=\"".$arreglo['idpago']."\">".$arreglo['pago']."  </option>";
    }
    
?> 
	<title>Actualizar Gasto</title>
    <main>
    <div class="container-flui">
        <div class="row">
            <div class="col-md-12" id="animacion">
                <form action="../php/actualizaciones/actualiza_gastos.php" method="post">
                    <div class="form-row text-center">
                        <div class="form-group col-md-2">
                            <label for="">FECHA</label>
                            <input  name="id" value="<?php echo $id;?>" onKeyUp="this.value = this.value.toUpperCase();" class="form-control text-center" type="hidden">
                            <input type="text" class="form-control text-center"  name="fecha" value="<?php echo $fecha;?>" required >
                        </div>
                        <div class="form-group col-md-2">
							<label for="">SEMANA</label>
							<input type="text" class="form-control text-center" onKeyUp="this.value = this.value.toUpperCase();" value="<?php echo $semana;?>" name="semana" required>
                        </div>
                        <div class="form-group col-md-2 text-center">
                            <label for="movimiento" class=" text-center">MOVIMIENTOS </label>
                            <select name="movimiento" id="movimiento" class="form-control browser-default text-center"  required>
                                <option value="<?php echo $idmovimiento; ?>">Selecciono <?php echo $movimiento; ?></option>
                                <?php echo $movimientos; ?>
                            </select>
                        </div>
                        <div class="form-group col-md-2 text-center">
                            <label for="pago" class="text-center"> PAGO </label>
                            <select name="pago" id="pago" class="form-control browser-default text-center"  required>
                                <option value="<?php echo $idpago; ?>">Selecciono <?php echo $pago; ?></option>
                                <?php echo $pagos; ?>
                            </select>
                        </div>
                        <div class="form-group col-md-2">
							<label for="">No. FACTURA</label>
							<input type="text" value= "<?php echo $idfactura; ?>" class="form-control text-center" onKeyUp="this.value = this.value.toUpperCase();" name="factura" required>
                        </div>
                    </div>
                    <div class="form-row text-center">
                        <div class="form-group col-md-4">
							<label for="">PROVEEDOR</label>
							<input type="text" value="<?php echo $proveedor ?>" class="form-control text-center" onKeyUp="this.value = this.value.toUpperCase();" name="proveedor" required>
                        </div>
                        <div class="form-group col-md-2 p-3">
                            <div><button type="submit" class="btn btn-primary" name="submit" value="submit"><i class="fa fa-save"></i></button></div>
                        </div>
                    </div>
                    <div class="form-row text-center">
                        <div class="form-group col-md-3">
							<label for="">CONCEPTO</label>
							<input name="concepto" value="<?php echo $concepto; ?>" type="text" class="form-control text-center" onKeyUp="this.value = this.value.toUpperCase();" required>
                        </div>
                        <div class="form-group col-md-1">
							<label for="">CANTIDAD</label>
							<input type="number" value="<?php echo $cantidad; ?>" min=0 class="form-control text-center"  name="cantidad" required>
                        </div>
                        <div class="form-group col-md-2">
							<label for="">PRECIO UNIT.</label>
							<input type="number" min=0 value="<?php echo $precio; ?>" class="form-control text-center" onKeyUp="reesultado6()" name="precio" required>
                        </div>
                        <div class="form-group col-md-1">
							<label for="">TOTAL</label>
							<input type="number" min=0 value="<?php echo $precio*$cantidad; ?>" class="form-control text-center" name="precio_total" required readonly>
                        </div>
                        <div class="form-group col-md-2">
							<label for="cargo">CARGO</label>
							<select name="cargo" id="destino" class="form-control browser-default text-center">
                            <option value="<?php echo $idconcepto; ?>"  id="">Selecciono <?php echo $rubro; ?> </option>
                                <option value="1" id=""> MAQUINARIA</option>
                                <option value="2" id=""> HUERTA</option>
                                <option value="3" id=""> GSTO. IND. ADMIN</option>
                                <option value="4" id=""> GSTO. IND. OPER</option>
                                <option value="5" id=""> ALMACEN</option>
                                <option value="6" id=""> BIOFABRICA</option>
                            </select>
                        </div>
                        <div class="form-group col-md-1 text-center">
                            <label for="camion">CAMION</label>
                            <input name="clave" id="clave_0" autocomplete="off" value="<?php echo $claveTransporte;?>" type="text" onKeyUp="this.value = this.value.toUpperCase();" class="form-control camion">
                            <input name="idtransporte" value="<?php echo $idTransporte; ?>" id="idtransporte_0" onKeyUp="this.value = this.value.toUpperCase();" class="form-control" type="hidden">
                        </div>
                        <div id="destino_salida" class="form-group col-md-2 ">
							<label for="destino" >HUERTA</label>
							<input  name="destino" id="buscador_0"  value="<?php echo $huerta;?>" autocomplete="off" type="text" onKeyUp="this.value = this.value.toUpperCase();" class="form-control huerta">
							<input  name="idhuerta" id="idhuerta_0" value="<?php echo $idheurta;?>" onKeyUp="this.value = this.value.toUpperCase();" class="form-control text-center" type="hidden">
						</div>
                    </div>
                    <div id="canciones"></div>
                    <div class="form-group">
                        <label for="exampleFormControlTextarea1">NOTAS:</label>
                        <textarea class="form-control" id="exampleFormControlTextarea1" name="notas" rows="3"><?php echo $detalle?></textarea>
                    </div>
                </form>
                <hr>
            </div>
        </div>
    </div>
</main>

<script src="../js/compra_general.js"></script> 
<script src="../js/jquery-ui.js"></script> 
