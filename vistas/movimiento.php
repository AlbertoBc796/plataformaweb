<?php include('include.php');
 ?> 
<title>Registar transaccion</title>
<main>
    <div class="container-fluid" >
    
    	<div class="row pt-3" id="ocultar" >
    		<div class="col-md-12" >
    			<form action="../php/inserciones/insertar_movimiento.php"  method="POST">
    				<div class="form-row">
    					<div class="form-group col-md-5 text-center">
    						<label id="label" for="">MOVIMIENTO</label>
    							<input type="text" class="form-control text-center" name="movimiento" onKeyUp="this.value = this.value.toUpperCase();"  required value="">
    					</div>

    					<div class="form-group col-sm-1 text-center offset-2 pt-3">
    						<button type="submit" class="btn btn-primary" name="submit" value="submit"><i class="fa fa-save"></i></button>
    					</div>

    				</div>	
    			</form>
    			
    		</div> 
    	</div>

        <hr>

    	<div class="row" >

            <div class="col-md-4 col-sm-4 text-center">
                <a id="btnSlide" class="btn-floating  btn-comm ">
                    <i class="fas fa-credit-card"></i> </a>
            </div>

           <!-- <div class="col-md-4 col-sm-4 text-center pt-3">
                <input  class="form-control text-center  " type="text" placeholder="Busqueda...." id="bs-prod"  onKeyUp="this.value = this.value.toUpperCase();"/>
            </div>-->

           <!-- <div class="col-md-4 col-sm-4 text-center">
                <a  href="../php/impresioes/.php" class="btn-floating btn-comm "><i class="fas fa-file-import"></i></a>
            </div>-->
        
        </div> <br>

        <div class="row">
            <div class="col-md-12">
                <div class="registros table table-responsive-sm" id="agrega-registromovimiento"></div>
            </div> 
            
        </div>

    </div> <!--CONTAINER FLUID-->
<br>
    <center>
         <nav aria-label="..."><ul class="pagination justify-content-center" id="paginationmovimiento">
         	</ul>
     	 </nav>
    </center>

</main><!--/Main layout-->

 
<script  src="../js/busca_movimiento.js"></script>
	
<script>
var toogle= false;

            $("#btnSlide").click(function()
{
    if (!toogle)
    {
        toogle = true;

        $("#ocultar").slideUp("slow");

        
        $("#btnSlide").html('<i class="fas fa-credit-card"></i>')
    }
    else
    {
        toogle = false;

        $("#ocultar").slideDown("slow");

        $("#btnSlide").html('<i class="fas fa-minus"></i>')
    }
})
</script>


