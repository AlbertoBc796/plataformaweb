<?php include('include.php');  ?>
<title>Facturacion</title>
<meta charset="UTF-8">
<main>

	<div class="container-fluid">
	<div class="row">

		<div class="col-sm-2 text-center pt-3">
			<input  class="form-control text-uppercase text-center" placeholder="DD/MM/AAA" type="date" id="bd-desde"/>
		</div>

		<div class="col-sm-1 text-center pt-3">
			<h6 class="mt-2 pl-4 text-center">HASTA</h6>
		</div>

		<div class="col-sm-2 text-center pt-3">
			<input class="form-control  text-uppercase text-center"  placeholder="DD/MM/AAA"  type="date" id="bd-hasta"/>
		</div>

		<div class="col-sm-2 text-center pt-3">
			<input  class="form-control  text-center" onKeyUp="this.value = this.value.toUpperCase();"  type="text" placeholder=" # FACTURA" id="bs-prod"/> 
		</div>

		<div class="col-sm-2 text-center pt-3">
			<input  class="form-control  text-center" onKeyUp="this.value = this.value.toUpperCase();"  type="text" placeholder=" #FOLIO" id="bs-prod2"/> 
		</div>

		<div class="col-sm-3 text-center">

			<a type="button" class="btn-floating btn-comm text-center" onclick="buscadorfactura();buscadorfacturacompleta();"><i class="fas fa-search"></i></a>

			<a  href="javascript:reportePDF();" class="btn-floating btn-comm text-center"><i class="fas fa-file-import"></i></a>

			<a  href="javascript:reportePDFcompleto();" class="btn-floating btn-comm text-center"><i class="far fa-file-pdf"></i></a>
		</div>

	</div> <br>


	<div class="row">
		<div class="registros col-sm-12 col-md-12 table table-responsive" id="agrega-registrosfactura"></div>
		<nav aria-label="..."><ul class="pagination justify-content-center" id="paginationfactura"></ul></nav>
	</div>

</div>

</main>

<script src="../js/busqueda_compra_venta_facturacion.js"></script>