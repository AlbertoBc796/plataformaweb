<?php include('include.php'); 
	extract($_GET);
	require("../php/conexion.php");
	$sql="SELECT a.idactividades_general,a.fecha,a.semana,a.mes,a.rubro,
	p.idpersona,concat(p.nombre ,' ',p.apellidop,' ',p.apellidom) as nombrec,a.cargo,a.salario,a.diat,a.pago_dia,
	a.idcategoria,a.idsubcategoria,a.detalle,a.idherramienta_segmento,
	a.producto,a.cantidad,a.precio,a.importe,a.idhuerta
	FROM actividades_general a
	inner join persona p on p.idpersona = a.idpersona where idactividades_general=$id";
	$ressql=mysqli_query($mysqli,$sql);
	while ($fila=mysqli_fetch_row ($ressql)){
		$id=$fila[0];
		$fecha=$fila[1];
		$semana=$fila[2];
		$mes=$fila[3];
		$rubro=$fila[4];
		$idpersona=$fila[5];
		$nombrec=$fila[6];
		$cargo=$fila[7];
		$salario=$fila[8];
		$diat=$fila[9];
		$pago_dia=$fila[10];
		$idcategoria=$fila[11];
		$idsubcategoria=$fila[12];
		$detalle=$fila[13];
		$idherramienta_segmento=$fila[14];
		$producto=$fila[15];
		$cantidad=$fila[16];
		$precio=$fila[17];
		$importe=$fila[18];
		$idhuerta=$fila[19];
	}
	$consulta="SELECT idhuerta,nombre,zona FROM  huerta order by nombre";
	$ress=mysqli_query($mysqli,$consulta);

	$consulta2="SELECT idcategoria,nombre FROM  categoria order by nombre";
	$ress2=mysqli_query($mysqli,$consulta2);
	
	$consulta3="SELECT idsubcategoria,nombre FROM  subcategoria order by nombre";
	$ress3=mysqli_query($mysqli,$consulta3);

	$consulta4="SELECT s.idstock as id, s.articulo as art FROM stock s where s.clave = 'HERRAMIENTA' 
	union all SELECT t.idtransporte as id, t.clave as art from transporte t WHERE (t.clave LIKE 'MA%' OR t.clave LIKE 'IM%') order by id";
	$ress4=mysqli_query($mysqli,$consulta4);
	$herramientas='';
	while($arreglo=mysqli_fetch_array($ress4))
	{
		$herramientas=$herramientas."<option value=\"".$arreglo['art']."\">".$arreglo['art']."  </option>";
	}
?> 
<title>Actualizar actividades generales</title>
<main>		
	<div class="container-fluid pt-2">
		<form action="../php/actualizaciones/actualizar_actividades_general.php" method="post" name="formulario" >
			<div class="form-row text-center">

				<input class="form-control text-center" type="hidden" name="id" value= "<?php echo $id ?>" readonly="readonly" >
				<div class="form-group col-md-3">
					<label for="fecha">FECHA</label>
					<input class="form-control text-center" type="text"  onKeyUp="this.value = this.value.toUpperCase();"  name="fecha"   value="<?php echo $fecha?>">
				</div>

				<div class="form-group col-md-3">
					<label for="fecha">SEMANA</label>
					<input class="form-control text-center" type="text"  onKeyUp="this.value = this.value.toUpperCase();" name="semana"   value="<?php echo $semana?>">
				</div>

				<div class="form-group col-md-3">
					<label for="fecha">MES</label>
					<input class="form-control text-center" type="text"  onKeyUp="this.value = this.value.toUpperCase();"  name="mes"   value="<?php echo $mes?>">
				</div>

				<div class="form-group col-md-3 col-sm-3">
					<label for="">RUBRO</label>
					<input class="form-control text-center" type="text"  onKeyUp="this.value = this.value.toUpperCase();" name="rubro"   value="<?php echo $rubro?>">
				</div>
			</div>

			<div class="form-row text-center">
				<div class="form-group col-md-3 col-sm-3">
					<label for="">NOMBRE</label>
					<input class="form-control text-center" type="hidden" name="idpersona" placeholder="" on value="<?php echo $idpersona?>">
					<input class="form-control text-center" type="text" name="nombrec" readonly placeholder="" on value="<?php echo $nombrec?>">
				</div>
				

				<div class="form-group col-md-3 col-sm-3">
					<label for="">CARGO</label>
					<input class="form-control text-center" type="text" readonly name="cargo"  value="<?php echo $cargo?>">
				</div>

				<div class="form-group col-md-2 col-sm-2">
					<label for="">SALARIO</label>
					<input class="form-control text-center" type="text" readonly name="salario" id="salario" value="<?php echo $salario?>">
				</div>
				
				<div class="form-group col-md-2 col-sm-2">
					<label for="">DIA/TRA</label>
					<input class="form-control text-center" type="text" name="diat" id="diat"  value="<?php echo $diat?>">
				</div>

				<div class="form-group col-md-2 col-sm-2">
					<label for="">PAGO</label>
					<input class="form-control text-center" id="importe"  type="text" onkeyup="reesultado6()"	 readonly name="pago_dia" value="<?php echo $pago_dia?>">
				</div>
			</div>

			<div class="form-row text-center">
				<div class="form-group col-md-3">
                    <label for=""> CATEGORIA</label>
					<?php
						extract($_GET);
						$mysqli = new mysqli("localhost", "root", "", "gm_cv");
						if ($mysqli->connect_errno) {
							echo "Fallo al conectar a MySQL: (" . $mysqli->connect_error . ") " . $mysqli->connect_error;
						}
						$query = "SELECT h.idcategoria,h.nombre from actividades_general c inner join categoria h on c.idcategoria=h.idcategoria WHERE idactividades_general=$id  "; 
						$rs = mysqli_query($mysqli,$query);
						$combo3 = " <select class='form-control browser-default ' name='categoria' >";
						while ($datos = mysqli_fetch_array($rs)){
							$x = "Seleccionaste";
							$selected = '';
							if ($idhuerta == $datos['idcategoria']){
								$selected = 'selected';
							}

							$combo3 .= "<option value='".$datos['idcategoria']."' 
							".$selected."> ".$x ." " .$datos['nombre']."</option>";

							while($arreglo=mysqli_fetch_array($ress2)){
							$combo3.= "<option  value='" .$arreglo['idcategoria']."' >"
							.$arreglo['nombre']. "";
							"</option>";
							} 

						}
						$combo3 .= "</select>";
					?>
                    <?php echo $combo3 ?>
				</div>

				<div class="form-group col-md-3  ui-widget">
                    <label for=""> SUBCATEGORIA</label>
                    <?php
						extract($_GET);
						$mysqli = new mysqli("localhost", "root", "", "gm_cv");
						if ($mysqli->connect_errno) {
							echo "Fallo al conectar a MySQL: (" . $mysqli->connect_error . ") " . $mysqli->connect_error;
						}
						$query = "SELECT h.idsubcategoria,h.nombre from actividades_general c inner join subcategoria h on c.idsubcategoria=h.idsubcategoria WHERE idactividades_general=$id"; 
						$rs = mysqli_query($mysqli,$query);
						$combo4 = " <select class='form-control browser-default ' name='subcategoria' >";
						while ($datos = mysqli_fetch_array($rs)){
							$x = "Seleccionaste";
							$selected = '';
							if ($idhuerta == $datos['idsubcategoria']){
								$selected = 'selected';
							}

							$combo4 .= "<option value='".$datos['idsubcategoria']."' 
							".$selected."> ".$x ." " .$datos['nombre']."</option>";
							while($arreglo=mysqli_fetch_array($ress3)){
								$combo4.= "<option  value='" .$arreglo['idsubcategoria']."' >" 
								.$arreglo['nombre']. "";
								"</option>";
							} 
						}
						$combo4 .= "</select>";
					?>
                    <?php echo $combo4 ?>					
				</div>



				<div class="form-group col-md-3 col-sm-3">
					<label for="">DETALLE 2</label>
					<input class="form-control text-center" type="text" name="detalle" value="<?php echo $detalle?>">
				</div>

				<div class="form-group col-md-3 text-center">
					<label for="idsegmento" class="text-center "> HERRAMIENTA O SEGMENTO </label>
					<select  name="idsegmento" class="form-control browser-default"  required>
					<?php
						$queryC = "SELECT idherramienta_segmento FROM actividades_general WHERE idactividades_general = '$id'";
						$check = mysqli_query($mysqli,$queryC);
						while ($b=mysqli_fetch_array($check)) {
							echo '<option value="'.$b['idherramienta_segmento'].'">Seleccionaste '.$b['idherramienta_segmento'].'</option>';
						}
						echo '<option value="">Sin herramienta</option>';
						echo $herramientas; ?>
					</select>
				</div>
			</div>


			<div class="form-row text-center">

				<div class="form-group col-md-2 col-sm-2">
					<label for="">PRODUCTO</label>
					<input class="form-control text-center"  onKeyUp="this.value = this.value.toUpperCase();" type="text" name="producto" placeholder="" on value="<?php echo $producto?>">
				</div>

				<div class="form-group col-md-2 col-sm-2">
					<label for="">CANTIDAD</label>
					<input class="form-control text-center"  onKeyUp="this.value = this.value.toUpperCase();" type="text" name="cantidad" placeholder="" value="<?php echo $cantidad?>">
				</div>

				<div class="form-group col-md-2 col-sm-2">
					<label for="">PRECIO</label>
					<input class="form-control text-center"  onKeyUp="this.value = this.value.toUpperCase();" type="text" name="precio" value="<?php echo $precio?>">
				</div>

				<div class="form-group col-md-2 col-sm-2">
					<label for="">IMPORTE</label>
					<input class="form-control text-center"  onKeyUp="this.value = this.value.toUpperCase();" type="text" name="importe"  value="<?php echo $importe?>">	
				</div>


				<div class="form-group col-md-2">
                    <label for=""> HUERTA</label>
					<?php
						extract($_GET);
						$mysqli = new mysqli("localhost", "root", "", "gm_cv");
						if ($mysqli->connect_errno) {
							echo "Fallo al conectar a MySQL: (" . $mysqli->connect_error . ") " . $mysqli->connect_error;
						}
						$query = "SELECT h.idhuerta,h.nombre from actividades_general c inner join huerta h on c.idhuerta=h.idhuerta WHERE idactividades_general=$id  "; 
						$rs = mysqli_query($mysqli,$query);
						$combo2 = " <select class='form-control browser-default ' name='huerta' >";
						while ($datos = mysqli_fetch_array($rs)){
							$x = "Seleccionaste ";
							$selected = '';
							if ($idhuerta == $datos['idhuerta']){
								$selected = 'selected';
							}
							$combo2 .= '<option value="'.$datos['idhuerta'].'" '.$selected."> ".$x.$datos['nombre']."</option>";		
						}
						while($arreglo=mysqli_fetch_array($ress))
							{
								$combo2.= "<option  value='" .$arreglo['idhuerta']."' >".$arreglo['nombre']."</option>";
							} 
						$combo2 .= "</select>";
						echo $combo2 ?>
				</div>

				<div class="form-group col-sm-1 col-md-1 p-3">
					<input align="" type="submit" value="Guardar" class="btn btn-success btn-primary">
				</div>
			</div>
		</form>
	</div> <!-- ========= TERMINA EL CONTEINER FLUID =========0 -->
</main>
<script src="../js/jquery-ui.js"></script> 
<script "text/javascript" src="../js/actividad_general.js"></script>


<script type="text/javascript">
	$(function() {
        $("#descripcion").autocomplete({
            source: "../php/autocompletadores/autocomplete_actualizacion_de_herramienta.php",
            minLength: 2,
            select: function(event, ui) {
				event.preventDefault();
				$('#descripcion').val(ui.item.descripcion);
				$('#idsegmento').val(ui.item.idsegmento);
			}
        });
    });
</script>