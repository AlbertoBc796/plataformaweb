<?php 
    include('include.php');
    extract($_GET);
	require("../php/conexion.php");
	$country = '';
	$query = "SELECT clave FROM stock WHERE clave NOT IN ('','10171701','BIOFABRICA','CINTA','COPLES','CORNETA','CABLE','COMBUSTIBLE',
	'CONTENEDOR','DIAFRAGMA','DISCOS','EMBUDOS','ESPONJA','ESTACAS','FLECHAS','FOCOS','GATOS','GRAPAS','HERRAMIENTA','LENTES','LLAVES',
	'MAQUINARIA','RIEGO','PERNO','PERNOS','PISTOLAS','PLANTA','POLEA','REDUCCIONES','TENSORES','TOMAS DE FUERZA','YUGOS','ZAPATILLAS')
	GROUP BY clave ORDER BY clave ASC";
	$statement = mysqli_query($mysqli,$query);
	while($row=mysqli_fetch_array($statement))
	{
		$country .= '<option value="'.$row["clave"].'">'.$row["clave"].'</option>';
	}
    
    $sql="SELECT m.folio, m.fecha, m.unidad, m.medicion,
    m.idtipo, m.chofer, t.clave, m.idservicio, m.proveedor,
    m.gasto,t.placas,m.notas
    FROM mantenimientos m
    INNER JOIN transporte t ON t.idtransporte = m.unidad
    WHERE m.folio LIKE '%$dato%'
    GROUP BY m.folio ORDER BY m.folio DESC";
    $ressql = mysqli_query($mysqli,$sql);
    while ($fila=mysqli_fetch_row($ressql)) {
        $folio = $fila[0];
        $fecha = $fila[1];
        $unidad = $fila[2];
        $medicion = $fila[3];
        $tipo = $fila[4];
        $chofer = $fila[5];
        $claveUnidad = $fila[6];
        $servicio = $fila[7];
        $proveedor = $fila[8];
        $gasto = $fila[9];
        $placas = $fila[10];
        $notas = $fila[11];
    }
    $folio = str_pad($folio, 4, "0", STR_PAD_LEFT);
?>
<title>Mantenimientos</title>
<main>
	<div class="container-fluid pt-3" >
		<div class="row">
			<div class="col-md-12 col-sm-12"  id='animacion'>
				<form action="../php/actualizaciones/actualizar_mantenimiento.php" method="post">
					<div class="form-row">
						<div class="form-group col-md-1 col-sm-3 text-center">
							<label for="fecha">FOLIO</label>
							<input type="text" class="form-control text-center"  name="folio" value="<?php echo $folio; ?>" required readonly>
						</div>

						<div class="form-group col-md-2 col-sm-3 text-center">
							<label for="fecha">FECHA</label>
							<input type="text" class="form-control text-center"  name="fecha" value="<?php echo $fecha; ?>" required >
						</div>

						<div class="form-group col-md-3  col-sm-3 text-center">
							<label  for="clave">UNIDAD</label>
							<input  onKeyUp="this.value = this.value.toUpperCase();" id="camion" type="text" value="<?php echo $claveUnidad; ?>" class="form-control text-center" name="camion"  required >
							<input  onKeyUp="this.value = this.value.toUpperCase();" id="idcamion" type="hidden" value="<?php echo $unidad; ?>" class="form-control text-center" name="idcamion"   >
						</div>

						<div class="form-group col-md-2 text-center">
							<label for="diat">KM / HRS</label>
							<input  type="number" min="0" step="0.1" class="form-control text-center diat" value="<?php echo $medicion; ?>"  name="km_hrs" id="km_hrs">
						</div>

                        <?php
                            if ($tipo == 1) {
                                $mantenimiento = array(1 => 'checked=""', 2 => '');
                            }else {
                                $mantenimiento = array(1 => '', 2 => 'checked=""');
                            }
                        ?>
                        <div class="form-group col-md-4  col-sm-3 text-center">
							<label for="clave">MANTENIMIENTO</label><br>
							<div class="form-check form-check-inline">
								<input class="form-check-input" type="radio" <?php echo $mantenimiento[1] ?> name="mantenimiento" id="radio1" value="1">
								<label class="form-check-label" for="radio1">
									Preventivo
								</label>
							</div>
							<div class="form-check form-check-inline">
								<input class="form-check-input" type="radio" <?php echo $mantenimiento[2] ?> name="mantenimiento" id="radio2" value="2">
								<label class="form-check-label" for="radio2">
									Correctivo
								</label>
							</div>
						</div>
					</div>

					<div class="form-row ui-widget">
						<div class="form-group col-md-1 col-sm-5 text-center">
							<label for="placas">PLACAS</label>
							<input onKeyUp="this.value = this.value.toUpperCase();" id="placas" type="text" class="form-control " value="<?php echo $placas ?>" name="placas">
						</div>

						<div class="form-group col-md-4  col-sm-3 text-center">
							<label  for="clave">CHOFER</label>
							<input  onKeyUp="this.value = this.value.toUpperCase();" id="chofer" value="<?php echo $chofer; ?>" type="text" class="form-control text-center" name="chofer"  required >
						</div>

						<?php
                            if ($servicio == 1) {
                                $lugar = array(1 => 'checked=""', 2 => '');
                            }else {
                                $lugar = array(1 => '', 2 => 'checked=""');
                            }
                        ?>
                        <div class="form-group col-md-3  col-sm-3 text-center">
							<label id="clave" for="clave">I/E</label><br>
							<div class="form-check form-check-inline">
								<input class="form-check-input" type="radio" <?php echo $lugar[1] ?> name="servicio" id="serv1" value="1" onclick="show1();">
								<label class="form-check-label" for="serv1">
									Interno
								</label>
							</div>
							<div class="form-check form-check-inline">
								<input class="form-check-input" type="radio" <?php echo $lugar[2] ?> name="servicio" id="serv2" value="2" onclick="show2();">
								<label class="form-check-label" for="serv2">
									Externo
								</label>
							</div>
						</div>

						<div id="proveedor" class="form-group col-md-3 text-center">
							<label for="">PROVEEDOR</label>
							<input type="text" value="<?php echo $proveedor; ?>" onKeyUp="this.value = this.value.toUpperCase();" class="form-control text-center" name="proveedor" id="proveedor">	
						</div>

						<div id="gasto" class="form-group col-md-1 text-center">
							<label for="">GASTO</label>
							<input type="number" value="<?php echo $gasto; ?>" onKeyUp="this.value = this.value.toUpperCase();" class="form-control text-center" name="gasto" id="gasto">	
						</div>
					</div>

					<div class="form-row">
						<div class="form-group col-md-5">
							<div class="container" style="width:500px;">
								<h6 align="center">Almacen</h6>
								<select name="country" id="country" class="form-control browser-default text-center action">
									<option value="">Seleccione componentes</option>
									<?php echo $country; ?>
								</select>
								<br />
								<select name="city" id="city" multiple class="form-control"></select>
								<br />
								<input type="hidden" name="hidden_city" id="hidden_city" />
								<center><button type="button" id="agregar" class="btn btn-info" name="insert" value="">Agregar</button></center>
							</div>
						</div>

						<div id="articulos" class="form-group col-md-3  col-sm-3 text-center">
							<label id="unidades" for="clave">UNIDAD</label>
                            <?php 
                                $articulos ="SELECT m.idmantenimiento, m.folio, m.refacciones, s.articulo
                                from mantenimientos m
                                inner join stock s on s.idstock = m.refacciones
                                where m.folio = $folio";
                                $registro = mysqli_query($mysqli,$articulos);
                                $num=100;
                                while($a = mysqli_fetch_array($registro)) {
                                    echo '<input class="form-control text-center" id="idmant'.$num.'" name="idmant[]" readonly="" type="hidden" value="'.$a['idmantenimiento'].'">';
                                    echo "<input type=\"text\" name=\"articulo[]\" id=\"articulo".$num."\" class=\"form-control text-center\" value='".$a['articulo']."'>
                                    <input type=\"hidden\" id=\"stock".$num."\" name=\"idstock[]\" class=\"form-control id\" value=\"".$a['refacciones']."\">";
                                    $num++;
                                }
                            ?>
						</div>

						<div class="form-group col-md-1  col-sm-1 text-center" id="cantidades">
							<label  for="clave">CANTIDAD</label>
                            <?php 
                                $cantidades ="SELECT m.folio, m.refacciones, m.cantidad, s.articulo
                                from mantenimientos m
                                inner join stock s on s.idstock = m.refacciones
                                where m.folio = $folio";
                                $registro2 = mysqli_query($mysqli,$cantidades);
                                $num=100;
                                while($a = mysqli_fetch_array($registro2)) {
                                    echo '<input type="text" id="cantidad'.$num.'" name="cantidad[]" value="'.$a['cantidad'].'" class="form-control text-center">';
                                    $num++;
                                }
                            ?>
						</div>

						<div class="form-group col-md-1  col-sm-1 text-center" id="actuales">
							<label  for="clave">ACTUAL</label>
                            <?php
                                $almacen="SELECT m.folio, m.refacciones, s.articulo,
                                s.existencia,s.preciounit FROM mantenimientos m
                                inner join stock s on s.idstock = m.refacciones where m.folio = $folio";
                                $registro3 = mysqli_query($mysqli,$almacen);
                                $num=100;
                                $cantidad = '';
                                $precio = '';
                                while ($b = mysqli_fetch_array($registro3)) {
                                    $cantidad .= '<input class="form-control text-center" name="saldoant[]" id="existencia'.$num.'" readonly="" value="'.$b['existencia'].'">';
                                    $precio .= '<input class="form-control text-center" id="precio'.$num.'" name="precio[]" readonly="" value="'.$b['preciounit'].'">';
                                    $num++;
                                }
                                echo $cantidad;
                            ?>
						</div>

						<div class="form-group col-md-1  col-sm-1 text-center" id="precios">
							<label  for="clave">PRECIO</label>
                            <?php echo $precio; ?>
						</div>

						<div class="form-group col-md-1  col-sm-1 text-center" id="eliminaciones">
							<label  for="clave">ELIMINAR</label>
                            <?php 
                                $cantidades ="SELECT m.folio, m.refacciones, m.cantidad, s.articulo
                                from mantenimientos m
                                inner join stock s on s.idstock = m.refacciones
                                where m.folio = $folio";
                                $registro2 = mysqli_query($mysqli,$cantidades);
                                $num=100;
                                while($a = mysqli_fetch_array($registro2)) {
                                    echo '<a href="#" id="eliminar'.$num.'" class="form-control" onclick="this.remove(); eliminaArticulo('.$num.');" style="background-color: #F93822; color: white;">Eliminar</a>';
                                    $num++;
                                }
                            ?>
						</div>

						<div class="form-group col-md-9 col-sm-3 text-center pt-3">
							<label for="exampleFormControlTextarea1">NOTAS:</label>
							<textarea class="form-control" id="exampleFormControlTextarea1" name="notas" rows="3"><?php echo $notas; ?></textarea>
						</div>

						<div class="form-group col-md-1 col-sm-3 text-center pt-3">
							<button type="submit" class="btn btn-primary" name="submit" value="submit"><i class="fa fa-save"></i></button>
						</div>
					</div>
				</form>
			</div>
		</div>
		<hr>
	</div>

</main>

<script src="../js/jquery-ui.js"></script>
<script "text/javascript" src="../js/busqueda_mantenimiento.js"></script>

<script type="text/javascript">
    $(function() {
            $("#camion").autocomplete({
                source: "../php/autocompletadores/transporte.php",
                minLength: 2,
                select: function(event, ui) {
                event.preventDefault();
                    $('#camion').val(ui.item.clave);       
                    $('#placas').val(ui.item.placas);
                    $('#idcamion').val(ui.item.idtransporte);
            }
            });
    });
</script>