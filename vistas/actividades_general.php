<?php include('include.php');
	setlocale(LC_ALL,"spanish");
	echo  strftime(" %A");
?>
    <!-- $cadena = "El 10 y el número 20 con menores que el 30"; 
	$resultado = intval(preg_replace('/[^0-9]+/', '', $cadena), 10); 
	echo $resultado; // resultado: 102030 -->
<title>Actividades Generales</title>
<main>
	<div class="container-fluid" >	
		<div class="row" >
			<div class="col-md-12" id="animacion">
				<form action="../php/inserciones/insertar_actividades_general.php"     method="post">
					<div class="form-row text-center">
						<div class="form-group col-md-3">
							<label for="">FECHA</label>
							<input type="date" class="form-control text-center"  name="fecha"  required >
						</div>
						<div class="form-group col-md-3">
							<label for="">SEMANA</label>
								<input type="text" class="form-control text-center" onKeyUp="this.value = this.value.toUpperCase();" value="<?php echo 'SEMANA '.date("W");?>" name="semana" required>
						</div>
						<div class="form-group col-md-3">
							<label for="">MES</label>
								<input value="<?php setlocale(LC_ALL, "es_ES"); echo strftime("%B ");  ?>" class="form-control text-center" name="mes" required="" onKeyUp="this.value = this.value.toUpperCase();" >
						</div>
						<div class="form-group col-md-3">
							<label for="">RUBRO</label>
								<input onKeyUp="this.value = this.value.toUpperCase();" type="text" class="form-control text-center" name="rubro" required="">
						</div>
					</div>

					<div class="form-row ui-widget">
						<div class="form-group col-md-4 text-center">
							<label >NOMBRE </label>
								<input  type="text" class="form-control text-center"   name="nombre" id="nombre">
								<input onKeyUp="this.value = this.value.toUpperCase();" id="idpersona" name="idpersona[]" class="form-control" type="hidden">
						</div>
								
						<div class="form-group col-md-3 text-center">
							<label for="cargo" >CARGO </label>
							<input  type="text" class="form-control  text-center"  onKeyUp="javascript:showMsg()"   name="cargo[]" id="cargo" >

						</div>

						<div class="form-group col-md-2 text-center">
							<label for="salario">SALARIO</label>
							<input type="number" class="form-control text-center salario"  name="salario[]" id="salario" readonly >
						</div>	
							
						<div class="form-group col-md-1 text-center">
							<label for="diat">DIA/TRA. </label>
							<input  type="number" min="0" step="0.1" class="form-control text-center diat"   name="diat[]" id="diat">
						</div>

						<div class="form-group col-md-2 text-center">
							<label for="importe">PAGO</label>
							<input  type="text" class="form-control text-center importe" onkeyup="reesultado6()" readonly name="importe[]" id="importe" >
						</div>

					</div>			

					<div class="form-row ui-widget">
						<div class="form-group col-md-3 text-center">
                            <label for="categoria" class="text-center">ACTIVIDAD </label>
                            <select name="categoria[]" id="categoria" class="form-control browser-default text-center"  required>
                            </select>
                        </div>
                        <div class="form-group col-md-3 text-center">
                            <label for="subcategoria" class="text-center">SUB-ACTIVIDAD </label>
                            <select name="subcategoria[]" id="subcategoria" class="form-control browser-default text-center"  required>
                            <option value="0">Seleccione</option>
                            </select>
                        </div>
                        <div class="form-group col-md-3 text-center">
							<label for="detalle">DETALLE 2</label>
								<input type="text" onKeyUp="this.value = this.value.toUpperCase();" class="form-control text-center"  name="detalle[]" required="">
						</div>
							<!--==========SELECCION DE HERRAMIENTA O TRANSPORTE ===========-->
						<?php 
							require("../php/conexion.php");
								$consulta="SELECT s.idstock as id, s.articulo as art FROM stock s 
								where s.clave = 'HERRAMIENTA'
								union all 
								SELECT t.idtransporte as id, t.clave as art from transporte t 
								WHERE (t.clave LIKE 'MA%' OR t.clave LIKE 'IM%') order by id";
								$ress=mysqli_query($mysqli,$consulta);
								$herramientas="";
								while($arreglo=mysqli_fetch_array($ress))
								{
									$herramientas=$herramientas."<option value=\"".$arreglo['art']."\">".$arreglo['art']."  </option>";
								}
						?>
						<div class="form-group col-md-3 text-center">
							<label for="idsegmento" class="text-center "> HERRAMIENTA O SEGMENTO </label>
							<select  name="idsegmento[]" class="form-control browser-default"  required>
								<option value="">Sin herramienta</option>
								<?php echo $herramientas; ?>
							</select>
						</div>
					</div>

					<div class="form-row">
						<div class="form-group col-md-2 text-center">
							<label for="producto">PRODUCTO</label>
							<input type="text"  onKeyUp="this.value = this.value.toUpperCase();" class="form-control text-center" name="producto" required="">
						</div>

						<div class="form-group col-md-2 text-center">
							<label for="cantidad">CANTIDAD</label>
							<input type="number" min="0" onKeyUp="this.value = this.value.toUpperCase();" class="form-control text-center" name="cantidad" required="" step="0.01">
						</div>

						<div class="form-group col-md-2 text-center" style="display:none;">
							<label for="precio">PRECIO</label>
							<input onKeyUp="this.value = this.value.toUpperCase();" type="number" min="0" class="form-control text-center" name="precio" required="" step="0.01">
						</div>

						<div class="form-group col-md-2 text-center" style="display:none;">
							<label for="importe2">IMPORTE</label>
							<input  type="number" min="0" class="form-control text-center" name="importe2" required="">
						</div>

						<?php 
							require("../php/conexion.php");
								$consulta="SELECT idhuerta,clave, nombre FROM huerta ORDER BY clave ASC;"; 				
								$ress=mysqli_query($mysqli,$consulta);
								$huertas="";
								while ($arreglo=mysqli_fetch_array($ress))
								{
									$huertas=$huertas."<option value=\"".$arreglo['idhuerta']."\">".$arreglo['nombre']." </option>";
								}
						?>

						<div class="form-group col-md-2 text-center">
							<label for="idhuerta" class="text-center "> HUERTA </label>
							<select  name="idhuerta[]" class="form-control browser-default"  required>
								<?php echo $huertas; ?>
							</select>
						</div>
						
						<div class=" form-group col-sm-1 text-center">
							<p></p>
							<button type="submit" class="btn btn-primary" name="submit" value="submit"><i class="fa fa-save"></i></button>
						</div>

						<div class="form-group col-md-1 p-3" id="agregar_campo" style='display:none;'>
                            <div id="agrego"><button type="button" class="btn btn-success" id="add" onClick="addCancion()" name="submit" value="submit"><i class="fa fa-plus"></i></button></div>                            
                        </div>
                        <div class="form-group col-md-1 p-3" id="remover_campo" style='display:none;'>
                            <div id = "elimino"><button type="button" id="quit" class="btn btn-danger" name="submit" value="submit"><i class="fa fa-minus"></i></button></div>
                        </div>
					</div>
					<div class="form-row" id="cuadrilla"></div>
					<div class="form-group" id="canciones"></div>
				</form>
		</div>
	</div> <!-- TERMONACION DEL ROW-->
	<hr>
	<!--https://www.youtube.com/watch?v=trdKCyQ2bdc-->

		<div class="row">
			<div class="col-md-3 col-sm-3 text-center">
				<a style="text-decoration: none;" id="btnSlide" class="btn-floating  btn-comm">
					<i class="fas fa-user-tag"></i>
				</a>
			</div>
			
			<div class="col-md-3 col-sm-3 text-center">
				<a href="#" style="text-decoration: none;" id="btnSlide2" class="btn-floating  btn-comm">
					<i class="fas fa-chevron-right"></i>
				</a>
			</div>
			
			<div class="col-md-2 col-sm-2 text-center">
				<a  href="javascript:reportePDF2();" class="btn-floating btn-comm"><i class="fas fa-file-import"></i></a>
			</div>

			<div class="col-md-2 col-sm-2 text-center">
				<a href="javascript:reportePDF3();" class="btn-floating btn-comm"><i class="far fa-file-pdf"></i></a>
			</div>
			
		</div>

		<div class="row">

			<div class="col-md-2 text-center p-3">
				<input class="form-control text-center " type="date" id="bd-desde" />
			</div>

			<div class="col-md-1 text-center p-3">
				<h6 class="pt-3 ">HASTA</h6>
			</div>

			<div class="col-md-2 text-center p-3">
				<input class="form-control text-center " type="date" id="bd-hasta" />
			</div>

			<div class="col-md-3 text-center p-3">
				<input onKeyUp="this.value = this.value.toUpperCase();" class="form-control text-center " type="text" placeholder="Busca persona o actividad" id="bs-prod"/>
			</div>

			<div class="col-md-2 text-center p-3">
				<input onKeyUp="this.value = this.value.toUpperCase();" class="form-control text-center " type="text" placeholder="Busca huerta" id="bs-huerta"/>
			</div>

			<div class="col-md-1 col-sm-1 text-center">
				<a type="button" class="btn-floating btn-comm" onclick="buscadorcito2();">
					<i class="fas fa-search"></i>
				</a>
			</div>
		</div> <br>

		<!-- https://www.youtube.com/watch?v=3idtCrrS_mQ
			https://es.stackoverflow.com/questions/79305/como-insertar-varios-datos-al-mismo-tiempo-en-mysql-y-php
		-->
		<div class="row">
            <div class="registros table table-responsive " id="agrega-registros3"></div>
		</div>
	</div> <!--DIV CONTAINER-->

	<center>
		<nav aria-label="..."><ul class="pagination justify-content-center" id="pagination3"></ul></nav>
	</center>
</main>

<script src="../js/busqueda_actividades_generales.js"></script>
<script src="../js/jquery-ui.js"></script> 
<script src="../js/funcion_combo.js"></script>
<script "text/javascript" src="../js/actividad_general.js"></script>

<script type="text/javascript">
	$(function() {
		$("#nombre").autocomplete({
			source: "../php/autocompletadores/autocomplete.php",
			minLength: 0,
			select: function(event, ui) {
				event.preventDefault();
				$('#nombre').val(ui.item.nombre);
				$('#cargo').val(ui.item.cargo);
				$('#salario').val(ui.item.salario);
				$('#idpersona').val(ui.item.idpersona);
			}
        });
    });
</script>

<script type="text/javascript">
	$(function() {
        $("#segmento").autocomplete({
            source: "../php/autocompletadores/autocomplete2.php",
            minLength: 0,
            select: function(event, ui) {
				event.preventDefault();
				$('#segmento').val(ui.item.segmento);
				$('#idsegmento').val(ui.item.idsegmento);
			}
        });
    });
</script>

<!-- ESCRIPT PARA OCULTAR EL FORMULARIO DE AGREGAR-->

<script>
	var toogle= true;
	$("#btnSlide").click(function()
	{
		if (!toogle)
		{
			toogle = true;
			$("#animacion").slideUp("slow");
			$("#btnSlide").html('<i class="fas fa-user-tag"></i>')
		}
		else
		{
			toogle = false;
			$("#animacion").slideDown("slow");
			$("#btnSlide").html('<i class="fas fa-minus"></i>')
		}
	})
</script>

<script>
	var toogle= false;
	$("#btnSlide2").click(function()
	{
		if (!toogle)
		{
			toogle = true;
			$(".oculto").slideUp("slow");
			$("#btnSlide2").html('<i class="fas fa-chevron-right"></i>')
		}
		else
		{
			toogle = false;
			$(".oculto").slideDown("slow");
			$("#btnSlide2").html('<i class="fas fa-chevron-left"></i>')
		}
	})
</script>