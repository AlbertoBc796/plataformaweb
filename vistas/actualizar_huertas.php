<?php include('include.php'); 
    require("../php/conexion.php");          
            $id = $_GET['id'];
            $sql = "SELECT idhuerta,fecha, clave, nombre, zona, hectareas, producto FROM huerta WHERE idhuerta = '$id'";
            $resultado = $mysqli->query($sql);
            $row = $resultado->fetch_array(MYSQLI_ASSOC); ?> 
<head>
	<title>ACTUALIZAR H</title>
</head>

<main>		
    <div class="container-fluid pt-2" >
        <div class="col-md-12 pt-3" >
		<form action="../php/actualizaciones/actualizar_huerta.php" method="post" name="formulario" >
            <div class="form-row">
                <div class="form-group col-md-2 text-center">
                    <input class="form-control text-center" type="hidden" name="id" value= "<?php echo $row['idhuerta'] ?>" readonly="readonly" >
                    <label for="" class="control-label ">FECHA</label>
                    <input class="form-control text-center" type="text" name="fecha" placeholder=""  value="<?php echo $row['fecha']?>">
                </div>
                <div class="form-group col-md-2 text-center">
                    <label for="" class="control-label ">CLAVE</label>
                    <input class="form-control text-center" type="text"  name="clave" placeholder="" name="clave" value="<?php echo $row['clave']?>">
                </div>

                <div class="form-group col-md-4 text-center">
                    <label for="" class="control-label">NOMBRE</label>
                    <input class="form-control text-center" type="text" name="nombre" placeholder="" on value="<?php echo $row['nombre']?>">
                </div>

                <div class="form-group col-md-2 text-center">
                    <label for="" class="control-label">ZONA</label>
                    <input class="form-control text-center" type="text" name="zona" placeholder="" on value="<?php echo $row['zona']?>">
                </div>

                <div class="form-group col-md-2 text-center">
                    <label for="" class="control-label ">HECTAREAS</label>
                        <input class="form-control text-center" type="text" name="hectareas" placeholder="" on value="<?php echo $row['hectareas']?>">
                </div>
                
            </div>

            <div class="form-row">
                <div class="form-group col-md-12">
                    <label for="intereses" class="control-label">PRODUCTOS:</label>
                    <div class="form-check form-check-inline col-ms-2 col-md-1">
                        <input type="checkbox"  class="form-check-input"  name="producto[]" id="materialInline1" value="NARANJA" <?php if(strpos($row['producto'], "NARANJA")!== false) echo 'checked'; ?>> 
                        <label class="form-check-label" for="materialInline1"> Naranja </label> 
                    </div>

                    <div class="form-check form-check-inline col-ms-2 col-md-1">
                        <input type="checkbox" class="form-check-input"  name="producto[]" id="materialInline2" value="LIMON" <?php if(strpos($row['producto'], "LIMON")!== false) echo 'checked'; ?>> 
                        <label class="form-check-label" for="materialInline2"> Limon </label>
                    </div>    

                    <div class="form-check form-check-inline col-ms-2  col-md-1">
                        <input type="checkbox" class="form-check-input"  name="producto[]" id="materialInline3" value="TANGERINA" <?php if(strpos($row['producto'], "TANGERINA")!== false) echo 'checked'; ?>> 
                        <label class="form-check-label" for="materialInline3"> Tangerina  </label>
                    </div>

                    <div class="form-check form-check-inline col-ms-2  col-md-1">
                        <input type="checkbox" class="form-check-input"  name="producto[]" id="materialInline4" value="FREMON" <?php if(strpos($row['producto'], "FREMON")!== false) echo 'checked'; ?>> 
                        <label class="form-check-label" for="materialInline4"> Fremon </label>
                    </div>

                    <div class="form-check form-check-inline col-ms-2 col-md-1">
                        <input type="checkbox" class="form-check-input" name="producto[]" id="materialInline5" value="TORONJA" <?php if(strpos($row['producto'], "TORONJA")!== false) echo 'checked'; ?>> 
                        <label class="form-check-label" for="materialInline5"> Toronja </label>
                    </div>

                    <div class="form-check form-check-inline col-ms-2 col-md-1">
                        <input type="checkbox" class="form-check-input" name="producto[]" id="materialInline6" value="NO APLICA" <?php if(strpos($row['producto'], "NO APLICA")!== false) echo 'checked'; ?>>
                        <label class="form-check-label" for="materialInline6">   No aplica</label>
                    </div>

                    <div class="form-check form-check-inline col-ms-2 col-md-1">
                        <input type="checkbox" class="form-check-input"  name="producto[]" id="materialInline7" value="GTOS. DE OPERACION" <?php if(strpos($row['producto'], "GTOS. DE OPERACION")!== false) echo 'checked'; ?>> 
                        <label class="form-check-label" for="materialInline7"> Gtos. de operacion </label>                            
                    </div>                    
                </div>
            </div>

            <div class="form-row text-center">
                <div class="form-group col-md-2 col-ms-2 text-center">
                    <input align="" type="submit" value="Guardar" class="text-center btn btn-success btn-primary">    
                </div>                
            </div>
        </form>
        </div>
    </div> <!-- TERMINA CONTAINER FLUID-->
</main> <!-- TERMINACION DEL MAIN-->
