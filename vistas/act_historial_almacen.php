<?php include('include.php');
  extract($_GET);
  require("../php/conexion.php");

  $sql="SELECT h.idhistorial, date_format(h.fecha,'%d/%m/%Y') as fecha, s.articulo,
  h.cantidad, h.idoperacion, h.viaje, h.lugar, o.tipo
  from historial h
  inner join stock s on s.idstock = h.idstock
  inner join operacion o on o.idoperacion = h.idoperacion
  WHERE idhistorial=$id";
  
  $ressql=mysqli_query($mysqli,$sql);
  while ($fila=mysqli_fetch_row ($ressql)){
    $id=$fila[0];
    $fecha=$fila[1];
    $articulo=$fila[2];
    $cantidad = $fila[3];
    $unidad = $fila[5];
    $lugar = $fila[6];
    $operacion = $fila[7];
  }
?> 
<head>
	<title>Historial</title>
</head>
	
<main>
  <form action="../php/actualizaciones/actualizar_historial_almacen.php"  method="POST">
    <div class="row text-center" >
      <div class="form-group col-sm-2 col-md-2">
        <label for="fecha">FECHA</label>
        <input type="text" class="form-control  text-center" type="text"  name="fecha" readonly="" value="<?php echo $fecha;?>">
      </div>

      <div class="form-group col-sm-4 col-md-4 ">
        <input class="form-control  " type="hidden" name="id" value= "<?php echo $id;?>"  >
        <label for="">NOMBRE</label>
        <input class="form-control  text-center" type="text"  name="articulo" readonly="" value='<?php echo $articulo;?>'>
      </div>

      <div class="form-group col-sm-3 col-md-3 ">
        <label for="">OPERACION</label>
        <input class="form-control  text-center" type="text"  name="operacion" readonly="" value='<?php echo $operacion;?>'>
      </div>

      <div class="form-group col-md-3 ">
        <label for="">CANTIDAD</label>
        <input class="form-control  text-center" type="number"  name="cantidad" readonly="" value='<?php echo $cantidad;?>'>
      </div>
    </div>
    <div class="row text-center" >
      <div class="form-group col-md-3 ">
        <label for="">UNIDAD</label>
        <input class="form-control  text-center" id='camion' type="text"  name="unidad" value='<?php echo $unidad;?>'>
      </div>
      <div class="form-group col-md-3 ">
        <label for="">LUGAR</label>
        <input class="form-control  text-center" type="text" id='buscador' name="lugar" value='<?php echo $lugar;?>'>
      </div>
      <div class="form-group col-md-3 text-center">
        <label for="producto">PRODUCTO</label>
        <select name="producto" id="producto" class="form-control browser-default"  required onchange="JavaScript:showMsg()" onchange="JavaScript:yesnoCheck()">
          <option value="0">Seleccione</option>
        </select>
      </div>
      <div class="form-group col-sm-2 col-md-2 pt-3 text-center">
        <input align="" type="submit" value="Guardar" class="btn btn-success btn-primary ">
      </div>
    </div>
  </form>
</main>

<script src="../js/jquery-ui.js"></script> 
<script type="text/javascript">
  $(function() {
    $("#buscador").autocomplete({
      source: "../php/autocompletadores/autocomplete_almacen_destino.php",
      minLength: 1,
      select: function(event, ui) {
        event.preventDefault();
        $('#buscador').val(ui.item.destino);       
        $('#iddestino').val(ui.item.iddestino);
      }
    });
  });
</script>

<script type="text/javascript">
	$(function() {
    $("#camion").autocomplete({
      source: "../php/autocompletadores/transporte.php",
      minLength: 1,
      select: function(event, ui) {
        event.preventDefault();
        $('#camion').val(ui.item.clave);
        $('#marca').val(ui.item.marca);;
        $('#idtransporte').val(ui.item.idtransporte);
      }
    });
  });
</script>