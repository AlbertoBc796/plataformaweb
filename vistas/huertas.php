
<?php include('include.php'); ?> 
<title>Registro huertas</title>
<main>
	<div class="container-fluid" >
			<div class="row" id="animacion" >

				<div class="col-md-12 col-sm-12">
					
				<form name="miformulario" action="../php/inserciones/insertar_huerta.php" id="" method="post">

					<div class="form-row" >
						<div class="form-group col-md-2 col-sm-2 text-center">
							<label id="label" for="">FECHA</label>
							<input type="date"   class="form-control text-center" name="fecha"  required >
						</div>

						<div class="form-group col-md-2 col-sm-2 text-center">
							<label id="label" for="">CLAVE</label>
							<input type="text" onKeyUp="this.value = this.value.toUpperCase();" class="form-control " name="clave"  required>
						</div>

						<div class="form-group col-md-3 col-sm-2 text-center">
							<label id="label" for="">HUERTA</label>
							<input type="text" class="form-control text-uppercase" name="nombre"  onKeyUp="this.value = this.value.toUpperCase();" required >
						</div>

						<div class="form-group col-md-3 col-sm-2 text-center">
							<label id="label" for="">ZONA</label>
							<input onkeypress="return soloLetras(event);"  onKeyUp="this.value = this.value.toUpperCase();" type="text" class="form-control text-uppercase" name="zona"  required>
						</div>

						<div class="form-group col-md-2 col-sm-2 text-center">
							<label  id="label" for="">HECTAREAS</label>
							<input onkeypress="return filterFloat(event,this);" type="text" class="form-control"  name="hectareas"  required>
						</div>
					</div>

					<div class="form-row pt-3">
						<div class="col-md-12">	
						<div class="form-check form-check-inline">
							<label class="form-check-label " for="producto">PRODUCTOS:</label>
						</div>
						
						<div class="form-check form-check-inline col-ms-2 col-md-1 ">
								<input type="checkbox" class="form-check-input" id="materialInline1"  name="producto[]" value="NARANJA">
								<label class="form-check-label" for="materialInline1">Naranja</label>
						</div>

						<div class="form-check form-check-inline col-ms-2 col-md-1 ">
								<input type="checkbox" class="form-check-input" id="materialInline2" name="producto[]" value="LIMON">
									<label class="form-check-label" for="materialInline2">Limon</label>
						</div>

						<div class="form-check form-check-inline col-ms-2 col-md-1">
								<input type="checkbox" class="form-check-input" id="materialInline3" name="producto[]" value="TANGERINA">
									<label class="form-check-label" for="materialInline3">Tangerina</label>
						</div>
						
						<div class="form-check form-check-inline col-ms-2 col-md-1">
								<input type="checkbox" class="form-check-input" id="materialInline4" name="producto[]" value="FREMON">
									<label class="form-check-label" for="materialInline4">Fremon</label>
						</div>

						<div class="form-check form-check-inline col-ms-2 col-md-1">
								<input type="checkbox" class="form-check-input" id="materialInline5" name="producto[]" value="TORONJA" >
									<label class="form-check-label" for="materialInline5">Toronja</label>
						</div>
			
						<div class="form-check form-check-inline col-ms-2 col-md-1">
								<input type="checkbox" class="form-check-input" id="materialInline6" name="producto[]" value="NO APLICA" >
									<label class="form-check-label" for="materialInline6">No Aplica</label>
						</div>

						<div class="form-check form-check-inline col-ms-2 col-md-1">
								<input type="checkbox" class="form-check-input " id="materialInline7" name="producto[]" value="GTOS DE OPERACION" >
									<label class="form-check-label" for="materialInline7">Gtos. de Operacion</label>
						</div>
						</div>
					</div><br>

					<div class="form-row">
						<div class="form-group col-md-1 col-sm-1 text-center">
							<button type="submit" class="btn btn-primary" name="submit" value="submit"><i class="fa fa-save"></i></button>
						</div>
						
					</div>
				</form>
			</div>
		</div> <!-- TERMINA EL ROW--> 
		<hr>

		<div class="row"> 
			<div class="col-sm-4 col-md-4 text-center">
				<a id="btnSlide" class="btn-floating  btn-comm ">
				<i class="far fa-lemon"></i> </a>
			</div>

			<div class="col-sm-4 col-md-4 pt-3 text-center">
				<input  class="form-control text-center"  onKeyUp="this.value = this.value.toUpperCase();" type="text" placeholder="Busqueda...." id="bs-prod"/>
			</div>

			<div class="col-sm-4 col-md-4 text-center">
				<a  href="../php/impresiones/huertas.php" class="btn-floating btn-comm "><i class="fas fa-file-import"></i></a>
			</div>
		</div><br>

		<div class="row">
			<div class="col-md-12 col-sm-12">
				<div class="registros table table-responsive-sm" id="agrega-registroshuerta"></div>
			</div> 
		</div>
	</div> 
	
	<center>
        <nav aria-label="..."><ul class="pagination justify-content-center" id="paginationhuerta"></ul></nav>
    </center>
</main>

<!--ESCRIPT PARA PAGINACION Y BUSQUEDAS-->
<script  src="../js/busqueda_huerta.js"></script>
<!-- ESCRIPT PARA OCULTAR EL FORMULARIO DE AGREGAR-->
<script>
	var toogle= false;
	$("#btnSlide").click(function()
	{
		if (!toogle)
		{
			toogle = true;
			$("#animacion").slideUp("slow");
			$("#btnSlide").html('<i class="far fa-lemon"></i>')
		}
		else
		{
			toogle = false;
			$("#animacion").slideDown("slow");
			$("#btnSlide").html('<i class="fas fa-minus"></i>')
		}
	})
</script>