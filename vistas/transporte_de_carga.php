<?php include('include.php'); ?>
<title>Transportes</title>
<main>
	<div class="container-fluid pt-3" >
		<div class="row">
			<div class="col-md-12 col-sm-12"  id='animacion'>
				<form action="../php/inserciones/insertar_transporte.php" id="" method="post">
					<div class="form-row">

						<div class="form-group col-md-3  col-sm-3 text-center">
							<label  for="clave">NOMBRE</label>
							<input  onKeyUp="this.value = this.value.toUpperCase();" id="nombre" type="text" class="form-control" name="nombre"  required >
						</div>

						<div class="form-group col-md-2 col-sm-5 text-center">
							<label for="descripcion">MARCA</label>
							<input onKeyUp="this.value = this.value.toUpperCase();" id="marca" type="text" class="form-control " name="marca"  required>
						</div>

						<div class="form-group col-md-2 col-sm-5 text-center">
							<label for="descripcion">COLOR</label>
							<input onKeyUp="this.value = this.value.toUpperCase();" id="color" type="text" class="form-control " name="color"  required>
						</div>

						<div class="form-group col-md-2 col-sm-5 text-center">
							<label for="descripcion">PLACAS</label>
							<input onKeyUp="this.value = this.value.toUpperCase();" id="placas" type="text" class="form-control " name="placas"  required pattern="^[A-Z]{1,2}\d{5,7}$">
						</div>
						
						<!--ESTA VALIDACION SIRVE PARA LAS PLACAS 
						[A-Z]{1,2}\d{5,7}$  SE  LEE DE LA SIGUEINTE MANERA ACEPTA 1 O 2 LETRAS DE LA A A LA Z SEGUIDO DE 5 A 7 DIGITOS
						-->

						<div class="form-group col-md-3 col-sm-3 text-center pt-3">
							<button type="submit" class="btn btn-primary" name="submit" value="submit"><i class="fa fa-save"></i></button>
						</div>

					</div>
				</form>
			</div>
		</div>
		<hr>
		<div class="row">
			<div class="col-sm-4  text-center">
				<a id="btnSlide" class="btn-floating  btn-comm ">
				<i class="fas fa-truck"></i> </a>
			</div>
			<div class="col-sm-4 pt-3 text-center">
				<input  class="form-control text-center" onKeyUp="this.value = this.value.toUpperCase();" type="text" placeholder="Transporte de carga...." id="bs-prod"/>
			</div>
			<div class="col-sm-4 text-center">
				<a  href="../php/impresiones/.php" class="btn-floating btn-comm ">
				<i class="fas fa-file-import"></i></a>
			</div>			
		</div> <br>

		
		<div class="row">
			<div class="col-md-12">
				<div class="registros table table-responsive-sm" id="agrega-registrosherramienta"></div>
			</div> 
		</div>
	</div> <!-- TERMINA EL FLUID -->

	<!-- ======= PAGINACION ========-->
	<center>
		<nav aria-label="..."><ul class="pagination justify-content-center" id="pagination7"></ul></nav>
	</center>
	<!-- ====== TERMINA PAGINACION ====-->
</main>

<!--ESCRIPT PARA PAGINACION Y BUSQUEDAS-->
<script  src="../js/busca_transporte.js"></script>


<!-- ESCRIPT PARA OCULTAR EL FORMULARIO DE AGREGAR-->

<script>
	var toogle= false;
	$("#btnSlide").click(function()
	{
		if (!toogle)
		{
			toogle = true;
			$("#animacion").slideUp("slow");
			$("#btnSlide").html('<i class="fas fa-truck"></i>')
		}
		else
		{
			toogle = false;
			$("#animacion").slideDown("slow");
			$("#btnSlide").html('<i class="fas fa-minus"></i>')
		}
	})
</script>