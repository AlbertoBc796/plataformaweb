<!DOCTYPE html>
<html lang="en">
<head>

    <title>COMPRA VENTA</title>

</head>

<body class="fixed-sn cyan-skin">
    
      <?php include('include.php'); ?> 
    
    
    <!--Main layout-->
    <main>

        <!-- Section: Blog v.1 -->
            <section class="my-5">

              <!-- Section heading -->
              <h2 class="h1-responsive font-weight-bold text-center my-5">QUIENES SOMOS</h2>
              <!-- Section description -->
              <p class="text-justify w-responsive mx-auto mb-5 ">Somos una empresa familiar con más de 25 años de experiencia, dedicada a las actividades agrícolas, inmobiliaria, dispersora de crédito, básculas y enceradoras. Estamos Comprometidos con la innovación y el desarrollo de nuevos servicios. 

            Grupo Murrieta se caracteriza por el trabajo, la honestidad y el compromiso como valores fundamentales, por lo tanto, nuestro reto radica en establecer relaciones al exterior creando un vínculo de confianza respaldado por nuestro equipo de trabajo y el cumplimiento ejemplar de todas nuestras actividades empresariales.</p>

              <!-- Grid row -->
              <div class="row">

                <!-- Grid column -->
                <div class="col-lg-5">

                  <!-- Featured image -->
                  <div class= "view overlay rounded z-depth-2 mb-lg-0 mb-4">
                    <img class="img-fluid" src="../img/8.jpg" alt="Sample image">
                    <a>
                      <div class="mask rgba-white-slight"></div>
                    </a>
                  </div>

                </div>
                <!-- Grid column -->

                <!-- Grid column -->
                <div class="col-lg-7">

                  <!-- Category --> 
                  <a href="#!" class="green-text"><h6 class="font-weight-bold mb-3"><i class="fa fa-lemon  pr-2"></i></h6> </a>
                  <!-- Post title -->
                  <h3 class="font-weight-bold mb-3"><strong>   AGRICOLA MEN</strong></h3>
                  <!-- Excerpt -->

             <p>Es una empresa 100% Veracruzana dedicada a la comercialización de frutas y hortalizas en el Mercado nacional, cuenta con más de 25 años de experiencia en la venta y distribución de naranja, limón persa, toronja, mandarina entre otros. 

            Actualmente Agrícola Men comercializa sus productos a nivel nacional, cuenta con altos estándares de calidad e inocuidad en todos sus productos.

            .</p>
                 
                  <!-- Post data -->

                </div>
                <!-- Grid column -->

              </div>
              <!-- Grid row -->

              <hr class="my-5">

              <!-- Grid row -->
              <div class="row">

                <!-- Grid column -->
                <div class="col-lg-7">

                  <!-- Category -->
                  <a href="#!" class="pink-text"><h6 class="font-weight-bold mb-3"><i class="fa fa-couch  pr-2"></i></h6></a>
                  <!-- Post title -->
                  <h3 class="font-weight-bold mb-3"><strong>INMOBILIARIA
                  </strong></h3>
                  <!-- Excerpt -->
          <p>Es una empresa Veracruzana comprometida a la participación de todos los sectores del mercado inmobiliario. Nuestro objetivo es alcanzar y mantener los más altos estándares de satisfacción al cliente a través de nuestros inmuebles y servicios innovadores.

            Buscamos aliarnos con las mejores inmobiliarias y constructoras ofreciendo valor agregado tanto para la empresa así como para nuestros clientes.

            Nuestro compromiso es acompañar al cliente en cada una de las etapas del proceso inmobiliario, tanto en la consignación como en la asignación del inmueble..</p>
                  <!-- Post data -->
                  

                </div>
                <!-- Grid column -->

                <!-- Grid column -->
                <div class="col-lg-5">

                  <!-- Featured image -->
                  <div class="view overlay rounded z-depth-2">
                    <img class="img-fluid" src="../img/9.jpg" alt="Sample image">
                    <a>
                      <div class="mask rgba-white-slight"></div>
                    </a>
                  </div>

                </div>
                <!-- Grid column -->

              </div>
              <!-- Grid row -->

              <hr class="my-5">

              

            </section>
            <!-- Section: Blog v.1 --> 

		
    </main>
    <!--/Main layout-->

</body>

</html>