<?php
    session_start();
    if(isset($_SESSION['user'])){
        header("Location: ./vistas/compra_venta.php");
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" charset="UTF-8">
	<link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/mdb.min.css">
    <link rel="stylesheet" href="css/font-awesome/css/all.css">
	<title>Agricola MEN</title>
</head>

<body style="background-image: url(img/az2.jpg);  background-repeat: no-repeat; background-position: center; background-attachment: fixed; background-size: cover; opacity: 0.9">
	<div class="container-fluid"  >
	
		<div class="row">
			<div class="col-md-4 col-sm-4" ></div>

			<div class="col-md-4 col-sm-4" style=" background: white; opacity:1; margin-top: 5%; border-radius: 5px; border: 1px solid">

				<img src="img/logo.jpg" class="img-fluid  mx-auto d-block" alt="" width="30%">
				<hr style="background: green;">
				<!--=============================INICIO DEL FORMULARIO DE SESIONES==============================-->
				<form action="php/sesiones/validar_usuario.php" method="POST" name ="login"> 
					<div class="form-row pt-3">
						<div class="form-group col-md-12 text-center" style=" font-family: sans-serif; font-size: 10px; font-style: italic;">
							<u class="h3"><strong>INICIAR SESION</strong></u>
						</div>
					</div>
					<section class="text-center ">
					<?php
						require("./php/sesiones/error_handle.php");
						if (isset($_GET['error'])) {
							$mensaje = errorCase($_GET['error']);
							echo "<span style='display: block; background-color: red; color:white'>".$mensaje."</span>";
						}
					?>
					<section class="left">

					<div class="form-row text-center ">
						<div class=" form-group col-md-12">
							<input type="text" name="user" class="form-control text-center" placeholder="Usuario" required=""  onKeyUp="this.value = this.value.toUpperCase();">
							<input type="password" name="password" class="form-control mt-3 text-center" placeholder="Contraseña" required="">
						</div>
					</div>

					<div class="form-row">
						<div class="col-md-12  text-center">
							<button type="submit" name="login-sbmt" class="btn btn-warning btn-block" style="font-size: 19px;">Iniciar</button>
						</div>
					</div>

					<hr style="background: green;">
					<div class="form-row text-center">
						<div class=" form-group col-md-12">
							<p>Aun no eres usuario (a). <a href="vistas/registrar_usuario.php">Registrate aqui</a>	</p>
						</div>	
					</div>
				</form>
				<section>
			</div>
		</div>
	</div>
	<script src="js/jquery-1.12.1.min.js"></script> 
    <script src="js/popper.min.js"></script>
    <script  src="js/bootstrap.min.js"></script>
    <script  src="js/mdb.min.js"></script>
</body>
</html>